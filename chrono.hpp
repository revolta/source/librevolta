#pragma once

#include <ratio.hpp>
#include <limits.hpp>
#include <type_traits.hpp>
#include <cstdint.hpp>
#include <ctime.hpp>

namespace ReVolta {

    namespace chrono {

        template<typename REP, typename PERIOD = ratio<1>>
        class duration;

        /* Trait to help determine if a duration can be converted to another duration with different tick period */
        template<typename REP>
        struct treat_as_floating_point : is_floating_point<REP> {};

        template<typename REP>
        inline constexpr bool treat_as_floating_point_v { treat_as_floating_point<REP>::value };

        namespace Detail {

            template<typename T>
            struct is_duration : false_type {};

            template<typename REP, typename PERIOD>
            struct is_duration<duration<REP, PERIOD>> : true_type {};

            template<typename D>
            inline constexpr bool is_duration_v { is_duration<D>::value };

        }

        template<typename D>
        concept Duration = Detail::is_duration_v<D>;

        template<typename REP>
        struct duration_values {
            static constexpr REP zero( void ) noexcept {
                return REP{ 0 };
            }

            static constexpr REP min( void ) noexcept {
                return numeric_limits<REP>::lowest();
            }

            static constexpr REP max( void ) noexcept {
                return numeric_limits<REP>::max();
            }
        };

        /* Class template chrono::duration represents a time interval. It consists of a count of ticks of type REP and a tick period,
        * where the tick period is a compile time rational fraction representing the time is seconds from one tick to the next.
        * The only data stored in a duration is a tick count fo type REP. If REP is a floating point, then the duration can represent
        * fractions of ticks.
        * PERIOD is included as part of duration's type and is only used when converting between different durations. */
        template<typename REP, typename PERIOD>
        class duration {
        public:
            using rep = REP;
            using period = typename PERIOD::type;

            /* REP cannot be a duration by itself */
            static_assert( !Detail::is_duration<REP>::value );

            /* PERIOD must be positive, non zero */
            static_assert( ::ReVolta::Detail::is_ratio<PERIOD>::value && ( PERIOD::num > 0 ) );

        private:
            rep m_ticks;

        public:
            constexpr duration( void ) = default;

            duration( const duration & ) = default;

            template<typename REP2>
                requires (
                    /* Implicitly convertible to REP (the type of duration's tick) */
                    ( is_convertible_v<const REP2 &, REP> ) &&
                    /* A duration with an integer tick count cannot be constructed from a floating point value, 
                    * but a duration with a floating point tick count can be constructed from an integral value */
                    ( treat_as_floating_point_v<REP> == true || treat_as_floating_point_v<REP2> == false )
                )
            constexpr explicit duration( const REP2 & ticks ) : m_ticks( static_cast<REP>( ticks ) ) {}

            template<typename REP2, typename PERIOD2>
                requires (
                    /* The conversion factor does not overflow */
                    /* TODO: 'ratio_divide<PERIOD2, PERIOD>' internally checks for overflows (use instead of is_convertible_v<>) */
                    ( is_convertible_v<const REP2 &, REP>) && (
                        /* The duration uses floating point ticks */
                        ( treat_as_floating_point_v<REP> == true ) ||   /* OR */
                        /* PERIOD2 is exactly divisible by PERIOD (is harmonic, division denominator equals 1) AND REP2 cannot be treated as flaoting point */
                        ( ( ratio_divide<PERIOD2, PERIOD>::den == 1 ) && ( treat_as_floating_point_v<REP2> == false ) )
                    )
                )
            constexpr duration( const duration<REP2, PERIOD2> & other ) : m_ticks( duration_cast<duration>( other ).count() ) {}

            ~duration( void ) = default;

            duration & operator=( const duration & other ) = default;

            /* Number of ticks for this duration */
            constexpr rep count( void ) const {
                return this->m_ticks;
            }

            /* Zero length duration */
            static constexpr duration zero( void ) noexcept {
                return duration{ duration_values<rep>::zero() };
            }

            /* Duration with the lowest possible value */
            static constexpr duration min( void ) noexcept {
                return duration{ duration_values<rep>::min() };
            }

            /* Duration with the largest possible value */
            static constexpr duration max( void ) noexcept {
                return duration{ duration_values<rep>::max() };
            }

            /* Unary plus. Returns a copy of this duration object */
            constexpr duration operator+( void ) const {
                return duration{ *this };
            }

            /* Unary minus. Returns a copy of this duration object with the number of ticks negated. */
            constexpr duration operator-( void ) const {
                return duration{ -this->m_ticks };
            }

            constexpr duration & operator++( void ) {
                ++this->m_ticks;
                return *this;
            }

            constexpr duration operator++( int ) {
                return duration{ this->m_ticks++ };
            }

            constexpr duration & operator--( void ) {
                --this->m_ticks;
                return *this;
            }

            constexpr duration operator--( int ) {
                return duration{ this->m_ticks-- };
            }

            constexpr duration & operator+=( const duration & other ) {
                this->m_ticks += other.count();
                return *this;
            }

            constexpr duration & operator-=( const duration & other ) {
                this->m_ticks -= other.count();
                return *this;
            }

            constexpr duration & operator*=( const duration & other ) {
                this->m_ticks *= other.count();
                return *this;
            }

            constexpr duration & operator/=( const duration & other ) {
                this->m_ticks /= other.count();
                return *this;
            }

            template<typename REP2 = REP> requires ( !treat_as_floating_point<REP2>::value )
            constexpr duration & operator%=( const REP & rhs ) {
                this->m_ticks %= rhs;
                return *this;
            }

            template<typename REP2 = REP> requires ( !treat_as_floating_point<REP2>::value )
            constexpr duration & operator%=( const duration & d ) {
                this->m_ticks %= d.count();
                return *this;
            }

        };

        template<typename REP1, typename PERIOD1, typename REP2, typename PERIOD2>
        constexpr common_type_t<duration<REP1, PERIOD1>, duration<REP2, PERIOD2>> operator+( const duration<REP1, PERIOD1> & lhs, const duration<REP2, PERIOD2> & rhs ) {
            using cd = common_type_t<duration<REP1, PERIOD1>, duration<REP2, PERIOD2>>;
            return cd{ cd{ lhs }.count() + cd{ rhs }.count() };
        }

        template<typename REP1, typename PERIOD1, typename REP2, typename PERIOD2>
        constexpr common_type_t<duration<REP1, PERIOD1>, duration<REP2, PERIOD2>> operator-( const duration<REP1, PERIOD1> & lhs, const duration<REP2, PERIOD2> & rhs ) {
            using cd = common_type_t<duration<REP1, PERIOD1>, duration<REP2, PERIOD2>>;
            return cd{ cd{ lhs }.count() - cd{ rhs }.count() };
        }

        template<typename REP1, typename PERIOD, typename REP2>
        constexpr duration<common_type_t<REP1, REP2>, PERIOD> operator*( const duration<REP1, PERIOD> & d, const REP2 & s ) {
            using cd = duration<common_type_t<REP1, REP2>, PERIOD>;
            return cd{ cd{ d }.count() * s };
        }

        template<typename REP1, typename PERIOD, typename REP2>
        constexpr duration<common_type_t<REP1, REP2>, PERIOD> operator*( const REP1 & s, const duration<REP2, PERIOD> & d ) {
            using cd = duration<common_type_t<REP1, REP2>, PERIOD>;
            return cd{ cd{ d }.count() * s };
        }

        template<typename REP1, typename PERIOD, typename REP2>
        constexpr duration<common_type_t<REP1, REP2>, PERIOD> operator/( const duration<REP1, PERIOD> & d, const REP2 & s ) {
            using cd = duration<common_type_t<REP1, REP2>, PERIOD>;
            return cd{ cd{ d }.count() / s };
        }

        template<typename REP1, typename PERIOD1, typename REP2, typename PERIOD2>
        constexpr common_type_t<REP1, REP2> operator/( const duration<REP1, PERIOD1> & lhs, const duration<REP2, PERIOD2> & rhs ) {
            using cd = common_type_t<duration<REP1, PERIOD1>, duration<REP2, PERIOD2>>;
            /* The result of this operation is not a duration */
            return cd{ lhs }.count() / cd{ rhs }.count();
        }

        template<typename REP1, typename PERIOD, typename REP2>
        constexpr duration<common_type_t<REP1, REP2>, PERIOD> operator%( const duration<REP1, PERIOD> & d, const REP2 & s ) {
            using cd = duration<common_type_t<REP1, REP2>, PERIOD>;
            return cd{ cd{ d }.count() % s };
        }

        template<typename REP1, typename PERIOD1, typename REP2, typename PERIOD2>
        constexpr common_type_t<duration<REP1, PERIOD1>, duration<REP2, PERIOD2>> operator%( const duration<REP1, PERIOD1> & lhs, const duration<REP2, PERIOD2> & rhs ) {
            using cd = common_type_t<duration<REP1, PERIOD1>, duration<REP2, PERIOD2>>;
            return cd{ cd{ lhs }.count() % cd{ rhs }.count() };
        }
       
        template<typename REP1, typename PERIOD1, typename REP2, typename PERIOD2>
        constexpr bool operator==( const duration<REP1, PERIOD1> & lhs, const duration<REP2, PERIOD2> & rhs ) {
            using ct = common_type_t<duration<REP1, PERIOD1>, duration<REP2, PERIOD2>>;
            return ct{ lhs }.count() == ct{ rhs }.count();
        }

        template<typename REP1, typename PERIOD1, typename REP2, typename PERIOD2>
        constexpr bool operator<( const duration<REP1, PERIOD1> & lhs, const duration<REP2, PERIOD2> & rhs ) {
            using ct = common_type_t<duration<REP1, PERIOD1>, duration<REP2, PERIOD2>>;
            return ct{ lhs }.count() < ct{ rhs }.count();
        }
        
        template<typename REP1, typename PERIOD1, typename REP2, typename PERIOD2>
        constexpr bool operator<=( const duration<REP1, PERIOD1> & lhs, const duration<REP2, PERIOD2> & rhs ) {
            return !( rhs < lhs );
        }

        template<typename REP1, typename PERIOD1, typename REP2, typename PERIOD2>
        constexpr bool operator>( const duration<REP1, PERIOD1> & lhs, const duration<REP2, PERIOD2> & rhs ) {
            return rhs < lhs;
        }

        template<typename REP1, typename PERIOD1, typename REP2, typename PERIOD2>
        constexpr bool operator>=( const duration<REP1, PERIOD1> & lhs, const duration<REP2, PERIOD2> & rhs ) {
            return !( lhs < rhs );
        }

        /* TODO: Enable Three way comparison */
#if false
        template<typename REP1, typename PERIOD1, typename REP2, typename PERIOD2>
            requires ( three_way_comparable<common_type_t<REP1, REP2>> )
        constexpr auto operator<=>( const duration<REP1, PERIOD1> & lhs, const duration<REP2, PERIOD2> & rhs ) {
            using ct = common_type<duration<REP1, PERIOD1>, duration<REP2, PERIOD2>>::type;
            return ct{ lhs }.count() <=> ct{ rhs }.count();
        }
#endif

        namespace Detail {

            /* Primary template. None of 'num' and 'den' is equal to one, calculation not optimized */
            template<typename D, typename PERIOD_RATIO, typename COMMON_REP, bool NUM_IS_ONE = false, bool DEN_IS_ONE = false>
            struct duration_cast_impl {
                template<typename REP, typename PERIOD>
                static constexpr D cast( const duration<REP, PERIOD> & other ) {
                    return D{ static_cast<typename D::rep>( static_cast<COMMON_REP>( other.count() ) * static_cast<COMMON_REP>( PERIOD_RATIO::num ) / static_cast<COMMON_REP>( PERIOD_RATIO::den ) ) };
                }
            };

            template<typename D, typename PERIOD_RATIO, typename COMMON_REP>
            struct duration_cast_impl<D, PERIOD_RATIO, COMMON_REP, true, true> {
                template<typename REP, typename PERIOD>
                static constexpr D cast( const duration<REP, PERIOD> & other ) {
                    return D{ static_cast<typename D::rep>( other.count() ) };
                }
            };

            template<typename D, typename PERIOD_RATIO, typename COMMON_REP>
            struct duration_cast_impl<D, PERIOD_RATIO, COMMON_REP, true, false> {
                template<typename REP, typename PERIOD>
                static constexpr D cast( const duration<REP, PERIOD> & other ) {
                    return D{ static_cast<typename D::rep>( static_cast<COMMON_REP>( other.count() ) / static_cast<COMMON_REP>( PERIOD_RATIO::den ) ) };
                }
            };

            template<typename D, typename PERIOD_RATIO, typename COMMON_REP>
            struct duration_cast_impl<D, PERIOD_RATIO, COMMON_REP, false, true> {
                template<typename REP, typename PERIOD>
                static constexpr D cast( const duration<REP, PERIOD> & other ) {
                    return D{ static_cast<typename D::rep>( static_cast<COMMON_REP>( other.count() ) * static_cast<COMMON_REP>( PERIOD_RATIO::num )  ) };
                }
            };

        }

        /* Converts 'chrono::duration' to a duration of different type 'D'. No implicit conversions are used. */
        template<Duration D, typename REP, typename PERIOD>
        constexpr D duration_cast( const duration<REP, PERIOD> & d ) {
            /* Period factor */
            using period_ratio = ratio_divide<PERIOD, typename D::period>;

            return Detail::duration_cast_impl<D, period_ratio, typename common_type<typename D::rep, REP, intmax_t>::type, ( period_ratio::num == 1 ), ( period_ratio::den == 1 )>::cast( d );
        }

        /* Returns the greatest duration representable in 'D' that is less or equal to 'd' */
        template<Duration D, typename REP, typename PERIOD>
        constexpr D floor( const duration<REP, PERIOD> & d ) {
            auto to = chrono::duration_cast<D>( d );
            return ( to > d ) ? ( to - D{ 1 } ) : to;
        }

        /* Returns the smallest duration representable in 'D' that is greater or equal to 'd' */
        template<Duration D, typename REP, typename PERIOD>
        constexpr D ceil( const duration<REP, PERIOD> & d ) {
            auto to = chrono::duration_cast<D>( d );
            return ( to < d ) ? ( to + D{ 1 } ) : to;
        }

        /* Returns the value representable in 'D' that is closest to 'd'. If there are two such values, returns the even value */
        template<Duration D, typename REP, typename PERIOD>
            requires( treat_as_floating_point_v<typename D::rep> == false )
        constexpr D round( const duration<REP, PERIOD> & d ) {
            D t0 { floor<D>( d ) };
            D t1 { t0 + D{ 1 } };
            auto diff0 { d - t0 };
            auto diff1 { t1 - d };
            if( diff0 == diff1 ) {
                return ( t0.count() & 1 ) ? t1 : t0;
            } else if( diff0 < diff1 ) {
                return t0;
            }
            return t1;
        }

        /* Returns the absolute value of the duration 'd' */
        template<typename REP, typename PERIOD>
            requires ( numeric_limits<REP>::is_signed )
        constexpr duration<REP, PERIOD> abs( duration<REP, PERIOD> d ) {
            return ( d >= d.zero() ) ? d : -d;
        }

        using nanoseconds   = duration<int64_t, nano>;
        using microseconds  = duration<int64_t, micro>;
        using miliseconds   = duration<int64_t, milli>;
        using seconds       = duration<int64_t>;
        using minutes       = duration<int32_t, ratio<60>>;
        using hours         = duration<int32_t, ratio<3600>>;
        using days          = duration<int32_t, ratio<86400>>;
        using weeks         = duration<int32_t, ratio<604800>>;
        using months        = duration<int32_t, ratio<2629746>>;
        using years         = duration<int32_t, ratio<31556952>>;

    }

    namespace Detail {

        template<typename COMMON_TYPE, typename PERIOD1, typename PERIOD2, typename = void>
        class duration_common_type {};

        template<typename COMMON_TYPE, typename PERIOD1, typename PERIOD2>
        class duration_common_type<COMMON_TYPE, PERIOD1, PERIOD2, void_t<typename COMMON_TYPE::type>> {
            using gcd_num = Detail::__static_gcd<PERIOD1::num, PERIOD2::num>;
            using gcd_den = Detail::__static_gcd<PERIOD1::den, PERIOD2::den>;
        public:
            using type = chrono::duration<typename COMMON_TYPE::type, typename ratio<gcd_num::value, ( PERIOD1::den / gcd_den::value ) * PERIOD2::den>::type>;
        };

    }

    /* Exposes the 'type' which is the common type of two chrono::durations whose period is the greatest common divisor
     * of PERIOD1 and PERIOD2 */
    template<typename REP1, typename PERIOD1, typename REP2, typename PERIOD2>
    struct common_type<chrono::duration<REP1, PERIOD1>, chrono::duration<REP2, PERIOD2>> : Detail::duration_common_type<common_type<REP1, REP2>, typename PERIOD1::type, typename PERIOD2::type> {};

    template<typename REP, typename PERIOD>
    struct common_type<chrono::duration<REP, PERIOD>, chrono::duration<REP, PERIOD>> {
        using type = chrono::duration<typename common_type<REP>::type, typename PERIOD::type>;
    };

    template<typename REP, typename PERIOD>
    struct common_type<chrono::duration<REP, PERIOD>> {
        using type = chrono::duration<typename common_type<REP>::type, typename PERIOD::type>;
    };

    inline namespace literals {

        inline namespace chrono_literals {

            /* Suppress compiler warning: literal operator suffixes not preceded by '_' are reserved for future standardization [-Wliteral-suffix] */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wliteral-suffix"

            constexpr chrono::hours operator""h( unsigned long long hrs ) {
                return chrono::hours{ hrs };
            }

            constexpr chrono::duration<long double, ratio<3600, 1>> operator""h( long double hrs ) {
                return chrono::duration<long double, ratio<3600, 1>>{ hrs };
            }

            constexpr chrono:: minutes operator""min( unsigned long long mins ) {
                return chrono::minutes{ mins };
            }

            constexpr chrono::duration<long double, ratio<60, 1>> operator""min( long double mins ) {
                return chrono::duration<long double, ratio<60, 1>>{ mins };
            }

            constexpr chrono::seconds operator""s( unsigned long long secs ) {
                return chrono::seconds{ secs };
            }

            constexpr chrono::duration<long double> operator""s( long double secs ) {
                return chrono::duration<long double>{ secs };
            }

            constexpr chrono::miliseconds operator""ms( unsigned long long milisecs ) {
                return chrono::miliseconds{ milisecs };
            }

            constexpr chrono::duration<long double, milli> operator""ms( long double milisecs ) {
                return chrono::duration<long double, milli>{ milisecs };
            }

            constexpr chrono::microseconds operator""us( unsigned long long usecs ) {
                return chrono::microseconds{ usecs };
            }

            constexpr chrono::duration<long double, micro> operator""us( long double usecs ) {
                return chrono::duration<long double, micro>{ usecs };
            }

            constexpr chrono::nanoseconds operator""ns( unsigned long long nanosecs ) {
                return chrono::nanoseconds{ nanosecs };
            }

            constexpr chrono::duration<long double, nano> operator""ns( long double nanosecs ) {
                return chrono::duration<long double, nano>{ nanosecs };
            }

#pragma GCC diagnostic pop

            /* Few checks */
            static_assert( 1h == 60min );
            static_assert( 1min == 60s );
            static_assert( 1ms == 1000us );

            static_assert( 59s < 1min );
            static_assert( 1h > 3599s );

        }

    }

    /* The Clock requirements describe the bundle consisting of 'chrono::duration' and 'chrono::time_point'
     * and a function 'now()' to get the current 'time_point'. The origin of the clock's 'time_point' is referred as the clock's epoch */        template<typename T>
    concept Clock = requires {
        /* The representation type of duration */
        typename T::rep;
        /* The tick period of the clock in seconds */
        typename T::period;
        /* The duration type of the clock */
        typename T::duration;
        /* chrono::time_point type of the epoch */
        typename T::time_point;
        /* Let 't1' and 't2' are values returned by 'now()'. 'true' if 't1 <= t2' is always true and the clock advances at a steady rate
         * relative to real time (so the difference between two distinct reported times is approximately the elapsed real time 
         * between the clock updates), otherwise 'false' */
        T::is_steady;
        /* Returns a 'time_point' object representing the current point in time */
        T::now();
    };

    namespace chrono {

        using namespace literals::chrono_literals;

        template<typename>
        struct is_clock : false_type {};

        template<Clock T>
        struct is_clock<T> : true_type {};

        template<typename T>
        inline constexpr bool is_clock_v = is_clock<T>::value;

        /* 'chrono::time_point' represents a point in time. It is implemented as if it stores a value of type 'DURATION' indicating
         * the time interval from the start of 'CLOCK''s epoch */
        template<typename CLOCK, Duration DURATION = typename CLOCK::duration>
#if false
            requires ( is_clock_v<CLOCK> )
#endif            
        class time_point {
        public:
            /* The clock on which this time point is measured */
            using clock = CLOCK;
            /* A 'chrono:;duration' type used to measure the time since epoch */
            using duration = DURATION;
            /* An arithmetic type representing the number of ticks of the duration */
            using rep = typename duration::rep;
            /* Period, the 'ratio<>' type representing the tick period of the duration */
            using period = typename duration::period;

        private:
            duration m_duration;

        public:
            /* Default constructor. Creates a time_point representing the 'clock''s epoch (i.e. time_since_epoch() is zero) */
            constexpr time_point( void ) : m_duration( duration::zero() ) {}

            /* Constructs a 'time_point' at 'clock''s epoch plus 'd' */
            constexpr explicit time_point( const duration & d ) : m_duration( d ) {}

            /* Constructs a 'time_point' by converting 't' to 'duration' */
            template<typename DURATION2>
            constexpr time_point( const time_point<clock, DURATION2> & t ) : m_duration( t.time_since_epoch() ) {}

            /* Returns a 'duration' representing the amount of time between '*this' and the clock's epoch */
            constexpr duration time_since_epoch( void ) const {
                return this->m_duration;
            }

            constexpr time_point & operator++( void ) {
                ++this->m_duration;
                return *this;
            }

            constexpr time_point operator++( int ) {
                return time_point{ this->m_duration++ };
            }

            constexpr time_point & operator--( void ) {
                --this->m_duration;
                return *this;
            }

            constexpr time_point operator--( int ) {
                return time_point{ this->m_duration-- };
            }

            constexpr time_point & operator+=( const duration & d ) {
                this->m_duration += d;
                return *this;
            }

            constexpr time_point & operator-=( const duration & d ) {
                this->m_duration -= d;
                return *this;
            }

            static constexpr time_point min( void ) {
                return time_point{ duration::min() };
            }

            static constexpr time_point max( void ) {
                return time_point{ duration::max() };
            }
        };

        /* TODO: Compare two time points */

        /* Converts a chrono::time_point from one duration to another */
        template<Duration TO_DURATION, typename CLOCK, typename D>
        constexpr time_point<CLOCK, TO_DURATION> time_point_cast( const time_point<CLOCK, D> & t ) {
            return time_point<CLOCK, TO_DURATION>{ duration_cast<TO_DURATION>( t.time_since_epoch() ) };
        }

        /* Returns the largest time point representable in TO_DURATION that is smaller or equal to the value given as argument */
        template<Duration TO_DURATION, typename CLOCK, typename DURATION>
        constexpr time_point<CLOCK, TO_DURATION> floor( const time_point<CLOCK, DURATION> & t ) {
            return time_point<CLOCK, TO_DURATION>{ floor<TO_DURATION>( t.time_since_epoch() ) };
        }

        template<Duration TO_DURATION, typename CLOCK, typename DURATION>
        constexpr time_point<CLOCK, TO_DURATION> ceil( const time_point<CLOCK, DURATION> & t ) {
            return time_point<CLOCK, TO_DURATION>{ ceil<TO_DURATION>( t.time_since_epoch() ) };
        }

        template<Duration TO_DURATION, typename CLOCK, typename DURATION>
        constexpr time_point<CLOCK, TO_DURATION> round( const time_point<CLOCK, DURATION> & t ) {
            return time_point<CLOCK, TO_DURATION>{ round<TO_DURATION>( t.time_since_epoch() ) };
        }

        template<typename CLOCK, typename DURATION, typename REP, typename PERIOD>
        constexpr time_point<CLOCK, common_type_t<DURATION, duration<REP, PERIOD>>> operator+( const time_point<CLOCK, DURATION> & p, const duration<REP, PERIOD> & d ) {
            return time_point<CLOCK, common_type_t<DURATION, duration<REP, PERIOD>>>{ p.time_since_epoch() + d };
        }

        template<typename CLOCK, typename DURATION, typename REP, typename PERIOD>
        constexpr time_point<CLOCK, common_type_t<duration<REP, PERIOD>, DURATION>> operator+( const duration<REP, PERIOD> & d, const time_point<CLOCK, DURATION> & p ) {
            return time_point<CLOCK, common_type_t<DURATION, duration<REP, PERIOD>>>{ p.time_since_epoch() + d };
        }

        template<typename CLOCK, typename DURATION, typename REP, typename PERIOD>
        constexpr time_point<CLOCK, common_type_t<DURATION, duration<REP, PERIOD>>> operator-( const time_point<CLOCK, DURATION> & p, const duration<REP, PERIOD> & d ) {
            return time_point<CLOCK, common_type_t<DURATION, duration<REP, PERIOD>>>{ p.time_since_epoch() - d };
        }

        template<typename CLOCK, typename DURATION1, typename DURATION2>
        constexpr common_type_t<DURATION1, DURATION2> operator-( const time_point<CLOCK, DURATION1> & lhs, const time_point<CLOCK, DURATION2> & rhs ) {
            return lhs.time_since_epoch() - rhs.time_since_epoch();
        }

    }

    /* Exposes the 'type' type which is the common type of two 'chrono::time_point's */
    template<typename CLOCK, typename DURATION1, typename DURATION2>
    struct common_type<chrono::time_point<CLOCK, DURATION1>, chrono::time_point<CLOCK, DURATION2>> {
        using type = chrono::time_point<CLOCK, typename common_type<DURATION1, DURATION2>::type>;
    };

    namespace chrono {

        /* 'chrono::system_clock' represents the system wide real time wall clock.
         * It might not be monotonic - on most systems the system time can be adjusted at any moment.
         * System clock measures UNIX time (i.e. time since 00:00:00 Coordinated Universal Time (UTC), Thursday, 1 January 1970, not counting leap seconds) */
        struct system_clock {
            using duration = nanoseconds;
            using rep = duration::rep;
            using period = duration::period;
            using time_point = chrono::time_point<system_clock, duration>;

            constexpr static bool is_steady { false };

            static time_point now( void ) noexcept;

            static time_t to_time_t( const time_point & t ) noexcept {
                return time_t{ duration_cast<seconds>( t.time_since_epoch() ).count() };
            }

            static time_point from_time_t( time_t t ) noexcept {
                return time_point_cast<system_clock::duration>( chrono::time_point<system_clock, seconds>( seconds( t ) ) );
            }
        };

        template<typename DURATION>
        using sys_time = time_point<system_clock, DURATION>;

        using sys_seconds = sys_time<seconds>;

        using sys_days = sys_time<days>;

        /* Class chrono::steady_clock represents the monotonic clock. The time points of this clock connot decrease as physical time moves forward and the time between ticks is constant.
         * This clock is not related to wall clock time and is most suitable for measuring intervals */
        struct steady_clock {
            using duration = nanoseconds;
            using rep = duration::rep;
            using period = duration::period;
            using time_point = chrono::time_point<steady_clock, duration>;

            constexpr static bool is_steady { true };

            static time_point now( void ) noexcept;
        };

        /* The high resolution clock is not implemented consistently accross different library implementations and shuould be avoided.
         * NOTE: Generally one should just use 'chrono::steady_clock' for duration measurements and 'chrono::system_clock' for wall time clock time */
        using high_resolution_clock = system_clock;

    }

}