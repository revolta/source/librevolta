#include "chrono.hpp"

namespace ReVolta::chrono {

    /* TODO: Might just use the syscalls interface to manage the time resources */

    system_clock::time_point system_clock::now( void ) noexcept {
        /* TODO - System clock shall use RTC? */
    }

    steady_clock::time_point steady_clock::now( void ) noexcept {
        /* TODO - Steady clock shall use TSC counter?  */
    }
}