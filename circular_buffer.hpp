#pragma once

#include "cstddef.hpp"
#include "array.hpp"
#include "iterator.hpp"
#include "mutex.hpp"
#include "algorithm.hpp"

namespace ReVolta {

    /* Logic explained here: https://en.wikipedia.org/wiki/Circular_buffer */

    template<typename T, size_t N>
    class circular_buffer {
    public:
        using value_type = T;
        /* Underlying container type. Its size is extended by one slot to follow waste slot strategy */
        using container_type = array<T, N + 1>;
        using size_type = size_t;
        using iterator = circular_iterator<typename container_type::iterator, container_type>;
        using const_iterator = circular_iterator<typename container_type::const_iterator, container_type>;
        using reverse_iterator = ReVolta::reverse_iterator<iterator>;
        using const_reverse_iterator = ReVolta::reverse_iterator<const_iterator>;
        using reference = value_type &;
        using const_reference = const value_type &;
        using difference_type = typename container_type::difference_type;
        using pointer = typename container_type::pointer;

    private:
        /* Underlying container holding all the data */
        container_type  m_container;
        /* Iterator pointing to the movable beginning of the circular buffer - the first element stored within */
        iterator m_begin;
        /* Iterator pointing to the movable end of the circular buffer - the element passed the last element in the container */
        iterator m_end;
        /* Thread safe access control mutex */
        mutable mutex m_access;
    
        /* Inspired by https://www.justsoftwaresolutions.co.uk/threading/thread-safe-copy-constructors.html */
        circular_buffer( const circular_buffer & other, const lock_guard<mutex> & )
            :   m_container( other.m_container ),
                m_begin( other.m_begin ),
                m_end( other.m_end )
        {}

        circular_buffer( circular_buffer && other, const lock_guard<mutex> & )
            :   m_container( move( other.m_container ) ),
                m_begin( move( other.m_begin ) ),
                m_end( move( other.m_end ) )
        {}

    public:
        /* Default constructor just constructs the circular buffer using 'nullptr' */
        constexpr circular_buffer( void ) : circular_buffer( nullptr ) {}

        /* 'nullptr' constructor makes up the empty container where the underlying container's elements are default constructed */
        constexpr circular_buffer( nullptr_t ) : m_container(), m_begin( m_container.begin(), m_container ), m_end( m_begin ) /* , m_empty( true ) */ {}

        /* Construct circular buffer and insert the value by copy at the end. Once constructed, the 'size() == 1' */
        constexpr circular_buffer( const value_type & value ) : circular_buffer( nullptr ) {
            this->push_back( value );
        }

        /* Construct circular buffer and insert the value at the end using move semantics. Once constructed, the 'size() == 1' */
        constexpr circular_buffer( value_type && value ) : circular_buffer( nullptr ) {
            this->push_back( move( value ) );
        }

        /* Construct circular buffer using the elements in range [first, last) */
        template<input_iterator I>
        constexpr circular_buffer( I first, I last ) : circular_buffer( nullptr ) {
            for( I it { first }; it < last; ++it ) {
                this->push_back( *it );
            }
        }

        /* Construct circular buffer and pre-fill it by the content of 'init'. The circular buffer is filled until it is full.
         * In case the 'init' contains more elements than the capacity of the circular buffer, those elements which exceed the buffer
         * capacity are not copied */
        constexpr circular_buffer( initializer_list<value_type> init ) : circular_buffer( init.begin(), init.end() ) {}

        /* Thread safe copy constructor locks the access automatically during the construction */
        circular_buffer( const circular_buffer & other ) : circular_buffer( other, lock_guard<mutex>{ other.m_access } ) {}

        /* Thread safe move constructor locks the access automatically during the construction */
        circular_buffer( circular_buffer && other ) : circular_buffer( move( other ), lock_guard<mutex>{ other.m_access } ) {}

        /* Thread safe copy assingment operator. Locks both 'this' and 'other' */
        circular_buffer & operator=( const circular_buffer & other ) {
            if( this != addressof( other ) ) {
                unique_lock<mutex> this_lock{ this->m_access, defer_lock };
                /* FIXME: Might be shared_lock<mutex> once available
                 * See: https://stackoverflow.com/a/29988626/5677080 */
                unique_lock<mutex> other_lock{ other.m_access, defer_lock };
                /* Lock the two mutexes simultaneously to prevent dead lock */
                lock( this_lock, other_lock );
                /* Copy the data */
                m_container = other.m_container;
                m_begin = other.m_begin;
                m_end = other.m_end;
            }
            return *this;
        }

        /* Thread safe move assingment operator. Locks both 'this' and 'other' */
        circular_buffer & operator=( circular_buffer && other ) {
            if( this != addressof( other ) ) {
                unique_lock<mutex> this_lock{ this->m_access, defer_lock };
                unique_lock<mutex> other_lock{ other.m_access, defer_lock };
                /* Lock the two mutexes simultaneously to prevent dead lock */
                lock( this_lock, other_lock );
                /* Copy the data */
                m_container = move( other.m_container );
                m_begin = move( other.m_begin );
                m_end = move( other.m_end );
            }
            return *this;
        }

        /* Returns the circular iterator pointing to the first element in the circular buffer */
        iterator begin( void ) noexcept {
            return m_begin;
        }

        /* Returns the circular iterator pointing to the first element in the circular buffer */
        const_iterator begin( void ) const {
            return const_iterator{ m_begin };
        }

        /* Returns the circular iterator pointing to the first element in the circular buffer */
        const_iterator cbegin( void ) const {
            return const_iterator{ m_begin };
        }

        reverse_iterator rbegin( void ) {
            return reverse_iterator{ end() };
        }

        const_reverse_iterator rbegin( void ) const {
            return const_reverse_iterator{ end() };
        }

        const_reverse_iterator crbegin( void ) const {
            return const_reverse_iterator{ end() };
        }

        /* Returns the circular iterator pointing to the element passed the last element in the circular buffer */
        iterator end( void ) noexcept {
            return m_end;
        }

        /* Returns the circular iterator pointing to the element passed the last element in the circular buffer */
        const_iterator end( void ) const {
            return const_iterator{ m_end };
        }

        /* Returns the circular iterator pointing to the element passed the last element in the circular buffer */
        const_iterator cend( void ) const {
            return const_iterator{ m_end };
        }

        reverse_iterator rend( void ) {
            return reverse_iterator{ begin() };
        }

        const_reverse_iterator rend( void ) const {
            return const_reverse_iterator{ begin() };
        }

        const_reverse_iterator crend( void ) const {
            return const_reverse_iterator{ begin() };
        }

        /* Circular buffer empty flag */
        [[nodiscard]] bool empty( void ) const {
            return begin() == end();
        } 

        /* Circular buffer full flag */
        [[nodiscard]] bool full( void ) const {
            return ++end() == begin();
        }

        size_type size( void ) {
            /* There might be a race condition reached once one thread is writing data while the other is reading map size */
            lock_guard<mutex> lock( this->m_access );
            return distance( begin(), end() );
        }

        constexpr size_type max_size( void ) {
            return N;
        }

        /* Prepends the given element value to the beginning of the container. The new element is initialized as copy of the given element */
        constexpr void push_front( const value_type & value ) {
             if( !this->full() ) {
                /* Emplace new value_type element before begin() by copy */
                emplace_front( value );
             }
        }

        /* Prepends the given element value to the beginning of the container. The value is moved into the new element */
        constexpr void push_front( value_type && value ) {
            if( !this->full() ) {
                /* Emplace new value_type element before begin() using move semantics */
                emplace_front( move( value ) );
            }
        }

        /* Appends the given element at the end of the container. The new element is initialized as copy of the given element */
        constexpr void push_back( const value_type & value  ) requires ( CopyInsertable<value_type> ) {
            if( !this->full() ) {
                /* Emplace new value_type element before begin() by copy */
                emplace_back( value );
            }
        }

        /* Appends the given element at the end of the container. The value is moved into the new element */
        constexpr void push_back( value_type && value ) requires ( MoveInsertable<value_type> ) {
            if( !this->full() ) {
                /* Emplace new value_type element before begin() by copy */
                emplace_back( move( value ) );
            }
        }

        /* Remove first element of the container */
        constexpr void pop_front( void ) {
            if( !this->empty() ) {
                erase( begin() );
            }
        }

        /* Remove last element of the container */
        constexpr void pop_back( void ) {
            if( !this->empty() ) {
                erase( --end() );
            }
        }

        /* Emplace element before begin */
        template<typename... ARGUMENTS>
        inline iterator emplace_front( ARGUMENTS &&... arguments ) {
            return iterator{ construct_at( addressof( *( --this->m_begin ) ), forward<ARGUMENTS>( arguments )... ), this->m_container };
        }

        /* Append element */
        template<typename... ARGUMENTS>
        inline iterator emplace_back( ARGUMENTS &&... arguments ) {
            return iterator{ construct_at( addressof( *( this->m_end++ ) ), forward<ARGUMENTS>( arguments )... ), this->m_container };
        }

        /* Inserts a new element into the container directly before 'position'. Returns iterator pointing to emplaced element */
        template<typename... ARGUMENTS>
        iterator emplace( const_iterator position, ARGUMENTS &&... arguments ) requires ( MoveAssignable<value_type> && MoveInsertable<value_type> && EmplaceConstructible<value_type> ) {
            /* Lock the buffer first */
            lock_guard<mutex> lock{ this->m_access };
            /* 'const_iterator position' is not convertible to 'iterator' so a temporary iterator shall be constructed pointing to the beginning and then advanced apropriately to match the 
             * given 'position' */
            iterator pos { this->begin() };
            advance( pos, distance<const_iterator>( this->cbegin(), position ) );
            /* Evaluate the optimal direction to emplace to rotate as few times as possible */
            if( distance( this->begin(), pos ) < distance( pos, this->end() ) ) {
                /* NOTE: Returns iterator to the element emplaced. begin() iterator updated within */
                emplace_front( forward<ARGUMENTS>( arguments )... );
                /* Rotate LEFT the subrange [ begin(), position ) to order the elements to make the emplaced one located at correct position
                 * (emplaced element is located at front, by single rotation it is moved to the last position witin the subrange) */
                rotate( this->begin(), this->begin() + 1, pos-- );
            } else {
                /* NOTE: Returns iterator to the element emplaced. end() iterator updated within */
                emplace_back( forward<ARGUMENTS>( arguments )... );
                /* Rotate RIGHT the subrange [ rbegin(), position ) to order the elements to make the emplaced one located at correct position
                 * (emplaced element is located at the back, by single rotation it is moved to the first position witin the subrange) */
                rotate( this->rbegin(), this->rbegin() + 1, make_reverse_iterator( pos ) );
            }
            return iterator{ pos };
        }

        /* Removes the element at 'position'. Returns an iterator following the last removed element */
        iterator erase( const_iterator position ) {
            /* Lock the buffer */
            lock_guard<mutex> lock{ this->m_access };
            /* 'const_iterator position' is not convertible to 'iterator' so a temporary iterator shall be constructed pointing to the beginning and then advanced apropriately to match the 
             * given 'position' */
            iterator pos { this->begin() };
            advance( pos, distance<const_iterator>( this->cbegin(), position ) );
            /* Evaluate the optimal direction to rotate the element to be removed so it appears at the very beginning or end to make it removed there */
            if( distance( begin(), pos ) < distance( pos, end() ) ) {
                /* Rotate the subrange to move the element to be destroyed at the front position */
                rotate( make_reverse_iterator( pos ) - 1, make_reverse_iterator( pos ), rend() );
                /* Destroy the element at the front position */
                destroy_at( addressof( *( this->m_begin++ ) ) );
            } else {
                /* Rotate the subrange to move the element to be destroyed at the back */
                rotate( pos, pos-- + 1, end() );
                /* Destroy the element at the back */
                destroy_at( addressof( *( --this->m_end ) ) );
            }
            /* Return the iterator to the element following the element removed */
            return iterator{ ++pos };
        }

        /* Removes the elements in the range [first, last). Returns an iterator following the last removed element */
        iterator erase( const_iterator first, const_iterator last ) {
            /* Iterate through the range [first, last) */
            for( ; first != last; ++first ) {
                this->erase( first );
            }
            return ++first;
        }

        /* Get the reference to the first element of the container */
        constexpr reference front( void ) {
            lock_guard<mutex> lock{ this->m_access };
            return *( begin() );
        }

        /* Get the reference to the first element of the container */
        constexpr const_reference front( void ) const {
            lock_guard<mutex> lock{ this->m_access };
            return *( begin() );
        }

        /* Get the reference to the last element of the container */
        constexpr reference back( void ) {
            lock_guard<mutex> lock{ this->m_access };
            return *( --end() );
        }

        /* Get the reference to the last element of the container */
        constexpr const_reference back( void ) const {
            lock_guard<mutex> lock{ this->m_access };
            return *( --end() );
        }

        constexpr void swap( circular_buffer & other ) {
            using ReVolta::swap;
            if( this != addressof( other ) ) {
                unique_lock<mutex> this_lock{ this->m_access, defer_lock };
                unique_lock<mutex> other_lock{ other.m_access, defer_lock };
                /* Lock the two mutexes simultaneously to prevent dead lock */
                lock( this_lock, other_lock );
                /* Swap the data */
                swap( m_container, other.m_container );
                swap( m_begin, other.m_begin );
                swap( m_end, other.m_end );
            }
        }
    };

    template<typename T, size_t N>
    constexpr void swap( circular_buffer<T, N> & lhs, circular_buffer<T, N> & rhs ) {
        lhs.swap( rhs );
    }

}