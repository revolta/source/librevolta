#pragma once

#include "cstddef.hpp"

#if defined ( _LIBREVOLTA_FREESTANDING )

namespace std {

    template<typename T>
    class initializer_list {
    public:
        using value_type = T;
        using reference = const T &;
        using const_reference = const T &;
        using size_type = size_t;
        using iterator = const T *;
        using const_iterator = const T *;

    private:
        iterator  m_array;
        size_type m_length;

        constexpr initializer_list( const_iterator array, size_type length ) : m_array( array ), m_length( length ) {}

    public:
        constexpr initializer_list( void ) noexcept : m_array( nullptr ), m_length( 0 ) {}

        constexpr size_type size( void ) const noexcept {
            return m_length;
        }

        constexpr const_iterator begin( void ) const noexcept {
            return m_array;
        }

        constexpr const_iterator end( void ) const noexcept {
            return begin() + size();
        }
    };

    template<typename T>
    constexpr const T * begin( initializer_list<T> init ) noexcept {
        return init.begin();
    }

    template<typename T>
    constexpr const T * end( initializer_list<T> init ) noexcept {
        return init.end();
    }
}

#endif /* _LIBREVOLTA_FREESTANDING */

namespace ReVolta {

    template<typename T>
    using initializer_list = std::initializer_list<T>;

}