#pragma once

#include "cstddef.hpp"
#include "cstdint.hpp"

#include "limits.h"

namespace ReVolta {

    template<typename T>
    struct numeric_limits {

    };

    template<typename T>
    struct numeric_limits<const T> : public numeric_limits<T> {};

    template<typename T>
    struct numeric_limits<volatile T> : public numeric_limits<T> {};

    template<typename T>
    struct numeric_limits<const volatile T> : public numeric_limits<T> {};

    template<>
    struct numeric_limits<unsigned long> {
        static constexpr bool is_specialized = true;
        static constexpr unsigned long min( void ) noexcept { return 0; }
        static constexpr unsigned long max( void ) noexcept { return __LONG_MAX__ * 2UL + 1; }
        static constexpr unsigned long lowest( void ) noexcept { return min(); }
        static constexpr bool is_signed = false;
        static constexpr bool is_integer = true;
        static constexpr bool is_exact = true;
        static constexpr int radix = 2;
        static constexpr unsigned long epsilon( void ) noexcept { return 0; }
        static constexpr unsigned long round_error( void ) noexcept { return 0; }
        /* TODO: More to come */
    };

}