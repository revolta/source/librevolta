#pragma once

#include "forward_list.hpp"
#include "memory.hpp"
#include "bits/move.hpp"

namespace ReVolta {

    /* The implementation inspired by:
     * https://www.codeproject.com/Articles/11015/The-Impossibly-Fast-C-Delegates
     * https://www.codeproject.com/Articles/1170503/The-Impossibly-Fast-Cplusplus-Delegates-Fixed
     */ 

    namespace Detail {

        template<typename> class delegate_base;

        template<typename RETVAL, typename... ARGUMENTS>
        class delegate_base<RETVAL( ARGUMENTS... )> {
        protected:
            using callable_type = RETVAL(*)( void * this_ptr, ARGUMENTS... );

            struct invocation_element {
                /* Default constructor */
                invocation_element( void ) noexcept = default;

                /* Copy constructor */
                invocation_element( const invocation_element & other ) noexcept : m_object( other.m_object ), m_callable( other.m_callable ) {}

                /* Move constructor */
                invocation_element( invocation_element && other ) noexcept : m_object( move( other.m_object ), m_callable( move( other.m_callable ) ) ){}

                explicit invocation_element( void * this_ptr, callable_type callable ) noexcept : m_object( this_ptr ), m_callable( callable ) {}

                bool operator==( const invocation_element & other ) const {
                    return ( ( other.m_callable == m_callable ) && ( other.m_object == m_object ) );
                }

                bool operator!=( const invocation_element & other ) const {
                    return ( ( other.m_callable != m_callable ) || ( other.m_object != m_object ) );
                }

                void * m_object { nullptr };

                callable_type m_callable { nullptr };
            };   
        };

    }

    /* Primary templates */
    template<typename> class delegate;
    template<typename> class multicast_delegate;

    template<typename RETVAL, typename... ARGUMENTS>
    class delegate<RETVAL( ARGUMENTS... )> final : private Detail::delegate_base<RETVAL( ARGUMENTS... )> {
#if false
        friend class multicast_delegate<RETVAL( ARGUMENTS... )>;
#endif
    public:
        using retval_type = RETVAL;
        using invocation_type = typename Detail::delegate_base<RETVAL(ARGUMENTS...)>::invocation_element;

    private:
        invocation_type m_invocation;

    public:
        /* Default constructor */
        delegate( void ) noexcept = default;

        /* Copy constructor */
        delegate( const delegate & other ) noexcept : m_invocation( other.m_invocation ) {}

        /* Move constructor */
        delegate( delegate && other ) noexcept : m_invocation( move( other.m_invocation ) ) {}

        /* Member function delegate factory function */
        template<class T, RETVAL( T::* FUNCTION )( ARGUMENTS... )>
        static delegate create( T * ptr ) noexcept {
            return delegate( ptr, member_function_callable<T, FUNCTION> );
        }

        /* Const member function delegate factory function */
        template<class T, RETVAL( T::* FUNCTION )( ARGUMENTS... ) const>
        static delegate create( const T * ptr ) noexcept {
            return delegate( const_cast<T *>( ptr ), const_member_function_callable<T, FUNCTION> );
        }

        /* Freestanding function delegate factory function */
        template<RETVAL(* FUNCTION )( ARGUMENTS... )>
        static delegate create( void ) noexcept {
            return delegate( nullptr, function_callable<FUNCTION> );
        }

        /* Lambda delegate factory function */
        template<typename LAMBDA>
        static delegate create( const LAMBDA & lambda ) noexcept {
            /* FIXME: static_cast does not work in here */
#if false
            return delegate( static_cast<void *>( &lambda ), lambda_callable<LAMBDA> );
#else
            return delegate( (void *)( &lambda ), lambda_callable<LAMBDA> );
#endif
        }

        /* operator() is used to invoke the stored callable */
        RETVAL operator()( ARGUMENTS... arguments ) const {
            return (* m_invocation.m_callable)( m_invocation.m_object, arguments... );
        }

        /* Delegate comparison operators */

        bool operator==( void * ptr ) const noexcept {
            return ( ( ptr == nullptr ) && ( this->is_null() ) );
        }

        bool operator!=( void * ptr ) const noexcept {
            return ( ( ptr != nullptr ) || ( !this->is_null() ) );
        }

        bool operator==( const delegate & other ) const noexcept {
            return ( m_invocation == other.m_invocation );
        }

        bool operator!=( const delegate & other ) const noexcept {
            return ( m_invocation != other.m_invocation );
        }

        bool is_null( void ) const noexcept {
            return ( m_invocation.m_callable == nullptr );
        }

    private:
        /* Constructor shall be private as is invoked only through the 'create()' factory functions */
        explicit delegate( void * object, typename Detail::delegate_base<RETVAL( ARGUMENTS... )>::callable_type callable ) noexcept {
            m_invocation.m_object = object;
            m_invocation.m_callable = callable;
        }

        template<RETVAL(* FUNCTION)( ARGUMENTS... )>
        static RETVAL function_callable( void *, ARGUMENTS... arguments ) noexcept {
            return (FUNCTION)( arguments... );
        }

        template<class T, RETVAL( T::* FUNCTION )( ARGUMENTS... )>
        static RETVAL member_function_callable( void * this_ptr, ARGUMENTS... arguments ) noexcept {
            T * function = static_cast<T *>( this_ptr );
            return ( function->*FUNCTION )( arguments... );
        }

        template<class T, RETVAL( T::* FUNCTION )( ARGUMENTS... ) const>
        static RETVAL const_member_function_callable( void * this_ptr, ARGUMENTS... arguments ) noexcept {
            T const * function = static_cast<T *>( this_ptr );
            return ( function->*FUNCTION )( arguments... );
        }

        template<typename LAMBDA>
        static RETVAL lambda_callable( void * this_ptr, ARGUMENTS... arguments ) noexcept {
            LAMBDA * lambda = static_cast<LAMBDA *>( this_ptr );
            return ( lambda->operator() )( arguments... );
        }
    };

    /* The multicast delegate contains a list of the assigned delegates. When the multicast delegate is called,
     * it invokes the delegates in the list, in order. Only delegates of the same type can be combined. */
    template<typename RETVAL, typename... ARGUMENTS>
    class multicast_delegate<RETVAL( ARGUMENTS... )> final : private Detail::delegate_base<RETVAL( ARGUMENTS... )> {
    
        using invocation_type = typename Detail::delegate_base<RETVAL( ARGUMENTS... )>::invocation_element;

        forward_list<invocation_type> m_invocation_list;

    public:
        /* Once new delegate is added to this multicast delegate, this is the type which is being allocated */
        using node_type = typename forward_list<invocation_type>::node_type;

        /* Default constructor */
        multicast_delegate( void ) noexcept = default;

        multicast_delegate & operator=( const multicast_delegate & ) = delete;

        multicast_delegate( const multicast_delegate & ) = delete;

        /* Destructor */
        ~multicast_delegate( void ) noexcept {
            m_invocation_list.clear();
        }

        void operator()( ARGUMENTS... arguments ) const noexcept {
            /* Iterate over the whole invocation list and invoke all the callables stored */
            for( auto & invocation : m_invocation_list ) {
                ( *( invocation.m_callable ))( invocation.m_object, arguments... );
            }
        }

        /* TODO: Constrain HANDLER with concept */
        template<typename HANDLER>
        void operator()( ARGUMENTS... arguments, HANDLER handler ) const noexcept {
            for( size_t index { 0 }; auto & invocation : m_invocation_list ) {
                RETVAL value = ( *( invocation.m_callable ))( invocation.m_object, arguments... );
                /* TODO: Use addressof() instead of '&' */
                /* TODO: Check - is that right to forward the address of local variable to the handler? */
                handler( index, &value );
                ++index;
            }
        }

        void operator()( ARGUMENTS... arguments, delegate<void( size_t, RETVAL* )> handler ) const noexcept {
            operator()<decltype( handler )>( arguments..., handler );
        }

        multicast_delegate & operator+=( const multicast_delegate & other ) noexcept {
            for( auto & element : other.m_invocation_list ) {
                this->m_invocation_list.emplace_front( element.m_object, element.m_callable );
            }
            return *this;
        }

        template<typename LAMBDA>
        multicast_delegate & operator+=( const LAMBDA & lambda ) noexcept {
            return *this += delegate<RETVAL( ARGUMENTS... )>::template create<LAMBDA>( lambda );
        }

        multicast_delegate & operator+=( const delegate<RETVAL( ARGUMENTS... )> & other ) noexcept {
            if( !other.empty() ) {
                this->m_invocation_list.emplace_front( other.m_invocation.m_object, other.m_invocation.m_callable );
            }
            return *this;
        }

        size_t size( void ) const noexcept {
            return m_invocation_list.size();
        }

        bool operator==( void * ptr ) const noexcept {
            return ( ( ptr == nullptr ) && ( this->empty() ) );
        }

        bool operator!=( void * ptr ) const noexcept {
            return ( ( ptr != nullptr ) || ( !this->empty() ) );
        }

        bool operator==( const multicast_delegate & other ) const noexcept {
            /* Once the sizes are not equal the multicast_delegate is considered unequal */
            if( this->size() != other.size() ) return false;
            return equal( this->m_invocation_list.begin(), this->m_invocation_list.end(), other.m_invocation_list.begin() );
        }

        bool operator!=( const multicast_delegate & other ) const noexcept {
            return !( *this == other );
        }

        bool operator==( const delegate<RETVAL( ARGUMENTS... )> & other ) const noexcept {
            /* TODO: Line #58 */
        }

        bool operator!=( const delegate<RETVAL( ARGUMENTS... )> & other ) const noexcept {
            return !( *this == other );
        }

        bool empty( void ) const noexcept {
            return m_invocation_list.empty();
        }

    };

}