#pragma once

#include <cstddef.hpp>

/* The following placement new/delete overloads are being used only in freestanding environmnet
 * used to build the kernel / bootstrap binaries. While testing the sources, the operator new 
 * are taken from standard library */
#if defined ( _LIBREVOLTA_FREESTANDING )

    /* Default placement versions of new operator */
    inline void * operator new( size_t, void * ptr ) noexcept { 
        return ptr; 
    }

    inline void * operator new[]( size_t, void * ptr ) noexcept { 
        return ptr;
    }

    /* Default placement versions of operator delete */
    inline void operator delete ( void *, void * ) noexcept {}

    inline void operator delete[]( void *, void * ) noexcept {}

#endif
