#pragma once

#include "cstddef.hpp"
#include "concepts.hpp"
#include "utility.hpp"
#include "memory.hpp"
#include "initializer_list.hpp"
#include "iterator.hpp"
#include "bits/move.hpp"

namespace ReVolta {

    /* Forward list's internal data storage type - the node - holds the instance of T and the link to the next node in list */
    template<typename T, typename PAYLOAD = T>
    class forward_list_node {
    public:
        forward_list_node( void ) noexcept : m_next( nullptr ), m_value() {}

        template<typename... ARGUMENTS>
        forward_list_node( forward_list_node * next, ARGUMENTS &&... arguments ) noexcept : m_next( next ), m_value( { forward<ARGUMENTS>( arguments )... } ) {}

        forward_list_node( forward_list_node * next, const PAYLOAD & value ) noexcept : m_next( next ), m_value( value ) {}

        forward_list_node( forward_list_node * next, PAYLOAD && value ) noexcept : m_next( next ), m_value( move( value ) ) {}

        /* Move constructor - moveable */
        forward_list_node( forward_list_node && other ) noexcept : m_next( other.m_next ), m_value() {
            other.m_next = nullptr;
            /* FIXME: how to invalidate moved other.m_value? */
        }

        /* Non copyable */
        forward_list_node( const forward_list_node & ) noexcept = delete;

        forward_list_node & operator=( forward_list_node && other ) noexcept {
            m_next = other.m_next;
            other.m_next = nullptr;
            return *this;
        }

        forward_list_node & operator=( const forward_list_node &  ) noexcept = delete;

        ~forward_list_node( void ) {
            /* Destory the stored value */
            m_value.~PAYLOAD();
        }

        PAYLOAD & get_value( void ) noexcept {
            return m_value;
        }

        const PAYLOAD & get_value( void ) const noexcept {
            return m_value;
        }

        forward_list_node<T> * get_next( void ) const noexcept {
            return m_next;
        }

        void set_next( forward_list_node<T> * next ) noexcept {
            m_next = next;
        }

    private:
        /* NOTE: ORDER/LAYOUT MATTERS: 
         * The member variables are layed in memory exactly in the order of declaration. Compiler is not allowed
         * to reorder the member variables in the same access block (public/private/protected). As the next pointer is
         * place at the beginning of the memory block allocated to store forward_list_node, it is safe to reinterpret
         * one node type to the other (mainly head_node_type and node_type) which main purpose is to dereference only 
         * the next pointer and not the payload unless explicitly requested in head_node_type to get the metadata and 
         * to get the value in case of regular node_type for all others. This reinterpretation happens only in 
         * before_begin() iterator. Dereference such iterator expecting the access to its metadata is undefined */
        forward_list_node<T> * m_next { nullptr };
        PAYLOAD m_value;
    };

    template<typename T>
    class forward_list_iterator {
    public:
        using value_type = T;
        using node_type = forward_list_node<value_type>;
        using pointer = T*;
        using reference = T&;
        using difference_type = ptrdiff_t;
        using iterator_category = forward_iterator_tag;

        constexpr forward_list_iterator( void ) : forward_list_iterator( nullptr ) {}

        /* FIXME: error: converting to ‘const_iterator’ from initializer list would use explicit constructor */
        /* explicit */ forward_list_iterator( node_type * node  ) noexcept : m_node( node ) {}

        constexpr forward_list_iterator( nullptr_t ) noexcept : m_node( nullptr ) {}

        forward_list_iterator( const forward_list_iterator & other ) noexcept : m_node( other.m_node ) {}

        forward_list_iterator & operator=( const forward_list_iterator & other ) noexcept {
            forward_list_iterator{ other }.swap( *this );
            return *this;
        }
        
        /* Prefix increment operator */
        forward_list_iterator & operator++( void ) noexcept {
            m_node = m_node->get_next();
            return *this;
        }

        /* Postfix increment operator */
        forward_list_iterator operator++( int ) noexcept {
            forward_list_iterator temp { *this };
            m_node = m_node->get_next();
            return temp;
        }

        const reference operator*( void ) const noexcept {
            return m_node->get_value();
        }

        reference operator*( void ) noexcept {
            return m_node->get_value();
        }

        pointer operator->( void ) const noexcept {
            return &( m_node->get_value() );
        }

        bool operator==( const forward_list_iterator & other ) const noexcept {
            return m_node == other.m_node;
        }

        bool operator!=( const forward_list_iterator & other ) const noexcept {
            return m_node != other.m_node;
        }

        node_type * get( void ) const {
            return m_node;
        }

        forward_list_iterator get_next( void ) const noexcept {
            if( m_node != nullptr ) {
                return forward_list_iterator { m_node->get_next() };
            } else {
                return forward_list_iterator { nullptr };
            }
        }

        void set_next( const forward_list_iterator next ) noexcept {
            m_node->set_next( next.m_node );
        }

        void swap( forward_list_iterator & other ) noexcept {
            using ReVolta::swap;
            swap( m_node, other.m_node );
        }

    private:
        node_type * m_node;
    };

    /* TODO: Implement const_forward_iterator. Maybe utilize CRTP base class containing all the common parts */

    template<typename T, Allocator ALLOCATOR = allocator<T>>
    class forward_list {       
    public:
        using value_type = T;
        using allocator_type = ALLOCATOR;
        using size_type = size_t;
        using difference_type = ptrdiff_t;
        using reference = value_type &;
        using const_reference = const value_type &;
        using pointer = typename allocator_traits<allocator_type>::pointer;
        using const_pointer = typename allocator_traits<allocator_type>::const_pointer;
        using iterator = forward_list_iterator<value_type>;
        /* FIXME: Shall satisfy 'LegacyForwardIterator' to 'const value_type' */
#if false
        using const_iterator = const iterator;
#else
        using const_iterator = forward_list_iterator<const value_type>;
#endif

    private:
        /* Regular forward list node type */
        using node_type = typename iterator::node_type;
        /* Forward list's internally used allocator is the allocator for T rebound to node<T> */
        using node_allocator_type = typename allocator_traits<allocator_type>::rebind_alloc<node_type>::other;

        /* HEAD is the compressed pair of a node which next node pointer points to the first node of the forward list.
         * The compressed pair holds the node allocator type instance. Once the allocator is stateless, empty base
         * optimization takes place and thus the allocator instance does not occupy any memory
         * HEAD does not contain any value_type instance, is used just to point to the first node containg the value. */
      
        /* Forward list metadata are placed as payload into the head element node. */
        struct metadata {
            /* Returns the number of elements/nodes stored within the forward list */
            size_type & size( void ) noexcept {
                return m_size;
            }

            /* Returns the number of elements/nodes stored within the forward list */
            size_type const & size( void ) const noexcept {
                return m_size;
            }

        private:
            size_type m_size { 0 };
        };

        /* Head node is different to regular nodes. It's payload is not a value of the forward list but the forward list metadata */
        using head_node_type = forward_list_node<value_type, metadata>;

        /* The head element itself is the only list element owned by the forward list itself. The other regular nodes are hosted in the memory
         * provided by the allocator */
        head_node_type m_head;

        /* If node allocator is empty, it shall occupy no space */
        [[no_unique_address]] node_allocator_type m_node_allocator;

        inline head_node_type & get_head_node( void ) noexcept {
            return m_head;
        }

        inline const head_node_type & get_head_node( void ) const noexcept {
            return m_head;
        }

        inline node_allocator_type & get_node_allocator( void ) noexcept {
            return m_node_allocator;
        }

        inline const node_allocator_type & get_node_allocator( void ) const noexcept {
            return m_node_allocator;
        }

    public:
        /* Default constructor constructs empty container with default constructed allocator */
        forward_list( void ) : forward_list( allocator_type {} ) {}

        /* Constructs an empty container with the given allocator */
        /* NOTE: forward list stores node allocator instead of value_type allocator. The expression there uses allocator
         * converting constructor to achieve so. */
        explicit forward_list( const allocator_type & allocator ) : m_node_allocator( allocator ) {
            /* Head node is the leaf at the same time - the list is empty */
            this->get_head_node().set_next( nullptr );
        }

        /* Constucts the container with count copies of elements with value 'value' */
        forward_list( size_type count, const T & value, const allocator_type & allocator = allocator_type() ) : forward_list( allocator ) {
            this->insert_after( before_begin(), count, value );
        }

        /* Constructs the container with count default-inserted instances of T. No copies are made */
        explicit forward_list( size_type count, const allocator_type & allocator = allocator_type() ) : forward_list( allocator ) {
            this->insert_after( before_begin(), count, value_type {} );
        }

        /* Copy constructor. Constructs the container with the copy of the contents of 'other' */
        forward_list( const forward_list & other ) : forward_list( other, allocator_traits<allocator_type>::select_on_container_copy_construction( other.get_allocator() ) ) {}

        /* FIXME: Copy constructor copies the values in backward order to the source container 'other' */
        /* TODO: Implement test to verify fixed implementation */
        /* Constructs the container with the copy of the contents of other, using 'allocator' as the allocator */
        forward_list( const forward_list & other, const allocator_type & allocator ) : forward_list( allocator ) {
            /* Iterate over all the values of 'other' forward list */
            for( const value_type & value : other ) {
                this->emplace_after( before_begin(), value );
            }
        }

        /* Move constructor. Constructs the container with the contents of other using move semantics.
         * Allocator is obtained by move-construction from the allocator belonging to other */
        forward_list( forward_list && other ) : forward_list( move( other.get_allocator() ) ) {
            get_head_node() = move( other.get_head_node() );
            other.get_head_node() = head_node_type{};
        }

        /* Allocator extended move constructor using 'allocator' for the new container moving the contents from 'other' */
        forward_list( forward_list && other, const allocator_type & allocator ) : forward_list( allocator ) {
            get_head_node() = move( other.get_head_node() );
            other.get_head_node() = head_node_type{};
        }

        forward_list( initializer_list<value_type> init, const allocator_type allocator = allocator_type() ) : forward_list( allocator ) {    
            this->insert_after( before_begin(), init );
        }

        /* Destructor - the element destructors are called and the used storage is deallocated */
        ~forward_list( void ) {
            /* Destruct and deallocate all elements within the forward_list */
            clear();
        }

        /* Copy assignment operator */
        forward_list & operator=( const forward_list & other ) {
            if( allocator_traits<allocator_type>::propagate_on_container_copy_assignment::value == true ) {
                forward_list( other, other.get_allocator() ).swap( *this );
            } else {
                forward_list( other ).swap( *this );
            }
            return *this;
        }

        forward_list & operator=( forward_list && other ) noexcept( allocator_traits<allocator_type>::is_always_equal::value ) {
            forward_list( move( other ) ).swap( *this );
            return *this;
        }

        forward_list & operator=( initializer_list<value_type> init ) {
            forward_list( init ).swap( *this );
            return *this;
        }

        template<input_iterator I>
        void assign( I first, I last ) {
            /* TODO */
        }

        void assign( size_type n, const_reference t ) {

        }

        void assign( initializer_list<value_type> init ) {

        }

        /* Get a copy of the memory allocation object */
        allocator_type get_allocator( void ) const noexcept {
            return allocator_type { get_node_allocator() };
        }

        reference front( void ) {
            return *begin();
        }

        const_reference front( void ) const {
            return *begin();
        }

        /* NOTE: To dereference the returned operator except to access it's next pointer is undefined and considered as bug
         * as the size of payload of head element might be different to the size of regular node */
        inline iterator before_begin( void ) noexcept {
            /* Construct iterator which references the head node which next pointer points to the first node */
            return { reinterpret_cast<node_type *>( addressof( get_head_node() ) ) };
        }

        inline const_iterator before_begin( void ) const noexcept {
            /* Construct iterator which references the head node which next pointer points to the first node */
            return { reinterpret_cast<node_type *>( &( this->get_head_node() ) ) };
        }

        inline const_iterator cbefore_begin( void ) const noexcept {
            /* Construct iterator which references the head node which next pointer points to the first node */
            return { reinterpret_cast<node_type *>( &( this->get_head_node() ) ) };
        }

        /* returns an iterator to the first element. if the forward list is empty, the returned iterato is equal to end() */
        inline iterator begin( void ) noexcept {
            /* Construct an iterator holding the HEAD's next node pointer as the stored value */
            return { this->get_head_node().get_next() };
        }

        inline const_iterator begin( void ) const noexcept {
            /* Construct an iterator holding the HEAD's next node pointer as the stored value */
            return { this->get_head_node().get_next() };
        }

        inline const_iterator cbegin( void ) const noexcept {
            /* Construct an iterator holding the HEAD's next node pointer as the stored value */
            return { this->get_head_node().get_next() };
        }

        /* Returns an iterator to the element following the last element of the forward list
         * Acts as placeholder; attempt to access it results in undefined behaviour */
        inline iterator end( void ) noexcept {
            return { nullptr };
        }

        inline const_iterator end( void ) const noexcept {
            return { nullptr };
        }

        inline const_iterator cend( void ) const noexcept {
            return { nullptr };
        }

        [[nodiscard]] bool empty( void ) const noexcept {
            return ( begin() == end() );
        }

        size_type size( void ) const noexcept {
            /* Get the number of elements stored in head node as payload */
            return get_head_node().get_value().size();
        }

        size_type max_size( void ) const noexcept {
            /* The size of the container is defined by the allocator which performs the node allocation */
            return allocator_traits<node_allocator_type>::max_size( get_node_allocator() );
        }

        /* Erases all elements from the container. After this call, size() returns zero. */
        void clear( void ) noexcept {
            while( !this->empty() ) {
                this->erase_after( before_begin() );
            }
        }

        /* Inserts value after the element pointed by 'position' */
        iterator insert_after( const_iterator position, const value_type & value ) {
            /* Emplace new node and construct the element within using the element's copy constructor */
            return this->emplace_after( position, value );
        }

        iterator insert_after( const_iterator position, value_type && value ) {
            /* Emplace new node and construct the element within using the element's move constructor */
            return this->emplace_after( position, move( value ) );
        }

        /* Insert 'count' copies of the 'value' after the element pointer by 'position' */
        iterator insert_after( const_iterator position, size_type count, const value_type & value ) {
            iterator insert_iterator { position };
            for( size_type n : count ) {
                insert_iterator = this->emplace_after( insert_iterator, value );
            }
            return insert_iterator;
        }

        iterator insert_after( const_iterator position, initializer_list<value_type> init ) {
            iterator element_inserter { position };
            /* Iterate over all the elements in initializer list */
            for( const value_type & element : init ) {
                element_inserter = this->emplace_after( element_inserter, element );
            }
            return element_inserter;
        }

        template<typename... ARGUMENTS>
        iterator emplace_after( const_iterator position, ARGUMENTS &&... arguments ) {
            /* Allocate new node and store the pointer to the iterator */
            iterator emplaced_iterator { allocator_traits<node_allocator_type>::allocate( get_node_allocator(), 1 ) };
            /* Verify iterator validity... */
            if( emplaced_iterator != iterator { nullptr } ) {
                /* Construct the instance of value_type object within the node. The object is constructed at the object address
                 * obtained by iterator dereference which returns the reference to stored object */
                allocator_traits<node_allocator_type>::construct( get_node_allocator(), &( *emplaced_iterator ), forward<ARGUMENTS>( arguments )... );
            }
            /* The emplaced node iterator's next pointer is the one which is the original next pointer of 'position' iterator */
            emplaced_iterator.set_next( position.get_next() );
            /* The node at 'position' next points to the emplaced node */
            ( iterator{ position } ).set_next( emplaced_iterator );
            /* Notice new element has been added */
            get_head_node().get_value().size()++;
            /* Return the iterator to the element being emplaced */
            return emplaced_iterator;
        }

        iterator erase_after( const_iterator position ) {
            /* Construct non const iterator refering to given position */
            iterator at_position { position };
            /* As the node being removed is the 'position' next one, construct such iterator refering to this element to be removed */
            iterator removed { position.get_next() };
            /* If the iterator to be removed is valid... */
            if( removed != iterator { nullptr } ) {
                /* ... set the given position's next pointer to be the removed one's next */
                at_position.set_next( removed.get_next() );          
                /* Destroy the element stored within the node */
                allocator_traits<node_allocator_type>::destroy( get_node_allocator(), removed.get() );
                /* Deallocate the node itself */
                allocator_traits<node_allocator_type>::deallocate( get_node_allocator(), removed.get(), 1 );
            } else {
                at_position.set_next( end() );
            }
            /* Notice the number of elements is decreased */
            get_head_node().get_value().size()--;
            /* Return the iterator to the next element after the erased one */
            return at_position.get_next();
        }

        void push_front( const value_type & value ) {
            this->emplace_after( before_begin(), value );
        }

        void push_front( value_type && value ) {
            this->emplace_after( before_begin(), move( value ) );
        }

        /* Inserts a new element to the beginning of the container */
        template<typename... ARGUMENTS>
        reference emplace_front( ARGUMENTS &&... arguments ) {
            this->emplace_after( before_begin(), forward<ARGUMENTS>( arguments )... );
            return front();
        }

        /* Removes the first element of the container. If there are no elements in the container, the behavior is undefined */
        void pop_front( void ) {
            this->erase_after( before_begin() );
        }

        void resize( size_type count ) {
            /* TODO */
        }

        void resize( size_type count, const value_type & value ) {
            /* TODO */
        }

        void swap( forward_list & other ) noexcept( allocator_traits<allocator_type>::is_always_equal::value ) {
            using ReVolta::swap;
            swap( get_head_node(), other.get_head_node() );
        }

        void merge( forward_list & other ) {
            /* TODO */
        }

        void merge( forward_list && other ) {
            /* TODO */
        }

        /* TODO: Constrain by concept */
        template<typename COMPARE>
        void merge( forward_list & other, COMPARE compare ) {
            /* TODO */
        }

        /* TODO: Constrain by concept */
        template<typename COMPARE>
        void merge( forward_list && other, COMPARE compare ) {
            /* TODO */
        }

        void splice_after( const_iterator position, forward_list & other ) {
            /* TODO */
        }

        void splice_after( const_iterator position, forward_list && other ) {
            /* TODO */
        }

        void splice_after( const_iterator position, forward_list & other, const_iterator iterator ) {
            /* TODO */
        }

        void splice_after( const_iterator position, forward_list && other, const_iterator iterator ) {
            /* TODO */
        }

        void splice_after( const_iterator position, forward_list & other, const_iterator first, const_iterator last ) {
            /* TODO */
        }

        void splice_after( const_iterator position, forward_list && other, const_iterator first, const_iterator last ) {
            /* TODO */
        }

        size_type remove( const_reference value ) {
            /* TODO */
            return 0;
        }

        /* TODO: Constrain by concept */
        template<typename UNARY_PREDICATE>
        size_type remove_if( UNARY_PREDICATE predicate ) {
            /* TODO */
            return 0;
        }

        /* Reverses the order of the elements in the container. No references or iterators become invalidated. */
        void reverse( void ) noexcept {
            /* TODO */
        }

        /* Removes all consecutive duplicate elements from the container. Only the first element in each group of equal elements is left.
         * Uses operator== to compare the elements */
        size_type unique( void ) {
            /* TODO */
            return 0;
        }

        /* Removes all consecutive duplicate elements from the container. Only the first element in each group of equal elements is left.
         * Uses the given binary predicate p to compare the elements */
        /* TODO: Constrain by concept */
        template<typename BINARY_PREDICATE>
        size_type unique( BINARY_PREDICATE predicate ) {
            /* TODO */
            return 0;
        }

        void sort( void ) {
            /* TODO */
        }

        template<typename COMPARE>
        void sort( COMPARE compare ) {
            /* TODO */
        }

    };

    /* Deducation guide */
    template<input_iterator I, Allocator ALLOCATOR = allocator<typename iterator_traits<I>::value_type>>
    forward_list( I, I, ALLOCATOR = ALLOCATOR() ) -> forward_list<typename iterator_traits<I>::value_type, ALLOCATOR>;

    template<typename T, Allocator ALLOCATOR>
    void swap( forward_list<T, ALLOCATOR> & lhs, forward_list<T, ALLOCATOR> & rhs ) noexcept( noexcept( lhs.swap( rhs ) ) ) {
        lhs.swap( rhs );
    }

    template<typename T, Allocator ALLOCATOR>
    bool operator==( const forward_list<T, ALLOCATOR> & lhs, const forward_list<T, ALLOCATOR> & rhs ) {
        return equal( lhs.begin(), lhs.end(), rhs.begin() );
    }

    /* TODO: Three way comparison operator */

    template<typename T, Allocator ALLOCATOR, typename U>
    typename forward_list<T, ALLOCATOR>::size_type erase( forward_list<T, ALLOCATOR> & container, const U & value ) {
        return container.remove_if( [&]( auto & element ) { return element == value; } );
    }

    template<typename T, Allocator ALLOCATOR, typename PREDICATE>
    typename forward_list<T, ALLOCATOR>::size_type erase( forward_list<T, ALLOCATOR> & container, PREDICATE predicate ) {
        return container.remove_if( predicate );
    }

}