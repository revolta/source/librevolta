#pragma once

#include "bits/move.hpp"
#include "bits/pair.hpp"
#include "cstdint.hpp"

namespace ReVolta {

	template<typename T, T... IDX>
	struct integer_sequence {
		using value_type = T;
		static constexpr size_t size( void ) noexcept { return sizeof...(IDX); }
	};

	template<size_t... IDX>
	using index_sequence = integer_sequence<size_t, IDX...>;

	template<typename T, T NUM>
	using make_integer_sequence = integer_sequence<T, __integer_pack( NUM )...>;

	template<size_t NUM>
	using make_index_sequence = make_integer_sequence<size_t, NUM>;

	template<typename... Ts>
	using index_sequence_for = make_index_sequence<sizeof...( Ts )>;

	class undefined;

	template<typename T>
	struct get_first_argument {
		using type = undefined; 
	};

	template<template<typename, typename...> class TEMPLATE, typename T, typename... Ts>
	struct get_first_argument<TEMPLATE<T, Ts...>> {
		using type = T;
	};

	template<typename T, typename U>
	struct replace_first_argument {};

	template<template<typename, typename...> class TEMPLATE, typename U, typename T, typename... Ts>
	struct replace_first_argument<TEMPLATE<T, Ts...>, U> {
		using type = TEMPLATE<U, Ts...>;
	};

	template<typename E>
	constexpr typename underlying_type<E>::type to_underlying( E e ) {
		return static_cast<typename underlying_type<E>::type>( e );
	}

	/* TODO: Get rid of it as this functionality is covered within Port class AND is architecture specific! */
	inline void port_write_byte( const uint16_t port, const uint8_t value ) noexcept {
		asm volatile( "outb %1, %0" : : "dN" ( port ), "a" ( value ) );
	}

	/* TODO: Get rid of it as this functionality is covered within Port class AND is architecture specific! */
	inline uint8_t port_read_byte( const uint16_t port ) noexcept {
		uint8_t value { 0 };
		asm volatile( "inb %1, %0" : "=a" ( value ) : "dN" ( port ) );
		return value;
	}

	template<typename T>
	struct tag_type {
		using type = T;
	};

	template<typename T>
	inline constexpr tag_type<T> tag {};

	namespace experimental {
    
        /* The struct erased_type is an empty struct that serves as a placeholder for a type 
         * in situations where the actual type is determined at runtime. For example, 
         * in classes that use type-erased allocators, the nested typedef allocator_type 
         * is an alias for erased_type. */
        struct erased_type {};

    }

}
