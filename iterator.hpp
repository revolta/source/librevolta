#pragma once

#include "bits/iterator_concepts.hpp"
#include "bits/iterator_types.hpp"
#include "bits/iterator_adaptors.hpp"
#include "bits/range_access.hpp"

namespace ReVolta {

    template<input_iterator I, typename DISTANCE>
    constexpr void advance( I & iterator, DISTANCE n ) {
        using category = typename iterator_traits<I>::iterator_category;
        typename iterator_traits<I>::difference_type distance { n };
        if constexpr ( is_base_of_v<input_iterator_tag, category> ) {
            iterator += distance;
        } else {
            while( distance > 0 ) {
                --distance;
                ++iterator;
            }
            if constexpr ( is_base_of_v<bidirectional_iterator_tag, category > ) {
                while( distance < 0 ) {
                    ++distance;
                    --iterator;
                }
            }
        }
    }

    template<input_iterator I>
    constexpr typename iterator_traits<I>::difference_type distance( I first, I last ) {
        if constexpr ( is_base_of_v<random_access_iterator_tag, typename iterator_traits<I>::iterator_category> ) {
            return last - first;
        } else {
            typename iterator_traits<I>::difference_type diff { 0 };
            while( first != last ) {
                ++first; ++diff;
            }
            return diff;
        }
    }

    template<bidirectional_iterator I>
    constexpr I prev( I iterator, typename iterator_traits<I>::difference_type n = 1 ) {
        advance( iterator, -n );
        return iterator;
    }

    template<input_iterator I>
    constexpr I next( I iterator, typename iterator_traits<I>::difference_type n = 1 ) noexcept {
        advance( iterator, n );
        return iterator;
    }

    /* TODO: O(1) trick to convert const_iterator to iterator : https://stackoverflow.com/a/10669041 */
    /* FIXME: In respect to aforementioned O(1) trick, the problem to fix is, that erase( it, it ) internally uses this 'const_iterator_cast()' which 
     * clearly results in infinite recusion. */
    template<typename CONTAINER>
    constexpr inline typename CONTAINER::iterator const_iterator_cast( CONTAINER & container, typename CONTAINER::const_iterator constant_iterator ) {
        typename CONTAINER::iterator it { container.begin() };
        advance( it, distance<typename CONTAINER::const_iterator>( container.cbegin(), constant_iterator ) );
        return it;
    }

}