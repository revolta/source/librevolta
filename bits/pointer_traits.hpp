#pragma once

#include "cstddef.hpp"
#include "type_traits.hpp"
#include "utility.hpp"

namespace ReVolta {

    template<typename T>
    inline constexpr T * addressof( T & argument ) noexcept {
        return __builtin_addressof( argument );
    }

    /* Rvalue overload is deleted to prevent taking the address of const rvalues */
    template<typename T>
    const T * addressof( const T && ) = delete;

    template<typename T, typename U> requires is_convertible<U *, T *>::value
    inline T * pointer_cast( U * ptr ) noexcept {
        return reinterpret_cast<T *>( ptr );
    }

    template<typename T, typename U> requires is_convertible<U *, T *>::value
    inline const T * pointer_cast( const U * ptr ) noexcept {
        return reinterpret_cast<const T *>( ptr );
    }

    template<typename T>
    inline const T * pointer_cast( const uintptr_t raw_ptr ) noexcept {
        return reinterpret_cast<const T *>( raw_ptr );
    }

    namespace Detail {

        struct pointer_defined : fallback {};

        template<typename P>
        auto has_element_type( pointer_defined ) noexcept -> typename P::element_type;

        template<typename P>
        auto has_element_type( fallback ) noexcept -> typename get_first_argument<P>::type;

        template<typename P>
        auto has_difference_type( pointer_defined ) noexcept -> typename P::difference_type;

        template<typename P>
        auto has_difference_type( fallback ) noexcept -> ptrdiff_t;

        template<typename P>
        auto has_raw_type( pointer_defined ) noexcept -> typename P::raw_type;

        template<typename P>
        auto has_raw_type( fallback ) noexcept -> uintptr_t;

        template<typename P>
        auto has_is_tagged( pointer_defined ) noexcept -> typename P::is_tagged;

        template<typename P>
        auto has_is_tagged( fallback ) noexcept -> false_type;

        template<typename P>
        auto has_is_physical( pointer_defined ) noexcept -> typename P::is_physical;

        template<typename P>
        auto has_is_physical( fallback ) noexcept -> false_type;
    }

    /* TODO: Shall 'P' be constrained by some 'smart pointer concept' */
    template<typename P>
    struct pointer_traits {
    private:
        template<typename T, typename OTHER, typename = void>
        struct rebinder : replace_first_argument<T, OTHER> {};

        template<typename T, typename OTHER>
        struct rebinder<T, OTHER, void_t<typename T::template rebind<OTHER>>> {
            using type = typename T::template rebind<OTHER>;
        };

    public:
        using pointer = P;

        using element_type = decltype( Detail::has_element_type<P>( Detail::pointer_defined {} ) );

        using difference_type = decltype( Detail::has_difference_type<P>( Detail::pointer_defined {} ) );

        using raw_type = decltype( Detail::has_raw_type<P>( Detail::pointer_defined {} ) );

        using is_tagged = decltype( Detail::has_is_tagged<P>( Detail::pointer_defined {} ) );

        using is_physical = decltype( Detail::has_is_physical<P>( Detail::pointer_defined {} ) );

        template<typename OTHER>
        using rebind = typename rebinder<P, OTHER>::type;

#if false
        /* FIXME: Causes compilation error once there is a smart pointer to void used somewhere -> forming reference to void */
        static pointer pointer_to( element_type & r ) {
            /* TODO */
        }
#endif        
    };

    /* Raw pointer types specialization */
    template<typename T>
    struct pointer_traits<T *> {
        using pointer = T *;

        using element_type = T;

        using difference_type = ptrdiff_t;

        using raw_type = uintptr_t;

        using is_tagged = false_type;

        using is_physical = false_type;

        template<typename U>
        using rebind = U *;

        static constexpr pointer pointer_to( element_type & r ) noexcept {
            return addressof( r );
        }
    };

    template<typename T>
	constexpr T * to_address( T * ptr ) noexcept {
#if false
		static_assert( !is_function<T>::value );
#endif
		return ptr;
	}

	template<typename P>
	constexpr auto to_address( const P & ref ) noexcept {
		if constexpr ( requires { pointer_traits<P>::to_address( ref ); } ) {
			return pointer_traits<P>::to_address( ref );
		} else {
			return to_address( ref.operator->() );
		}
	}

}