#pragma once

#include "named_type.hpp"

namespace ReVolta::Kernel {

    /* Type used to identify the PAGE TABLE within the page directory */
    using page_directory_index = named_type<size_t, struct page_directory_index_tag>;
    /* Type used to identify the FRAME within page table */
    using page_table_index = named_type<size_t, struct page_table_index_tag>;
    /* Type used to identify the offset within the frame */
    using frame_offset = named_type<size_t, struct frame_offset_tag>;

    template<typename SMART_POINTER>
    class paging : public SMART_POINTER {
    public:
        using SMART_POINTER::SMART_POINTER;

        constexpr paging( const SMART_POINTER & other ) noexcept : SMART_POINTER( other ) {}

        constexpr paging( SMART_POINTER && other ) noexcept : SMART_POINTER( move( other ) ) {}

        /* Get the offset within the frame */
        constexpr frame_offset get_offset( void ) const noexcept {
            return decoder{ this->raw() }.m_indices.offset & pointer_mask;
        }

        /* Get the index to FRAME within the page table which is by itself identified by page directory index */
        constexpr page_table_index get_page_table_index( void ) const noexcept {
            return page_table_index{ decoder{ this->raw() }.m_indices.page_table_index };
        }

        /* Get the index to PAGE TABLE within the page directory */
        constexpr page_directory_index get_page_directory_index( void ) const noexcept {
            return page_directory_index{ decoder{ this->raw() }.m_indices.page_directory_index };
        }

    private:
        constexpr static size_t pointer_mask = ~( alignof( typename pointer_traits<SMART_POINTER>::element_type ) - 1 );

        union decoder {
            decoder( const typename pointer_traits<SMART_POINTER>::raw_type raw ) noexcept : m_raw( raw ) {}

            /* FIXME: This implementation is purely 32bit implementation, find a way to make it compatible with 64bit as well.
             * See: https://www.iaik.tugraz.at/teaching/materials/os/tutorials/paging-on-intel-x86-64/ */
            struct indices {
                size_t offset : 12;
                size_t page_table_index : 10;
                size_t page_directory_index : 10;
            };

            indices m_indices;
            typename pointer_traits<SMART_POINTER>::raw_type m_raw;

            static_assert( sizeof( indices ) == sizeof( typename pointer_traits<SMART_POINTER>::raw_type ) );
        };
    };

}