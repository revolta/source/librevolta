#pragma once

#include "bits/move.hpp"

namespace ReVolta {

    struct input_iterator_tag {};

    struct output_iterator_tag {};

    /* Forward iterators support a superset of input iterator operations */
    struct forward_iterator_tag : public input_iterator_tag {};

    /* Bidirectional iterators support a superset of forward iterator operations */
    struct bidirectional_iterator_tag : public forward_iterator_tag {};

    /* Random access iterators support a superset of bidirectional iterator operations */
    struct random_access_iterator_tag : public bidirectional_iterator_tag {};

    /* Contiguous iterators point to objects stored contiguously in memory */
    struct contiguous_iterator_tag : public random_access_iterator_tag {};

    namespace Detail {

        template<typename T>
        using with_reference_t = T &;

        template<typename T>
        concept CanReference = requires { typename with_reference_t<T>; };

        template<typename T>
        concept Dereferenceable = requires( T & t ) {
            { *t } -> CanReference;
        };

    }

    template<Detail::Dereferenceable T>
    using iter_reference_t = decltype( *declval<T &>() );

    /* incrementable_traits */
    template<typename T>
    struct incrementable_traits {};

    template<typename T>
        requires ( is_object_v<T> )
    struct incrementable_traits<T *> {
        using difference_type = ptrdiff_t;
    };

    template<typename T>
        requires requires { typename T::difference_type; }
    struct incrementable_traits<T> {
        using difference_type = typename T::difference_type;
    };

    template<typename T>
        requires ( 
            !requires { typename T::difference_type; } && 
            requires ( const T & a, const T & b ) {
                { a - b } -> integral;
            }
        )
    struct incrementable_traits<T> {
        /* FIXME: Enable correct implementation once 'make_signed_t' is available */
#if false        
        using difference_type =  make_signed_t<decltype( declval<T>() - declval<T>() )>;
#else
        using difference_type = ptrdiff_t;
#endif
    };

    template<typename I>
    struct incrementable_traits<const I> : incrementable_traits<I> {};

    /* indirectly_readable_traits */
    template<typename>
    struct indirectly_readable_traits {};

    template<typename T>
        requires ( is_object_v<T> )
    struct indirectly_readable_traits<T *> {
        using value_type = remove_cv_t<T>;
    };

    template<typename I>
        requires ( is_array_v<I> )
    struct indirectly_readable_traits<I> {
        using value_type = remove_cv_t<remove_extent_t<I>>;
    };

    template<typename I>
    struct indirectly_readable_traits<const I> : indirectly_readable_traits<I> {};

    template<typename T>
        requires ( requires { typename T::value_type; } )
    struct indirectly_readable_traits<T> {
        using value_type = typename T::value_type;
    };

    template<typename T>
        requires ( requires { typename T::element_type; } )
    struct indirectly_readable_traits<T> {
        using value_type = typename T::element_type;
    };

    /* iterator_traits */
    namespace Detail {  

        template<typename T>
        concept Referenceable = !is_void<T>::value;

        template<typename I>
        concept LegacyIterator = 
            requires( I iterator ) {
                {  *iterator   } -> Referenceable;
                { ++iterator   } -> same_as<I &>;
                {  *iterator++ } -> Referenceable;
            }
            && copyable<I>
        ;

        template<typename I>
        concept LegacyInputIterator = 
            LegacyIterator<I> && 
            equality_comparable<I> && 
            requires( I iterator ) {	
                typename incrementable_traits<I>::difference_type;                
                typename indirectly_readable_traits<I>::value_type;
                typename common_reference_t<iter_reference_t<I> &&, typename indirectly_readable_traits<I>::value_type &>;
                *iterator++;	
                typename common_reference_t<decltype( *iterator++ ) &&, typename indirectly_readable_traits<I>::value_type &>;
                requires signed_integral<typename incrementable_traits<I>::difference_type>;
            }
        ;

        template<typename I>
        concept LegacyForwardIterator = 
            LegacyInputIterator<I> &&
            constructible_from<I> &&
            is_lvalue_reference_v<iter_reference_t<I>> &&
            same_as<remove_cv_t<iter_reference_t<I>>, typename indirectly_readable_traits<I>::value_type> &&
            requires( I iterator ) {
                {  iterator++ } -> convertible_to<const I &>;
                { *iterator++ } -> same_as<iter_reference_t<I>>;
            }
        ;

        template<typename I>
        concept LegacyBidirectionalIterator = 
            LegacyForwardIterator<I> &&
            requires( I iterator ) {
                { --iterator   } -> same_as<I &>;
                {   iterator-- } -> convertible_to<const I &>;
                {  *iterator-- } -> same_as<iter_reference_t<I>>;
            }
        ;

        template<typename I>
        concept LegacyRandomAccessIterator =
            LegacyBidirectionalIterator<I> &&
            totally_ordered<I> &&
            requires( I iterator, typename incrementable_traits<I>::difference_type n ) {
                { iterator += n } -> same_as<I &>;
                { iterator -= n } -> same_as<I &>;
                { iterator +  n } -> same_as<I>;
                { n +  iterator } -> same_as<I>;
                { iterator -  n } -> same_as<I>;
                { iterator - iterator } -> same_as<decltype( n )>;
                { iterator[ n ] } -> convertible_to<iter_reference_t<I>>;
            }
        ;

        template<typename I>
        concept IteratorWithNestedTypes =
            requires {
                typename I::difference_type;
                typename I::value_type;
                typename I::reference;
                typename I::iterator_category;
            }
        ;

        template<typename I>
        concept IteratorWihoutNestedTypes = !IteratorWithNestedTypes<I>;

        template<typename I>
        concept IteratorWithoutCategory = !requires { typename I::iterator_category; };

    }

    template<typename I>
    struct iterator_traits;

    template<typename I>
        requires ( Detail::IteratorWithNestedTypes<I> )
    struct iterator_traits<I> {
    private:
        template<typename ITERATOR>
        struct pointer_detector {
            using type = void;
        };

        template<typename ITERATOR> requires requires { typename ITERATOR::pointer; }
        struct pointer_detector<ITERATOR> {
            using type = typename I::pointer;
        };

    public:
        using difference_type = typename I::difference_type;
        using value_type = typename I::value_type;
        using pointer = typename pointer_detector<I>::type;
        using reference = typename I::reference;
        using iterator_category = typename I::iterator_category;
    };

    template<typename I> 
        requires ( Detail::LegacyInputIterator<I> && Detail::IteratorWihoutNestedTypes<I> )
    struct iterator_traits<I> {
    private:
        template<typename ITERATOR>
        struct pointer_detector {
            using type = void;
        };

        template<typename ITERATOR>
            requires requires { typename ITERATOR::pointer; }
        struct pointer_detector<ITERATOR> {
            using type = typename ITERATOR::pointer;
        };

        template<typename ITERATOR>
            requires (
                !requires { typename ITERATOR::pointer; } &&
                requires( ITERATOR & iterator ) {
                    iterator.operator->();
                }
            )
        struct pointer_detector<ITERATOR> {
            using type = decltype( declval<ITERATOR &>().operator->() );
        };

        template<typename ITERATOR>
        struct reference_detector {
            using type = iter_reference_t<ITERATOR>;
        };

        template<typename ITERATOR>
            requires ( requires { typename ITERATOR::reference; } )
        struct reference_detector<ITERATOR> {
            using type = typename ITERATOR::reference;
        };

        template<typename ITERATOR>
        struct iterator_category_detector {
            using type = input_iterator_tag;
        };

        template<typename ITERATOR>
            requires ( requires { typename ITERATOR::iterator_category; } )
        struct iterator_category_detector<ITERATOR> {
            using type = typename ITERATOR::iterator_category;
        };

        template<typename ITERATOR>
            requires ( Detail::IteratorWithoutCategory<ITERATOR> && Detail::LegacyRandomAccessIterator<ITERATOR> )
        struct iterator_category_detector<ITERATOR> {
            using type = random_access_iterator_tag;
        };

        template<typename ITERATOR>
            requires ( Detail::IteratorWithoutCategory<ITERATOR> && Detail::LegacyBidirectionalIterator<ITERATOR> )
        struct iterator_category_detector<ITERATOR> {
            using type = bidirectional_iterator_tag;
        };

        template<typename ITERATOR>
            requires ( Detail::IteratorWithoutCategory<ITERATOR> && Detail::LegacyForwardIterator<ITERATOR> )
        struct iterator_category_detector<ITERATOR> {
            using type = forward_iterator_tag;
        };

    public:
        using difference_type = incrementable_traits<I>::difference_type;
        using value_type = indirectly_readable_traits<I>::value_type;
        using pointer = typename pointer_detector<I>::type;
        using reference = typename reference_detector<I>::type;
        using iterator_category = typename iterator_category_detector<I>::type;
    };

    template<typename I>
        requires ( Detail::LegacyIterator<I> && Detail::IteratorWihoutNestedTypes<I> )
    struct iterator_traits<I> {
    private:
        template<typename ITERATOR>
        struct difference_type_detector {
            using type = void;
        };

        template<typename ITERATOR>
            requires requires { typename incrementable_traits<ITERATOR>::difference_type; }
        struct difference_type_detector<ITERATOR> {
            using type = typename incrementable_traits<ITERATOR>::difference_type;
        };

    public:
        using difference_type = difference_type_detector<I>::type;
        using value_type = void;
        using pointer = void;
        using reference = void;
        using iterator_category = output_iterator_tag;
    };

     /* 'iterator_traits' may be specialized for user-provided types that may be used as iterators. 
     * The standard library provides partial specializations for pointer types 'T *', which makes it possible 
     * to use all iterator-based algorithms with raw pointers.  */
    template<typename T> 
        requires ( is_object_v<T> )
    struct iterator_traits<T *> {
        using difference_type = ptrdiff_t;
        using value_type = remove_cv_t<T>;
        using pointer = T *;
        using reference = T &;
        using iterator_category = random_access_iterator_tag;
        using iterator_concept = contiguous_iterator_tag;
    };

    /* FIXME: If iterator_traits<remove_cvref_t<T>> is not specialized,
     * then iter_value_t<T> is indirectly_readable_traits<remove_cvref_t<T>>::value_type. 
     * Otherwise, it is iterator_traits<remove_cvref_t<T>>::value_type */
    template<typename T>
    using iter_value_t = iterator_traits<remove_cvref_t<T>>::value_type;

    /* FIXME: If std::iterator_traits<std::remove_cvref_t<T>> is not specialized,
     * then std::iter_difference_t<T> is std::incrementable_traits<std::remove_cvref_t<T>>::difference_type. 
     * Otherwise, it is std::iterator_traits<std::remove_cvref_t<T>>::difference_type */
    template<typename T>
    using iter_difference_t = iterator_traits<remove_cvref_t<T>>::difference_type;

    namespace Detail {

        template<typename T>
        using with_ref = T &;

        template<typename T>
        concept can_reference = requires { typename with_ref<T>; };

    }

    namespace ranges {

        namespace cust_imove {

            void iter_move();

            template<typename T>
            concept adl_imove = 
                ( is_class_v<remove_reference_t<T>> || is_union_v<remove_reference_t<T>> || is_enum_v<remove_reference_t<T>> ) &&
                requires( T && t ) {
                    iter_move( static_cast<T &&>( t ) );
                }
            ;

            struct imove {
            private:
                template<typename T>
                struct result {
                    using type = iter_reference_t<T>;
                };

                template<typename T>
                    requires adl_imove<T>
                struct result<T> {
                    using type = decltype( iter_move( declval<T>() ) );
                };

                template<typename T>
                    requires ( !adl_imove<T> ) && is_lvalue_reference_v<iter_reference_t<T>>
                struct result<T> {
                    using type = remove_reference_t<iter_reference_t<T>> &&;
                };

                template<typename T>
                static constexpr bool s_noexcept( void ) {
                    if constexpr ( adl_imove<T> ) {
                        return noexcept( iter_move( declval<T>() ) );
                    } else {
                        return noexcept( *declval<T>() );
                    }
                }
            
            public:
                template<Detail::Dereferenceable T>
                using type = typename result<T>::type;

                template<Detail::Dereferenceable T>
                constexpr type<T> operator()( T && e ) const noexcept( s_noexcept<T>() ) {
                    if constexpr ( adl_imove<T> ) {
                        return iter_move( static_cast<T &&>( e ) );
                    } else if constexpr ( is_lvalue_reference_v<iter_reference_t<T>> ) {
                        return static_cast<type<T>>( *e );
                    } else {
                        return *e;
                    }
                }
            };
        }

        inline namespace customization {

            inline constexpr cust_imove::imove iter_move{};

        }

    }

    template<Detail::Dereferenceable T>
        requires Detail::can_reference<ranges::cust_imove::imove::type<T &>>
    using iter_rvalue_reference_t = ranges::cust_imove::imove::type<T &>;

}