#pragma once

#include "move.hpp"

namespace ReVolta {

    /* TODO: Current implementation does not support time related functions */
    template<typename MUTEX>
    class unique_lock {
    public:
        using mutex_type = MUTEX;

    private:
        mutex_type * m_device;
        bool m_owns;

    public:
        unique_lock( void ) noexcept : m_device( 0 ), m_owns( false ) {}

        /* Locks the associated mutex by calling m.lock() */
        explicit unique_lock( mutex_type & m ) : m_device( addressof( m ) ), m_owns( false ) {
            lock();
            m_owns = true;
        }

        /* Does not lock the associated mutex */
        unique_lock( mutex_type & m, defer_lock_t ) noexcept : m_device( addressof( m ) ), m_owns( false ) {}

        /* Tries to lock the associated mutex without blocking by calling m.try_lock() */
        unique_lock( mutex_type & m, try_to_lock_t ) noexcept : m_device( addressof( m ) ), m_owns( m_device->try_lock() ) {}

        /* Assumes the calling thread already holds an non-shared lock */
        unique_lock( mutex_type & m, adopt_lock_t ) noexcept : m_device( addressof( m ) ), m_owns( true ) {}

        unique_lock( const unique_lock &  ) = delete;

        unique_lock & operator=( const unique_lock & ) = delete;

        unique_lock( unique_lock && other ) noexcept : m_device( other.m_device ), m_owns( other.m_owns ) {
            other.m_device = nullptr;
            other.m_owns = false;
        }

        unique_lock & operator=( unique_lock && other ) noexcept {
            if( m_owns ) unlock();
            unique_lock{ move( other ) }.swap( *this );
            other.m_device = nullptr;
            other.m_owns = false;
            return *this;
        }

        ~unique_lock( void ) {
            if( m_owns ) unlock();
        }

        void lock( void ) {
            if( !m_device ) { /* TODO: throw system error "operation not permitted" */ }
            else if( m_owns ) { /* TODO: throw system error "resource deadlock would occur" */ }
            else {
                m_device->lock();
                m_owns = true;
            }
        }

        bool try_lock( void ) {
            if( !m_device ) {
                /* TODO: throw system error "operation not permitted" */
                return false;
            } else if( m_owns ) { 
                /* TODO: throw system error "resource deadlock would occur" */
                return false;
            }
            else {
                m_owns = m_device->try_lock();
                return m_owns;
            }
        }

        void unlock( void ) {
            if( !m_owns ) { /* TODO: throw system error "operation not permitted" */ }
            else if( m_device ) {
                m_device->unlock();
                m_owns = false;
            }
        }

        void swap( unique_lock & other ) noexcept {
            using ReVolta::swap;
            swap( m_device, other.m_device );
            swap( m_owns, other.m_owns );
        }

        mutex_type * release( void ) noexcept {
            mutex_type * retval = m_device;
            m_device = nullptr;
            m_owns = false;
            return retval;
        }

        bool owns_lock( void ) const noexcept {
            return m_owns;
        }

        operator bool( void ) const noexcept {
            return owns_lock();
        }

        mutex_type * mutex( void ) const noexcept {
            return m_device;
        }
    };

    template<typename MUTEX>
    inline void swap( unique_lock<MUTEX> & x, unique_lock<MUTEX> & y ) noexcept {
        x.swap( y );
    }

}