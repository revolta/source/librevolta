#pragma once

#include "iterator.hpp"

namespace ReVolta {

    /* See: https://www.fluentcpp.com/2018/03/13/heaps-priority-queues-stl-part-1/ */

    template<random_access_iterator ITERATOR>
    constexpr bool is_heap( ITERATOR first, ITERATOR last ) {
        /* TODO */
        return true;
    }

    template<random_access_iterator ITERATOR, typename COMPARE>
    constexpr bool is_heap( ITERATOR first, ITERATOR last, COMPARE compare ) {
        /* TODO */
        return true;
    }

    template<random_access_iterator ITERATOR>
    constexpr void make_heap( ITERATOR first, ITERATOR last ) {
        /* TODO */
    }

    template<random_access_iterator ITERATOR, typename COMPARE>
    constexpr void make_heap( ITERATOR first, ITERATOR last, COMPARE compare ) {
        /* TODO */
    }

    template<random_access_iterator ITERATOR>
    constexpr inline void push_heap( ITERATOR first, ITERATOR last ) {
        /* TODO */
    }

    template<random_access_iterator ITERATOR, typename COMPARE>
    constexpr inline void push_heap( ITERATOR first, ITERATOR last, COMPARE compare ) {
        /* TODO */
    }

    template<random_access_iterator ITERATOR>
    constexpr inline void pop_heap( ITERATOR first, ITERATOR last ) {
        /* TODO */
    }

    template<random_access_iterator ITERATOR, typename COMPARE>
    constexpr inline void pop_heap( ITERATOR first, ITERATOR last, COMPARE compare ) {
        /* TODO */
    }
}