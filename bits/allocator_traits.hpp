#pragma once

#include "cstddef.hpp"
#include "construct.hpp"
#include "concepts.hpp"
#include "limits.hpp"

namespace ReVolta {

    namespace Detail {

        template<typename ALLOCATOR, typename T, typename... ARGUMENTS>
        concept has_construct = 
            requires( const ALLOCATOR & allocator, T * ptr, ARGUMENTS &&... arguments ) { 
                allocator.construct( ptr, forward<ARGUMENTS>( arguments )... ); 
            } 
        ;

        template<typename ALLOCATOR, typename SIZE_TYPE>
        concept has_allocate_hint =
            requires( const ALLOCATOR & allocator, SIZE_TYPE n, const void * hint ) {
                allocator.allocate( n, hint );
            }
        ;

        template<typename ALLOCATOR, typename T>
        concept has_destroy = 
            requires( const ALLOCATOR & allocator, T * ptr ) { 
                allocator.destroy( ptr ); 
            } 
        ;

        template<typename ALLOCATOR>
        concept has_max_size = 
            requires( const ALLOCATOR & allocator ) { 
                { allocator.max_size() } -> integral;
            } 
        ;

        template<typename ALLOCATOR>
        concept has_select_on_container_copy_construction = 
            requires( const ALLOCATOR & allocator ) {
                { allocator.select_on_container_copy_construction() } -> Allocator;
            }
        ;
    }   

    /* The allocator_traits class template provides the standardized way to access various properties of Allocators.
     * The standard containers and other standard library components access allocators through this template, 
     * which makes it possible to use any class type as an allocator, as long as the user-provided specialization 
     * of allocator_traits implements all required functionality. */
    template<typename ALLOCATOR>
    class allocator_traits {
        /* 'pointer' detector */
        template<typename ALLOC>
        struct pointer_detector {
            using type = typename ALLOC::value_type *;
        };

        template<typename ALLOC>
            requires ( requires { typename ALLOC::pointer; } )
        struct pointer_detector<ALLOC> {
            using type = typename ALLOC::pointer;
        };

        /* 'const_pointer' detector */
        template<typename ALLOC>
        struct const_pointer_detector {
            using type = typename pointer_traits<typename ALLOC::pointer>::rebind<const typename ALLOC::value_type>;
        };

        template<typename ALLOC>
            requires ( requires { typename ALLOC::const_pointer; } )
        struct const_pointer_detector<ALLOC> {
            using type = typename ALLOC::const_pointer;
        };

        /* 'void_pointer' detector */
        template<typename ALLOC>
        struct void_pointer_detector {
            using type = typename pointer_traits<typename ALLOC::pointer>::rebind<void>;
        };

        template<typename ALLOC>
            requires ( requires { typename ALLOC::void_pointer; } )
        struct void_pointer_detector<ALLOC> {
            using type = typename ALLOC::void_pointer;
        };

        /* 'const_void_pointer' detector */
        template<typename ALLOC>
        struct const_void_pointer_detector {
            using type = typename pointer_traits<typename ALLOC::pointer>::rebind<const void>;
        };

        template<typename ALLOC>
            requires ( requires { typename ALLOC::const_void_pointer; } )
        struct const_void_pointer_detector<ALLOC> {
            using type = typename ALLOC::const_void_pointer;
        };

        /* 'difference_type' detector */
        template<typename ALLOC>
        struct difference_type_detector {
            using type = typename pointer_traits<typename ALLOC::pointer>::difference_type;
        };

        template<typename ALLOC>
            requires ( requires { typename ALLOC::difference_type; } )
        struct difference_type_detector<ALLOC> {
            using type = typename ALLOC::difference_type;
        };

        /* 'size_type' detector */
        template<typename ALLOC>
        struct size_type_detector {
            using type = make_unsigned<typename difference_type_detector<ALLOC>::type>::type;
        };

        template<typename ALLOC>
            requires ( requires { typename ALLOC::size_type; } )
        struct size_type_detector<ALLOC> {
            using type = typename ALLOC::size_type;
        };

        /* 'propagate_on_container_copy_assignment' detector */
        template<typename ALLOC>
        struct propagate_on_container_copy_assignment_detector {
            using type = false_type;
        };

        template<typename ALLOC>
            requires ( requires { typename ALLOC::propagate_on_container_copy_assignment; } )
        struct propagate_on_container_copy_assignment_detector<ALLOC> {
            using type = typename ALLOC::propagate_on_container_copy_assignment;
        };

        /* 'propagate_on_container_move_assignment' detector */
        template<typename ALLOC>
        struct propagate_on_container_move_assignment_detector {
            using type = false_type;
        };

        template<typename ALLOC>
            requires ( requires { typename ALLOC::propagate_on_container_move_assignment; } )
        struct propagate_on_container_move_assignment_detector<ALLOC> {
            using type = typename ALLOC::propagate_on_container_move_assignment;
        };

        /* 'propagate_on_container_swap' detector */
        template<typename ALLOC>
        struct propagate_on_container_swap_detector {
            using type = false_type;
        };

        template<typename ALLOC>
            requires ( requires { typename ALLOC::propagate_on_container_swap; } )
        struct propagate_on_container_swap_detector<ALLOC> {
            using type = typename ALLOC::propagate_on_container_swap;
        };

        /* 'is_always_equal' detector */
        template<typename ALLOC>
        struct is_always_equal_detector {
            using type = typename is_empty<ALLOC>::type;
        };

        template<typename ALLOC>
            requires ( requires { typename ALLOC::is_always_equal; } )
        struct is_always_equal_detector<ALLOC> {
            using type = typename ALLOC::is_always_equal;
        };

        /* allocator rebind */
        template<typename ALLOC, typename>
        struct allocator_rebinder {
            using other = ALLOCATOR;
        };

        template<template<typename, typename...> class ALLOC, typename U, typename... ARGUMENTS, typename T>
        struct allocator_rebinder<ALLOC<U, ARGUMENTS...>, T> {
            using other = ALLOC<T, ARGUMENTS...>;
        };

        template<typename ALLOC, typename T>
        struct rebind_alloc_detector {
            using type = allocator_rebinder<ALLOCATOR, T>;
        };

        template<typename ALLOC, typename T>
            requires ( requires { typename ALLOC::rebind_alloc<T>; } )
        struct rebind_alloc_detector<ALLOC, T> {
            using type = typename ALLOC::rebind_alloc<T>;
        };

    public:
        using allocator_type = ALLOCATOR;
        using value_type = typename allocator_type::value_type;

        /* 'ALLOCATOR::pointer' if present, otherwise 'value_type *' */
        using pointer = typename pointer_detector<allocator_type>::type;

        /* 'ALLOCATOR::const_pointer' if present, otherwise 'pointer_traits<pointer>::rebind<const value_type>' */
        using const_pointer = typename const_pointer_detector<allocator_type>::type;

        /* 'ALLOCATOR::void_pointer' if present, otherwise 'pointer_traits<pointer>::rebind<void>' */
        using void_pointer = typename void_pointer_detector<allocator_type>::type;

        /* 'ALLOCATOR::const_void_pointer' if present, otherwise 'pointer_traits<pointer>::rebind<const void>' */
        using const_void_pointer = typename const_void_pointer_detector<allocator_type>::type;

        /* 'ALLOCATOR::difference_type' if present, otherwise 'pointer_traits<pointer>::difference_type' */
        using difference_type = typename difference_type_detector<allocator_type>::type;

        /* 'ALLOCATOR::size_type' if present otherwise 'make_unsigned<difference_type>::type' */
        using size_type = typename size_type_detector<allocator_type>::type;

        /* 'ALLOCATOR::propagate_on_container_copy_assignment' if present, otherwise 'false_type' */
        using propagate_on_container_copy_assignment = typename propagate_on_container_copy_assignment_detector<allocator_type>::type;

        /* 'ALLOCATOR::propagate_on_container_move_assignment' if present, otherwise 'false_type' */
        using propagate_on_container_move_assignment = typename propagate_on_container_move_assignment_detector<allocator_type>::type;

        /* 'ALLOCATOR::propagate_on_container_swap' if present, otherwise 'false_type' */
        using propagate_on_container_swap = typename propagate_on_container_swap_detector<allocator_type>::type;

        /* 'ALLOCATOR::is_always_equal' if present, otherwise 'is_empty<ALLOCATOR>::type (results in 'true_type' or 'false_type')' */
        using is_always_equal = typename is_always_equal_detector<allocator_type>::type;

        /* 'ALLOCATOR::rebind<OTHER>::other' if present, otherwise 'ALLOCATOR<OTHER, ARGUMENTS>' if this 'ALLOCATOR' is 'ALLOCATOR<U, ARGUMENTS>' */
        template<typename OTHER>
        using rebind_alloc = typename rebind_alloc_detector<ALLOCATOR, OTHER>::type;

        /* Alias to 'allocator_traits<rebind_alloc<OTHER>>' */
        template<typename OTHER>
        using rebind_traits = allocator_traits<rebind_alloc<OTHER>>;

        /* Uses the 'allocator' a to allocate 'n * sizeof( ALLOCATOR::value_type )' bytes of uninitialized storage. */
        [[nodiscard]] static constexpr pointer allocate( const allocator_type & allocator, size_type n ) noexcept {
            return allocator.allocate( n );
        }

        
        [[nodiscard]] static constexpr pointer allocate( const allocator_type & allocator, size_type n, const_void_pointer hint ) noexcept 
            requires ( Detail::has_allocate_hint<allocator_type, size_type> ) {
            return allocator.allocate( n, hint );
        }

        [[nodiscard]] static constexpr pointer allocate( const allocator_type & allocator, size_type n, const_void_pointer ) noexcept 
            requires ( !Detail::has_allocate_hint<allocator_type, size_type> ) {
            return allocator.allocate( n );
        }

        /* Deallocates the storage using the allocator */
        static constexpr void deallocate( const allocator_type & allocator, pointer ptr, size_type n ) noexcept {
            allocator.deallocate( ptr, n );
        }

        /* Constructs an object in the allocated storage */
        template<typename T, typename... ARGUMENTS>
            requires ( Detail::has_construct<allocator_type, T, ARGUMENTS...> )
        static constexpr void construct( const allocator_type & allocator, T * ptr, ARGUMENTS &&... arguments ) {
            allocator.construct( ptr, forward<ARGUMENTS>( arguments )... );
        }

        /* Constructs an object in the allocated storage */
        template<typename T, typename... ARGUMENTS>
            requires ( !Detail::has_construct<allocator_type, T, ARGUMENTS...> )
        static constexpr void construct( const allocator_type &, T * ptr, ARGUMENTS &&... arguments ) {
            construct_at( ptr, forward<ARGUMENTS>( arguments )... );
        }
   
        /* Destructs an object stored in the allocated storage */
        template<typename T>
            requires ( Detail::has_destroy<allocator_type, T> )
        static constexpr void destroy( const allocator_type & allocator, T * ptr ) {
            allocator.destroy( ptr );
        }

        /* Destructs an object stored in the allocated storage */
        template<typename T>
            requires ( !Detail::has_destroy<allocator_type, T> )
        static constexpr void destroy( const allocator_type &, T * ptr ) {
            destroy_at( ptr );
        }
     
        /* Returns the maximum object size supported by the allocator */
        static constexpr size_type max_size( const allocator_type & allocator ) noexcept 
            requires ( Detail::has_max_size<allocator_type> ) {
            return allocator.max_size();
        }

        /* Returns the maximum object size supported by the allocator */
        static constexpr size_type max_size( const allocator_type & allocator ) noexcept 
            requires ( !Detail::has_max_size<ALLOCATOR> ) {
            return numeric_limits<size_type>::max();
        }

        /* Obtains the allocator to use after copying a standard container */
        static constexpr allocator_type select_on_container_copy_construction( const allocator_type & allocator )
            requires ( Detail::has_select_on_container_copy_construction<allocator_type> ) {
            return allocator.select_on_container_copy_construction();
        }

        /* Obtains the allocator to use after copying a standard container */
        static constexpr allocator_type select_on_container_copy_construction( const allocator_type & allocator )
            requires ( !Detail::has_select_on_container_copy_construction<allocator_type> ) {
            return allocator;
        }  
    };

}