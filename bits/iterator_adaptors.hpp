#pragma once

#include "bits/iterator_concepts.hpp"
#include "memory.hpp"
#include "concepts.hpp"

namespace ReVolta {

    template<bidirectional_iterator I>
    class reverse_iterator {
    
        template<typename IT>
        struct iterator_concept_detector {
            using type = bidirectional_iterator_tag;
        };

        template<typename IT>
            requires ( random_access_iterator<IT> )
        struct iterator_concept_detector<IT> {
            using type = random_access_iterator_tag;
        };

        template<typename IT>
        struct iterator_category_detector {
            using type = iterator_traits<IT>::iterator_category;
        };

        template<typename IT>
            requires ( derived_from<typename iterator_traits<IT>::iterator_category, random_access_iterator_tag> )
        struct iterator_category_detector<IT> {
            using type = random_access_iterator_tag;
        };

    public:
        using iterator_type = I;
        using iterator_concept = typename iterator_concept_detector<iterator_type>::type;
        using iterator_category = typename iterator_category_detector<iterator_type>::type;
        using value_type = iter_value_t<iterator_type>;
        using difference_type = iter_difference_t<iterator_type>;
        using pointer = iterator_traits<iterator_type>::pointer;
        using reference = iter_reference_t<iterator_type>;

    private:
        template<typename T>
        inline constexpr static T * to_pointer( T * ptr ) {
            return ptr;
        }

        template<typename T>
        inline constexpr static pointer to_pointer( T t ) {
            return t.operator->();
        }

    protected:
        /* The underlying iterator of which 'base()' returns a copy */
        iterator_type current;
    
    public:
        /* Default constructor. The underlying iterator is value-initialized. Operations on the resulting iterator have defined behavior 
         * if and only if the corresponding operations on a value-initialized Iter also have defined behavior */
        constexpr reverse_iterator( void ) : current() {}

        /* The underlying iterator is initialized with 'iterator' */
        constexpr explicit reverse_iterator( iterator_type iterator ) : current( iterator ) {}

        /* The underlying iterator is initialized with that of other */
        template<typename U>
            requires ( convertible_to<const U &, iterator_type> ) 
        constexpr reverse_iterator( const reverse_iterator<U> & other ) : current( other.current ) {}

        template<typename U>
        constexpr reverse_iterator & operator=( const reverse_iterator<U> & other ) 
            requires ( !same_as<U, iterator_type> && convertible_to<const U &, iterator_type> && assignable_from<iterator_type &, const U &> )
        {
            current = other.current;
            return *this;
        }

        constexpr iterator_type base( void ) const {
            return current;
        }

        constexpr reference operator*( void ) const {
            iterator_type temp = current;
            return *--temp;
        }

        constexpr pointer operator->( void ) const 
            requires ( is_pointer_v<iterator_type> || requires( const iterator_type i ) { i.operator->(); } )
        {
            iterator_type temp = current;
            --temp;
            return to_pointer( temp );
        }

        constexpr reference operator[]( difference_type n ) const {
            return *( *this + n );
        }

        constexpr reverse_iterator & operator++( void ) {
            --current;
            return *this;
        }

        constexpr reverse_iterator operator++( int ) {
            reverse_iterator temp = *this;
            --current;
            return temp;
        }

        constexpr reverse_iterator & operator--( void ) {
            ++current;
            return *this;
        }

        constexpr reverse_iterator operator--( int ) {
            reverse_iterator temp = *this;
            ++current;
            return temp;
        }

        constexpr reverse_iterator operator+( difference_type n ) const {
            return reverse_iterator { current - n };
        }

        constexpr reverse_iterator & operator+=( difference_type n ) {
            current -= n;
            return *this;
        }

        constexpr reverse_iterator operator-( difference_type n ) const {
            return reverse_iterator { current + n };
        }

        constexpr reverse_iterator & operator-=( difference_type n ) {
            current += n;
            return *this;
        }
    };

    template<typename I1, typename I2>
    inline constexpr bool operator==( const reverse_iterator<I1> & lhs, const reverse_iterator<I2> & rhs ) {
        return lhs.base() == rhs.base();
    }

    template<typename I1, typename I2>
    inline constexpr bool operator!=( const reverse_iterator<I1> & lhs, const reverse_iterator<I2> & rhs ) {
        return lhs.base() != rhs.base();
    }

    template<typename I1, typename I2>
    inline constexpr bool operator<( const reverse_iterator<I1> & lhs, const reverse_iterator<I2> & rhs ) {
        return lhs.base() < rhs.base();
    }

    template<typename I1, typename I2>
    inline constexpr bool operator<=( const reverse_iterator<I1> & lhs, const reverse_iterator<I2> & rhs ) {
        return lhs.base() <= rhs.base();
    }

    template<typename I1, typename I2>
    inline constexpr bool operator>=( const reverse_iterator<I1> & lhs, const reverse_iterator<I2> & rhs ) {
        return lhs.base() >= rhs.base();
    }

    /* TODO: three way comparison operator */

    template<typename I>
    inline constexpr reverse_iterator<I> operator+( typename reverse_iterator<I>::difference_type n, const reverse_iterator<I> & iterator ) {
        return reverse_iterator<I>{ iterator.base() - n };
    }

    template<typename I1, typename I2>
    constexpr auto operator-( const reverse_iterator<I1> & lhs, const reverse_iterator<I2> & rhs ) -> decltype( rhs.base() - lhs.base() ) {
        return rhs.base() - lhs.base();
    }

    template<typename I>
    reverse_iterator<I> make_reverse_iterator( I iterator ) {
        return reverse_iterator<I> { iterator };
    }

    template<typename I1, typename I2>
        requires ( !sized_sentinel_for<I1, I2> )
    inline constexpr bool disable_sized_sentinel_for<reverse_iterator<I1>, reverse_iterator<I2>> { true };

    /* Iterator adaptor which behaves exaclty like the underlying iterator except that dereferencing converts the value returned by the underlying
     * iterator into an rvalue. If this iterator is used as an input iterator, the effect is that the values are moved from rather than copied from. */
    template<input_iterator I>
    class move_iterator {
        template<typename IT>
        struct iterator_category_detector {
            using type = iterator_traits<IT>::iterator_category;
        };

        template<typename IT>
            requires ( derived_from<typename iterator_traits<IT>::iterator_category, random_access_iterator_tag> )
        struct iterator_category_detector<IT> {
            using type = random_access_iterator_tag;
        };

    public:
        using iterator_type = I;
        using iterator_category = typename iterator_category_detector<iterator_type>::type;
        using iterator_concept = input_iterator_tag;
        using value_type = iter_value_t<iterator_type>;
        using difference_type = iter_difference_t<iterator_type>;
        using pointer = iterator_type;
        using reference = iter_rvalue_reference_t<iterator_type>;

    private:
        /* The underlying iterator from which base() copies or moves */
        iterator_type m_current;

    public:
        constexpr move_iterator( void ) : m_current() {}

        constexpr explicit move_iterator( iterator_type iterator ) : m_current( move( iterator ) ) {}

        template<typename U>
            requires ( !same_as<U, iterator_type> && convertible_to<const U &, iterator_type> )
        constexpr move_iterator( const move_iterator<U> & other ) : m_current( other.m_current ) {}

        /* Returns the reference to the underlying iterator */
        constexpr const iterator_type & base( void ) const & noexcept {
            return m_current;
        }

        /* Move constructs the return value from the underlying iterator */
        constexpr iterator_type base( void ) && {
            return move( m_current );
        }

        template<typename U>
            requires ( !same_as<U, I> && convertible_to<const U &, I> && assignable_from<iterator_type &, const U &> )
        constexpr move_iterator & operator=( const move_iterator<U> & other ) {
            m_current = other.m_current;
            return *this;
        }

        constexpr reference operator*( void ) const {
            return ranges::iter_move( base() );
        }

        constexpr reference operator[]( difference_type n ) const {
            return ranges::iter_move( base() + n );
        }

        constexpr move_iterator & operator++( void ) {
            ++m_current;
            return *this;
        }

        constexpr move_iterator & operator--( void ) {
            --m_current;
            return *this;
        }

        constexpr auto operator++( int ) {
            move_iterator temp = *this;
            ++m_current;
            return temp;
        }

        constexpr move_iterator operator--( int ) {
            move_iterator temp = *this;
            --m_current;
            return temp;
        }

        constexpr move_iterator operator+( difference_type n ) const {
            return move_iterator{ m_current + n };
        }

        constexpr move_iterator operator-( difference_type n ) const {
            return move_iterator{ m_current - n };
        }

        constexpr move_iterator & operator+=( difference_type n ) {
            m_current += n;
            return *this;
        }

        constexpr move_iterator & operator-=( difference_type n ) {
            m_current -= n;
            return *this;
        }

        /* TODO: Enable once 'move_sentinel<>' is implemented */
#if false
        template<sentinel_for<iterator_type> S>
        friend constexpr bool operator==( const move_iterator & iterator, const move_sentinel<S> & sentinel ) {
            return iterator.base() == sentinel.base();
        }
#endif

    };

    template<typename I1, typename I2>
    inline constexpr bool operator==( const move_iterator<I1> & lhs, const move_iterator<I2> & rhs ) {
        return lhs.base() == rhs.base();
    }

    template<typename I1, typename I2>
    inline constexpr bool operator<( const move_iterator<I1> & lhs, const move_iterator<I2> & rhs ) {
        return lhs.base() < rhs.base();
    }

    template<typename I1, typename I2>
    inline constexpr bool operator<=( const move_iterator<I1> & lhs, const move_iterator<I2> & rhs ) {
        return !( rhs < lhs );
    }

    template<typename I1, typename I2>
    inline constexpr bool operator>( const move_iterator<I1> & lhs, const move_iterator<I2> & rhs ) {
        return rhs < lhs;
    }

    template<typename I1, typename I2>
    inline constexpr bool operator>=( const move_iterator<I1> & lhs, const move_iterator<I2> & rhs ) {
        return !( lhs < rhs );
    }

#if false
    template<typename I1, threeway_comparable_with<I1> I2>
    inline constexpr compare_threeway_result_t<I1, I2> operator<=>( const move_iterator<I1> & lhs, const move_iterator<I2> & rhs ) {
        return lsh.base() <=> rhs.base();
    }
#endif

    template<typename I>
    inline constexpr move_iterator<I> operator+( typename move_iterator<I>::difference_type n, const move_iterator<I> & iterator ) {
        return iterator + n;
    }

    template<typename I1, typename I2>
    inline constexpr auto operator-( const move_iterator<I1> & lhs, const move_iterator<I2> & rhs ) -> decltype( lhs.base() - rhs.base() ) {
        return lhs.base() - rhs.base();
    }

    /* Convenience function template to construct move_iterator for the given 'iterator' with the type deduced from the type of the argument */
    template<typename I>
    inline constexpr move_iterator<I> make_move_iterator( I iterator ) {
        return move_iterator<I>( move( iterator ) );
    }

    template<typename I>
        /* Once the iterator's 'value_type' is not "nothrow move constructible" AND is "copy constructible", return given iteratory by copy */
        requires ( !is_nothrow_move_constructible_v<typename iterator_traits<I>::value_type> && is_copy_constructible_v<typename iterator_traits<I>::value_type> )
    inline constexpr I make_move_if_noexcept_iterator( I iterator ) {
        return iterator;
    }

    template<typename I>
        /* Once the iterator's 'value_type' is "nothrow move constructible" OR is not "copyble" then return 'move_iterator<I>' */
        requires ( !( !is_nothrow_move_constructible_v<typename iterator_traits<I>::value_type> && is_copy_constructible_v<typename iterator_traits<I>::value_type> ) )
    inline constexpr move_iterator<I> make_move_if_noexcept_iterator( I iterator ) {
        return move_iterator<I>{ move( iterator ) };
    }

    template<typename T>
        requires ( !is_nothrow_move_constructible_v<typename iterator_traits<T>::value_type> && is_copy_constructible_v<typename iterator_traits<T>::value_type> )
    inline constexpr const T * make_move_if_noexcept_iterator( T * ptr ) {
        return ptr;
    }

    template<typename T>
    inline constexpr move_iterator<T *> make_move_if_noexcept_iterator( T * ptr ) {
        return move_iterator<T *>( ptr );
    }

    /* FIXME */
#if false
    template<bidirectional_iterator I, SequenceContainer CONTAINER>
#else
    template<bidirectional_iterator I, typename CONTAINER>
#endif
    class circular_iterator {
    public:
        using iterator_type = I;
        using container_type = CONTAINER;
        using iterator_category = bidirectional_iterator_tag;
        using value_type = iter_value_t<iterator_type>;
        using difference_type = iter_difference_t<iterator_type>;
        using pointer = typename iterator_traits<iterator_type>::pointer;
        using reference = iter_reference_t<iterator_type>;

    private:
        /* The underlying iterator which 'base()' returns by copy */
        iterator_type m_current;
        /* In order to get actual underlying iterator values from begin() and end() it is required to have the m_container coupling */
        observer_ptr<container_type> m_container;

    public:
        /* Constructs circular iterator by default - sets internally stored current iterator to nullptr and well as the pointer to referenced container */
        constexpr circular_iterator( void ) : circular_iterator( nullptr ) {}

        constexpr circular_iterator( nullptr_t ) : m_current( nullptr ), m_container( nullptr ) {}

        constexpr explicit circular_iterator( iterator_type iterator, container_type & container ) : m_current( iterator ), m_container( addressof( container ) ) {}

        /* Converting constructor */
        template<typename U>
            requires ( !same_as<U, iterator_type> && convertible_to<const U &, iterator_type> )
        constexpr circular_iterator( const circular_iterator<U, CONTAINER> & other ) 
            : m_current( other.base() ), m_container( addressof( other.container() ) ) 
        {}

        constexpr circular_iterator( const circular_iterator & other ) : m_current( other.m_current ), m_container( other.m_container ) {}

        constexpr circular_iterator( circular_iterator && other ) : m_current( move( other.m_current ) ), m_container( other.m_container ) {}

        constexpr circular_iterator & operator=( const circular_iterator & other ) {
            circular_iterator( other ).swap( *this );
            return *this;
        }

        constexpr circular_iterator & operator=( circular_iterator && other ) {
            circular_iterator( move( other ) ).swap( *this );
            return *this;
        }

        /* Get underlying non-circular iterator by copy */
        constexpr iterator_type base( void ) const {
            return m_current;
        }

        /* Get the reference to linked container */
        constexpr container_type & container( void ) const {
            return *m_container;
        }

        /* Dereference the circular iterator is equal to dereference the underlying iterator itself */
        reference operator*( void ) {
            return *m_current;
        }

        /* FIXME: Use const_reference once available */
        const reference operator*( void ) const {
            return *m_current;
        }

        pointer operator->( void ) {
            return addressof( *m_current );
        }

        constexpr circular_iterator & operator++( void ) {
            m_current++;
            if( m_current == m_container->end() ) {
                m_current = m_container->begin();
            }
            return *this;
        }

        constexpr circular_iterator operator++( int ) {
            circular_iterator temp { *this };
            m_current++;
            if( m_current == m_container->end() ) {
                m_current = m_container->begin();
            }
            return temp;
        }

        constexpr circular_iterator & operator--( void ) {
            if( m_current == m_container->begin() ) {
                m_current = m_container->end();
            }
            m_current--;
            return *this;
        }

        constexpr circular_iterator operator--( int ) {
            circular_iterator temp { *this };
            if( m_current == m_container->begin() ) {
                m_current = m_container->end();
            }
            m_current--;
            return temp;
        }

        constexpr circular_iterator operator+( difference_type n ) const {
            circular_iterator temp { *this };
            for( difference_type index { 0 }; index < n; index++ ) {
                temp++;
            }
            return temp;
        }

        constexpr circular_iterator & operator+=( difference_type n ) {
            for( difference_type index { 0 }; index < n; index++ ) {
                this->operator++();
            }
            return *this;
        }

        constexpr circular_iterator operator-( difference_type n ) const {
            circular_iterator temp { *this };
            for( difference_type index { 0 }; index < n; index++ ) {
                temp--;
            }
            return temp;
        }

        constexpr circular_iterator & operator-=( difference_type n ) {
            for( difference_type index { 0 }; index < n; index++ ) {
                *this--;
            }
            return *this;
        }

        constexpr void swap( circular_iterator & other ) {
            using ReVolta::swap;
            swap( m_current, other.m_current );
            swap( m_container, other.m_container );
        }
    };

    template<bidirectional_iterator I, typename CONTAINER>
    inline constexpr bool operator==( const circular_iterator<I, CONTAINER> & lhs, const circular_iterator<I, CONTAINER> & rhs ) {
        return lhs.base() == rhs.base();
    }

    template<bidirectional_iterator I, typename CONTAINER>
    inline constexpr bool operator!=( const circular_iterator<I, CONTAINER> & lhs, const circular_iterator<I, CONTAINER> & rhs ) {
        return lhs.base() != rhs.base();
    }

    /* TODO: Add more operators / non/member functions */

}