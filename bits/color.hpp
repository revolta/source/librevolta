#pragma once

#include "named_type.hpp"

namespace ReVolta {

    enum class Color : unsigned char {
		BLACK, BLUE, GREEN, CYAN, RED, PURPLE, BROWN, GRAY,
		DARK_GRAY, LIGHT_BLUE, LIGHT_GREEN, LIGHT_CYAN, LIGHT_RED,
		LIGHT_PURPLE, YELLOW, WHITE
	};

	using ForegroundColor = named_type<Color, struct ForegroundColorTag>;
	using BackgroundColor = named_type<Color, struct BackgroundColorTag>;

}