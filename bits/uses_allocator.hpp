#pragma once

#include "type_traits.hpp"
#include "concepts.hpp"
#include "utility.hpp"

namespace ReVolta {

    /* In generic template expect 'false' type */
    template<typename T, typename ALLOCATOR>
    struct uses_allocator : public integral_constant<bool, false> {};

    /* Specialize for 'T' and 'ALLOCATOR' once the requirements are met */
    template<typename T, typename ALLOCATOR>
        requires (
            Allocator<ALLOCATOR> &&
            requires { typename T::allocator_type; } &&
            ( is_convertible_v<ALLOCATOR, typename T::allocator_type> ||
            same_as<typename T::allocator_type, experimental::erased_type> )
         )  
    struct uses_allocator<T, ALLOCATOR> : public integral_constant<bool, true> {};

    /* If T has a member typedef 'allocator_type' which is convertible from ALLOCATOR, the member constant value is true. Otherwise value is false. */
    template<typename T, Allocator ALLOCATOR>
    inline constexpr bool uses_allocator_v = uses_allocator<T, ALLOCATOR>::value;

}