#pragma once

#include <concepts.hpp>
#include <type_traits.hpp>
#include <atomic.hpp>
#include <bits/unique_ptr.hpp>
#include <bits/observer_ptr.hpp>

namespace ReVolta {

    namespace Detail {

        /* TODO: Who owns the shared_ptr control block? Is it self-owned and destroyed once the reference count reaches zero? */

        /* TODO: Manage atomic reference count within the base as it is common to both control blcok types */

        class shared_ptr_control_block_base {
        public:
            using counter_type = atomic<long>;

        private:
            counter_type m_use_count;
            counter_type m_weak_count;

        public:
            counter_type::value_type use_count( void ) const {
                return static_cast<counter_type::value_type>( this->m_use_count );
            }

            inline void increment_use_count( size_t n = 1 ) {
                this->m_use_count++;
            }

            inline void decrement_use_count( size_t n = 1 ) {
                if( this->m_use_count > 0 ) {
                    this->m_use_count--;
                }
                if( this->m_use_count == 0 ) {
                    /* TODO: Destroy itself - call the deleter stored internally in control block */
                    /* TODO: To know which deleter to call, get inspired in base_ptr<> */
                    /* TODO: Maybe - there might be a control block deleter, and then the owned object deleter whcih are two different things */
#if false

#endif                    
                }
            }

        };

        /* The control block itself is allocated by the allocator which must satisfy the 'Allocator' requirements. When a custom allocator is not provided,
         * the 'ReVolta::allocator' is used that dynamically allocates the control block. The control block keeps the copy of the allocator. */
        template<typename T, Allocator ALLOCATOR, Deleter DELETER = default_deleter<T>>
        class shared_ptr_control_block : public shared_ptr_control_block_base {
        public:
            /* Rebind allocator to work with the control block containing the instance of T rather than the T by itself */
            using allocator_type = typename allocator_traits<ALLOCATOR>::rebind_alloc<shared_ptr_control_block>::other;

        private:
            /* The control block keeps the copy of the allocator used to allocate the control block */
            [[no_unique_address]] allocator_type m_control_block_allocator;
            /* Owning pointer to the managed object. Control block owns it and is responsible for its deletion. The deleter is stored within. */
            unique_ptr<T, DELETER> m_managed_object_ptr;

        public:
            shared_ptr_control_block( void ) = default;

            shared_ptr_control_block( T * ptr, DELETER deleter, const ALLOCATOR & allocator ) noexcept
                : m_control_block_allocator( allocator ), m_managed_object_ptr( ptr, move( deleter ) )
            {}

            /* Constructor used by shared_ptr<T>::shared_ptr( unique_ptr<T, DELETER> && ) directly trasfers object ownership to the control block */
            shared_ptr_control_block( unique_ptr<T, DELETER> && ptr, const ALLOCATOR & allocator ) noexcept 
                : m_control_block_allocator( allocator ), m_managed_object_ptr( move( ptr ) )
            {}

            /* Object is being managed by 'unique_ptr' which ensures the instance is being destroyed and deallocated once
             * the owning control block is being destroyed */
            /* FIXME: Shall the destructor be 'virtual'? */
            ~shared_ptr_control_block( void ) {}
        };

        template<typename T, Allocator ALLOCATOR>
        class shared_ptr_control_block_inplace : public shared_ptr_control_block_base {
        public:
            /* TODO: allocator shall be rebound externally to the control block, the allocator there shall be directly the control block allocator */
            /* Rebind allocator to work with the control block containing the instance of T rather than the T by itself */
            using allocator_type = typename allocator_traits<ALLOCATOR>::rebind_alloc<shared_ptr_control_block_inplace>::other;

        private:
            /* The control block keeps the copy of the allocator used to allocate the control block */
            [[no_unique_address]] allocator_type m_control_block_allocator;
            /* Memory to hold the emplaced instance of T */
            alignas( T ) byte m_storage [ sizeof( T ) ];

        public:
            shared_ptr_control_block_inplace( void ) = default;

            template<typename... ARGUMENTS>
            shared_ptr_control_block_inplace( ALLOCATOR allocator, ARGUMENTS &&... arguments ) : m_control_block_allocator( allocator ) {
                allocator_traits<ALLOCATOR>::construct( allocator, addressof( this->m_storage ), forward<ARGUMENTS>( arguments )... );
            }

            /* FIXME: Shall the destructor be 'virtual'? */
            /* Destroys the instance of 'T' stored within */
            ~shared_ptr_control_block_inplace( void ) noexcept {
                /* FIXME: Shall the deleter defined out of the ALLOCATOR be used here? */
                /* As the control block owns the instance of T stored within, call the T's destructor once the block prior the control block being destroyed */
                allocator_traits<ALLOCATOR>::destroy( ALLOCATOR{}, addressof( this->m_storage ) );
            }

        };

        /* Wrapper class to the observer_ptr holding the reference to the control block (pointer).
         * Shall manage the reference counting in its constructors (move c-tors, copy ctors etc.)
         * Basically shall just forward all the contructor arguments to the control block itself, create one instead of the shared_ptr itself (aka allocate_) */
        class control_block_proxy {
        public:
            using control_block_ptr = observer_ptr<Detail::shared_ptr_control_block_base>;

            using pointer = typename control_block_ptr::pointer;

        private:
            /* Non owning pointer to the control block */
            observer_ptr<Detail::shared_ptr_control_block_base> m_control_block;

            template<Allocator ALLOCATOR, typename... ARGUMENTS>
            inline static observer_ptr<Detail::shared_ptr_control_block_base> allocate_control_block( ALLOCATOR allocator, ARGUMENTS &&... arguments ) {
            /* FIXME: It seems the default constructor of 'shared_ptr_control_block' is required, but why? "return new T;" reported */
            typename ALLOCATOR::pointer control_block { allocator_traits<ALLOCATOR>::allocate( allocator, 1 ) };
            /* Avoid constructing the block on invalid address */
            if( control_block ) {
                /* Construct newly allocated control block - store the pointer to become the owner responsible for deletion, the deleter to be used for such
                 * and a copy of the allocator used to allocate the control block */
                allocator_traits<ALLOCATOR>::construct( allocator, control_block, forward<ARGUMENTS>( arguments )... );
            } else {
#if false
                /* Required additional memory could not be obtained */
                throw bad_alloc;
#endif                
            }
            return make_observer<Detail::shared_ptr_control_block_base>( control_block );
        }

        public:
            control_block_proxy( void ) = delete;

            /* Constructs new control block */
            template<typename... ARGUMENTS>
            control_block_proxy( ARGUMENTS &&... arguments ) : m_control_block( allocate_control_block( forward<ARGUMENTS>( arguments )... ) ) {
                /* Increment the use count */
                this->m_control_block->increment_use_count();
            }

            /* Once the control block proxy is being copied, means one more shared_ptr is about to share the ownership, thus the counter shall be incremented */
            /* TODO: Add tag to identify we're about to increment the weak count */
            control_block_proxy( const control_block_proxy & other ) : m_control_block( other.m_control_block ) {
                /* Increment the use count */
                this->m_control_block->increment_use_count();
            }

            /* Moves the proxy from one shared_ptr to the other so the ownership is moved, not extended (copied) so no counter management */
            control_block_proxy( control_block_proxy && other ) : m_control_block( move( other.m_control_block ) ) {}

            ~control_block_proxy( void ) {
                /* Decrement the use count */
                this->m_control_block->decrement_use_count();
            }

            /* Copy assignment operator */
            control_block_proxy & operator=( const control_block_proxy & other ) {
                control_block_proxy{ other }.swap( *this );
                return *this;
            }

            /* Move assignment operator */
            control_block_proxy & operator=( control_block_proxy && other ) {
                control_block_proxy{ move( other ) }.swap( *this );
                return *this;
            }

            pointer operator->( void ) const {
                return this->m_control_block.operator->();
            }

            /* TODO: dereference operator */

            void swap( control_block_proxy & other ) {
                ReVolta::swap( this->m_control_block, other.m_control_block );
            }
        };

    }

    template<typename T>
    class weak_ptr { /* TODO */ };

    template<typename T>
    class shared_ptr {    
    public:
        using element_type = remove_extent_t<T>;
        /* Corrensponding weak_ptr type for this shared_ptr */
        using weak_type = weak_ptr<T>;

    private:
        /* Containing pointer. The object being pointer to is owned by the referenced shared pointer control block,
         * not by the shared_ptr itself */
        observer_ptr<T> m_ptr;
        /* A proxy object owning the non-owning pointer to control block. The proxy manages the reference counts */
        Detail::control_block_proxy m_control_block;

        template<typename CONTROL_BLOCK>
        struct control_block_tag {
            using type = CONTROL_BLOCK;
        };

        /* A pointer type 'Y*' is said to be "compatible with" a pointer type 'T*' if either 'Y*' is convertible to 'T*' 
         * or 'Y' is the array type 'U[N]' and 'T' is 'U cv []' (where cv is some set of cv-qualifiers). */
        template<typename Y>
        struct is_compatible_with;

        template<typename Y>
        struct is_compatible_with<Y *> : public integral_constant<bool, is_convertible_v<Y *, T *>> {};

        template<typename Y>
            requires ( is_array_v<T> )
        struct is_compatible_with<Y(*)[]> : public integral_constant<bool, is_convertible_v<Y(*)[], T *>> {};

        template<typename Y, size_t N>
        struct is_compatible_with<Y(*)[ N ]> : public integral_constant<bool, is_convertible_v<Y(*)[ N ], T *>> {};

        /* If T is an array type U[N], (3-4,6) do not participate in overload resolution if Y(*)[N] is not convertible to T*. 
         * If T is an array type U[], (3-4,6) do not participate in overload resolution if Y(*)[] is not convertible to T*. Otherwise, 
         * (3-4,6) do not participate in overload resolution if Y* is not convertible to T*. */
        template<typename Y>
        inline constexpr static bool is_compatible_with_v = is_compatible_with<Y>::value;

        template<typename U>
        void enable_shared_from_this( observer_ptr<U> ptr ) {
            /* TODO:
             * A constructor enables shared_from_this with a pointer ptr of type U* means that it determines if U has an unambiguous and accessible (since C++17) 
             * base class that is a specialization of std::enable_shared_from_this, and if so, the constructor evaluates the statement:
             *  
             *   if (ptr != nullptr && ptr->weak_this.expired())
             *       ptr->weak_this = std::shared_ptr<std::remove_cv_t<U>>(
             *                           *this, const_cast<std::remove_cv_t<U>*>(ptr));
             * 
             * Where weak_this is the hidden mutable std::weak_ptr member of std::enable_shared_from_this. The assignment to the weak_this member is not atomic 
             * and conflicts with any potentially concurrent access to the same object. This ensures that future calls to shared_from_this() would share ownership 
             * with the std::shared_ptr created by this raw pointer constructor.
             * The test ptr->weak_this.expired() in the exposition code above makes sure that weak_this is not reassigned if it already indicates an owner. 
             * This test is required as of C++17.
             */
        }

    public:
        /* (1) Constructs an empty shared_ptr (use_count == 0, get() == nullptr) */
        constexpr shared_ptr( void ) noexcept : shared_ptr( nullptr ) {}

        /* (2) Constructs an empty shared_ptr (use_count == 0, get() == nullptr) */
        constexpr shared_ptr( nullptr_t ) noexcept : shared_ptr( nullptr, default_deleter<T>{} ) {}

        /* (3) Constructs a shared_ptr with 'ptr' as the pointer to the managed object. */
        /* NOTE: Forwards to (4). Enables 'shared_from_this' through the forwarded constructor */
        template<typename Y>
            /* Y must be a complete, non void type */
            requires ( !is_void_v<Y> && ( sizeof( Y ) > 0 ) )
        explicit shared_ptr( Y * ptr ) : shared_ptr( ptr, deleter<fallback_allocator<Y>>{} ) {}

        /* (4) Constructs a shared_ptr with 'ptr' as the pointer to the managed object. */
        /* NOTE: Forwards to (6). Type requirements are checked later in the forwarded constructor */
        /* FIXME: Maybe the 'Deleter' concept cannot be used here as it does not cover in-place defined deleter objects like lambdas.
         * Or extend the 'Deleter' concept to cover also this case... */
        template<typename Y, typename DELETER>
        shared_ptr( Y * ptr, DELETER deleter ) : shared_ptr( ptr, move( deleter ), allocator<Y>{} ) {}

        /* (5) Constructs a 'shared_ptr' with specific deleter */
        /* NOTE: Forwards to (7). Type requirements are checked later in the forwarded constuctor */
        template<typename DELETER>
        shared_ptr( nullptr_t, DELETER deleter ) : shared_ptr( nullptr, move( deleter ), allocator<T>{} ) {}

        /* (6) Constructs a 'shared_ptr' with 'ptr' as the pointer to the managed pointer. Uses specific 'DELETER deleter' as the deleter.
         * Additionally uses a copy of 'ALLOCATOR' 'allocator' for allocation of data for internal use. */
        /* NOTE: This is the stadnard constructor to which all the others non-nullptr above are forwarded to */
        template<typename Y, typename DELETER, Allocator ALLOCATOR>
            requires (
                /* 'Y' must be compatible to 'T' */
                /* FIXME */
#if false
                is_compatible_with_v<Y> &&
#endif
                /* Requires the deleter to be move constructible */
                is_move_constructible_v<DELETER> &&
                /* The invocation of the deleter must be well formed */
                is_invocable_v<DELETER &, Y *&>
            )
        shared_ptr( Y * ptr, DELETER deleter, ALLOCATOR allocator ) : m_ptr( ptr ), m_control_block( [&]() -> Detail::control_block_proxy {
            /* Get the allocator rebound to allocate the control block itself rather than the instance of Y */
            using control_block_allocator_type = typename Detail::shared_ptr_control_block<Y, ALLOCATOR, DELETER>::allocator_type;
            /* Create a copy of allocator rebound to allocate control block */
            control_block_allocator_type control_block_allocator { allocator };
            /*  */
            return Detail::control_block_proxy{ control_block_allocator, ptr, deleter, control_block_allocator };
        }() ) {
            /* Enable shared_from_this */
            this->enable_shared_from_this( this->m_ptr );
        } 

        /* (7) */
        /* NOTE: Does not enable 'shared_from_this' */
        template<typename DELETER, Allocator ALLOCATOR>
            requires (
                /* Requires the deleter to be move constructible */
                is_move_constructible_v<DELETER> &&
                /* The invocation of the deleter must be well formed */
                is_invocable_v<DELETER &, nullptr_t>
            )
        shared_ptr( nullptr_t, DELETER deleter, ALLOCATOR allocator ) : m_ptr( nullptr ), m_control_block( [&]() -> Detail::control_block_proxy {
            /* Get the allocator rebound to allocate the control block itself rather than the instance of Y */
            using control_block_allocator_type = typename Detail::shared_ptr_control_block<T, ALLOCATOR, DELETER>::allocator_type;
            /* Create a copy of allocator rebound to allocate control block */
            control_block_allocator_type control_block_allocator { allocator };
            /*  */
            return Detail::control_block_proxy{ control_block_allocator, nullptr, deleter, control_block_allocator };
        }() ) {}

        /* (8) The aliasing constructor constructs a shared_ptr which shares ownership info with the initial value of 'other'
         * but holds an unrelated and unmanaged pointer 'ptr'. If this shared_ptr is the last of the group to go out of scope,
         * it will call the stored 'deleter' for the object originally managed by 'other' */
        template<typename Y>
        shared_ptr( const shared_ptr<Y> & other, element_type * ptr ) noexcept : m_ptr( ptr ), m_control_block( other.m_control_block ) {}

        /* (8) The aliasing move constructor */
        template<typename Y>
        shared_ptr( shared_ptr<Y> && other, element_type * ptr ) noexcept : m_ptr( ptr ), m_control_block( move( other.m_control_block ) ) {}

        /* (9) Constructs a 'shared_ptr' which shares ownership fo the object managed by 'other'. If 'other' manages no object, '*this' manages no object either. */
        shared_ptr( const shared_ptr & other ) noexcept : m_ptr( other.m_ptr ), m_control_block( other.m_control_block ) {}

        /* (9) Constructs a 'shared_ptr' which shares ownership fo the object managed by 'other'. If 'other' manages no object, '*this' manages no object either. */
        template<typename Y>
            /* TODO Y* must be compatible with T* */
        shared_ptr( const shared_ptr<Y> & other ) noexcept : m_ptr( other.m_ptr ), m_control_block( other.m_control_block ) {}

        /* (10) Move-constructs a 'shared_ptr' from 'other'. After the construction, '*this' contains a copy of the previous state of 'other', 'other' is empty
         * and it's stored pointer is 'nullptr' */
        shared_ptr( shared_ptr && other ) noexcept : m_ptr( other.m_ptr ), m_control_block( move( other.m_control_block ) ) {
            other.m_ptr = nullptr;
        }

        /* (10) Move-constructs a 'shared_ptr' from 'other'. After the construction, '*this' contains a copy of the previous state of 'other', 'other' is empty
         * and it's stored pointer is 'nullptr' */
        template<typename Y>
            /* TODO Y* must be compatible with T* */
        shared_ptr( shared_ptr<Y> && other ) noexcept  : m_ptr( other.m_ptr ), m_control_block( move( other.m_control_block ) ) {
            other.m_ptr = nullptr;
        }

        /* (11) Constructs a shared_ptr which shares ownership of the object managed by 'other'.
         * NOTE: 'other.lock()' may be used for the same purpose. The difference is that this constructor thwrows an exception
         *       if the argument is empty while 'weak_ptr<T>::lock()' constructs an empty 'shared_ptr' in that case */
        template<typename Y>
            /* TODO: Y* must be compatible with T* */
        shared_ptr( const weak_ptr<Y> & other ) { /* TODO */ }

        /* (13) Constructs a 'shared_ptr' which manages the object currently managed by 'other'. The deleter associated with 'other' is stored for future
         * deletion of the managed object. 'other' manages no object after the call. */
        template<typename Y, typename DELETER>
            /* unique_ptr<Y, DELETER>::pointer must be compatible with T* */
            requires ( is_compatible_with_v<typename unique_ptr<Y, DELETER>::pointer> )
        shared_ptr( unique_ptr<Y, DELETER> && other ) : m_ptr( other.get() ), m_control_block( [&]() -> Detail::control_block_proxy {
            /* Get the allocator rebound to allocate the control block itself rather than the instance of Y */
            using control_block_allocator_type = typename Detail::shared_ptr_control_block<Y, allocator<Y>, DELETER>::allocator_type;
            /* Create a copy of allocator rebound to allocate control block */
            control_block_allocator_type control_block_allocator;
            /*  */
            return Detail::control_block_proxy{ control_block_allocator, move( other ), control_block_allocator };
        }() ) {
            /* Enable shared_from_this */
            this->enable_shared_from_this( this->m_ptr );
        } 

    private:
        /* Make allocate_shared() a friend to allow the access to the private non-standard constructor below */
        template<typename U, Allocator ALLOCATOR, typename... ARGUMENTS>
        friend shared_ptr<U> allocate_shared( const ALLOCATOR & allocator, ARGUMENTS &&... arguments );

        struct control_block_inplace_tag {};

#if false
        template<Allocator ALLOCATOR, typename... ARGUMENTS>
        shared_ptr( control_block_inplace_tag, ALLOCATOR allocator, ARGUMENTS &&... arguments ) {
            typename Detail::shared_ptr_control_block_inplace<>
        }
#endif
        /* TODO: Hidden constructor to support 'allocate_shared()' */

    public:

        /* If '*this' owns an object and it is the last 'shared_ptr' owning it, the object is destroyed through the owned deleter.
         * After the destruction, the smart pointers that shared ownership with '*this', if any, will report a 'use_count()'
         * that is one less than its previous value  */
        ~shared_ptr( void ) {}

        /* Shares ownership of the object managed by 'other'. If 'other' manages no object, '*this' manages no object as well. */
        shared_ptr & operator=( const shared_ptr & other ) noexcept {
            shared_ptr{ other }.swap( *this );
            return *this;
        }

        /* Move assigns as 'shared_ptr' from 'other'. After the assignment '*this' contains a copy of the previous state of 'other'
         * and 'other' is empty. */
        shared_ptr & operator=( shared_ptr && other ) noexcept {
            shared_ptr{ move( other ) }.swap( *this );
            return *this;
        }

        /* Shares ownership of the object managed by 'other'. If 'other' manages no object, '*this' manages no object as well. */
        template<typename Y>
        shared_ptr<T> & operator=( const shared_ptr<Y> & other ) noexcept {
            shared_ptr<T>{ other }.swap( *this );
            return *this;
        }

        /* Move assigns as 'shared_ptr' from 'other'. After the assignment '*this' contains a copy of the previous state of 'other'
         * and 'other' is empty. */
        template<typename Y>
        shared_ptr & operator=( shared_ptr<Y> && other ) noexcept {
            shared_ptr<T>{ move( other ) }.swap( *this );
            return *this;
        }

        /* Transfers the ownership of the object managed by 'other' to '*this'. The deleter associated to 'other' is stored for future
         * deletion of the managed object. 'other' manages no object after the call. */
        template<typename Y, typename DELETER>
        shared_ptr & operator=( unique_ptr<Y, DELETER> && other ) {
            shared_ptr<T>{ move( other ) }.swap( *this );
            return *this;
        }

        /* Releases the ownership of the managed object, if any. After the call '*this' manages no object. */
        void reset( void ) noexcept {
            shared_ptr{}.swap( *this );
        }

        /* Replaces the managed object with an object pointed to by 'ptr'. 'Y' must be a complete type and implicitly convertible to 'T' */
        template<typename Y>
        void reset( Y * ptr ) {
            shared_ptr<T>{ ptr }.swap( *this );
        }

        /* Replaces the managed object with an object pointed to by 'ptr'. 'Y' must be a complete type and implicitly convertible to 'T' */
        template<typename Y, typename DELETER>
        void reset( Y * ptr, DELETER deleter ) {
            shared_ptr<T>{ ptr, move( deleter ) }.swap( *this );
        }

        template<typename Y, typename DELETER, Allocator ALLOCATOR>
        void reset( Y * ptr, DELETER deleter, ALLOCATOR allocator ) {
            shared_ptr<T>{ ptr, move( deleter ), move( allocator ) }.swap( *this );
        }

        /* Exchanges the stored pointer values and the ownerships of '*this' and 'other'. Reference counts, if any, are not adjusted */
        void swap( shared_ptr & other ) noexcept {
            ReVolta::swap( this->m_ptr, other.m_ptr );
            ReVolta::swap( this->m_control_block, other.m_control_block );
        }

        /* Returns the stored pointer */
        element_type * get( void ) const noexcept {
            return this->m_ptr.get();
        }

        /* Dereferences the stored pointer. The behavior is undefined if the stored pointer is 'nullptr' */
        element_type & operator*( void ) const noexcept {
            return *this->get();
        }

        /* Dereferences the stored pointer. The behavior is undefined if the stored pointer is 'nullptr' */
        element_type * operator->( void ) const noexcept {
            return this->get();
        }

        /* Index into the array pointed to by the stored pointer. The behavior is undefined if the stored pointer is 'nullptr' or if 'index' is negative.
         * If 'T' is an array type 'U[ N ]', index must be less than 'N' otherwise the behavior is undefined. */
        element_type & operator[]( ptrdiff_t index ) const requires ( is_array_v<T> ) {
            return this->get()[ index ];
        }

        /* Returns the number of different shared_ptr instances ('this' included) managing current object. If there is none, 0 is returned */
        long use_count( void ) const noexcept {
            return ( this->m_control_block )->use_count();
        }

        explicit operator bool( void ) const noexcept {
            return this->m_ptr != nullptr;
        }

        /* Checks whether this shared_ptr precedes 'other' in implementation defined owner-based (as opposed to value-based) order.
         * The order is such that two smart pointers compare equivalent only if they are both empty or if they both own the same object.
         * Common implementations compare the addresses of the control blocks. */
        template<typename Y>
        bool owner_before( const shared_ptr<Y> & other ) const noexcept {
            /* TODO */
        }

        template<typename Y>
        bool owner_before( const weak_ptr<Y> & other ) const noexcept {
            /* TODO */
        }

    };

    /* Constructs an object of type 'T' and wraps it in a 'shared_ptr' using 'arguments' as the parameter list to the constructor of 'T'.
     * The 'shared_ptr' constructor called by this function enables shared_from_this with a pointer to the newly constructed object of type 'T'. */
    template<typename T, Allocator ALLOCATOR, typename... ARGUMENTS>
        /* T must not be an array type */
        requires ( !is_array_v<T> )
    shared_ptr<T> allocate_shared( const ALLOCATOR & allocator, ARGUMENTS &&... arguments ) {
        /* FIXME */
#if false        
        //return shared_ptr<T>{ allocator, forward<ARGUMENTS>( arguments )... };
        return shared_ptr<T>{ shared_ptr<T>::template control_block_tag<Detail::shared_ptr_control_block_inplace>{},  }
#endif
    }

    /* TODO: More overloads of 'allocate_shared()' to come here */

    /* Constructs an object fo type 'T' and wraps it in a shared_ptr using 'arguments' as the parameter list for the constructor of 'T'.
     * The internal storage is typically bigger than 'sizeof( T )' in order to use one allocation for both the 'shared_ptr' control block and the instance of 'T'.
     * The 'shared_ptr' constructor called by this function enables 'shared_from_this' with a pointer to the newly constructed object of type 'T' */
    template<typename T, typename... ARGUMENTS>
    shared_ptr<T> make_shared( ARGUMENTS &&... arguments ) {
        return allocate_shared<T>( allocator<remove_cv_t<T>>{}, forward<ARGUMENTS>( arguments )... );
    }

    template<typename T>
    shared_ptr<T> make_shared( size_t n ) {
        /* TODO */
    }

    template<typename T>
    shared_ptr<T> make_shared( void ) {
        /* TODO */
    }

    template<typename T>
    shared_ptr<T> make_shared( size_t n, const remove_extent_t<T> & value ) {
        /* TODO */
    }

    template<typename T>
    shared_ptr<T> make_shared( const remove_extent_t<T> & value ) {
        /* TODO */
    }

    template<typename T>
    shared_ptr<T> make_shared_for_overwrite( void ) {
        /* TODO */
    }

    template<typename T>
    shared_ptr<T> make_shared_for_overwrite( size_t n ) {
        /* TODO */
    }

}