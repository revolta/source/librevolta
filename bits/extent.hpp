#pragma once

#include "limits.hpp"

namespace ReVolta {

    inline constexpr size_t dynamic_extent = numeric_limits<size_t>::max();

    namespace Detail {

        template<size_t E>
        struct extent_storage {
            constexpr extent_storage( size_t ) noexcept {}

            static constexpr size_t value( void ) noexcept {
                return E;
            }
        };

        template<>
        struct extent_storage<dynamic_extent> {
            constexpr extent_storage( size_t value ) noexcept : m_value( value ) {}

            constexpr size_t value( void ) const noexcept {
                return this->m_value;
            }

        private:
            size_t m_value;
        };

    }

}