#pragma once

#include "deleter.hpp"
#include "concepts.hpp"
#include "utility.hpp"

namespace ReVolta {

    /* TODO: Add concurrency support - atomic counter? */

    /* CRTP base class to mixin the retainable requirements to any type */
    template<typename T, typename COUNTER_TYPE = size_t>
    struct use_counter {
        using counter_type = COUNTER_TYPE;

        void set_use_count( const counter_type & value ) noexcept {
			m_use_count = value;
		}

		counter_type get_use_count( void ) const noexcept {
			return m_use_count;
		}

        constexpr size_t max_use_count( void ) const noexcept {
            counter_type max_value { static_cast<counter_type>( ~0 ) };
            return max_value;
		}

    private:
        counter_type m_use_count { 0 };
    };

    template<Retainable T, Deleter DELETER = default_deleter<T>>
    struct retain_traits {
        using deleter_type = DELETER;

        static void increment( T * entry ) noexcept {
            if( ( entry != nullptr ) && ( entry->get_use_count() < entry->max_use_count() ) ) {
                entry->set_use_count( entry->get_use_count() + 1 );
            }
        }

        static void decrement( T * entry ) noexcept {
            if( entry != nullptr ) {
                entry->set_use_count( entry->get_use_count() - 1 );
                if( entry->get_use_count() == 0 ) {
                    deleter_type()( entry );
                }
            }
        }

        static size_t use_count( T * entry ) noexcept {
            return entry->get_use_count();
        }
    };

    template<Retainable T, typename R = retain_traits<T>>
    class retain_ptr {
        /* Make all other retain_ptr types friends in order to have an access to raw() */
        template<Retainable, typename>
        friend class retain_ptr;

    public:
        using pointer = T*;
        using element_type = T;
        using traits_type = R;
        using raw_type = uintptr_t;

    private:
        constexpr static size_t pointer_mask = ~( alignof( T ) - 1 );

        raw_type m_ptr;

    public:
        /* FIXME: replace 0UL by pointer_storage_type() - default constructed value, shall be initialized to 0 */
        constexpr retain_ptr( void ) noexcept : m_ptr( 0UL ) {}

        constexpr retain_ptr( nullptr_t ) noexcept : retain_ptr() {}

        /* FIXME: Does the pointer_mask make any sense in here? The 'pointer' shall be valid pointer only, no tags built in. 
         * BUT shall the tags be managed using this constructor? Or just by copy/move operations?
         * How about implementing another constructor taking raw values as arguments? OR to implement special construcotr / assingment operation in tagged<> itself */
        explicit retain_ptr( pointer ptr, bool increment_use_count = true ) noexcept : m_ptr( reinterpret_cast<raw_type>( ptr ) ) {
            if( increment_use_count && ptr ) {
                traits_type::increment( ptr );
            }
        }

        /* Copy constructor */
        retain_ptr( const retain_ptr & other ) noexcept : m_ptr( other.raw() ) {
            if( get() ) traits_type::increment( get() );
        }

        /* Converting copy constructor */
        /* FIXME: SafelyConvertible concept prevents the conversion from page directory to page table */
        template<Retainable U, typename TRAITS> //requires SafelyConvertible<retain_ptr<U, TRAITS>, retain_ptr>
        retain_ptr( const retain_ptr<U, TRAITS> & other ) noexcept : m_ptr( other.raw() ) {
            if( get() ) traits_type::increment( get() );
        }

        /* Move constructor */
        retain_ptr( retain_ptr && other ) noexcept : m_ptr( move( other.raw() ) ) {
            other.detach();
        }

        /* Converting move constructor */
        template<Retainable U, typename TRAITS> requires SafelyConvertible<retain_ptr<U, TRAITS>, retain_ptr>
        retain_ptr( retain_ptr<U, TRAITS> && other ) noexcept : m_ptr( move( other.raw() ) ) {
            other.detach();
        }

        /* Destructor */
        ~retain_ptr( void ) noexcept {
            pointer ptr = get();
            if( ptr != nullptr ) {
                traits_type::decrement( ptr );
            }
            ptr = pointer();
        }

        /* Dereference operator */
        typename add_lvalue_reference<element_type>::type operator*( void ) const noexcept {
            return *get();
        }

        pointer operator->( void ) const noexcept {
            return get();
        }

        /* Nullptr assignment operator */
        retain_ptr & operator=( nullptr_t ) noexcept {
            reset( nullptr );
            return *this;
        }

        /* Copy assignment operator */
        retain_ptr & operator=( const retain_ptr & other ) noexcept {
            /* Use copy constructor to increment the reference count */
            retain_ptr( other ).swap( *this );
            return *this;
        }

        /* Move assignment operator */
        retain_ptr & operator=( retain_ptr && other ) noexcept {
            /* Move constructor does not increment the reference count */
            retain_ptr( move( other ) ).swap( *this );
            return *this;
        }

        /* Converting move assignment operator */
        template<typename U, typename TRAITS> requires SafelyAssignable<retain_ptr<U, TRAITS>, retain_ptr>
        retain_ptr & operator=( retain_ptr<U, TRAITS> && other ) noexcept {
            /* Use converting move constructor */
            retain_ptr( move( other ) ).swap( *this );
            return *this;
        }

        /* Converting copy assignment operator */
        template<typename U, typename TRAITS> requires SafelyAssignable<retain_ptr<U, TRAITS>, retain_ptr>
        retain_ptr & operator=( retain_ptr<U, TRAITS> & other ) noexcept {
            /* Use converting copy constructor */
            retain_ptr( other ).swap( *this );
            return *this;
        }

        size_t use_count( void ) const noexcept {
            return ( get() ) ? traits_type::use_count( get() ) : 0;
        }

        pointer get( void ) const noexcept {
            return reinterpret_cast<pointer>( m_ptr & pointer_mask );
        }

        void reset( pointer ptr = nullptr, bool increment_use_count = false ) noexcept {
            retain_ptr( ptr, increment_use_count ).swap( *this );
        }

        pointer detach( void ) noexcept {
            pointer ptr = get();
            this->m_ptr = reinterpret_cast<raw_type>( nullptr );
            return ptr;
        }

        explicit operator pointer( void ) const noexcept {
            return get();
        }

        explicit operator bool( void ) const noexcept {
            return get() == pointer() ? false : true;
        }

        inline void swap( retain_ptr & other ) noexcept {
            using ReVolta::swap;
            swap( m_ptr, other.m_ptr );
        }

    protected:
        raw_type & raw( void ) noexcept {
            return m_ptr;
        }

        const raw_type & raw( void ) const noexcept {
            return m_ptr;
        }
    };

    template<Retainable T, typename R, Retainable U, typename S>
	inline bool operator==( const retain_ptr<T,R> & x, const retain_ptr<U,S> & y ) noexcept {
		return ( x.get() == y.get() );
	}

	template<Retainable T, typename R>
	inline bool operator==( const retain_ptr<T,R> & x, nullptr_t ) noexcept {
		return !x;
	}

	template<Retainable T, typename R>
	inline bool operator==( nullptr_t, const retain_ptr<T,R> & x ) noexcept {
		return !x;
	}

	template<Retainable T, typename R, Retainable U, typename S>
	inline bool operator!=( const retain_ptr<T,R> & x, const retain_ptr<U,S> & y ) noexcept {
		return ( x.get() != y.get() );
	}

	template<Retainable T, typename R>
	inline bool operator!=( const retain_ptr<T,R> & x, nullptr_t ) noexcept {
		return static_cast<bool>( x );
	}

	template<Retainable T, typename R>
	inline bool operator!=( nullptr_t, const retain_ptr<T,R> & x ) noexcept {
		return static_cast<bool>( x );
	}

    template<Retainable T, Allocator ALLOCATOR, typename... ARGUMENTS>
    retain_ptr<T, retain_traits<T>> allocate_retain( const ALLOCATOR & allocator, ARGUMENTS &&... arguments ) noexcept {
        T * ptr = allocator_traits<ALLOCATOR>::allocate( allocator, 1 );
        if( ptr != nullptr ) {
            allocator_traits<ALLOCATOR>::construct( allocator, ptr, forward<ARGUMENTS>( arguments )... );
        }
        return retain_ptr<T, retain_traits<T>>( ptr, true );
    }

    template<Retainable T>
    retain_ptr<T, retain_traits<T>> make_retain( T * raw ) noexcept {
        return retain_ptr<T, retain_traits<T>>( raw, true );
    }

}