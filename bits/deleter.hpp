#pragma once

#include "cstddef.hpp"
#include "concepts.hpp"
#include "bits/allocator_traits.hpp"
#include "bits/allocator.hpp"

namespace ReVolta {

	template<Allocator ALLOCATOR>
	class deleter {
	public:
		using pointer = typename ALLOCATOR::pointer;
		using value_type = typename ALLOCATOR::value_type;
		using allocator_type = ALLOCATOR;

		/* Default deleter constructor */
		constexpr deleter( void ) noexcept = default;

		/* Converting constructor allows conversions from a deleter for objects of another type
		 * only if U* and the pointer to built-in allocator type are convertible */
		template<Deleter COMPATIBLE_DELETER> 
			requires ( is_convertible_v<typename COMPATIBLE_DELETER::pointer, pointer> )
		deleter( const COMPATIBLE_DELETER & ) noexcept {}

		void operator()( pointer ptr ) const noexcept {
			if( ptr != nullptr ) {
				allocator_type allocator;
				/* Destruct the instance being deleted */
				allocator_traits<allocator_type>::destroy( allocator, ptr );
				/* Deallocate resource */
				allocator_traits<allocator_type>::deallocate( allocator, ptr, 1 );
			}
		}
	};

	template<typename T>
	using default_deleter = deleter<allocator<T>>;

	template<typename T>
	struct forward_deleter {
		using pointer = T *;
		void operator()( pointer ptr ) noexcept;
	};

	template<typename T>
	struct blank_deleter {
		using pointer = T *;
		inline void operator()( pointer ) noexcept {}
	};
}
