#pragma once

#include "deleter.hpp"
#include "concepts.hpp"
#include "allocator_traits.hpp"
#include "bit.hpp"

/* TODO: add feature test macro "__cpp_lib_constexpr_unique_ptr" to <memory.hpp> once version management is available */

/* TODO: add feature test macro "__cpp_lib_constexpr_make_unique" to <memory.hpp> once version management is available */

namespace ReVolta {

	template<typename FROM, typename TO>
	concept CompatibleDeleter = 
		conditional<
			is_reference<TO>::value,
			is_same<FROM,TO>,
			is_convertible<FROM,TO>
		>::type::value
	;

	template<typename T, typename DELETER = default_deleter<T>>
	class unique_ptr {
		/* Make all other unique_ptr types friends in order to have an access to raw() */
		template<typename U, typename E>
		friend class unique_ptr;

	public:
		using pointer = T *;
		using element_type = T;
		using deleter_type = DELETER;
		using raw_type = uintptr_t;

	private:
		constexpr static size_t pointer_mask = ~( alignof( T ) - 1 );
		/* Underlying raw pointer being managed by unique_ptr */
		raw_type m_ptr;
		/* Associated deleter */
		[[no_unique_address]] deleter_type m_deleter;

	public:
		/* Default constructor - creates pointer owning nothing */
		constexpr unique_ptr( void ) noexcept : m_ptr( bit_cast<raw_type>( nullptr ) ) {}

		/* Construct by nullptr */
		constexpr unique_ptr( nullptr_t ) noexcept : unique_ptr() {}

		/* Takes ownership of pointer */
		explicit unique_ptr( pointer ptr ) noexcept : m_ptr( bit_cast<raw_type>( ptr ) ) {}

		/* Takes ownership of a pointer with reference to deleter */
		template<typename D = deleter_type> 
			requires ( is_copy_constructible_v<D> )
		constexpr unique_ptr( pointer ptr, const D & del ) noexcept : m_ptr( bit_cast<raw_type>( ptr ) ), m_deleter( del ) {}

		/* Takes ownership with rvalue reference to deleter */
		constexpr unique_ptr( pointer ptr, typename remove_reference<deleter_type>::type && del ) noexcept
			: m_ptr( bit_cast<raw_type>( ptr ) ), m_deleter( move( del ) ) {}

		/* Move constructor */
		constexpr unique_ptr( unique_ptr && other ) noexcept 
			: m_ptr( other.m_ptr ), m_deleter( move( other.m_deleter ) ) {
			other.release();
		}

		template<typename U, typename E> 
			requires ( SafelyConvertible<unique_ptr<U, E>, unique_ptr> && CompatibleDeleter<E, deleter_type> )
		constexpr unique_ptr( unique_ptr<U, E> && other ) noexcept
			: m_ptr( other.m_ptr ), m_deleter( forward<E>( other.m_deleter ) ) {
			other.release();
		}

		/* Copy constructor is deleted as unique_ptr is noncopyable */
		constexpr unique_ptr( const unique_ptr & ) noexcept = delete;

		/* Destructor */
		constexpr ~unique_ptr( void ) noexcept {
			pointer ptr = this->get();
			/* Invoke deleter */
			if( ptr != nullptr ) {
				this->m_deleter( move( ptr ) );
			}
			ptr = pointer();
		}

		/* Move assignment operator */
		constexpr unique_ptr & operator=( unique_ptr && other ) noexcept {
			unique_ptr{ move( other ) }.swap( *this );
			return *this;
		}

		/* Copy assignment operator is deleted as unique_ptr is noncopyable */
		constexpr unique_ptr & operator=( const unique_ptr & ) noexcept = delete;

		/* Converting assignment operator */
		template<typename U, typename E> requires ( SafelyAssignable<unique_ptr<U, E>, unique_ptr> )
		constexpr unique_ptr & operator=( unique_ptr<U, E> && other ) noexcept {
			/* Use converting constructor */
			unique_ptr{ move( other ) }.swap( *this );
			return *this;
		}

		constexpr unique_ptr & operator=( nullptr_t ) noexcept {
			this->reset();
			return *this;
		}

		constexpr pointer operator->( void ) const noexcept {
			return this->get();
		}

		/* Dereference stored pointer */
		constexpr typename add_lvalue_reference<element_type>::type & operator*( void ) const noexcept {
			return *this->get();
		}

		/* Return stored pointer */
		constexpr pointer get( void ) const noexcept {
			return bit_cast<pointer>( this->m_ptr & pointer_mask );
		}

		constexpr deleter_type & get_deleter( void ) noexcept {
			return this->m_deleter;
		}

		constexpr const deleter_type & get_deleter( void ) const noexcept {
			return this->m_deleter;
		}

		constexpr explicit operator pointer( void ) const noexcept {
			return this->get();
		}

		constexpr explicit operator bool( void ) const noexcept {
			return this->get() == pointer() ? false : true;
		}

		constexpr pointer release( void ) noexcept {
            pointer ptr = this->get();
			this->m_ptr = bit_cast<raw_type>( nullptr );
            return ptr;
		}

		constexpr void reset( pointer ptr = pointer() ) noexcept {
            pointer old_ptr = this->get();
			this->m_ptr = bit_cast<raw_type>( ptr );
			/* Run deleter for 'old_ptr' */
			if( old_ptr ) this->m_deleter( bit_cast<typename deleter_type::pointer>( old_ptr ) );
		}

		constexpr void swap( unique_ptr & other ) noexcept {
			using ReVolta::swap;
			swap( this->m_ptr, other.m_ptr );
			swap( this->m_deleter, other.m_deleter );
		}

	protected:
		constexpr raw_type & raw( void ) noexcept {
			return this->m_ptr;
		}

		constexpr const raw_type & raw( void ) const noexcept {
			return this->m_ptr;
		}
	};

	template<typename T, typename D, typename U, typename E>
	constexpr inline bool operator==( const unique_ptr<T,D> & x, const unique_ptr<U,E> & y ) noexcept {
		return ( x.get() == y.get() );
	}

	template<typename T, typename D>
	constexpr inline bool operator==( const unique_ptr<T,D> & x, nullptr_t ) noexcept {
		return !x;
	}

	template<typename T, typename D>
	constexpr inline bool operator==( nullptr_t, const unique_ptr<T,D> & x ) noexcept {
		return !x;
	}

	template<typename T, typename D, typename U, typename E>
	constexpr inline bool operator!=( const unique_ptr<T,D> & x, const unique_ptr<U,E> & y ) noexcept {
		return ( x.get() != y.get() );
	}

	template<typename T, typename D>
	constexpr inline bool operator!=( const unique_ptr<T,D> & x, nullptr_t ) noexcept {
		return static_cast<bool>( x );
	}

	template<typename T, typename D>
	constexpr inline bool operator!=( nullptr_t, const unique_ptr<T,D> & x ) noexcept {
		return static_cast<bool>( x );
	}

	/* TODO: Implement 'is_swappable<>' */
	template<typename T, typename D>
	constexpr void swap( unique_ptr<T, D> & lhs, unique_ptr<T, D> & rhs ) noexcept /* requires( is_swappable_v<D> ) */ {
		lhs.swap( rhs );
	}

#if !defined ( _LIBREVOLTA_FREESTANDING )

	/* Constructs a non-array type T. The arguments args are passed to the constructor of T.
	 * This overload participates in overload resolution only if T is not an array type. */
	template<typename T, typename... ARGUMENTS>
		requires ( !is_array_v<T> )
	constexpr inline unique_ptr<T> make_unique( ARGUMENTS &&... arguments ) {
		return unique_ptr<T>( new T( forward<ARGUMENTS>( arguments )... ) );
	}

	/* Constructs an array of the given dynamic size. The array elements are value-initialized.
	 * This overload participates in overload resolution only if T is an array of unknown bound */
	template<typename T>
		requires ( is_unbounded_array_v<T> )
	constexpr inline unique_ptr<T> make_unique( size_t size ) {
		return unique_ptr<T>( new remove_extent_t<T>[ size ]() );
	}

	/* Construction of arrays of known bound is disallowed */
	template<typename T, typename... ARGUMENTS>
		requires ( is_bounded_array_v<T> )
	constexpr void make_unique( ARGUMENTS &&... ) = delete;

	template<typename T>
		requires ( !is_array_v<T> )
	constexpr inline unique_ptr<T> make_unique_for_overwrite( void ) {
		return unique_ptr<T>( new T );
	}

	template<typename T>
		requires ( is_unbounded_array_v<T> )
	constexpr inline unique_ptr<T> make_unique_for_overwrite( size_t size ) {
		return unique_ptr<T>( new remove_extent_t<T>[ size ] );
	}

	template<typename T, typename... ARGUMENTS>
		requires ( is_bounded_array_v<T> )
	constexpr void make_unique_for_overwrite( ARGUMENTS &&... ) = delete;

#endif

	/* http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2017/p0316r0.html#allocate-unique */
	template<typename T, Allocator ALLOCATOR, typename... ARGUMENTS>
	unique_ptr<T, deleter<ALLOCATOR>> allocate_unique_hint( const ALLOCATOR & allocator, const void * hint, ARGUMENTS &&... arguments ) noexcept {
		/* Allocate space for new instance of T */
		typename allocator_traits<ALLOCATOR>::pointer ptr { allocator_traits<ALLOCATOR>::allocate( allocator, 1, hint ) };
		/* Once valid memory is allocated, construct the instance of T */
		if( ptr != nullptr ) {
			allocator_traits<ALLOCATOR>::construct( allocator, ptr, forward<ARGUMENTS>( arguments )... );
		}
		/* Return newly created instance of T */
		return unique_ptr<T, deleter<ALLOCATOR>>( ptr, deleter<ALLOCATOR>() );
	}

	template<typename T, Allocator ALLOCATOR, typename... ARGUMENTS>
	unique_ptr<T, deleter<ALLOCATOR>> allocate_unique( const ALLOCATOR & allocator, ARGUMENTS &&... arguments ) noexcept {
		return allocate_unique_hint<T>( allocator, invalid_ptr, forward<ARGUMENTS>( arguments )... );
	}

#if false
	template<typename T, size_t N, typename ALLOCATOR>
	unique_ptr<T[], deleter<ALLOCATOR,N>> allocate_array( ALLOCATOR & allocator, T const (& initializers)[ N ] ) noexcept {
		/* Allocate space for N-instances of T */
		T * ptr = allocator.template allocate<N>();
		/* Once a valid memory is returned, call the elements(s) constructor(s) */
		if( ptr != nullptr ) {
			/* Construct T in allocated memory */
			for( size_t idx = 0; idx < N; idx++ ) {
				::new( static_cast<void*>( ptr + idx ) ) T( initializers[ idx ] );
			}
		}
		return unique_ptr<T[], deleter<ALLOCATOR,N>>( ptr, deleter<ALLOCATOR,N>() );
	}
#endif
}
 