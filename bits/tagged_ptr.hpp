#pragma once

#include "concepts.hpp"
#include "type_traits.hpp"

namespace ReVolta {

	/* Primary template to be partially specialized for particular type T */
	template<typename TAGGED_POINTER, typename T>
	class tags;

	/* The 'tagged' mixin class requires the 'TAGS' to be class template taking one template
	 * parameter (CRTP). The other template parameters (if any) must be built in a type.
	 * This is the purpose of make_tags struct
	 * Usage:
	 * 'template<template<typename> class TAGS = make_tags<alignof( T )>::template type>'
	 */
	template<template<typename, typename...> class TAGS, typename... Ts>
	struct make_tags {
		template<typename TAGGED_PTR>
		using type = TAGS<TAGGED_PTR, Ts...>;
	};

	/* Concept describing the smart pointer having the interface required for tagging */
	template<typename T>
	concept TaggableSmartPointer = requires( T ptr ) {
		typename T::element_type;
	};

	/* Tagged pointer mixin to wrap around compatible smart pointer types */
	template<TaggableSmartPointer SMART_POINTER, template<typename> class TAGS>
	class tagged : public SMART_POINTER, public TAGS<tagged<SMART_POINTER, TAGS>> {
		friend class TAGS<tagged<SMART_POINTER, TAGS>>;

	public:
		/* The type of underlying smart pointer */
		using tagged_pointer = SMART_POINTER;
		/* Allow pointer_traits to identify the tagged pointer */
		using is_tagged = true_type;
		/* Tags type alias defined within the TAGS */
		using tags_type = typename TAGS<tagged<SMART_POINTER, TAGS>>::tags_type;
		/* Inherit constructors from tagged_pointer base class */
		using tagged_pointer::tagged_pointer;

		/* TODO: The tags type in tagged retain_ptr must be the same in converting constructors and converting assignments operators. 
		 * --> implement tagged_ptr specific converting constructors / assignment operators having the checks for the TAGS 
		 * BUT then it must be checked / enable_if each converting constructor variant if it supports particular ctor/operator...
		 * MAYBE it might be managed through SafelyConvertible<FROM, TO> concept (see concepts.hpp) as the tagged pointers with incompatible TAGS
		 * might not be SafelyConvertible... */

		/* Converting copy constructor */
		tagged( const tagged_pointer & ptr ) noexcept : tagged_pointer( ptr ) {}

		/* Converting move constructor */
		tagged( tagged_pointer && ptr ) noexcept : tagged_pointer( move( ptr ) ) {}

		/* Converting copy constructor specifying the tags during construction */
		tagged( const tagged_pointer & ptr, const tags_type & tags ) noexcept : tagged_pointer( ptr ) {
			this->get_tags() = tags;
		}

		/* Converting move constructor specifying the tags during construction */
		tagged( tagged_pointer && ptr, const tags_type & tags ) noexcept : tagged_pointer( move( ptr ) ) {
			this->get_tags() = tags;
		}

		/* TODO: Try to get rid of reinterpret_cast, maybe using union based conversion */
		tags_type & get_tags( void ) noexcept {
			/* Get the address of the pointer raw value  managed by smart pointer, reinterpret it to be used as the pointer
			 * to requested structure and dereference this pointer to return the reference to that */
			return *( reinterpret_cast<tags_type *>( addressof( this->raw() ) ) );
		}

		/* TODO: Try to get rid of reinterpret_cast, maybe using union based conversion */
		const tags_type & get_tags( void ) const noexcept {
			/* Get the address of the pointer raw value  managed by smart pointer, reinterpret it to be used as the pointer
			 * to requested structure and dereference this pointer to return the reference to that */
			return *( reinterpret_cast<const tags_type *>( addressof( this->raw() ) ) );
		}

		operator tagged_pointer & ( void ) noexcept {
			return static_cast<tagged_pointer &>( *this );
		}

	private:
		/* Calculate how many bits are free for tagging in relation to the alignment of T */
		static constexpr size_t available_tag_bit_width { n_bits<alignof( typename tagged_pointer::element_type )>() };

		/* Check whether the number of bits managed by the tags is less or equal to the available amount
		 * given by the smart pointer's managed type alignment */
		static_assert( TAGS<tagged<tagged_pointer, TAGS>>::required_tags_bit_width <= available_tag_bit_width );
	};

	/* Forward declarations of all supported smart pointer types */
	template<typename, typename> class unique_ptr;
	template<Retainable, typename> class retain_ptr; 

	namespace Detail {
		/* 'tagged_ptr_factory' is tagged pointer internal class to provide partially specialized
		 * implementations of tagged pointer construction */

		/* Primary template */
		template<template<typename> class TAGS, template<typename, typename...> class SMART_POINTER, typename T, typename ALLOCATOR>
		struct tagged_ptr_factory;

		/* Partial specialization for unique_ptr<> smart pointer class template */
		template<template<typename> class TAGS, typename T, Allocator ALLOCATOR>
		struct tagged_ptr_factory<TAGS, unique_ptr, T, ALLOCATOR> {
			using type = tagged<unique_ptr<T, deleter<ALLOCATOR>>, TAGS>;

			static constexpr type create( T * ptr ) noexcept {
				return type( ptr, deleter<ALLOCATOR>() );
			}
		};

		/* NOTE: observer_ptr shall not be used in allocate_tagged() as this type of smart pointer is non owning */

		/* Partial specialization for retain_ptr<> smart pointer class template */
		template<template<typename> class TAGS, typename T, Allocator ALLOCATOR>
		struct tagged_ptr_factory<TAGS, retain_ptr, T, ALLOCATOR> {
			using type = tagged<retain_ptr<T, retain_traits<T>>, TAGS>;

			static constexpr type create( T * ptr ) noexcept {
				return type( ptr );
			}
		};
	}

	template<template<typename, typename...> class SMART_POINTER, typename T, template<typename> class TAGS = make_tags<tags, T>::template type, Allocator ALLOCATOR, typename... ARGUMENTS>
	auto allocate_tagged_hint( const ALLOCATOR & allocator, const void * hint, ARGUMENTS &&... arguments ) noexcept {
		/* Allocate space for the instance of T */
		T * ptr = allocator_traits<ALLOCATOR>::allocate( allocator, 1, hint );
		if( ptr != nullptr ) {
			/* Construct the instance of T in the space allocated previously */
			allocator_traits<ALLOCATOR>::construct( allocator, ptr, forward<ARGUMENTS>( arguments )... );
		}
		/* Create smart pointer */
		return Detail::tagged_ptr_factory<TAGS, SMART_POINTER, T, ALLOCATOR>::create( ptr );
	}

	/* TODO: Maybe to apply template function design pattern to unify the function body which is more or less identical in all allocation functions */
	/* TODO: Find a way to forward the tags as well during construction */
	template<template<typename, typename...> class SMART_POINTER, typename T, template<typename> class TAGS = make_tags<tags, T>::template type, Allocator ALLOCATOR, typename... ARGUMENTS>
	auto allocate_tagged( const ALLOCATOR & allocator, ARGUMENTS &&... arguments ) noexcept {
		return allocate_tagged_hint<SMART_POINTER, T, TAGS>( allocator, invalid_ptr, forward<ARGUMENTS>( arguments )... );
	}

	template<TaggableSmartPointer SMART_POINTER, template<typename> class TAGS = make_tags<tags, typename SMART_POINTER::element_type>::template type>
	auto make_tagged( SMART_POINTER && ptr ) noexcept {
		return tagged<SMART_POINTER, TAGS>( move( ptr ) );
	}

	template<TaggableSmartPointer SMART_POINTER, template<typename> class TAGS = make_tags<tags, typename SMART_POINTER::element_type>::template type>
	auto make_tagged( SMART_POINTER && ptr, const typename tagged<SMART_POINTER, TAGS>::tags_type tags ) noexcept {
		return tagged<SMART_POINTER, TAGS>( move( ptr ), tags );
	}

}