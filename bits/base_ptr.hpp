#pragma once

#include "unique_ptr.hpp"

namespace ReVolta {

    template<typename T>
    class base_ptr {
    public:
        using element_type = T;
        using deleter_type = void (*)( T * );
        using underlying_type = unique_ptr<element_type, deleter_type>;
        using pointer = underlying_type::pointer;
    
    protected:
        using raw_type = underlying_type::raw_type;

    private:
        underlying_type m_ptr;

    public:
        constexpr base_ptr( void ) : base_ptr( nullptr ) {}

        constexpr base_ptr( nullptr_t ) : m_ptr( nullptr ) {}

        template<typename U, Deleter D>
            requires ( is_base_of_v<T, U> )
        constexpr base_ptr( U * ptr, [[maybe_unused]] D deleter = default_deleter<U>{} )
            : m_ptr( ptr, []( pointer p ) { 
                D{}( static_cast<U *>( p ) ); 
            } )
        {}

        template<typename U, Deleter D>
            requires ( is_base_of_v<T, U> )
        constexpr base_ptr( unique_ptr<U, D> && other ) : base_ptr( other.release(), other.get_deleter() ) {}

        constexpr base_ptr( base_ptr && other ) : m_ptr( move( other.m_ptr ) ) {}

        constexpr base_ptr( const base_ptr & ) = delete;

        constexpr ~base_ptr( void ) = default;

        constexpr inline base_ptr & operator=( base_ptr && other ) {
            this->m_ptr = move( other.m_ptr );
            return *this;
        }

        constexpr base_ptr & operator=( const base_ptr & ) = delete;

        constexpr inline pointer operator->( void ) const noexcept {
            return this->m_ptr.operator->();
        }

        constexpr inline typename add_lvalue_reference<element_type>::type & operator*( void ) const noexcept {
            return *this->m_ptr;
        }

        constexpr inline pointer get( void ) const noexcept {
            return this->m_ptr.get();
        }

        constexpr inline deleter_type & get_deleter( void ) noexcept {
            return this->m_ptr.get_deleter();
        }

        constexpr inline const deleter_type & get_deleter( void ) const noexcept {
            return this->m_ptr.get_deleter();
        }

        constexpr inline explicit operator pointer( void ) const noexcept {
            return static_cast<pointer>( m_ptr );
        }

        constexpr inline explicit operator bool( void ) const noexcept {
            return static_cast<bool>( m_ptr );
        }

        constexpr inline pointer release( void ) noexcept {
            return this->m_ptr.release();
        }

        constexpr inline void reset( pointer ptr = pointer{} ) noexcept {
            this->m_ptr.reset( ptr );
        }

        constexpr inline void swap( base_ptr & other ) noexcept {
            this->m_ptr.swap( other.m_ptr );
        }

    protected:
        constexpr inline raw_type & raw( void ) noexcept {
            return this->m_ptr.raw();
        }

        constexpr inline const raw_type & raw( void ) const noexcept {
            return this->m_ptr.raw();
        }
    };
    
}