#pragma once

namespace ReVolta {

    template<typename CONTAINER>
    inline constexpr auto begin( CONTAINER & container ) noexcept -> decltype( container.begin() ) {
        return container.begin();
    }

    template<typename CONTAINER>
    inline constexpr auto begin( const CONTAINER & container ) noexcept -> decltype( container.begin() ) {
        return container.begin();
    }

    template<typename CONTAINER>
    inline constexpr auto end( CONTAINER & container ) noexcept -> decltype( container.end() ) {
        return container.end();
    }

    template<typename CONTAINER>
    inline constexpr auto end( const CONTAINER & container ) noexcept -> decltype( container.end() ) {
        return container.end();
    }

}