#include "atomic_base.hpp"

namespace ReVolta {

    class mutex {
        volatile atomic_flag m_flag { false };

    public:
        /* Constructs the mutex. The mutex is in unlocked state after the constructor completes */
        constexpr mutex( void ) noexcept {}

        ~mutex( void ) = default;

        mutex( const mutex & ) noexcept = delete;

        mutex & operator=( const mutex & ) noexcept = delete;

        /* Locks the mutex. If another thread has already locked the mutex, a call to lock will block execution until the lock is acquired */
        void lock( void ) noexcept {
            /* TODO: operating system or thread library might provide a mutex 'lock()' implementation that blocks by suspending the calling process 
             * or thread while the mutex is locked -- instead of letting the caller waste CPU time spinning, it doesn't use any CPU time again 
             * until the mutex is unlocked. In practice, adaptive implementations are used that switch between busy-waiting and suspension depending 
             * on factors like priority, context switch time, etc. */
            while( m_flag.test_and_set() );
        }

        /* Tries to lock the mutex. Returns immediately. On successful lock acquisition returns true, otherwise returns false */
        bool try_lock( void ) noexcept {
            return m_flag.test_and_set();
        }

        /* Unlocks the mutex */
        void unlock( void ) noexcept {
            m_flag.clear();
        }
    };

    struct defer_lock_t { explicit defer_lock_t( void ) = default; };
    
    struct try_to_lock_t { explicit try_to_lock_t( void ) = default; };
    
    struct adopt_lock_t { explicit adopt_lock_t( void ) = default; };

    /* Do not acquire ownership of the mutex */
    inline constexpr defer_lock_t defer_lock;

    /* Try to acquire ownership of the mutex without blocking */
    inline constexpr try_to_lock_t try_to_lock;

    /* Assume the calling thread already has ownership of the mutex */
    inline constexpr adopt_lock_t adopt_lock;
}
