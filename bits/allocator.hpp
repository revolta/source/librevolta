#pragma once

#include <type_traits.hpp>

namespace ReVolta {

    template<typename T>
    struct fallback_allocator {
        using value_type = T;
        using pointer = value_type *;

        /* Default constructor */
        constexpr fallback_allocator( void ) noexcept = default;

        /* Converting constructor used by rebinding */
        template<typename U = T>
        constexpr fallback_allocator( const fallback_allocator<U> & ) noexcept {}

        template<typename U = T>
        constexpr fallback_allocator( fallback_allocator<U> && ) noexcept {}

        [[nodiscard]] pointer allocate( [[maybe_unused]] size_t n, [[maybe_unused]] const pointer hint = nullptr ) const noexcept {
            if constexpr ( is_array_v<T> ) {
                return new T[ n ];
            } else {
                return new T;
            }
        }

        void deallocate( pointer ptr, [[maybe_unused]] size_t n ) const noexcept {
            if constexpr ( is_array_v<T> ) {
                delete[] ptr;
            } else {
                delete ptr; 
            }
        }
    };

    template<typename T>
    struct forward_allocator {
        using value_type = T;
        using pointer = value_type *;

        constexpr forward_allocator( void ) noexcept = default;

        template<typename U = T>
        constexpr forward_allocator( const forward_allocator<U> & ) noexcept {}

        [[nodiscard]] pointer allocate( [[maybe_unused]] size_t n, [[maybe_unused]] const pointer hint = nullptr ) const noexcept;

        void deallocate( pointer ptr, [[maybe_unused]] size_t n ) const noexcept;
    };

    class no_default_allocator_defined;

    /* Primary template which needs to be specialized in order to define the default
     * allocator for particular type T */
    template<typename T>
    struct default_allocator {
#if defined ( _LIBREVOLTA_FREESTANDING )
        using type = no_default_allocator_defined;
#else
        using type = fallback_allocator<T>;
#endif
    };

    template<typename T>
    using allocator = default_allocator<T>::type;

}