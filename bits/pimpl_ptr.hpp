#pragma once

#include <bits/construct.hpp>

#include <cstddef.hpp>

namespace ReVolta {

    namespace Detail {

        template<typename T, size_t SIZE, size_t ALIGNMENT>
        struct is_valid_pimpl_ptr : integral_constant<bool, ( ( sizeof( T ) == SIZE ) && ( alignof( T ) >= ALIGNMENT ) )> {};

    }

    template<typename T, size_t SIZE, size_t ALIGNMENT>
        requires ( is_alignment_v<ALIGNMENT> && Detail::is_valid_pimpl_ptr<T, SIZE, ALIGNMENT>::value )
    class pimpl_ptr {
    public:
        using element_type = T;
        using pointer = element_type *;
        using const_pointer = element_type const *;
        

        using raw_type = alignas( ALIGNMENT ) byte [ sizeof( element_type ) ];
    private:

        raw_type m_data;

    public:
        template<typename... ARGUMENTS>
        pimpl_ptr( ARGUMENTS &&... arguments ) {
            /* Construct the instance of 'element_type' in-place of the pimpl_ptr<> aligned storage */
            construct_at( addressof( this->m_data ), forward<ARGUMENTS>( arguments )... );
        }

        pimpl_ptr( const pimpl_ptr & other ) {
            construct_at( addressof( this->m_data ), *other );
        }

        pimpl_ptr( pimpl_ptr && other ) {
            construct_at( addressof( this->m_data ), move( *other ) );
        }

        ~pimpl_ptr( void ) {
            reinterpret_cast<element_type *>( addressof( this->m_data )->~element_type() );
        }

        pimpl_ptr & operator=( const pimpl_ptr & other ) {
            pimpl_ptr{ other }.swap( *this );
            return *this;
        }

        pimpl_ptr & operator=( pimpl_ptr && other ) {
            pimpl_ptr{ move( other ) }.swap( *this );
        }

        element_type & operator*( void ) {
            return *( reinterpret_cast<element_type *>( addressof( this->m_data ) ) );
        }

        const element_type & operator*( void ) const {
            return *( reinterpret_cast<element_type const *>( addressof( this->m_data ) ) );
        }

        pointer operator->( void ) {
            return addressof( **this );
        }

        const_pointer operator->( void ) const {
            return addressof( **this );
        }

        void swap( pimpl_ptr & other ) {
            ReVolta::swap( **this, *other );
        }
    
    };
}