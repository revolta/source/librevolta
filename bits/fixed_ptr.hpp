#pragma once

namespace ReVolta {

    template <typename T>
    class fixed_ptr {
    public:
        using pointer = T *;
        using element_type = T;
        using raw_type = uintptr_t;

    private:
        constexpr static size_t pointer_mask = ~( alignof( T ) - 1 );

        const raw_type m_ptr;

    public:
        constexpr fixed_ptr( void ) noexcept = delete;

        constexpr fixed_ptr( fixed_ptr && ) noexcept = delete;

        inline constexpr fixed_ptr( uintptr_t p ) noexcept : m_ptr( p ) {}

        inline constexpr fixed_ptr( const fixed_ptr & other ) noexcept : m_ptr( other.m_ptr ) {}

        inline operator pointer( void ) const {
            return reinterpret_cast<pointer>( m_ptr );
        }

        template<typename U = T>
        inline operator U *( void ) const {
            return reinterpret_cast<U *>( m_ptr );
        }

        inline constexpr operator observer_ptr<element_type>( void ) const {
            return make_observer<element_type>( m_ptr );
        } 

        inline element_type & operator*( void ) const noexcept {
            return *get();
        }

        inline pointer operator->( void ) const {
            return get();
        }

        pointer get( void ) const noexcept {
            return reinterpret_cast<pointer>( m_ptr & pointer_mask );
        }

        constexpr void swap( fixed_ptr & other ) noexcept {
            using ReVolta::swap;
			swap( m_ptr, other.m_ptr );
        }

    protected:
		constexpr const raw_type & raw( void ) const noexcept {
			return m_ptr;
		}
    };

}