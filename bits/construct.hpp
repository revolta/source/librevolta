#pragma once

#include "type_traits.hpp"
#include "new.hpp"
#include "bits/move.hpp"
#include "bits/pointer_traits.hpp"

namespace ReVolta {

    template<typename T, typename... ARGUMENTS>
    constexpr T * construct_at( T * ptr, ARGUMENTS &&... arguments ) noexcept {
        return ::new ( const_cast<void *>( static_cast<const volatile void *>( ptr ) ) ) T( forward<ARGUMENTS>( arguments )... );
    }

    template<typename T>
    void destroy_at( T * ptr ) noexcept {
        if constexpr ( is_array<T>::value ) {
            for( auto & element : *ptr ) {
                destroy_at( addressof( element ) );
            }
        } else {
            ptr->~T();
        }
    }
}