#pragma once

#include "bits/move.hpp"

namespace ReVolta {

    template<typename T1, typename T2>
	struct pair {
		using first_type = T1;
		using second_type = T2;

		constexpr pair( void ) noexcept = default;

		constexpr pair( const first_type & x, const second_type & y ) noexcept : first( x ), second( y ) {}

		template<typename U1, typename U2>
		constexpr pair( U1 && x, U2 && y ) noexcept : first( forward<U1>( x ) ), second( forward<U2>( y ) ) {}

		template<typename U1, typename U2>
		pair( const pair<U1, U2> & other ) noexcept : first( other.first ), second( other.second ) {}

		template<typename U1, typename U2>
		constexpr pair( pair<U1, U2> && other ) noexcept : first( forward<U1>( other.first ) ), second( forward<U2>( other.second ) ) {}

		pair( const pair & ) noexcept = default;

		pair( pair && ) noexcept = default;

		constexpr pair & operator=( const pair & other ) noexcept {
			first = other.first;
			second = other.second;
			return *this;
		}

		template<typename U1, typename U2>
		constexpr pair & operator=( const pair<U1, U2> & other ) noexcept {
			first = other.first;
			second = other.second;
			return *this;
		}

		constexpr pair & operator=( pair && other ) noexcept {
			first = forward<first_type>( other.first );
			second = forward<second_type>( other.second );
			return *this;
		}

		template<typename U1, typename U2>
		constexpr pair & operator=( pair<U1, U2> && other ) noexcept {
			first = forward<U1>( other.first );
			second = forward<U2>( other.second );
			return *this;
		}

		constexpr void swap( pair & other ) noexcept {
			using ReVolta::swap;
			swap( first, other.first );
			swap( second, other.second );
		}

		[[no_unique_address]] first_type first;
		[[no_unique_address]] second_type second;
	};

	/* Deduction guide */
	template<typename T1, typename T2> pair( T1, T2 ) -> pair<T1, T2>;

	template<typename T1, typename T2>
	inline pair<T1, T2> make_pair( T1 first, T2 second ) noexcept {
		return pair<T1, T2>( first, second );
	}

	template<typename T1, typename T2>
	constexpr pair<T1, T2> make_pair( T1 && first, T2 && second ) noexcept {
		return pair<T1, T2>( forward<T1>( first ), forward<T2>( second ) );
	}

	/* TODO: pair comparison operators */

}