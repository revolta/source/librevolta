#pragma once

#include "bits/iterator_concepts.hpp"
#include "bits/pair.hpp"
#include "bits/construct.hpp"

namespace ReVolta {

    /* Move-construct from the range [first, first+count) into destination. */
	template<input_iterator IT, typename SIZE, forward_iterator NOTHROW_FORWARD_IT>
	pair<IT, NOTHROW_FORWARD_IT> uninitialized_move_n( IT first, SIZE count, NOTHROW_FORWARD_IT destination ) {
		for( ; count > 0; ++destination, ++first, --count ) {
			construct_at( addressof( *destination ), move( *first ) );
		}
		return pair<IT, NOTHROW_FORWARD_IT>{ first, destination };
	}

	template<input_iterator IT, forward_iterator NOTHROW_FORWARD_IT>
	NOTHROW_FORWARD_IT uninitialized_move( IT first, IT last, NOTHROW_FORWARD_IT destination ) {
		for( ; first != last; ++destination, ++first ) {
			construct_at( addressof( *destination ), move( *first ) );
		}
		return destination;
	}

}