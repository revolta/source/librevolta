
#pragma once

#include "utility.hpp"

namespace ReVolta {

    template<typename DERIVED, typename BASE>
    concept SafelyObservable = 
        is_convertible<DERIVED *, BASE *>::value && !is_array<DERIVED>::value
    ;

    /* observer_ptr is a non-owning pointer, the observer. The observer stores a pointer
     * to a watched object. An observer_ptr may also have no watched object.
     * An observer is not responsible in any way for the watched object. */
    template<typename T>
    class observer_ptr {
        /* Make all other observer_ptr types friends in order to have an access to raw() */
        template<typename U>
        friend class observer_ptr;

    public:
        using pointer = T*;
        using element_type = T;
        using raw_type = uintptr_t;

    private:
        constexpr static size_t pointer_mask = ~( alignof( T ) - 1 );

        raw_type m_ptr;

    public:
        /* Constructs 'observer_ptr' that observes nothing */
        constexpr observer_ptr( void ) noexcept : m_ptr( 0UL ) {}

        constexpr observer_ptr( nullptr_t ) noexcept : observer_ptr() {}

        /* Constructs 'observer_ptr' that watches 'ptr' */
        constexpr explicit observer_ptr( pointer ptr ) noexcept : m_ptr( bit_cast<raw_type>( ptr ) ) {}

        /* Copy constructor */
        constexpr observer_ptr( const observer_ptr & other ) noexcept : m_ptr( other.raw() ) {}

        /* Converting copy constructor */
        template<typename W> 
            requires ( SafelyObservable<W, T> )
        constexpr observer_ptr( const observer_ptr<W> & other ) noexcept : m_ptr( other.raw() ) {}

        /* Move constructor */
        constexpr observer_ptr( observer_ptr && other ) noexcept : m_ptr( move( other.raw() ) ) {
            other.release();
        }

        /* Converting move constructor */
        template<typename W> 
            requires ( SafelyObservable<W, T> )
        constexpr observer_ptr( observer_ptr<W> && other ) noexcept : m_ptr( move( other.raw() ) ) {
            other.release();
        }

        /* Destructor */
        constexpr ~observer_ptr( void ) noexcept = default;

        constexpr typename add_lvalue_reference<element_type>::type operator*( void ) const noexcept {
            return *this->get();
        }

        constexpr element_type * operator->( void ) const noexcept {
            return this->get();
        }

        /* Copy assignment operator */
        constexpr observer_ptr & operator=( const observer_ptr & other ) noexcept {
            observer_ptr( other ).swap( *this );
            return *this;
        }

        /* Move assignment operator */
        constexpr observer_ptr & operator=( observer_ptr && other ) noexcept {
            observer_ptr( move( other ) ).swap( *this );
            return *this;
        }

        /* TODO: Converting copy assignment operator */

        /* TODO: Converting move assignment operator */

        constexpr void reset( pointer ptr = nullptr ) noexcept {
            observer_ptr( ptr ).swap( *this );
        }

        constexpr pointer release( void ) noexcept {
            pointer ptr = this->get();
            this->reset( nullptr );
            return ptr;
        }

        constexpr void swap( observer_ptr & other ) noexcept {
            ReVolta::swap( this->m_ptr, other.m_ptr );
        }

        constexpr pointer get( void ) const noexcept {
            return bit_cast<pointer>( this->m_ptr & pointer_mask );
        }

        constexpr explicit operator bool( void ) const noexcept {
            return ( this->get() != nullptr );
        }

        constexpr explicit operator pointer( void ) const noexcept {
            return this->get();
        }

        constexpr element_type & operator[]( size_t n ) const {
            return this->get()[ n ];
        }

    protected:
    	constexpr raw_type & raw( void ) noexcept {
			return this->m_ptr;
		}

        constexpr const raw_type & raw( void ) const noexcept {
			return this->m_ptr;
		}
    };

	template<typename T, typename U>
	constexpr inline bool operator==( const observer_ptr<T> & x, const observer_ptr<U> & y ) noexcept {
		return ( x.get() == y.get() );
	}

	template<typename T>
	constexpr inline bool operator==( const observer_ptr<T> & x, nullptr_t ) noexcept {
		return !x;
	}

	template<typename T>
	constexpr inline bool operator==( nullptr_t, const observer_ptr<T> & x ) noexcept {
		return !x;
	}

	template<typename T, typename U>
	constexpr inline bool operator!=( const observer_ptr<T> & x, const observer_ptr<U> & y ) noexcept {
		return ( x.get() != y.get() );
	}

	template<typename T>
	constexpr inline bool operator!=( const observer_ptr<T> & x, nullptr_t ) noexcept {
		return static_cast<bool>( x );
	}

	template<typename T>
	constexpr inline bool operator!=( nullptr_t, const observer_ptr<T> & x ) noexcept {
		return static_cast<bool>( x );
	}

    template<typename W>
    constexpr inline observer_ptr<W> make_observer( uintptr_t raw ) noexcept {
        return observer_ptr( bit_cast<W *>( raw ) );
    }

    template<typename W>
    constexpr inline observer_ptr<W> make_observer( const W * ptr ) noexcept {
        return observer_ptr( const_cast<W *>( ptr ) );
    }

    template<typename B, typename T>
    constexpr inline observer_ptr<B> make_observer( T * ptr ) noexcept {
        return observer_ptr( static_cast<B *>( ptr ) );
    }

    template<typename B, typename T>
    constexpr inline observer_ptr<B> make_observer( const observer_ptr<T> & ptr ) noexcept {
        /* FIXME: Try to avoid the usage of cast - use converting constructor instead */
        return observer_ptr( bit_cast<B *>( ptr.get() ) );
    }

    template<typename P>
        requires requires( const P & p ) { p.get(); }
    constexpr inline observer_ptr<typename P::element_type> make_observer( const P & ptr ) noexcept {
        return observer_ptr<typename P::element_type>{ ptr.get() };
    }

    template<typename B, typename T>
        requires ( is_base_of_v<B, T> )
    constexpr inline observer_ptr<B> make_observer( const unique_ptr<T> & ptr ) {
        return observer_ptr<B>( static_cast<B *>( ptr.get() ) );
    }

}