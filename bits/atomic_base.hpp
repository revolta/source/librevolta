#pragma once

#include "cstdint.hpp"
#include "utility.hpp"

namespace ReVolta {

    /* See https://gcc.gnu.org/onlinedocs/gcc/_005f_005fatomic-Builtins.html */

    enum class memory_order : uint32_t {
        RELAXED,    /* Relaxed operation: there are no synchronization or ordering constraints imposed on other reads or writes,
                     * only this operation's atomicity is guaranteed */
                    
        CONSUME,    /* A load operation with this memory order performs a consume operation on the affected memory location: 
                     * no reads or writes in the current thread dependent on the value currently loaded can be reordered before this load. 
                     * Writes to data-dependent variables in other threads that release the same atomic variable are visible in the current thread. 
                     * On most platforms, this affects compiler optimizations only */

        ACQUIRE,    /* A load operation with this memory order performs the acquire operation on the affected memory location: 
                     * no reads or writes in the current thread can be reordered before this load. All writes in other threads 
                     * that release the same atomic variable are visible in the current thread */

        RELEASE,    /* A store operation with this memory order performs the release operation: no reads or writes in the current 
                     * thread can be reordered after this store. All writes in the current thread are visible in other threads that 
                     * acquire the same atomic variable and writes that carry a dependency into the atomic variable become visible 
                     * in other threads that consume the same atomic */
        
        ACQ_REL,    /* A read-modify-write operation with this memory order is both an acquire operation and a release operation. 
                     * No memory reads or writes in the current thread can be reordered before or after this store. All writes 
                     * in other threads that release the same atomic variable are visible before the modification and the modification 
                     * is visible in other threads that acquire the same atomic variable. */
                    
        SEQ_CST    /* A load operation with this memory order performs an acquire operation, a store performs a release operation,
                    * and read-modify-write performs both an acquire operation and a release operation, plus a single total order
                    * exists in which all threads observe all modifications in the same order */ 
    };

    enum class memory_order_modifier : uint32_t {
        MASK            = 0x0000FFFF,
        MODIFIER_MASK   = 0xFFFF0000,
        HLE_ACQUIRE     = 0x00010000,
        HLE_RELEASE     = 0x00020000
    };

    constexpr memory_order operator|( memory_order order, memory_order_modifier modifier ) noexcept {
        return memory_order( to_underlying( order ) | to_underlying( modifier ) );
    }

     constexpr memory_order operator&( memory_order order, memory_order_modifier modifier ) noexcept {
        return memory_order( to_underlying( order ) & to_underlying( modifier ) );
    }

    class atomic_flag {
        bool m_value { false };

    public:
        atomic_flag( void ) noexcept = default;

        ~atomic_flag( void ) noexcept = default;

        atomic_flag( const atomic_flag & ) = delete;

        atomic_flag & operator=( const atomic_flag & ) noexcept = delete;

        atomic_flag & operator=( const atomic_flag & ) volatile = delete;

        constexpr atomic_flag( bool value ) noexcept : m_value( value ) {}

        inline bool test_and_set( memory_order order = memory_order::SEQ_CST ) noexcept {
            return __atomic_test_and_set( &m_value, to_underlying( order ) );
        }

        inline bool test_and_set( memory_order order = memory_order::SEQ_CST ) volatile noexcept {
            return __atomic_test_and_set( &m_value, to_underlying( order ) );
        }

        inline void clear( memory_order order = memory_order::SEQ_CST ) noexcept {
            __atomic_clear( &m_value, to_underlying( order ) );
        }

        inline void clear( memory_order order = memory_order::SEQ_CST ) volatile noexcept {
            __atomic_clear( &m_value, to_underlying( order ) );
        }
    };

    /* TODO: DO NOT COMMIT UNTIL IMPLEMENTED */
#if false
    /* TODO: Assuming T is an integral scalar type 1, 2, 4 or 8bytes as those types are expected 
     * by GCC built-ins 
     * --> contrain by concept checking that? */
    template<typename T>
    class atomic_base {
    public:
        using value_type = T;
        using difference_type = value_type;

    private:
        static constexpr int alignment = sizeof( value_type ) > alignof( value_type ) ? sizeof( value_type ) : alignof( value_type );

        alignas( alignment ) value_type m_value { 0 };

    public:
        atomic_base( void ) noexcept = default;

        ~atomic_base( void ) noexcept = default;

        atomic_base( const atomic_base & ) noexcept = delete;

        atomic_base & operator=( const atomic_base & ) noexcept = delete;

        atomic_base & operator=( const atomic_base & ) volatile = delete;

        constexpr atomic_base( value_type value ) noexcept : m_value( value ) {}

        operator value_type( void ) const noexcept {
            return load();
        }

        operator value_type( void ) const volatile noexcept {
            return load();
        }

        value_type operator=( value_type value ) noexcept {
            store( value );
            return value;
        }

        value_type operator=( value_type value ) volatile noexcept {
            store( value );
            return value;
        }

    private:
        /* #397 */
        void store( value_type value, memory_order order = memory_order::SEQ_CST ) noexcept {

        }
    };
#endif

}