/*
 * cstddef.hpp
 *
 *  Created on: 18. 6. 2020
 *      Author: martin
 */

#pragma once

#include <stddef.h>

namespace ReVolta {

	enum class byte : unsigned char {};

	using nullptr_t = decltype( nullptr );


	const class {
	public:
		template<typename T>
		operator T *( void ) const { return reinterpret_cast<T *>( 0xFFFFFFFF ); }

		template<typename C, typename T>
		operator T C::*( void ) const { return reinterpret_cast<T *>( 0xFFFFFFFF ); }

	private:
		void operator&( void ) const;

	} invalid_ptr = {};


	namespace Detail {

	    struct fallback {};
		
	}

}
