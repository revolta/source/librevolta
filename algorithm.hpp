#pragma once

#include "initializer_list.hpp"
#include "iterator.hpp"

namespace ReVolta {

	template<input_iterator I, typename UNARY_FUNCTION>
	constexpr UNARY_FUNCTION for_each( I first, I last, UNARY_FUNCTION function ) {
		for( ; first != last; ++first ) {
			function( *first );
		}
		/* Implicit move since C++11 */
		return function;
	}

	template<typename T>
	inline constexpr T const & max( T const & value ) noexcept {
		return value;
	}

	template<typename T, typename ... Ts>
	inline constexpr T const & max( T const & value_1, T const & value_2, Ts const & ... others ) noexcept {
		return max( ( value_1 > value_2 ) ? value_1 : value_2, others... );
	}

	template<typename INPUT_IT, typename OUTPUT_IT>
		requires ( input_iterator<INPUT_IT> && output_iterator<OUTPUT_IT, typename iterator_traits<INPUT_IT>::value_type> )
	constexpr OUTPUT_IT copy( INPUT_IT first, INPUT_IT last, OUTPUT_IT other_first ) noexcept {
		while( first != last ) {
			*other_first++ = *first++;
		}
		return other_first;
	}

	template<typename INPUT_IT, typename OUTPUT_IT, typename UNARY_PREDICATE>
		requires ( input_iterator<INPUT_IT> && output_iterator<OUTPUT_IT, typename iterator_traits<INPUT_IT>::value_type> )
	constexpr OUTPUT_IT copy_if( INPUT_IT first, INPUT_IT last, OUTPUT_IT other_first, UNARY_PREDICATE predicate ) noexcept {
		while( first != last ) {
			if( predicate( *first ) ) {
				*other_first++ = *first;
			}
			first++;
		}
	}

	/* Returns true if the range [first1, last1) is equal to the range [first2, first2 + (last1 - first1)), and false otherwise */
	template<input_iterator I1, input_iterator I2 >
	constexpr bool equal( I1 first1, I1 last1, I2 first2 ) noexcept {
		for ( ; first1 != last1; ++first1, ++first2 ) {
			if ( !( *first1 == *first2 ) ) {
				return false;
			}
		}
		return true;
	}

	template<forward_iterator I1, forward_iterator I2>
	constexpr void iter_swap( I1 a, I2 b ) {
		/* Swap dereferenced iterators which means to swap the values the iterators are point to */
		ReVolta::swap( *a, *b );
	}

	template<forward_iterator I1, forward_iterator I2>
	constexpr void node_swap( I1 a, I2 b ) {
		a.node().swap( b.node() );
	}

	template<forward_iterator I1, forward_iterator I2>
	constexpr I2 swap_ranges( I1 first1, I1 last1, I2 first2 ) {
		for( ; first1 != last1; ++first1, ( void )++first2 )
			iter_swap( first1, first2 );
		return first2;
	}

	template<typename OUTPUT_IT, typename SIZE, typename T>
		requires ( output_iterator<OUTPUT_IT, typename iterator_traits<OUTPUT_IT>::value_type> )
	constexpr OUTPUT_IT fill_n( OUTPUT_IT first, SIZE count, const T & value ) {
		for( SIZE i = 0; i < count; i++ ) {
			*first++ = value;
		}
		return first;
	}

	/* Returns iterator to the new location of the element pointed by first. Equal to first + (last - n_first) */
	template<forward_iterator I >
	constexpr I rotate( I first, I n_first, I last ) {
		if( first == n_first ) return last;
		if( n_first == last ) return first;

		I read { n_first };
		I write { first };
		I next_read { first };

		while( read != last ) {
			if( write == next_read ) {
				next_read = read;
				iter_swap( write++, read++ );
			}
		}

		/* Recursive call itself */
		rotate( write, next_read, last );
		return write;
	}

	/* Reverses the order of elements in the range [first, last). Behaves as if applying iter_swap to every pair of 
	 * iterators 'first + i' and '(last - i) - 1' for each non-negaitve 'i < ( last - first ) / 2' */
#if false
	template<bidirectional_iterator I>
#else
	template<bidirectional_iterator I, void (* SWAP)( I, I ) = iter_swap>
#endif
	constexpr void reverse( I first, I last ) {
		if constexpr ( is_base_of_v<random_access_iterator_tag, typename iterator_traits<I>::iterator_category> ) {
			if( first == last ) return;
			for( --last; first < last; ++first, --last ) {
#if false
				iter_swap( first, last );
#else
				SWAP( first, last );
#endif
			}
		} else {
			while( ( first != last ) && ( first != --last ) ) {
#if false
				iter_swap( first, last );
#else
				SWAP( first++, last );
#endif
			}
		}
	}

}
