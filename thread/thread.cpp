#include <thread.hpp>

/* The userspace thread implementation goes here */

namespace ReVolta {

    unsigned int thread::hardware_concurrency( void ) noexcept {
        /* FIXME: Shall return 0 only if there is no way to get the real value */
        return 0;
    }

    void thread::join( void ) {

    }

    void thread::detach( void ) {

    }

    thread::id this_thread::get_id( void ) noexcept {
        /* TODO: Originally constructs new 'thread::id' by '__gthread_self()' */
    }

    void this_thread::yield( void ) noexcept {
        /* TODO: Originally refers to __gthread_yield() */
    }

    namespace Detail {

        void sleep_for( chrono::seconds sec, chrono::nanoseconds nanosec ) {
            /* TODO: Use chrono::steady_clock there */
        }

    }

}