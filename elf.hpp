#pragma once

#include "cstdint.hpp"
#include "utility.hpp"

namespace ReVolta {

	/* http://www.vishalchovatiya.com/understand-elf-file-format/
	 * https://en.wikipedia.org/wiki/Executable_and_Linkable_Format
	 * https://wiki.osdev.org/ELF */

    enum class elf_type {
        _32_BIT,
        _64_BIT
    };

    #define USE_E_IDENT_ABI	false

    template<elf_type> struct elf_traits;

    template<>
    struct elf_traits<elf_type::_32_BIT> {
        using addr_type  = uint32_t;    
        using half_type  = uint16_t;
        using off_type   = uint32_t;
        using sword_type = int32_t;
        using word_type  = uint32_t;
    };

    template<>
    struct elf_traits<elf_type::_64_BIT> {
        using addr_type  = uint64_t;    
        using half_type  = uint16_t;
        using off_type   = uint64_t;
        using sword_type = int32_t;
        using word_type  = uint32_t;
    };

    template<elf_type TYPE>
    class elf {
    public: 
        using addr_type  = typename elf_traits<TYPE>::addr_type;
        using half_type  = typename elf_traits<TYPE>::half_type;
        using off_type   = typename elf_traits<TYPE>::off_type;
        using sword_type = typename elf_traits<TYPE>::sword_type;
        using word_type  = typename elf_traits<TYPE>::word_type;

        class program_header {
        public:
            enum class p_type_t : word_type {
				PT_NULL		    = 0x00000000,	/* Program header table entry unused */
				PT_LOAD 	    = 0x00000001,	/* Loadable segment */
				PT_DYNAMIC	    = 0x00000002,	/* Dynamic linking information */
				PT_INTERP	    = 0x00000003,	/* Interpreter information */
				PT_NOTE		    = 0x00000004,	/* Auxiliary information */
				PT_SHLIB	    = 0x00000005,	/* reserved */
				PT_TLS		    = 0x00000007,	/* Thread Local Storage template */
				PT_LOOS		    = 0x60000000,
				PT_HIOS		    = 0x6FFFFFFF,
				PT_LOPROC	    = 0x70000000,
				PT_HIPROC	    = 0x7FFFFFFF
			};

			enum class p_flags_t : word_type {
				PF_X		    = 0x1,			/* Execute */
				PF_W		    = 0x2,			/* Write */
				PF_R		    = 0x4,			/* Read */
				PF_MASKPROC	    = 0xF0000000	/* Unspecified */
			};

			inline p_type_t get_segment_type( void ) const noexcept {
				return static_cast<p_type_t>( p_type );
			}

			inline size_t get_segment_offset( void ) const noexcept {
				return static_cast<size_t>( p_offset );
			}

			/* TODO: Replace 'void *' by some concrete pointer type */
			/* TODO: Think about better naming as the function is returning pointer, not the address */
			inline void * get_segment_virtual_address( void ) const noexcept {
				return reinterpret_cast<void *>( p_vaddr );
			}

			/* TODO: Replace 'void *' by some concrete pointer type */
			/* TODO: Think about better naming as the function is returning pointer, not the address */
			inline void * get_segment_physical_address( void ) const noexcept {
				/* NOTE: System V ignores physical addressing for userspace applications */
				return reinterpret_cast<void *>( p_paddr );
			}

			inline size_t get_segment_file_image_size( void ) const noexcept {
				return static_cast<size_t>( p_filesz );
			}

			inline size_t get_segment_memory_size( void ) const noexcept {
				return static_cast<size_t>( p_memsz );
			}

			inline bool check_segment_permission( const p_flags_t permission ) const noexcept {
				return ( p_flags & to_underlying( permission ) ) != 0;
			}

			inline size_t get_segment_alignment( void ) const noexcept {
				// FIXME return check_alignment() ? static_cast<size_t>( p_align ) : __SIZE_MAX__;
                return static_cast<size_t>( p_align );
			}

        private:
            word_type   p_type;		    /* Segment type */
			off_type    p_offset;	    /* Offset of the segment in the file image */
			addr_type   p_vaddr;	    /* Virtual address of the segment in memory */
			addr_type   p_paddr;	    /* Segment physical address */
			word_type   p_filesz;	    /* Size in bytes of the segment in the file image */
			word_type   p_memsz;	    /* Size in bytes of the segment in memory */
			word_type   p_flags;	    /* Segment dependent flags (position for 32bit structure) */
			word_type   p_align;	    /* 0 and 1 specify no alignment. Otherwise should be positive, integral power of 2, with p_vaddr equating p_offset modulus p_align */
		} __attribute__(( packed ));

        class elf_header {
        public:
            enum class e_type_t : half_type {
                ET_NONE		    = 0x0000,
                ET_REL		    = 0x0001,
                ET_EXEC		    = 0x0002,
                ET_DYN		    = 0x0003,
                ET_CORE		    = 0x0004,
                ET_LOOS		    = 0xFE00,
                ET_HIOS		    = 0xFEFF,
                ET_LOPROC	    = 0xFF00,
                ET_HIPROC	    = 0xFFFF
            };

            /* TODO: Try to invent any better way to perform the chain of bool tests */
            bool is_valid( void ) const noexcept {
                bool valid { true };
                /* ELF file is said to be valid once the magic numbers match to 0x7F 'E' 'L' 'F' */
                if( !( get_identification( e_ident_index::EI_MAG0 ) == 0x7F ) ||
                    !( get_identification( e_ident_index::EI_MAG1 ) == 'E' ) ||
                    !( get_identification( e_ident_index::EI_MAG2 ) == 'L' ) ||
                    !( get_identification( e_ident_index::EI_MAG3 ) == 'F' ) ) {
                    valid = false;
                }
                /* FIXME: Made it generic for 32 and 64bits --> constexpr if */
                /* Must be 32-bit elf */
                if( get_ident_class() != e_ident_class_t::ELFCLASS32 ) {
                    valid = false;
                }
                /* TODO: Data encoding must match the architecture */
                /* Elf header identification version must match EV_CURRENT */
                if( get_ident_version() != e_ident_version_t::EV_CURRENT ) {
                    valid = false;
                }
                /* TODO: Target machine must match */
                /* 'e_version' must match EV_CURRENT */
                if( get_version() != e_version_t::EV_CURRENT ) {
                    valid = false;
                }
                /* Verify the header sizes */
                if( get_elf_header_size() != sizeof( elf_header ) ) {
                    valid = false;
                }
                if( get_program_header_table_entry_size() != sizeof( program_header ) ) {
                    valid = false;
                }
                return valid;
            }

			inline e_type_t get_type( void ) const noexcept {
				return static_cast<e_type_t>( e_type );
			}

            /* TODO: use real entry point address type - maybe a function pointer to main()? */
			inline void * get_entry( void ) const noexcept {
				return reinterpret_cast<void *>( e_entry );
			}

			inline size_t get_elf_header_size( void ) const noexcept {
				return static_cast<size_t>( e_ehsize );
			}

			inline size_t get_program_header_table_offset( void ) const noexcept {
				/* As long as size_t is same width as Elf32_Off it is safe to reinterpret */
				return static_cast<size_t>( e_phoff );
			}

			inline size_t get_program_header_table_entry_size( void ) const noexcept {
				return static_cast<size_t>( e_phentsize );
			}

			inline uint16_t get_program_header_table_entry_number( void ) const noexcept {
				return static_cast<uint16_t>( e_phnum );
			}

			inline size_t get_section_header_table_offset( void ) const noexcept {
				return static_cast<size_t>( e_shoff );
			}

			inline size_t get_section_header_table_entry_size( void ) const noexcept {
				return static_cast<size_t>( e_shentsize );
			}

			inline uint16_t get_section_header_table_entry_number( void ) const noexcept {
				return static_cast<uint16_t>( e_shnum );
			}

        private:
        	enum class e_ident_index : unsigned char {
				EI_MAG0 		= 0,	    /* File identification */
				EI_MAG1			= 1,	    /* File identification */
				EI_MAG2			= 2,	    /* File identification */
				EI_MAG3			= 3,	    /* File identification */
				EI_CLASS		= 4,	    /* File class */
				EI_DATA			= 5,	    /* Data encoding */
				EI_VERSION		= 6,	    /* ELF header version number */
#if USE_E_IDENT_ABI 
				EI_OSABI		= 7,	    /* Target operating system ABI */
				EI_ABIVERSION	= 8,	    /* ABI version */
				EI_PAD			= 9,	    /* Start of padding bytes */
#else   
				EI_PAD			= 7,	    /* Start of padding bytes */
#endif  
				EI_NIDENT		= 16	    /* Size of e_ident[] */
			};

			enum class e_ident_class_t : unsigned char {
				ELFCLASSNONE	= 0,	    /* Invalid class */
				ELFCLASS32		= 1,	    /* 32bit ELF file */
				ELFCLASS64		= 2		    /* 64bit ELF file */
			};

			enum class e_ident_data_t : unsigned char {
				ELFDATANONE		= 0,	    /* Invalid data encoding */
				ELFDATA2LSB		= 1,	    /* Little endian */
				ELFDATA2MSB		= 2		    /* Big endian */
			};

			enum class e_ident_version_t : unsigned char {
				EV_NONE		    = 0,        /* Invalid version */
				EV_CURRENT	    = 1	        /* Current version */
			};      

#if USE_E_IDENT_ABI
			enum class e_ident_osabi_t : unsigned char {
				SYSTEM_V,
				HP_UX,
				NETBSD,
				LINUX,
				GNU_HURD,
				SOLARIS,
				AIX,
				IRIX,
				FREEBSD,
				TRU64,
				NOVELL_MODESTO,
				OPENBSD,
				OPENVMS,
				NONSTOP_KERNEL,
				AROS,
				FENIX_OS,
				CLOUDABI,
				STRATIX_TECHNOLOGIES_OPENVOS
			};
#endif

			enum class e_machine_t : half_type {
				EM_NONE		    = 0x00,
				EM_SPARC	    = 0x02,
				EM_X86		    = 0x03,
				EM_MIPS		    = 0x08,
				EM_POWERPC	    = 0x14,
				EM_S390		    = 0x16,
				EM_ARM		    = 0x28,
				EM_SUPERH	    = 0x2A,
				EM_IA_64	    = 0x32,
				EM_AMD64	    = 0x3E,
				EM_AARCH64	    = 0xB7,
				EM_RISCV	    = 0xF3
			};

			enum class e_version_t : word_type {
				EV_NONE		    = 0,	    /* Invalid version */
				EV_CURRENT	    = 1		    /* Current version */
			};

            inline unsigned char get_identification( const e_ident_index index ) const noexcept {
				return e_ident[ to_underlying( index ) ];
			}

			inline e_ident_class_t get_ident_class( void ) const noexcept {
				return static_cast<e_ident_class_t>( e_ident[ to_underlying( e_ident_index::EI_CLASS ) ] );
			}

			inline e_ident_data_t get_ident_data_encoding( void ) const noexcept {
				return static_cast<e_ident_data_t>( e_ident[ to_underlying( e_ident_index::EI_DATA ) ] );
			}

			inline e_ident_version_t get_ident_version( void ) const noexcept {
				return static_cast<e_ident_version_t>( e_ident[ to_underlying( e_ident_index::EI_VERSION ) ] );
			}

#if USE_E_IDENT_ABI
			inline e_ident_osabi_t get_ident_osabi( void ) const noexcept {
				return static_cast<e_ident_osabi_t>( e_ident[ to_underlying( e_ident_index::EI_OSABI ) ] );
			}

			inline unsigned char get_ident_abiversion( void ) const noexcept {
				return e_ident[ to_underlying( e_ident_index::EI_ABIVERSION ) ];
			}
#endif

			inline e_machine_t get_machine( void ) const noexcept {
				return static_cast<e_machine_t>( e_machine );
			}

			inline e_version_t get_version( void ) const noexcept {
				return static_cast<e_version_t>( e_version );
			}

            unsigned char   e_ident[ to_underlying( e_ident_index::EI_NIDENT ) ];	/* Elf file identification */
            half_type       e_type;			/* Identifies object file type */
            half_type       e_machine;		/* Instruction set architecture */
            word_type       e_version;
            addr_type       e_entry;		/* Memory address of the entry point from where the process starts executing */
            off_type        e_phoff;		/* Offset pointing to the start of the program header table */
            off_type        e_shoff;		/* Offset pointing to the start of the section header table */
            word_type       e_flags;		/* Interpretation of this field depends on the target architecture */
            half_type       e_ehsize;		/* Size of this header.52Bytes on 32bit ELF, 64Bytes on 64bit ELF */
            half_type       e_phentsize;	/* Size of program header table entry */
            half_type       e_phnum;		/* Number of entries in the program header table */
            half_type       e_shentsize;	/* Size of section header table entry */
            half_type       e_shnum;		/* Number of entries in the section header table */
            half_type       e_shstrndx;		/* Index of the section header table entry that contains the section names */	
        } __attribute__(( packed ));

    };

    /* Check the correct size of ELF header */
	static_assert( sizeof( typename elf<elf_type::_32_BIT>::elf_header ) == 52 );
    static_assert( sizeof( typename elf<elf_type::_64_BIT>::elf_header ) == 64 );

}