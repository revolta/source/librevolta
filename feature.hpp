/*
 * feature.hpp
 *
 *  Created on: 26. 8. 2020
 *      Author: martin
 */

#pragma once

namespace ReVolta {

	/* Once any feature within the platform is not supported, this type shall be used */
	class NotSupported; /* NOTE: Must never be defined */

	/* NOTE: The is_supported<FEATURE> should be used together with "if constexpr ()", i.e:
	 * 	if constexpr ( is_supported<Platform::Output> ) {
	 *		cout << "Output Supported." << endl;
	 *	} else {
	 *		cerr << "Output not supported." << endl;
	 *	}
	 */
	template<typename FEATURE, typename = void>
	constexpr bool is_supported = false;

	/* Once the class / class template is defined, not just declared, return true */
	template<typename FEATURE>
	constexpr bool is_supported<FEATURE, decltype( sizeof( FEATURE ), void() )> = true;

}
