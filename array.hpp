#pragma once

#include "type_traits.hpp"
#include "concepts.hpp"
#include "cstddef.hpp"
#include "algorithm.hpp"
#include "utility.hpp"
#include "iterator.hpp"

namespace ReVolta {

    template<typename T, size_t N>
    struct array {
        using value_type = T;
        using size_type = size_t;
        using difference_type = ptrdiff_t;
        using reference = value_type &;
        using const_reference = const value_type &;
        using pointer = value_type *;
        using const_pointer = const value_type *;
        using iterator = value_type *;
        using const_iterator = const value_type *;
        using reverse_iterator = ReVolta::reverse_iterator<iterator>;
        using const_reverse_iterator = ReVolta::reverse_iterator<const_iterator>;

        /* NOTE: No implicitly defined constructors/destructor/copy for aggregate type */

        /* The encapsulated fixed size array */
        value_type m_elements[ N ];

        /* Returns a reference to the element at specified location pos, with bounds checking. */
        constexpr reference at( size_type position ) {
            if( position >= size() ) {
                /* FIXME: If pos is not within the range of the container, an exception of type std::out_of_range is thrown. */
            }
            return m_elements[ position ];
        }

        constexpr const_reference at( size_type position ) const {
            if( position >= size() ) {
                /* FIXME: If pos is not within the range of the container, an exception of type std::out_of_range is thrown. */
            }
            return m_elements[ position ];
        }

        constexpr reference operator[]( size_type position ) {
            return this->at( position );
        }

        constexpr const_reference operator[]( size_type position ) const {
            return this->at( position );
        }

        constexpr reference front( void ) {
            return this->at( 0 );
        }

        constexpr const_reference front( void ) const {
            return this->at( 0 );
        }

        /* Returns a reference to the last element in the container. */
        constexpr reference back( void ) {
            return this->at( size() - 1 );
        }

        constexpr const_reference back( void ) const {
            return this->at( size() - 1 );
        }

        /* Returns pointer to the underlying array serving as element storage. The pointer is such that range [data(); data() + size()) 
         * is always a valid range, even if the container is empty (data() is not dereferenceable in that case) */
        constexpr pointer data( void ) noexcept {
            return m_elements;
        }

        constexpr const_pointer data( void ) const noexcept {
            return m_elements;
        }

        constexpr iterator begin( void ) noexcept {
            return { addressof( m_elements[ 0 ] ) };
        }

        constexpr const_iterator cbegin( void ) const noexcept {
            return { addressof( m_elements[ 0 ] ) };
        }

        constexpr iterator end( void ) noexcept {
            return { addressof( m_elements[ N ] ) };
        }

        constexpr const_iterator cend( void ) const noexcept {
            return { addressof( m_elements[ N ] ) };
        }

        constexpr reverse_iterator rbegin( void ) noexcept {
            return { addressof( m_elements[ N - 1 ] ) };
        }

        constexpr const_reverse_iterator crbegin( void ) const noexcept {
            return { addressof( m_elements[ N - 1 ] ) };
        }

        constexpr reverse_iterator rend( void ) noexcept {
            return { addressof( m_elements[ 0 ] ) - 1 };
        }

        constexpr const_reverse_iterator crend( void ) const noexcept {
            return { addressof( m_elements[ 0 ] ) - 1 };
        }

        /* Checks if the container has no elements */
        [[nodiscard]] constexpr bool empty( void ) const noexcept {
            return begin() == end();
        }

        /* Returns the number of elements */
        constexpr size_type size( void ) const noexcept {
            return N;
        }

        /* Returns the maximum number of elements the container is able to hold due to system or library implementation limitations */
        constexpr size_type max_size( void ) const noexcept {
            /* Because each array<T, N> is a fixed-size container, the value returned by max_size equals N 
             * (which is also the value returned by size) */
            return N;
        }

        /* Assigns the given value value to all elements in the container */
        constexpr void fill( const_reference value ) {
            fill_n( begin(), size(), value );
        }

        constexpr void swap( array & other ) noexcept( is_nothrow_swappable_v<value_type> ) {
            swap_ranges( begin(), end(), other.begin() );
        }
    };

    /* Deduction guide */
    /* Construction of std::array from a variadic parameter pack. The program is ill-formed if (std::is_same_v<T, U> && ...) is not true. 
     * Note that it is true when sizeof...(U) is zero. */
    template<typename T, typename... Ts> requires ( is_same<T, Ts>::value && ... )
    array( T, Ts... ) -> array<T, 1 + sizeof...( Ts )>;

    template<class T, size_t N>
    constexpr bool operator==( const array<T,N>& lhs, const array<T,N> & rhs ) {
        return equal( lhs.begin(), lhs.end(), rhs.begin() );
    }

    /* TODO: array threeway comparison operator */

    namespace Detail {

        template<typename T, size_t N, size_t... I>
        constexpr array<remove_cv_t<T>, N> to_array_impl( T (& a)[ N ], index_sequence<I...> ) {
            return { { a[ I ]... } };
        }

        template<typename T, size_t N, size_t... I>
        constexpr array<remove_cv_t<T>, N> to_array_impl( T (&& a)[ N ], index_sequence<I...> ) {
            return { { move( a[ I ] )... } };
        }

    }

    template<typename T, size_t N>
    constexpr array<remove_cv_t<T>, N> to_array( T (& a)[ N ] ) {
        return Detail::to_array_impl( a, make_index_sequence<N>{} );
    }

    template<typename T, size_t N>
    constexpr array<remove_cv_t<T>, N> to_array( T (&& a)[ N ] ) {
        return Detail::to_array_impl( move( a ), make_index_sequence<N>{} );
    }
   
    template<typename T, size_t N>
        requires ( swappable<T> || ( N == 0 ) )
    inline void swap( array<T, N> & first, array<T, N> & second ) noexcept( noexcept( first.swap( second ) ) ) {
        first.swap( second );
    }

    /* Tuple like interface for array<> */
    template<typename T>
    struct tuple_size;

    template<typename T, size_t N>
    struct tuple_size<array<T, N>> : public integral_constant<size_t, N> {};

    template<size_t I, typename T>
    struct tuple_element;

    template<size_t I, typename T, size_t N>
        requires ( I < N )
    struct tuple_element<I, array<T, N>> {
        using type = T;
    };
    
    template<size_t I, typename T, size_t N>
        requires ( I < N )
    constexpr T & get( array<T, N> & a ) noexcept { 
        return const_cast<T &>( a.data()[ I ] );
    }

    template<size_t I, typename T, size_t N>
        requires ( I < N )
    constexpr T && get( array<T, N> && a ) noexcept {
        return move( get<I>( a ) );
    }

    template<size_t I, typename T, size_t N>
        requires ( I < N )
    constexpr T & get( const array<T, N> & a ) noexcept {
        return const_cast<T &>( a.data()[ I ] );
    }

    template<size_t I, typename T, size_t N>
        requires ( I < N )
    constexpr T && get( const array<T, N> && a ) noexcept {
        return move( get<I>( a ) );
    }
}