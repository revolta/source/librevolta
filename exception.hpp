#pragma once

namespace ReVolta {

    [[noreturn]] void terminate( void ) noexcept;

}