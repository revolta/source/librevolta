/*
 * named_type.hpp
 *
 *  Created on: 21. 6. 2020
 *      Author: martin
 */

#pragma once

#include "bits/move.hpp"

namespace ReVolta {

	template<typename T, typename TAG>
	class named_type {
	public:
		using type = T;

		constexpr explicit named_type( T const & value ) noexcept : m_value( value ) {}

		constexpr explicit named_type( T && value ) noexcept : m_value( move( value ) ) {}

		constexpr explicit operator T &( void ) noexcept { return m_value; }

		constexpr explicit operator const T &( void ) const noexcept { return m_value; }

	private:
		T m_value;
	};

}
