
#pragma once

#include "stream.hpp"
#include "iostream.hpp"
#include "filesystem.hpp"

namespace ReVolta {
    
    /* IDEAS / NOTES:
     * - The device might be framebuffer, serial port, the filesystem etc. All shall have common interface
     * - additional parameters like colors for framebuffer shall be defined in traits class
     */

    class file_buffer {
    public:
        file_buffer( void ) noexcept {
            /* TODO */
        }

        /* Noncopyable */
        file_buffer( const file_buffer & ) noexcept = delete;

        /* Move constructor */
        file_buffer( file_buffer && other ) noexcept {
            /* TODO */
        }

        /* Destructor */
        ~file_buffer( void ) noexcept {
            this->close();
        }

        /* Copy assignment operator is deleted as file_buffer is noncopyable */
        file_buffer & operator=( const file_buffer & ) noexcept = delete;

        /* Move assignment operator */
        file_buffer & operator=( file_buffer && ) noexcept {
            /* TODO */
        }

        /* Return file buffer pointer with opened file or nullptr once the action fails */
        file_buffer * open( const Filesystem::path & filename, flags<stream::open_mode> mode ) noexcept {
            /* TODO */
            return nullptr;
        }

        file_buffer * close( void ) noexcept {
            /* TODO */
            return nullptr;
        }

        bool is_open( void ) const noexcept {
            /* FIXME: return real value */
            return true;
        }

    private:

    };

    template<template<typename> class STREAM, typename TRAITS, stream::open_mode... MODE_FLAGS>
    class file_stream : public STREAM<TRAITS> {
    public:
        using traits_type = TRAITS;

        file_stream( void ) noexcept {
            /* TODO: Mainly initialize file buffer and possibly other members */
        }

        file_stream( const Filesystem::path & filename, flags<stream::open_mode> mode = ( MODE_FLAGS | ... ) ) noexcept : file_stream() {
            open( filename, mode );
        }

        file_stream( const file_stream & ) noexcept = delete;

        ~file_stream( void ) noexcept {}

        file_stream * open( const Filesystem::path & filename, flags<stream::open_mode> mode = ( MODE_FLAGS | ... ) ) noexcept {
            /* Open file buffer for reading and/or writing, depends on MODE_FLAGS given */
            if( !m_file_buffer.open( filename, mode = ( MODE_FLAGS | ... ) ) ) {
                /* Once the opening failes, switch the stream to failed state */
                this->template switch_state<stream::failed>();
            } else {
                /* Once opened successfully, clear any previous errors - go to 'good' state */
                this->clear();
            }
        }

        void close( void ) noexcept {
            if( !m_file_buffer.close() ) {
                this->template switch_state<stream::failed>();
            }
        }

    private:
        file_buffer m_file_buffer;
    };

    template<typename TRAITS>
    using input_file_stream = file_stream<input_output_stream, TRAITS, stream::open_mode::READ>;

    template<typename TRAITS>
    using output_file_stream = file_stream<input_output_stream, TRAITS, stream::open_mode::WRITE>;

    template<typename TRAITS>
    using input_output_file_stream = file_stream<input_output_stream, TRAITS, stream::open_mode::READ, stream::open_mode::WRITE>;

}