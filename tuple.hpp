#pragma once

#include "utility.hpp"
#include "type_traits.hpp"
#include "bits/pair.hpp"

namespace ReVolta {

    namespace Detail {

        struct ignore_t {
            template<typename T>
            constexpr void operator=( T && ) const noexcept {}
        };

        template<size_t INDEX, typename T>
        struct wrap {
            friend constexpr auto operator==( const wrap &, const wrap & ) -> bool = default;

            /* Once T is empty, no space is occupied, default T constructor is called */
            [[no_unique_address]] T data {};

            constexpr wrap( void ) noexcept = default;

            constexpr wrap( const T & value ) noexcept : data( value ) {}

            constexpr wrap( T && value ) noexcept : data( move( value ) ) {}

            constexpr wrap( const wrap & other ) noexcept : data( other.data ) {}

            constexpr wrap( wrap && other ) noexcept : data( move( other.data ) ) {}

            constexpr void swap( wrap & other ) {
                using ReVolta::swap;
                swap( data, other.data );
            }
        };

        template<size_t INDEX, typename T>
        struct wrap<INDEX, T &> {
            friend constexpr auto operator==( const wrap &, const wrap & ) -> bool = default;

            T & data;

            constexpr wrap( void ) noexcept = delete;

            constexpr wrap( T & value ) noexcept : data( value ) {}

            constexpr wrap( T && value ) noexcept : data( move( value ) ) {}

            constexpr wrap( const wrap & other ) noexcept : data( other.data ) {}

            constexpr wrap( wrap && ) noexcept = default;

            constexpr wrap & operator=( const wrap & other ) noexcept {
                data = other.data;
                return *this;
            }

            constexpr wrap & operator=( wrap && other ) noexcept {
                data = move( other.data );
                return *this;
            }

            constexpr void swap( wrap & other ) {
                using ReVolta::swap;
                swap( data, other.data );
            }
        };

        template<typename...>
        class base {};

        template<size_t... INDEX, typename... Ts>
        class base<index_sequence<INDEX...>, Ts...> : public wrap<INDEX, Ts>... {
        public:
            /* (1) Default constructor */
            constexpr /* TODO: explicit() */ base( void ) noexcept requires ( is_default_constructible_v<Ts> && ... ) = default;

            /* (2) Direct constructor. Initializes each element of the tuple with the corresponding parameter. */
            constexpr explicit( !( is_convertible_v<const Ts &, Ts> && ... ) ) base( const Ts &... arguments ) noexcept /* requires ( is_copy_constructible_v<Ts> ) */ 
                : wrap<INDEX, Ts>::wrap( arguments )... 
            {}

            /* (3) Converting constructor. Initializes each element of the tuple with the corresponding value */
            template<typename... Us>
            constexpr explicit( !( is_convertible_v<Us &&, Ts> && ... ) ) base( Us &&... arguments ) noexcept 
                requires( ( sizeof...( Ts ) >= 1 ) && ( sizeof...( Ts ) == sizeof...( Us ) ) && ( is_constructible_v<Ts, Us &&> && ... ) ) 
                : wrap<INDEX, Ts>::wrap( forward<Us>( arguments ) )...
            {}

            /* (4) Converting copy constructor */
            template<typename... Us>
            constexpr explicit( !( is_convertible_v<const Us &, Ts> && ... ) ) base( const base<Us...> & other ) noexcept
                requires( ( sizeof...( Ts ) == sizeof...( Us ) ) && ( is_constructible_v<Ts, const Us &> && ... ) && ( ( sizeof...( Ts ) != 1 ) /* || is_convertible_v<const base<Us> &, Ts> */ ) )
                : wrap<INDEX, Ts>::wrap( static_cast<wrap<INDEX, Ts>>( other ).data )...
            {}

            /* (5) Converting move constructor */
            template<typename... Us>
            constexpr explicit( !( is_convertible_v<Us &&, Ts> && ... ) ) base( base<Us...> && other ) noexcept requires ( is_convertible_v<Us, Ts> && ... )
                : wrap<INDEX, Ts>::wrap( move( static_cast<wrap<INDEX, Ts>>( other ).data ) )...
            {}

            /* (8) Copy constructor */
            constexpr base( const base & other ) noexcept
                : wrap<INDEX, Ts>::wrap( static_cast<wrap<INDEX, Ts>>( other ).data )...
            {}

            /* (9) Move constructor */
            constexpr base( base && other ) noexcept
                : wrap<INDEX, Ts>::wrap( move( static_cast<wrap<INDEX, Ts>>( other ).data ) )...
            {}

            /* (1) Copy assignment operator */
            constexpr base & operator=( const base & other ) {
                base{ other }.swap( *this );
                return *this;
            }

            /* (2) Move assignment operator */
            constexpr base & operator=( base && other ) noexcept( /* TODO */ true ) {
                base{ move( other ) }.swap( *this );
                return *this;
            }

            /* (3) */
            template<typename... Us>
            base & operator=( const base<Us...> & other ) 
                requires( ( sizeof...( Ts ) == sizeof...( Us ) ) && ( is_assignable_v<Ts &, const Us &> && ... ) )
            {
                base{ other }.swap( *this );
                return *this;
            }

            /* (4) */
            template<typename... Us>
            constexpr base & operator=( base<Us...> && other ) {
                base{ forward<base<Us...>>( other ) }.swap( *this );
                return *this;
            }

            template<size_t START, size_t END, size_t INC, typename F>
            constexpr void constexpr_for( F && f ) {
                if constexpr ( START < END ) {
                    f( integral_constant<size_t, START>() );
                    constexpr_for<START + INC, END, INC>( f );
                }
            }

            constexpr void swap( base & other ) noexcept
#if false
                requires( is_swappable_v<Ts> && ... )
#endif
            {
                /* Swap the wrappers for all the tuple elements with its corresponding counterparts in 'other' */
                ( wrap<INDEX, Ts>::swap( static_cast<wrap<INDEX, Ts> &>( other ) ), ... );
            }
        };

    }

    inline constexpr Detail::ignore_t ignore;

    template<typename... Ts>
    class tuple : public Detail::base<index_sequence_for<Ts...>, Ts...> {
    public:
        /* Inherit base constructors */
        /* NOTE: The reason to inherit constructors from base rather than to aggregate them directly in tuple
         * is to avoid another level of forwarding */
        /* NOTE: Inherited default constructor deduces correct explicit */
        using Detail::base<index_sequence_for<Ts...>, Ts...>::base;
    };

    /* Deduction guides */
    template<typename T1, typename T2>
    tuple( pair<T1, T2> ) -> tuple<T1, T2>;

    /* Emplty tuple specialization */
    template<>
    class tuple<> {
    public:
        constexpr tuple( void ) noexcept = default;

        constexpr void swap( tuple & ) noexcept { /* No operation performed */ }
    };

    /* Two element tuple specialization - interface to pair<> */
    template<typename T1, typename T2>
    class tuple<T1, T2> : public Detail::base<index_sequence_for<T1, T2>, T1, T2> {
    public:
        using base_type = Detail::base<index_sequence_for<T1, T2>, T1, T2>;

        using base_type::base;

        /* (6) pair<> copy constructor */
        template<typename U1, typename U2>
        constexpr /* TODO: explicit() */ tuple( const pair<U1, U2> & p ) noexcept
            requires( is_constructible_v<T1, const U1 &> && is_constructible_v<T2, const U2 &> )
            : base_type::base( p.first, p.second )
        {}

        /* (7) pair<> move constructor */
        template<typename U1, typename U2>
        constexpr /* TODO: explicit() */ tuple( pair<U1, U2> && p ) noexcept
            requires( is_constructible_v<T1, U1 &&> && is_constructible_v<T2, U2 &&> )
            : base_type::base( move( p.first ), move( p.second ) )
        {}

        /* (5) */
        template<typename U1, typename U2>
        constexpr tuple & operator=( const pair<U1, U2> & p ) {
            /* TODO */
        }

        /* (6) */
        template<typename U1, typename U2>
        constexpr tuple & operator=( pair<U1, U2> && p ) {
            /* TODO */
        }
    };

    /* Primary template which shall never be instantiated - only full or partial specializations shall be used */
    template<size_t INDEX, typename T>
    struct tuple_element;

    template<size_t INDEX, typename T>
    struct tuple_element<INDEX, const T> {
        using type = add_const<typename tuple_element<INDEX, T>::type>::type;
    };

    template<size_t INDEX, typename T>
    struct tuple_element<INDEX, volatile T> {
        using type = add_volatile<typename tuple_element<INDEX, T>::type>::type;
    };

    template<size_t INDEX, typename T>
    struct tuple_element<INDEX, const volatile T> {
        using type = add_cv<typename tuple_element<INDEX, T>::type>::type;
    };

    template<size_t INDEX, typename T>
    using tuple_element_t = typename tuple_element<INDEX, T>::type;

    /* tuple_element<> specialization for tuple<> */
    template<size_t INDEX, typename T, typename... Ts>
    struct tuple_element<INDEX, tuple<T, Ts...>> : public tuple_element<INDEX - 1, tuple<Ts...>> {};

    template<typename T, typename... Ts>
    struct tuple_element<0, tuple<T, Ts...>> {
        /* The type of Ith element of the tuple, where I is in [0, sizeof...(Types)) */
        using type = T;
    };

    /* Primary template shall never be instantiated */
    template<typename T>
    struct tuple_size;

    template<typename T>
    struct tuple_size<const T> : public integral_constant<size_t, tuple_size<T>::value> {};

    template<typename T>
    struct tuple_size<volatile T> : public integral_constant<size_t, tuple_size<T>::value> {};

    template<typename T>
    struct tuple_size<const volatile T> : public integral_constant<size_t, tuple_size<T>::value> {};

    template<typename T>
    inline constexpr size_t tuple_size_v = tuple_size<T>::value;

    /* tuple_size specialization for tuple<> */
    template<typename... Ts>
    struct tuple_size<tuple<Ts...>> : public integral_constant<size_t, sizeof...( Ts )> {};

    template<size_t INDEX, typename... Ts> requires ( INDEX < sizeof...( Ts ) )
    tuple_element_t<INDEX, tuple<Ts...>> & get( tuple<Ts...> & tpl ) noexcept {
        using result_type = tuple_element_t<INDEX, tuple<Ts...>>;
        return static_cast<Detail::wrap<INDEX, result_type> &>( tpl ).data;
    }

    template<size_t INDEX, typename... Ts> requires ( INDEX < sizeof...( Ts ) )
    tuple_element_t<INDEX, tuple<Ts...>> const & get( const tuple<Ts...> & tpl ) noexcept {
        using result_type = tuple_element_t<INDEX, tuple<Ts...>>;
        return static_cast<Detail::wrap<INDEX, result_type> &>( tpl ).data;
    }

    template<typename... Ts>
    tuple<Ts &&...> forward_as_tuple( Ts &&... arguments ) noexcept {
        return { forward<Ts>( arguments )... };
    }

    template<typename... Ts>
    constexpr tuple<Ts &...> tie( Ts &... arguments ) noexcept {
        return { arguments... };
    }

    template<typename... Ts>
    constexpr void swap( tuple<Ts...> & lhs, tuple<Ts...> & rhs ) noexcept( noexcept( lhs.swap( rhs ) ) ) {
        lhs.swap( rhs );
    }
}