#pragma once

#include "concepts.hpp"
#include "iterator.hpp"
#include "array.hpp"
#include "initializer_list.hpp"
#include "memory.hpp"
#include "circular_buffer.hpp"

namespace ReVolta {

    namespace Detail {

        /* TODO: Extend to real traits class containing the detectors etc. */
        template<typename T, Allocator ALLOCATOR = allocator<T>>
        struct deque_traits {
#if true
            /* Node size (count of value_type elements) */
            static constexpr size_t node_size { 5 };
            /* Node size in bytes */
            static constexpr size_t node_size_bytes { node_size * sizeof( T ) };
#else
            /* Node size in bytes */
            static constexpr size_t node_size_bytes { 64 };
            /* Node size in elements count */
            static constexpr size_t node_size { ( sizeof( T ) < node_size_bytes ) ? size_t( node_size_bytes / sizeof( T ) ) : size_t( 1 ) };
            /* Ensure at least one value_type element fits the given node */
            static_assert( node_size_bytes >= sizeof( T ), "deque node too small to fit even single instance of value" );
#endif
            using node_type = circular_buffer<T, node_size>;

            using node_iterator = node_type::iterator;

            using const_node_iterator = node_type::const_iterator;

            using node_allocator_type = typename allocator_traits<ALLOCATOR>::rebind_alloc<node_type>::other;

            /* TODO: Shall be removed once 'map_type' is changed to 'list<...>' */
            constexpr static size_t map_size { 5 };

            /* TODO: Shall be changed to 'list<unique_ptr<node_type, deleter<node_allocator_type>>>' once available to provide "endless" functionality */
            using map_type = circular_buffer<unique_ptr<node_type, deleter<node_allocator_type>>, map_size>;

            using map_iterator = map_type::iterator;

            using const_map_iterator = map_type::const_iterator;
        };       


        /* REFERENCE - the reference type to T, might be just 'T &' or 'const T &' */
        template<typename T, Allocator ALLOCATOR, typename REFERENCE>
        class deque_iterator {
        public:
            /* Ensure the deque element is of the same type as defined in traits */
            using value_type = typename deque_traits<T, ALLOCATOR>::node_type::value_type;

            /* TODO: Make it random access iterator category once all required is in */
#if false
            /* Deque iterator is of random access type */
            using iterator_category = random_access_iterator_tag;
#else
            using iterator_category = bidirectional_iterator_tag;
#endif
            /* Size type derived from traits */
            using size_type = typename deque_traits<value_type, ALLOCATOR>::node_type::size_type;
            /* Difference type derived from traits */
            using difference_type = typename deque_traits<value_type, ALLOCATOR>::node_type::difference_type;
            /* Pointer to value_type element */
            using pointer = typename deque_traits<value_type, ALLOCATOR>::node_type::pointer;
            /* Reference to value_type element */
            using reference = REFERENCE;
            /* Iterator used to iterate through the 'node_type', referencing a 'node_type::value_type' */
            using node_iterator = typename deque_traits<value_type, ALLOCATOR>::node_iterator;
            /* Iterator used to iterate through the 'node_type', referencing a 'const value_type' element */
            using const_node_iterator = typename deque_traits<value_type, ALLOCATOR>::const_node_iterator;

            using map_iterator = typename deque_traits<value_type, ALLOCATOR>::map_iterator;

            using const_map_iterator = typename deque_traits<value_type, ALLOCATOR>::const_map_iterator;

        private:
            /* Iterator pointing to current element within the node */
            node_iterator m_current;
            /* Link to the node itself */
            map_iterator m_node;

        public:
            /* By default the deque iterator is constructed as nullptr one */
            constexpr deque_iterator( void ) : deque_iterator( nullptr ) {}

            /* 'nullptr' deque iterator just points to nowhere */
            constexpr deque_iterator( nullptr_t ) : m_current( nullptr ), m_node( nullptr ) {}

            /* Deque iterator explicit constructor
             * 'current' might be equal to 'nullptr' in case no node is allocated yet (node is managed by unique_ptr in default traits setup) */
            constexpr explicit deque_iterator( node_iterator current, map_iterator node ) : m_current( current ), m_node( node ) {}

            /* Copy constructor */
            constexpr deque_iterator( const deque_iterator & other ) : m_current( other.m_current ), m_node( other.m_node ) {}

            /* Copy assignment operator */
            constexpr deque_iterator & operator=( const deque_iterator & other ) = default;

            /* Deque iterator dereference returns the value_type reference */
            reference operator*( void ) const {
                return *m_current;
            }

            pointer operator->( void ) const {
                return m_current.operator->();
            }

            deque_iterator & operator++( void ) {
                /* Increment 'm_current' iterator and check whether it has reached the end of actual node. */
                if( ++m_current == (* m_node)->end() ) {
                    /* Once the 'm_current' is pointing to the end of current node, switch to the next node
                     * and set the 'm_current' to point to the first element within this newly set node */
                    m_current = ( *( ++m_node ) )->begin();
                }
                return *this;
            }

            deque_iterator operator++( int ) {
                deque_iterator temp = *this;
                ++*this;
                return temp;
            }

            deque_iterator & operator--( void ) {
                if( m_current == (* m_node)->begin() ) {
                    m_current = ( *( --m_node ) )->end();
                }
                --m_current;
                return *this;
            }

            deque_iterator operator--( int ) {
                deque_iterator temp = *this;
                --*this;
                return temp;
            }

#if false
            deque_iterator & operator+=( difference_type n ) {
                /* TODO */
/* TODO: Just remove the block below once the current implementation is prooven correct */
#if false                
                const difference_type offset = n + ( m_current - m_first );
                if( offset >= 0 && offset < difference_type( buffer_size() ) ) {
                    m_current += n;
                } else {
                    const difference_type node_offset = ( offset > 0 ) ? offset / difference_type( buffer_size() ) : -difference_type( ( -offset - 1 ) / buffer_size() ) - 1;
                    set_node( m_node + node_offset );
                    m_current = m_first + ( offset - node_offset * difference_type( buffer_size() ) );
                }
#endif 
                return *this;
            }
#endif

            deque_iterator & operator-=( difference_type n ) {
                return *this += -n;
            }

            reference operator[]( difference_type n ) const {
                /* FIXME: Is that valid in iterator based implementation? */
                return *( *this + n );
            }

            friend bool operator==( const deque_iterator & lhs, nullptr_t ) {
                return lhs.m_current == nullptr;
            }

            friend bool operator==( nullptr_t, const deque_iterator & rhs ) {
                return rhs.m_current == nullptr;
            }

            friend bool operator==( const deque_iterator & lhs, const deque_iterator & rhs ) {
                return lhs.m_current == rhs.m_current;
            }

            friend bool operator!=( const deque_iterator & lhs, const deque_iterator & rhs ) {
                return lhs.m_current != rhs.m_current;
            }

            friend bool operator<( const deque_iterator & lhs, const deque_iterator & rhs ) {
                return ( lhs.m_current == rhs.m_current ) ? ( lhs.m_current < rhs.m_current ) : ( lhs.m_node < rhs.m_node );
            }

            friend bool operator>( const deque_iterator & lhs, const deque_iterator & rhs ) {
                return rhs < lhs;
            }

            friend bool operator<=( const deque_iterator & lhs, const deque_iterator & rhs ) {
                return !( rhs < lhs );
            }

            friend bool operator>=( const deque_iterator & lhs, const deque_iterator & rhs ) {
                return !( lhs < rhs );
            }

#if false
            friend difference_type operator-( const deque_iterator & lhs, const deque_iterator & rhs ) {
                /* TODO */

/* TODO: Just remove the block below once the current implementation is prooven correct */
#if false                
                return difference_type( buffer_size() )
                    * ( lhs.m_node - rhs.m_node - int( lhs.m_node != 0 ))
                    + ( lhs.m_current - lhs.m_first )
                    + ( rhs.m_last - rhs.m_current );
#endif                    
            }
#endif

            friend deque_iterator operator+( const deque_iterator & lhs, difference_type n ) {
                deque_iterator temp { lhs };
                temp += n;
                return temp;
            }

            friend deque_iterator operator-( const deque_iterator & lhs, difference_type n ) {
                deque_iterator temp { lhs };
                temp -= n;
                return temp;
            }

            friend deque_iterator operator+( difference_type n, const deque_iterator & rhs ) {
                return rhs + n;
            }
        };

    }

    template<typename T, Allocator ALLOCATOR = allocator<T>>
    class deque {
    public:
        /* Deque value type */
        using value_type = T;
        /* Element of value_type allocator */
        using allocator_type = ALLOCATOR;

    private:
        /* Traits class containing all the common definitions */
        using deque_traits_type = Detail::deque_traits<value_type, allocator_type>;
        /* The map container type used to manage the node pointers */
        using map_type = deque_traits_type::map_type;
        /* Map iterator type used to iterate over the map container */
        using map_iterator = deque_traits_type::map_iterator;

        using const_map_iterator = deque_traits_type::const_map_iterator;
        /* Iterator used to iterate over the node */
        using node_iterator = deque_traits_type::node_iterator;

        using const_node_iterator = deque_traits_type::const_node_iterator;

    public:
        /* Node type used to hold all the instances of T (value_type) */
        using node_type = deque_traits_type::node_type;
        /* Size type derived from traits */
        using size_type = node_type::size_type;
        /* Difference type derived from traits */
        using difference_type = node_type::difference_type;
        /* Pointer to value_type element */
        using pointer = node_type::pointer;
        /* Reference to value_type element */
        using reference = node_type::reference;
        /* Const reference to value_type element */
        using const_reference = const reference;
        /* Deque iterator to walk through the elements of value_type stored within */
        using iterator = Detail::deque_iterator<value_type, allocator_type, reference>;
        /* Const deque iterator */
        using const_iterator = Detail::deque_iterator<value_type, allocator_type, const_reference>;
        /* Reverse iterator */
        using reverse_iterator = ReVolta::reverse_iterator<iterator>;
        /* Const reverse iterator */
        using const_reverse_iterator = ReVolta::reverse_iterator<const_iterator>;

    private:
        /* Node allocator is used to allocate the whole nodes containing fixed amount of value_type instances.
         * As the value_type allocator is given instead of node type allocator, it must be rebound to node type used internally */
        using node_allocator_type = deque_traits_type::node_allocator_type;

        /* Node allocator instance optimized for space (empty allocator uses no memory space) */
        [[no_unique_address]] node_allocator_type m_node_allocator;

        /* Returns the reference to node allocator stored */
        inline node_allocator_type & get_node_allocator( void ) {
            return m_node_allocator;
        }

        /* Returns the reference to node allocator stored */
        inline const node_allocator_type & get_node_allocator( void ) const {
            return m_node_allocator;
        }

        /* Container serving as the map hodling the pointers to all the nodes allocated */
        map_type m_map;

        /* Allocate new node at the given position using the node allocator stored */
        template<typename... ARGUMENTS>
        iterator allocate_node( const_iterator position, ARGUMENTS &&... arguments ) {
            /* TODO: The function just overwrites the node pointer if already stored, the node itself is deallocated automatically thanks to the unique_ptr */
            (* position) = allocate_unique<node_type>( this->get_node_allocator(), forward<ARGUMENTS>( arguments )... );
            return position;
        }

        /* Destroy all elements and deallocate all memory, then replace with the elements created from given 'arguments' */
        template<typename... ARGUMENTS>
        void replace_map( ARGUMENTS &&... arguments ) {
            /* TODO */
        }

        /* Constant time nothrow move assingment when other object memory can be moved because the allocators are equal */
        void move_assign_allocators_equal( deque && other, [[maybe_unused]] true_type allocators_equal ) noexcept {
            this->swap( other );
            other.clear();
            if constexpr ( allocator_traits<allocator_type>::propagate_on_container_move_assignment::value ) {
                this->m_node_allocator = move( other.get_allocator() );
            }
        }

        void move_assign_allocators_equal( deque && other, [[maybe_unused]] false_type allocators_equal ) {
            if( this->get_allocator() == other.get_allocator() ) {
                return move_assign_allocators_equal( move( other ), true_type{} );
            }
            move_assign_allocator_propagates( move( other ), typename allocator_traits<allocator_type>::propagate_on_container_move_assignment{} );
        }

        /* Perform move assingment once the allocator propagates */
        void move_assign_allocator_propagates( deque && other, [[maybe_unused]] true_type allocator_propagates ) {
            /* Make a copy of the original allocator */
            allocator_type allocator { other.get_allocator() };
            /* Allocator propagates so the data can be moved from 'other' leaving 'other' in valid empty state with moved-from allocator */
            this->replace_map( move( other ) );
            /* Move the corresponding allocator too */
            this->m_node_allocator = move( allocator );
        }

        /* Perform move assignment when it may not be possible to move 'other' object's memmory resulting in linear time operation */
        void move_assign_allocator_propagates( deque && other, [[maybe_unused]] false_type allocator_propagates ) {
            /* Allocators are equal so the data can be moved from 'other' leaving 'other' in a valid state with its current allocator */
            if( other.get_allocator() == this->get_allocator() ) {
                this->replace_map( move( other, other.get_allocator() ) );
            } else {
                /* The 'other's allocator cannot be moved and is not equal so all the elements must be moved individually */
                uninitialized_move( make_move_iterator( other.begin() ), make_move_iterator( other.end() ),this->begin() );
                other.clear();
            }
        }

    public:
        /* Default constructor. Constructs an empty container with a default-constructed allocator */
        deque( void ) : deque( allocator_type {} ) {}

        /* Constructs an empty container with the given allocator 'allocator'. The container does contain just the default constructed map which is full of node pointers
         * pointing to nowhere (unique_ptr<node> == nullptr) as no nodes are allocated yet */
        explicit deque( const allocator_type & allocator )
            :   /* Use allocator converting constructor to store given 'value_type' allocator rebound to 'node_type' used to hold the instances of 'value_type' */
                m_node_allocator( allocator ),
                /* Construct deque's map by default - no nodes allocated yet, all the map elements (the node pointers) are just 'nullptr's */
                m_map()
        {}

        /* Constructs the container with 'count' copies of elements with value 'value' */
        deque( size_type count, const value_type & value, const allocator_type & allocator = allocator_type {} ) : deque( allocator ) {
            insert( begin(), count, value );
        }

        /* Constructs the container with count default-inserted instances of T. No copies are made */
        explicit deque( size_type count, const allocator_type & allocator = allocator_type {} ) : deque( allocator ) {
            /* FIXME: Is the "no copies are made" requirement satisfied? */
            insert( begin(), count, value_type{} );
        }
        /* Constructs the container with the contents of the range [first, last). This overload participates
         * in overload resolution only if InputIt satisfies LegacyInputIterator */
        template<input_iterator I>
        deque( I first, I last, const allocator_type & allocator = allocator_type {} ) : deque( allocator ) {
            insert( begin(), first, last );
        }

        /* Copy constructor. Constructs the container with the copy of the contents of other */
        deque( const deque & other ) : deque( other, allocator_traits<allocator_type>::select_on_container_copy_construction( other.get_allocator() ) ) {}

        /* Constructs the container with the copy of the contents of other, using alloc as the allocator */
        deque( const deque & other, const allocator_type & allocator ) : deque( allocator ) {
            /* Copy all the elements in range of the 'other' container starting at the beginning of this container */
            insert( begin(), other.begin(), other.end() );
        }

        /* Move constructor. Constructs the container with the contents of other using move semantics.
         * Allocator is obtained by move-construction from the allocator belonging to other */
        deque( deque && other ) : deque( move( other ), other.get_allocator() ) {}

        /* Allocator-extended move constructor. Using alloc as the allocator for the new container, moving the contents from other; 
         * if 'alloc != other.get_allocator()', this results in an element-wise move */
        deque( deque && other, const allocator_type & allocator ) : deque( move( other ), allocator, typename allocator_traits<allocator_type>::is_always_equal{} ) {}

    private:
        deque( deque && other, const allocator_type & allocator, [[maybe_unused]] true_type allocator_always_equal ) : deque( allocator ) {
            this->move_assign_allocators_equal( move( other ), allocator_always_equal );
        }

        deque( deque && other, const allocator_type & allocator, [[maybe_unused]] false_type allocator_always_equal ) : deque( allocator ) {
            this->move_assign_allocators_equal( move( other ), allocator_always_equal );
        }

    public:
        /* Constructs the container with the contents of the initializer list 'init' */
        deque( initializer_list<value_type> init, const allocator_type & allocator = allocator_type() ) {
            insert( begin(), init );
        }

        /* Destructs the deque. The destructors of the elements are called and the used storage is deallocated.
         * Note, that if the elements are pointers, the pointed-to objects are not destroyed */
        ~deque( void ) {
            /* TODO: Destroy all nodes containing all the data */
            /* TODO: Destroy the map itself */
        }

        /* TODO */
        deque & operator=( const deque & other ) {   
            /* Backup originally stored allocator */
            const node_allocator_type old_allocator { m_node_allocator };
            /* If allocator_traits<allocator_type>::propagate_on_container_copy_assignment::value is true, the allocator 
             * of *this is replaced by a copy of that of other. */
            if( allocator_traits<allocator_type>::propagate_on_container_copy_assignment::value == true ) {
                get_node_allocator() = node_allocator_type{ other.get_allocator() };
            }
            /* If the allocator of *this after assignment would compare unequal
             * to its old value, the old allocator is used to deallocate the memory, then the new allocator is used to allocate it
             * before copying the elements. Otherwise, the memory owned by *this may be reused when possible. In any case, 
             * the elements originally belong to *this may be either destroyed or replaced by element-wise copy-assignment. */
            if( get_node_allocator() != old_allocator ) {

            }

            return *this;
        }

        /* Move assignment operator. Replaces the contents with those of 'other' using move semantics 
         * (i.e. the data in 'other' is moved from 'other' into this container). 'other' is in a valid but unspecified state afterwards.
         * If 'allocator_traits<allocator_type>::propagate_on_container_move_assignment::value' is 'true', the allocator of '*this' 
         * is replaced by a copy of that of 'other'. If it is 'false' and the allocators of '*this' and 'other' do not compare equal, 
         * '*this' cannot take ownership of the memory owned by other and must move-assign each element individually, allocating 
         * additional memory using its own allocator as needed. In any case, all elements originally belong to '*this' are either 
         * destroyed or replaced by element-wise move-assignment. */
        deque & operator=( deque && other ) noexcept( allocator_traits<allocator_type>::is_always_equal::value ) {
            this->move_assign_allocators_equal( move( other ), typename allocator_traits<allocator_type>::is_always_equal{} );
            return *this;
        }

        /* Replaces the contents with those identified by initializer list 'init' */
        deque & operator=( initializer_list<value_type> init ) {
            deque( init ).swap( *this );
            return *this;
        }

        void assign( size_type count, const value_type & value ) {
            /* TODO */
        }

        template<input_iterator IT>
        void assign( IT first, IT last ) {
            /* TODO */
        }

        void assign( initializer_list<value_type> init ) {
            /* TODO */
        }

        /* Returns the allocator associated with the container */
        allocator_type get_allocator( void ) const noexcept {
            return allocator_type{ get_node_allocator() };
        }

        /* Returns a reference to the element at specified location pos, with bounds checking */
        reference at( size_type position ) {
            /* Check bounds */
            if( position >= this->size() ) {
#if false
                throw out_of_range;
#endif                
            }
            return (* this)[ position ];
        }

        /* Returns a reference to the element at specified location pos, with bounds checking */
        const_reference at( size_type position ) const {
            /* Check bounds */
            if( position >= this->size() ) {
#if false
                throw out_of_range;
#endif                
            }
            return (* this)[ position ];
        }

        /* Returns a reference to the element at specified location pos. No bounds checking is performed */
        reference operator[]( size_type index ) {
            return ( this->begin() )[ index ];
        }

        /* Returns a reference to the element at specified location pos. No bounds checking is performed */
        const_reference operator[]( size_type index ) const {
            return ( this->begin() )[ index ];
        }

        /* Returns a reference to the first element in the container */
        reference front( void ) {
            return *begin();
        }

        /* Returns a reference to the first element in the container */
        const_reference front( void ) const {
            return *begin();
        }

        /* Returns a reference to the last element in the container */
        reference back( void ) {
            /* FIXME: Is valid for non-empty container. Shall it be checked here? */
            return *prev( this->end() );
        }

        /* Returns a reference to the last element in the container */
        const_reference back( void ) const {
            /* FIXME: Is valid for non-empty container. Shall it be checked here? */
            return *prev( this->end() );
        }

        /* Returns an iterator to the first element of the deque. If the deque is empty, the returned iterator will be equal to end() */
        iterator begin( void ) noexcept {
            /* By default let's assume the deque is empty and thus begin() iterator points to nowhere (there are no elements of 'value_type' yet)  */
            iterator begin_iterator { nullptr };
            /* Once the map is non empty, means there is at least one node allocated, return the iterator pointing to the first element of this first node */
            if( !this->m_map.empty() ) {
                begin_iterator = iterator{ (* m_map.begin() )->begin(), m_map.begin() };
            }
            return begin_iterator;
        }

        /* Returns an iterator to the first element of the deque. If the deque is empty, the returned iterator will be equal to end() */
        const_iterator begin( void ) const noexcept {
            /* Cast 'this' pointer to non-const to get 'iterator' using 'iterator deque::begin()' and then construct 'const_iterator' using 'iterator' */
            return const_iterator{ const_cast<deque *>( this )->begin() };
        }

        /* Returns an iterator to the first element of the deque. If the deque is empty, the returned iterator will be equal to end() */
        const_iterator cbegin( void ) const noexcept {
            return begin();
        }

        /* Returns an iterator to the element following the last element of the deque. This element acts as a placeholder; 
         * attempting to access it results in undefined behavior. */
        iterator end( void ) noexcept {
            /* By default let's assume the deque is empty and thus the end() iterator equals begin() */
            iterator end_iterator { this->begin() };
            /* Once non-empty (at least one node is allocated), construct the real iterator */
            if( !this->m_map.empty() ) {
                /* TODO: Verify the changed implementation */
#if false
                end_iterator = iterator{ (* --m_map.end() )->end(), --m_map.end() };
#else
                end_iterator = iterator{ (* prev( m_map.end() ) )->end(), prev( m_map.end() ) };
#endif
            }
            return end_iterator;
        }

        /* Returns an iterator to the element following the last element of the deque. This element acts as a placeholder; 
         * attempting to access it results in undefined behavior. */
        const_iterator end( void ) const noexcept {
            return const_iterator{ const_cast<deque *>( this )->end() };
        }

        /* Returns an iterator to the element following the last element of the deque. This element acts as a placeholder; 
         * attempting to access it results in undefined behavior. */
        const_iterator cend( void ) const noexcept {
            return end();
        }

        /* Returns a reverse iterator to the first element of the reversed deque. It corresponds to the last element of the non-reversed deque. 
         * If the deque is empty, the returned iterator is equal to 'rend()' */
        reverse_iterator rbegin( void ) noexcept {
            return reverse_iterator{ end() };
        }

        /* Returns a reverse iterator to the first element of the reversed deque. It corresponds to the last element of the non-reversed deque. 
         * If the deque is empty, the returned iterator is equal to 'rend()' */
        const_reverse_iterator rbegin( void ) const noexcept {
            return const_reverse_iterator{ end() };
        }

        /* Returns a reverse iterator to the first element of the reversed deque. It corresponds to the last element of the non-reversed deque. 
         * If the deque is empty, the returned iterator is equal to 'rend()' */
        const_reverse_iterator crbegin( void ) const noexcept {
            return const_reverse_iterator{ end() };
        }

        reverse_iterator rend( void ) noexcept {
            return reverse_iterator{ begin() };
        }

        /* Returns an iterator to the element following the last element of the deque. This element acts as a placeholder; 
         * attempting to access it results in undefined behavior. */
        const_reverse_iterator rend( void ) const noexcept {
            return const_reverse_iterator{ begin() };
        }

        /* Returns an iterator to the element following the last element of the deque. This element acts as a placeholder; 
         * attempting to access it results in undefined behavior. */
        const_reverse_iterator crend( void ) const noexcept {
            return const_reverse_iterator{ begin() };
        }

        [[nodiscard]] bool empty( void ) const noexcept {
            return begin() == end();
        }

        /* Returns the number of elements in the container */
        size_type size( void ) const noexcept {
            return this->empty() ? 0 : distance( begin(), end() );
        }

        /* Returns maximum number of elements */
        size_type max_size( void ) const noexcept {
            return ( deque_traits_type::map_size * deque_traits_type::node_size );
        }

        /* Requests the removal of unused capacity. It is a non-binding request to reduce the memory usage without
         * changing the size of the sequence. It depends on the implementation whether the request is fulfilled.
         * All iterators and references are invalidated. Past-the-end iterator is also invalidated */
        void shrink_to_fit( void )
            requires ( MoveInsertable<value_type> ) {
            /* TODO */
        }

        /* Erases all elements from the container. After this call, size() returns zero */
        void clear( void ) noexcept {
            this->erase( begin(), end() );
        }

        /* Inserts value before 'position' by copy */
        iterator insert( const_iterator position, const value_type & value ) 
            requires ( CopyAssignable<value_type> &&  CopyInsertable<value_type> ) {
            return emplace( position, value );
        }

        /* Inserts 'value' before 'position' by move */
        iterator insert( const_iterator position, value_type && value ) 
            requires ( MoveAssignable<value_type> &&  MoveInsertable<value_type> ) {
            return emplace( position, move( value ) );
        }

        /* Inserts 'count' copies of the 'value' before 'position'. Returns an iterator pointing to the first element inserted, or pos if 'count' == 0 */
        iterator insert( const_iterator position, size_type count, const value_type & value ) 
            requires ( CopyAssignable<value_type> &&  CopyInsertable<value_type> ) {
            iterator first_element_inserted { nullptr };
            /* Nice trick about '-->' explained here: https://stackoverflow.com/a/1642035/5677080 
             * (It is in fact two separate operators, -- and >. The conditional's code decrements 'count', while returning count's original (not decremented) value, 
             * and then compares the original value with 0 using the > operator) */
            while( count --> 0 ) {
                /* FIXME: This produces potentially many rewritings of 'first_element_inserted' as only the last "update" is really used as return value */
                /* FIXME: Does that really work? As the 'position' is declared const, the absolute value is never changed between the loops thus one element is continuously rewritten... */
                first_element_inserted = insert( --position, value );
            }
            return first_element_inserted;
        }

        /* Inserts elements from range ['first', 'last') before 'position'. Returns an iterator pointing to the first element inserted, or 'position' if 'first' == 'last' */
        template<input_iterator IT>            
        iterator insert( const_iterator position, IT first, IT last ) 
            requires ( EmplaceConstructible<value_type> && Swappable<value_type> && MoveAssignable<value_type>
                       && MoveConstructible<value_type> && MoveInsertable<value_type> ) {
            while( first == last ) {
                this->emplace( position--, *( --last ) );
            }
            return position;
        }

        /* Inserts elements from initializer list 'init' before 'position'. Returns an iterator pointing to the first element inserted, or 'position' if 'init' is empty */
        iterator insert( const_iterator position, initializer_list<value_type> init ) 
            requires ( EmplaceConstructible<value_type> && Swappable<value_type> && MoveAssignable<value_type>
                       && MoveConstructible<value_type> && MoveInsertable<value_type> ) {
            return insert( position, init.begin(), init.end() );
        }

        /* Inserts a new element into the container directly before 'position'. Returns iterator pointing to emplaced element */
        template<typename... ARGUMENTS>
        iterator emplace( const_iterator position, ARGUMENTS &&... arguments ) 
            requires( MoveAssignable<value_type> && MoveInsertable<value_type> && EmplaceConstructible<value_type> ) {
            /* TODO The element is constructed through std::allocator_traits::construct, which typically uses placement-new 
             * to construct the element in-place at a location provided by the container. However, if the required location 
             * has been occupied by an existing element, the inserted element is constructed at another location at first, 
             * and then move assigned into the required location. */

            /* FIXME: ENSURE position is valid! Very often it is not, as the constructors are using insert() functions which are redirected to 
             * this emplace() function while having no node allocated yet and thus the begin() iterator is 'nullptr' */

            /* Once the container is empty, the nodes must be allocated first */
            if( this->empty() ) {

            }

            return position;
        }

        /* Removes the element at 'position'. Returns an iterator following the last removed element */
        iterator erase( const_iterator position ) {
            /* FIXME: What if the position is not at begin() nor end(), is somewhere in the middle... special handling is required here.
             * Shall either copy/move all the entries to the right/left of the 'position' or it might be to adjust the "borders" within the node 
             * itself to have "the end" or "the beginning" not at the "physical" physical end of the node underlying container... */
            /* TODO:--> would mean to manage two more iterators per node to hold the "virtual beginning and end" as explained above */
        }

        /* Remove the elements in range [first, last) */
        iterator erase( const_iterator first, const_iterator last ) requires ( MoveAssignable<value_type> ) {
            /* TODO */
           
        }

        /* Appends the given element 'value' to the end of the container. The new element is initialized as a copy of 'value' */
        void push_back( const value_type & value ) requires ( CopyInsertable<value_type> ) {
            insert( end(), value );
        }

        /* Appends the given element 'value' to the end of the container. 'value' is moved into the new element */
        void push_back( value_type && value ) requires ( MoveInsertable<value_type> ) {
            insert( end(), move( value ) );
        }

        /* Appends a new element to the end of the container. Returns reference to the inserted element */
        template<typename... ARGUMENTS>
        reference emplace_back( ARGUMENTS &&... arguments ) {
            return *( emplace( end(), forward<ARGUMENTS>( arguments )... ) );
        }

        /* Removes the last element of the container */
        void pop_back( void ) {
            erase( --end() );
        }

        /* Prepends the given element value to the beginning of the container */
        void push_front( const value_type & value ) {
            insert( begin(), value );
        }

        /* Prepends the given element value to the beginning of the container */
        void push_front( value_type && value ) {
            insert( begin(), move( value ) );
        }

        /* Inserts a new element to the beginning of the container */
        template<typename... ARGUMENTS>
        reference emplace_front( ARGUMENTS &&... arguments ) 
            requires ( EmplaceConstructible<value_type> ) {
            return *( emplace( begin(), forward<ARGUMENTS>( arguments )... ) );
        }

        /* Removes the first element of the container. If there are no elements in the container, the behavior is undefined */
        void pop_front( void ) {
            erase( begin() );
        }

        /* Resizes the container to contain count elements. 
         * If the current size is greater than 'count', the container is reduced to its first 'count' elements.
         * If the current size is less than count, additional default-inserted elements are appended */
        void resize( size_type count ) requires ( MoveInsertable<value_type> && DefaultInsertable<value_type> ) {
            /* TODO */
        }

        /* Resizes the container to contain count elements. 
         * If the current size is greater than 'count', the container is reduced to its first 'count' elements.
         * If the current size is less than count, additional copies of value are appended */
        void resize( size_type count, const value_type & value ) requires ( CopyInsertable<value_type> ) {
            /* TODO */
        }

        void swap( deque & other ) noexcept( allocator_traits<allocator_type>::is_always_equal::value ) {
            /* TODO */
        }
    };

    /* Checks if the contents of 'lhs' and 'rhs' are equal, that is, they have the same number of elements 
     * and each element in lhs compares equal with the element in 'rhs' at the same position */
    template<typename T, Allocator ALLOCATOR>
    bool operator==( const deque<T, ALLOCATOR> & lhs, const deque<T, ALLOCATOR> & rhs ) {
        return ( lhs.size() == rhs.size() ) && equal( lhs.begin(), lhs.end(), rhs.begin() );
    }

    /* TODO: operator <=> */

    /* Swaps the contents of 'lhs' and 'rhs' */
    template<typename T, Allocator ALLOCATOR>
    void swap( deque<T, ALLOCATOR> & lhs, deque<T, ALLOCATOR> & rhs ) noexcept( noexcept( lhs.swap( rhs ) ) ) {
        lhs.swap( rhs );
    }

    template<typename T, Allocator ALLOCATOR, typename U>
    inline typename deque<T, ALLOCATOR>::size_type erase( deque<T, ALLOCATOR> & container, const U & value ) {
        /* TODO */
#if false
        auto it = std::remove(c.begin(), c.end(), value);
        auto r = std::distance(it, c.end());
        c.erase(it, c.end());
        return r;
#endif
    }

    template<typename T, Allocator ALLOCATOR, typename PREDICATE>
    inline typename deque<T, ALLOCATOR>::size_type erase_if( deque<T, ALLOCATOR> & container, PREDICATE predicate ) {
        /* TODO */
#if false
        auto it = std::remove_if(c.begin(), c.end(), pred);
        auto r = std::distance(it, c.end());
        c.erase(it, c.end());
        return r;
#endif
    }

    /* Deduction guide */

    /* This deduction guide is provided for deque to allow deduction from an iterator range. This overload participates 
     * in overload resolution only if InputIt satisfies LegacyInputIterator and Alloc satisfies Allocator. */
    template<input_iterator IT, Allocator ALLOCATOR = allocator<typename iterator_traits<IT>::value_type>>
    deque( IT, IT, ALLOCATOR = ALLOCATOR() ) -> deque<typename iterator_traits<IT>::value_type, ALLOCATOR>;

}