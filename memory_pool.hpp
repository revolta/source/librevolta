#pragma once

#include "cstddef.hpp"
#include "memory.hpp"
#include "forward_list.hpp"

namespace ReVolta {

    /* NOTE: T is the type of object being stored within the memory pool */
    template<typename T> struct memory_pool_traits;

    template<typename> class memory_pool;

    template<typename TRAITS>
    concept MemoryPoolTraits = requires ( TRAITS traits ) {
        typename TRAITS::element_type;
        /* TODO: Integral constant holding the pool capacity */
    };

    template<typename T>
    class pool_allocator {
    public:
        using value_type = T;
        using pointer = value_type *;

        /* Default constructor */
        constexpr pool_allocator( void ) noexcept = default;

        /* Converting constructor used for rebinding */
        template<typename U>
        constexpr pool_allocator( const pool_allocator<U> & ) noexcept {}

        [[nodiscard]] pointer allocate( size_t n, [[maybe_unused]] const pointer hint = nullptr ) const noexcept {
            return get_pool().allocate( n );
        }

        void deallocate( pointer ptr, size_t n ) const noexcept {
            get_pool().deallocate( ptr, n );
        }

    private:
        static memory_pool<T> & get_pool( void ) noexcept;
    };

    template<typename T>
    class memory_pool {
    public:
        using traits = memory_pool_traits<T>;
        using element_type = typename traits::element_type;
        using pointer = element_type *;

        static constexpr size_t capacity { traits::capacity };

    private:
        union alignas( alignof( element_type ) ) block {
            block( block * next_ptr = nullptr ) noexcept : m_next( next_ptr ) {}

            bool is_leaf( void ) const noexcept {
                return ( m_next == nullptr );
            }

            /* Pointer to the next block. If nullptr then the current block is considered leaf element */
            block * m_next;
            element_type m_value;
        };

        /* Holds the information how many blocks are currently used */
        size_t m_size { 0 };

        /* FIXME: What if the capacity is unknown at compile time - thus simple array cannot be used in there.
         * The storage is just the memory placeholder - the area where the blocks are being constructed.
         * Maybe the pointer to the storage beginning is enough and manage the capacity somehow dynamically 
         * (frame allocations where the amount of RAM is unknown at compile time) */
        byte m_storage [ capacity * sizeof( block ) ];

        /* The head is initially the first element in m_blocks */
        block * m_head { nullptr };

    public:
        memory_pool( void ) noexcept : m_head( construct_at( reinterpret_cast<block *>( m_storage ), nullptr ) ) {}

        [[nodiscard]] inline pointer allocate( [[maybe_unused]] size_t n = 1 ) noexcept {
            if( ( m_head != nullptr ) && ( m_size < capacity ) ) {
                /* Once HEAD is the leaf element - no more blocks were used yet */
                if( m_head->is_leaf() ) {
                    /* Construct another block to be used as new head later */
                    m_head->m_next = construct_at( ( m_head + 1 ), nullptr );
                }
                /* The HEAD element is the one being allocated */
                block * allocated = m_head;
                /* The original HEAD's next is the new HEAD */
                m_head = m_head->m_next;
                /* Increment the number of used block counter */
                m_size++;
                /* Return the pointer to allocated block */
                /* NOTE: 'allocated' is the 'block' which is union. The address of m_value equals the address of the block
                 * itself but encodes the right type */ 
                return addressof( allocated->m_value );
            } else {
                return nullptr;
            }
        }

        inline void deallocate( pointer ptr, [[maybe_unused]] size_t n = 1 ) noexcept {
            /* Cannot deallocate nullptr as the chain of blocks would be broken */
            if( ptr != nullptr ) {
                /* Construct new block containing the pointer to the next one in place of the object being deallocated */
                m_head = construct_at( reinterpret_cast<block *>( ptr ), m_head );
                /* Decrement the amount of used blocks */
                ( m_size >= 1 ) ? m_size-- : m_size = 0;
            }
        }

        size_t size( void ) const noexcept {
            return m_size;
        }
    };

}