#pragma once

#include "cstring.hpp"
#include "bits/pointer_traits.hpp"
#include "type_traits.hpp"

namespace ReVolta {

#if __cplusplus > 201703l && __has_builtin( __builtin_bit_cast )

    /* Create a value of type 'TO' from the bits of 'FROM' */
    template<typename TO, typename FROM>
    [[nodiscard]] constexpr TO bit_cast( const FROM & from ) noexcept {
        return __builtin_bit_cast( TO, from );
    }

#else

    /* Obtain a value of type To by reinterpreting the object representation of from. Every bit in the value
     * representation of the returned To object is equal to the corresponding bit in the object representation 
     * of from. The values of padding bits in the returned To object are unspecified. */
    template<typename TO, typename FROM> requires ( ( sizeof( TO ) == sizeof( FROM ) ) && is_trivially_copyable<TO>::value && is_trivially_copyable<FROM>::value )
    TO bit_cast( const FROM & from ) noexcept {
        TO destination;
        memcpy( addressof( destination ), addressof( from ), sizeof( TO ) );
        return destination;
    }

#endif

}