#pragma once

#include "bits/unique_ptr.hpp"
#include "bits/base_ptr.hpp"
#include "bits/observer_ptr.hpp"
#include "bits/retain_ptr.hpp"
#include "bits/tagged_ptr.hpp"
/* FIXME: paging_ptr is arch specific, shall not be here */
#include "bits/paging_ptr.hpp"
#include "bits/fixed_ptr.hpp"
#include "bits/pimpl_ptr.hpp"
#include "bits/shared_ptr.hpp"
#include "bits/pointer_traits.hpp"
#include "bits/construct.hpp"
#include "bits/allocator_traits.hpp"
#include "bits/allocator.hpp"
#include "bits/uses_allocator.hpp"
#include "bits/uninitialized.hpp"

#include "concepts.hpp"

namespace ReVolta {

	constexpr unsigned long long  operator"" _KB( const unsigned long long value ) { return value * 1024; }

	constexpr unsigned long long  operator"" _MB( const unsigned long long  value ) noexcept { return value * ( 1024_KB ); }

	constexpr unsigned long long  operator"" _GB( const unsigned long long  value ) noexcept { return value * ( 1024_MB ); }

	/* Given a pointer 'ptr' to a buffer of size 'space', returns a pointer aligned by the specified 'alignment' for 'size' number 
	 * of bytes and decreases 'space' argument by the number of bytes used for 'alignment'. The first aligned address is returned. */
	inline void * align( size_t alignment, size_t size, void * & ptr, size_t & space ) noexcept {
		const uintptr_t uptr = reinterpret_cast<uintptr_t>( ptr );
		const uintptr_t aligned_ptr = ( uptr - 1 + alignment ) & - alignment;
		const ptrdiff_t diff = aligned_ptr - uptr;

		if( ( size + diff ) > space ) {
			return nullptr;
		} else {
			space -= diff;
			return ptr = reinterpret_cast<void *>( aligned_ptr );
		}
	}

}
