#pragma once

#include "cstddef.hpp"
#include "iterator.hpp"
#include "bits/uses_allocator.hpp"
#include "bits/move.hpp"
#include "bits/heap.hpp"

namespace ReVolta {

    template<typename T, SequenceContainer CONTAINER>
    class queue {
    public:
        using container_type = CONTAINER;
        using value_type = typename CONTAINER::value_type;
        using size_type = typename CONTAINER::size_type;
        using reference = typename CONTAINER::reference;
        using const_reference = typename CONTAINER::const_reference;

    protected:
        /* Underlying container */
        container_type m_container;

    public:
        /* Default constructor value-initializes the container */
        queue( void ) requires ( is_default_constructible<CONTAINER>::value ) : queue( container_type() ) {}

        /* Copy constructs the underlying container with the contents of 'container' */
        explicit queue( const container_type & container ) : m_container( container ) {}

        /* Move constructs the underlying container with 'move( container )' */
        explicit queue( container_type && container ) : m_container( move( container ) ) {}

        /* Copy constructor. The adaptor is copy-constructed with the contents of other.container */
        queue( const queue & other ) : m_container( other.m_container ) {}

        /* Move constructor. The adaptor is constructed with 'move( other.container )' */
        queue( queue && other ) : m_container( move( other.m_container ) ) {}

        /* Constructs the underlying container using 'allocator' as allocator as if by 'container( allocator )' */
        template<Allocator ALLOCATOR>
        explicit queue( const ALLOCATOR & allocator ) requires ( uses_allocator<container_type, ALLOCATOR>::value )
            : m_container( allocator )
        {}

        /* Constructs the underlying container with the contents of 'container' using 'allocator' as allocator, 
         * as if by 'container( container, allocator )' */
        template<Allocator ALLOCATOR>
        queue( const container_type & container, const ALLOCATOR & allocator ) requires ( uses_allocator<container_type, ALLOCATOR>::value ) 
            : m_container( container, allocator )
        {}

        /* Constructs the underlying container with the contents of 'container' using move semantics while utilizing 'allocator' as allocator,
         * as if by c(std::move(cont), alloc) */
        template<Allocator ALLOCATOR>
        queue( container_type && container, const ALLOCATOR & allocator ) requires ( uses_allocator<container_type, ALLOCATOR>::value )
            : m_container( move( container ), allocator )
        {}

        /* Constructs the adaptor with the contents of 'other.container' and using 'allocator' as allocator,
         * as if by 'container( other.container, allocator )' */
        template<Allocator ALLOCATOR>
        queue( const queue & other, const ALLOCATOR & allocator ) requires ( uses_allocator<container_type, ALLOCATOR>::value )
            : m_container( other.m_container, allocator )
        {}

        /* Constructs the adaptor with the contents of 'other' using move semantics while utilizing 'allocator' as allocator,
         * as if by container( move( other.container ), allocator ) */
        template<Allocator ALLOCATOR>
        queue( queue && other, const ALLOCATOR & allocator ) requires ( uses_allocator<container_type, ALLOCATOR>::value )
            : m_container( move( other.m_container ), allocator )
        {}

        /* Destructs the queue. The destructors of the elements are called and the used storage is deallocated */
        ~queue( void ) = default;

        reference front( void ) {
            return m_container.front();
        }

        const_reference front( void ) const {
            return m_container.front();
        }

        reference back( void ) {
            return m_container.back();
        }

        const_reference back( void ) const {
            return m_container.back();
        }

        [[nodiscard]] bool empty( void ) const {
            return m_container.empty();
        }

        /* Returns the number of elements in the underlying container */
        size_type size( void ) const {
            return m_container.size();
        }

        void push( const value_type & value ) {
            m_container.push_back( value );
        }

        void push( value_type && value ) {
            m_container.push_back( move( value ) );
        }

        /* Pushes a new element to the end of the queue. The element is constructed in-place, 
         * i.e. no copy or move operations are performed. The constructor of the element is called 
         * with exactly the same arguments as supplied to the function */
        template<typename... ARGUMENTS>
        decltype( auto ) emplace( ARGUMENTS &&... arguments ) {
            return m_container.emplace_back( forward<ARGUMENTS>( arguments )... );
        }

        /* Removes an element from the front of the queue */
        void pop( void ) {
            m_container.pop_front();
        }

        /* FIXME: Fix 'noexcept' specifier */
        void swap( queue & other ) noexcept /* ( is_nothrow_swappable_v<container_type> ) */ {
            using ReVolta::swap;
            swap( m_container, other.m_container );
        }
    };

    /* TODO: Non member functions */

    /* TODO: HEAP OPERATIONS EXPLAINED: https://hackingcpp.com/cpp/std/algorithms/heap_operations.html */

    template<typename T, SequenceContainer CONTAINER, typename COMPARE>
    class priority_queue {
    public:
        using container_type = CONTAINER;
        using value_compare = COMPARE;
        using value_type = typename CONTAINER::value_type;
        using size_type = typename CONTAINER::size_type;
        using reference = typename CONTAINER::reference;
        using const_reference = typename CONTAINER::const_reference;

    protected:
        /* The underlying container */
        container_type m_container;

        /* The comparison function object */
        value_compare m_compare;

    public:
        priority_queue( void ) : priority_queue( value_compare(), container_type() ) {}

        explicit priority_queue( const value_compare & compare ) : priority_queue( compare, container_type() ) {}

        /* Copy-constructs the underlying container with the contents of 'container'. Copy-constructs the comparison functor comp with the contents of 'compare'. */
        priority_queue( const value_compare & compare, const container_type & container ) : m_container( container ), m_compare( compare ) {
            make_heap( m_container.begin(), m_container.end(), m_compare );
        }

        /* Move-constructs the underlying container c with std::move(cont). Copy-constructs the comparison functor comp with compare */
        priority_queue( const value_compare & compare, container_type && container ) : m_container( move( container ) ), m_compare( compare ) {
            make_heap( m_container.begin(), m_container.end(), m_compare );
        }

        template<input_iterator ITERATOR>
        priority_queue( ITERATOR first, ITERATOR last, const value_compare & compare = value_compare() ) : m_container( first, last ), m_compare( compare ) {
            make_heap( m_container.begin(), m_container.end(), m_compare );
        }

        /* Copy-constructs underlying container from 'container' and 'compare' from compare. Then calls c.insert(c.end(), first, last); */
        template<input_iterator ITERATOR>
        priority_queue( ITERATOR first, ITERATOR last, const value_compare & compare, const container_type & container ) : m_container( container ), m_compare( compare ) {
            m_container.insert( m_container.end(), first, last );
            make_heap( m_container.begin(), m_container.end(), m_compare );
        }

        template<input_iterator ITERATOR>
        priority_queue( ITERATOR first, ITERATOR last, const value_compare & compare, container_type && container ) : m_container( move( container ) ), m_compare( compare ) {
            m_container.insert( m_container.end(), first, last );
            make_heap( m_container.begin(), m_container.end(), m_compare );
        }

        /* Constructs the underlying container using 'allocator' as allocator */
        template<Allocator ALLOCATOR>
        explicit priority_queue( const ALLOCATOR & allocator ) requires ( uses_allocator<container_type, ALLOCATOR>::value )
            : m_container( allocator ), m_compare() 
        {}

        /* Constructs the underlying container using 'allocator' as allocator. Copy-constructs comp from compare. */
        template<Allocator ALLOCATOR>
        priority_queue( const value_compare & compare, const ALLOCATOR & allocator )  requires ( uses_allocator<container_type, ALLOCATOR>::value )
            : m_container( allocator ), m_compare( compare )
        {}

        /* Constructs the underlying container with the contents of cont and using alloc as allocator, as if by c(cont, alloc). Copy-constructs comp from compare.  */
        template<Allocator ALLOCATOR>
        priority_queue( const value_compare & compare, const container_type & container, const ALLOCATOR & allocator )  requires ( uses_allocator<container_type, ALLOCATOR>::value )
            : m_container( container, allocator ), m_compare( compare ) {
            make_heap( m_container.begin(), m_container.end(), m_compare );
        }

        template<Allocator ALLOCATOR>
        priority_queue( const value_compare & compare, container_type && container, const ALLOCATOR & allocator )  requires ( uses_allocator<container_type, ALLOCATOR>::value )
            : m_container( move( container ), allocator ), m_compare( compare ) {
            make_heap( m_container.begin(), m_container.end(), m_compare );
        }

        /* Constructs the underlying container with the contents of other.m_container and using 'allocator' as allocator. Copy-constructs 'm_compare' from 'other.m_compare' */
        template<Allocator ALLOCATOR>
        priority_queue( const priority_queue & other, const ALLOCATOR & allocator )  requires ( uses_allocator<container_type, ALLOCATOR>::value )
            : m_container( other.m_container, allocator ), m_compare( other.m_compare )
        {}

        template<Allocator ALLOCATOR>
        priority_queue( priority_queue && other, const ALLOCATOR & allocator )  requires ( uses_allocator<container_type, ALLOCATOR>::value ) 
            : m_container( move( other.m_container ), allocator ), m_compare( move( other.m_compare ) )
        {}

#if false
        template<input_iterator ITERATOR, Allocator ALLOCATOR>
        priority_queue( ITERATOR first, ITERATOR last, const ALLOCATOR & allocator )  requires ( uses_allocator<container_type, ALLOCATOR>::value ) {
            /* TODO */
        }

        template<input_iterator ITERATOR, Allocator ALLOCATOR>
        priority_queue( ITERATOR first, ITERATOR last, const value_compare & compare, const ALLOCATOR & allocator ) {
            /* TODO */
        }

        template<input_iterator ITERATOR>
        priority_queue( ITERATOR first, ITERATOR last, const value_compare & compare, const container_type & container, const ALLOCATOR & allocator ) {
            /* TODO */
        }

        template<input_iterator ITERATOR, Allocator ALLOCATOR>
        priority_queue( ITERATOR first, ITERATOR last, const value_compare & compare, container_type && container, const ALLOCATOR & allocator ) {
            /* TODO */
        }
#endif     

        ~priority_queue( void ) = default;

        bool empty( void ) const {
            return m_container.empty();
        }

        size_type size( void ) const {
            return m_container.size();
        }

        const_reference top( void ) const {
            return m_container.front();
        }

        void push( const value_type & value ) {
            m_container.push_back( value );
            push_heap( m_container.begin(), m_container.end(), m_compare );
        }

        void push( value_type && value ) {
            m_container.push_back( move( value ) );
            push_heap( m_container.begin(), m_container.end(), m_compare );
        }

        template<typename... ARGUMENTS>
        void emplace( ARGUMENTS &&... arguments ) {
            m_container.emplace_back( forward<ARGUMENTS>( arguments )... );
            push_heap( m_container.begin(), m_container.end(), m_compare );
        }

        void pop( void ) {
            pop_heap( m_container.begin(), m_container.end(), m_compare );
            m_container.pop_back();
        }

        void swap( priority_queue & other ) noexcept /* ( TODO ) */ {
            using ReVolta::swap;
            swap( m_container, other.m_container );
            swap( m_compare, other.m_compare );
        }
    };

    /* TODO: Deduction guides */

    template<typename T, SequenceContainer CONTAINER, typename COMPARE>
    void swap( priority_queue<T, CONTAINER, COMPARE> & x, priority_queue<T, CONTAINER, COMPARE> & y ) noexcept( noexcept( x.swap( y ) ) ) {
        x.swap( y );
    }

    template<typename T, SequenceContainer CONTAINER, typename COMPARE, Allocator ALLOCATOR>
    struct uses_allocator<priority_queue<T, CONTAINER, COMPARE>, ALLOCATOR> : public uses_allocator<CONTAINER, ALLOCATOR>::type {};

}