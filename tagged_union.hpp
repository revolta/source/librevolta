#pragma once

#include "algorithm.hpp"
#include "new.hpp"
#include "utility.hpp"
#include "cstddef.hpp"
#include "type_traits.hpp"

namespace ReVolta {

	template<typename T, typename... Ts>
	concept IsAlternative = contains<T, Ts...>::value;

	template<typename ... Ts>
	class tagged_union {
		/* Stored data type identification */
		size_t m_index = { 0 };

		/* The storage fitting any of the types expanded from Ts */
		alignas( max( alignof( Ts )... ) ) byte m_storage [ max( sizeof( Ts )... ) ] /* = { 0 } */;

		/* Pointer to destructor of stored data object */
		void (* m_destructor)( const void * ) { nullptr };

		/* Get index of type in parameter pack */
		template<typename T, typename U, typename ... Us>
		struct type_index {
			static constexpr size_t value = type_index<T, Us...>::value + 1;
		};

		template<typename T, typename ... Us>
		struct type_index<T, T, Us...> {
			static constexpr size_t value = 0;
		};

		/* Get type at given index in parameter pack */
		template<size_t INDEX, typename U, typename ... Us>
		struct index_type {
			using type = typename index_type<INDEX - 1, Us...>::type;
		};

		template<typename U, typename ... Us>
		struct index_type<0, U, Us...> {
			using type = U;
		};

	public:
		/* Default constructor */
		constexpr tagged_union( void ) noexcept {
			/* Define the initial type to be the first type in parameter pack */
			using initial_type = typename index_type<0, Ts...>::type;
			/* Switch the storage to the initial type and register it's destructor */
			switch_storage<initial_type>();
			/* Use placement new operator to run default constructor of the initial type */
			construct_at( reinterpret_cast<initial_type *>( m_storage ) );
		}

#if false
		/* Copy constructor */
		constexpr tagged_union( const tagged_union & other ) noexcept {
			/* TODO: constructs a tagged_union holding the same alternative as other and direct-initializes 
			 * the contained value with std::get<other.index()>(other) */

			/* TODO: Use switch_storage<T>() to register the correct destructor */
		}

		/* Move constructor */
		constexpr tagged_union( tagged_union && other ) noexcept {
			/* TODO: constructs a tagged_union holding the same alternative as other and direct-initializes 
			 * the contained value with std::get<other.index()>(std::move(other)) */
			/* TODO: Use switch_storage<T>() to register the correct destructor */
		}

		/* TODO: Add converting constructor */
#endif

		template<typename T, typename... ARGUMENTS> requires IsAlternative<T, Ts...>
		constexpr tagged_union( tag_type<T>, ARGUMENTS &&... arguments ) noexcept {
			/* Switch storage to gived type T */
			switch_storage<T>();
			/* Use placement new operator to run T's construcotr */
			new ( m_storage ) T( forward<ARGUMENTS>( arguments )... );
		}

		/* FIXME: This constructor might be used but how does it behave in overload resolution in respect to the one above?
		 * Maybe to use 'explicit' ? */
#if false
		template<typename... ARGUMENTS>
		tagged_union( ARGUMENTS &&... arguments ) noexcept {
			/* Define the initial type to be the first type in parameter pack */
			using initial_type = typename index_type<0, Ts...>::type;
			/* Switch the storage to the initial type and register it's destructor */
			switch_storage<initial_type>();
			/* Use placement new operator to run default constructor of the initial type */
			new ( m_storage ) initial_type( forward<ARGUMENTS>( arguments )... );
		}
#endif

		~tagged_union( void ) noexcept {
			/* Call stored instance destructor, if any... */
			if( m_destructor ) m_destructor( m_storage );
		}

		constexpr size_t index( void ) const noexcept {
			return m_index;
		}

		template<typename T, typename ... ARGUMENTS> requires IsAlternative<T, Ts...>
		T & emplace( ARGUMENTS && ... arguments ) noexcept {
			/* Destruct previously stored type instance and switch to T */
			switch_storage<T>();

			/* Use placement new in order to call T's constructor in place of tagged union storage */
			new ( m_storage ) T( forward<ARGUMENTS>( arguments )... );

			return reinterpret_cast<T &>( m_storage );
		}

		template<typename T> requires IsAlternative<T, Ts...>
		tagged_union & operator=( T & other ) noexcept {
			/* Destruct previously stored type instance and switch to T */
			switch_storage<T>();
			/* Copy data */
			for( size_t idx = 0; idx < sizeof(T); idx++ ) {
				m_storage[ idx ] = reinterpret_cast<const byte *>( &other )[ idx ];
			}
			return *this;
		}

		template<typename T> requires IsAlternative<T, Ts...>
		bool holds_alternative( void ) const noexcept {
			return type_index<T, Ts...>::value == index();
		}

		/* Returns pointer to stored data if correct type is requested, nullptr otherwise */
		/* TODO: Use 'optional<>' in here to indicate the validity */
		template<typename T> requires IsAlternative<T, Ts...>
		T * get_if( void ) noexcept {
			if( type_index<T,Ts...>::value == index() ) {
				return reinterpret_cast<T*>( m_storage );
			} else {
				return nullptr;
			}
		}

		template<size_t I> requires ( I < sizeof...(Ts) )
		typename index_type<I, Ts...>::type * get_if( void ) noexcept {
			if( index() == I ) {
				return reinterpret_cast<typename index_type<I, Ts...>::type *>( m_storage );
			} else {
				return nullptr;
			}
		}

		template<typename VISITOR, typename... ARGUMENTS>
		decltype(auto) visit( VISITOR && visitor, ARGUMENTS &&... arguments ) noexcept {
			return apply_visitor<VISITOR, 0, sizeof...(Ts)>( forward<VISITOR>( visitor ), forward<ARGUMENTS>( arguments )... );
		}

	private:
		/* TODO: Fix warning: control reaches end of non-void function [-Wreturn-type] rather than suppress it */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
		template<typename VISITOR, size_t N, size_t LENGTH, typename... ARGUMENTS>
		decltype(auto) apply_visitor( VISITOR && visitor, ARGUMENTS &&... arguments ) noexcept {
			if constexpr ( N < LENGTH  ) {
				/* If the type being examined matches the type of object being actually stored */
				if( holds_alternative<typename index_type<N, Ts...>::type>() ) {
					/* Run operator() defined in visitor matching the stored type */
					return forward<VISITOR>( visitor )( *reinterpret_cast<typename index_type<N, Ts...>::type *>( m_storage ), forward<ARGUMENTS>( arguments )... );
				}
			}
			if constexpr ( N + 1 < LENGTH ) {
				return apply_visitor<VISITOR, N + 1, LENGTH>( forward<VISITOR>( visitor ), forward<ARGUMENTS>( arguments )... );
			}
		}
#pragma GCC diagnostic pop

		template<typename T> requires IsAlternative<T, Ts...>
		inline void switch_storage( void ) noexcept {
			/* Run previously saved destructor, if any... */
			if( m_destructor ) m_destructor( m_storage );
			/* Save new type identification */
			m_index = type_index<T, Ts...>::value;
			/* Save pointer to destructor - see https://herbsutter.com/2016/09/25/to-store-a-destructor/ */
			m_destructor = [](const void * x) { static_cast<const T*>(x)->~T(); };
		}
	};

	/* TODO: Implement get functions, see https://en.cppreference.com/w/cpp/utility/variant/get */
}
