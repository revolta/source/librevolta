#pragma once

namespace ReVolta {

    template<typename> class packaged_task;

    /* The class template 'packaged_task' wraps any 'Callable' target (function, lambda expression, bind expression or antother function object)
     * so that it can be invoked asynchronously. Its return value or exception thrown is stored within */
    template<typename R, typename... ARGUMENTS>
    class packaged_task<R( ARGUMENTS... )> {
    public:

    private:

    public:
        /* Constructs 'packaged_task' object with no task and no shared state */
        packaged_task( void ) noexcept {}

        /* Constructs 'packaged_task' object with shared state and a copy fo the task initialized with 'forward<CALLABEL>( callable )' */
        template<typename CALLABLE>
        explicit packaged_task( CALLABLE && callable ) {}
    };

}