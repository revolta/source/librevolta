#pragma once

#include <type_traits.hpp>
#include <bits/atomic_base.hpp>
#include <bits/pointer_traits.hpp>

namespace ReVolta {

    inline constexpr memory_order memory_order_relaxed = memory_order::RELAXED;
    inline constexpr memory_order memory_order_consume = memory_order::CONSUME;
    inline constexpr memory_order memory_order_acquire = memory_order::ACQUIRE;
    inline constexpr memory_order memory_order_release = memory_order::RELEASE;
    inline constexpr memory_order memory_order_acq_rel = memory_order::ACQ_REL;
    inline constexpr memory_order memory_order_seq_cst = memory_order::SEQ_CST;

    template<typename T>
        requires (
            /* FIXME: Enable once implemented */
#if false            
            is_trivially_constructible_v<T> &&
#endif
            is_copy_constructible_v<T> &&
            is_move_constructible_v<T> &&
            is_copy_assignable_v<T> &&
            is_move_assignable_v<T>
        )
    struct atomic {
        using value_type = T;
        
    private:
        static constexpr int min_alignment = ( sizeof( value_type ) & ( sizeof( value_type ) - 1 ) ) || sizeof( value_type ) > 16 ? 0 : sizeof( value_type );

        static constexpr int alignment = min_alignment > alignof( value_type ) ? min_alignment : alignof( value_type );

        alignas( alignment ) value_type m_value { value_type{} };

    public:
        /* Value initializes the underlying object (i.e. with 'T()'). The initialization is not atomic. */
        constexpr atomic( void ) noexcept( is_nothrow_default_constructible_v<T> ) = default;

        /* Initializes the underlying object with 'desired'. The initialization is not atomic. */
        constexpr atomic( value_type desired ) noexcept : m_value( desired ) {}

        /* Atomic variables are not CopyConstructible */
        atomic( const atomic & ) = delete;

        ~atomic( void ) = default;

        /* Atomically assigns the 'desired' value to the atomic variable. */
        value_type operator=( value_type desired ) noexcept  {
            this->store( desired );
            return desired;
        }

        /* Atomically assigns the 'desired' value to the atomic variable. */
        /* TODO: Deprecated if 'atomic<T>::is_always_lock_free == false' */
        value_type operator=( value_type desired ) volatile noexcept {
            this->store( desired );
            return desired;
        }

        /* Atomic variables are not CopyAssignable */
        atomic & operator=( const atomic & ) = delete;

        /* Atomic variables are not CopyAssignable */
        atomic & operator=( const atomic & ) volatile = delete;

        /* Checks whether the atomic operations on all objects of this type are lock-free */
        bool is_lock_free( void ) const noexcept {
            /* Produce fake, minimally aligned pointer */
            return __atomic_is_lock_free( sizeof( m_value ), reinterpret_cast<void *>( -alignment ) );
        }

        /* Checks whether the atomic operations on all objects of this type are lock-free */
        bool is_lock_free( void ) const volatile noexcept {
            /* Produce fake, minimally aligned pointer */
            return __atomic_is_lock_free( sizeof( m_value ), reinterpret_cast<void *>( -alignment ) );
        }

        /* Equals 'true' if this atomic type is always lock-free and 'false' if it is never or sometimes lock-free */
        static constexpr bool is_always_lock_free { __atomic_always_lock_free( sizeof( m_value ), 0 ) };

        /* Atomically replaces the current value with 'desired'. Memory is affected according to the value of 'order' */
        void store( value_type desired, memory_order order = memory_order_seq_cst ) noexcept {
            __atomic_store( addressof( m_value ), addressof( desired ), int( order ) );
        }

        /* TODO: Deprecated if 'atomic<T>::is_always_lock_free == false' */
        void store( value_type desired, memory_order order = memory_order_seq_cst ) volatile noexcept {
            __atomic_store( addressof( m_value ), addressof( desired ), int( order ) );
        }

        /* Atomically loads and returns the current value fo the atomic variable. Memory is affected according to the value of 'order' */
        value_type load( memory_order order = memory_order_seq_cst ) const noexcept {
            alignas( value_type ) unsigned char buffer[ sizeof( value_type ) ];
            value_type * ptr = reinterpret_cast<value_type *>( buffer );
            __atomic_load( addressof( m_value ), ptr, int( order ) );
            return *ptr;
        }

        /* Atomically loads and returns the current value fo the atomic variable. Memory is affected according to the value of 'order' */
        /* TODO: Deprecated if 'atomic<T>::is_always_lock_free == false' */
        value_type load( memory_order order = memory_order_seq_cst ) const volatile noexcept {
            alignas( value_type ) unsigned char buffer[ sizeof( value_type ) ];
            value_type * ptr = reinterpret_cast<value_type *>( buffer );
            __atomic_load( addressof( m_value ), ptr, int( order ) );
            return *ptr;
        }

        /* Atomically loads and returns the current value of the atomic variable */
        operator value_type( void ) const noexcept {
            return this->load();
        }

        /* Atomically loads and returns the current value of the atomic variable */
        /* TODO: Deprecated if 'atomic<T>::is_always_lock_free == false' */
        inline operator value_type( void ) const volatile noexcept {
            return this->load();
        }

        /* Atomically replaces the underlying value with 'desired' (a read-modify-write operation). Memory is affected according to the value of 'order' */
        value_type exchange( value_type desired, memory_order order = memory_order_seq_cst ) {
            /* TODO */
        }

        /* Atomically replaces the underlying value with 'desired' (a read-modify-write operation). Memory is affected according to the value of 'order' */
        /* TODO: Deprecated if 'atomic<T>::is_always_lock_free == false' */
        value_type exchange( value_type desired, memory_order order = memory_order_seq_cst ) volatile {
            /* TODO */
        }

        /* TODO compare_exchange_weak, compare_exchange_strong() */

        /* Performs atomic waiting operations */
        void wait( value_type old, memory_order order = memory_order_seq_cst ) const noexcept {
            /* TODO */
        }

        /* Performs atomic waiting operations */
        void wait( value_type old, memory_order order = memory_order_seq_cst ) const volatile noexcept {
            /* TODO */
        }

        /* Performs atomic notifying operations. If there is a thread blocked in atomic waiting operation (i.e. 'wait()') on '*this', then unblocks
         * at least one such thread */
        void notify_one( void ) noexcept {
            /* TODO */
        }

        /* Performs atomic notifying operations. If there is a thread blocked in atomic waiting operation (i.e. 'wait()') on '*this', then unblocks
         * at least one such thread */
        void notify_one( void ) volatile noexcept {
            /* TODO */
        }

        /* Performs atomic notifying operations. Unblocks all threads blocked in atomic waiting operations (i.e. 'wait()') on '*this', if there is any; otherwise does nothing */
        void notify_all( void ) noexcept {
            /* TODO */
        }

        /* Performs atomic notifying operations. Unblocks all threads blocked in atomic waiting operations (i.e. 'wait()') on '*this', if there is any; otherwise does nothing */
        void notify_all( void ) volatile noexcept {
            /* TODO */
        }

        /* Atomically performs pre-increment the current value. The operation is read-modify-write operation */
        inline value_type operator++( void ) noexcept requires ( is_integral_v<value_type> ) {
            return __atomic_add_fetch( addressof( m_value ), 1, int( memory_order_seq_cst ) );
        }

        /* Atomically performs pre-increment the current value. The operation is read-modify-write operation */
        /* TODO: Deprecated if 'atomic<T>::is_always_lock_free == false' */
        inline value_type operator++( void ) volatile noexcept requires ( is_integral_v<value_type> ) {
            return __atomic_add_fetch( addressof( m_value ), 1, int( memory_order_seq_cst ) );
        }

        /* Atomically performs post-increment of the current value */
        inline value_type operator++( int ) noexcept requires ( is_integral_v<value_type> ) {
            return this->fetch_add( 1 );
        }

        /* Atomically performs post-increment of the current value */
        /* TODO: Deprecated if 'atomic<T>::is_always_lock_free == false' */
        inline value_type operator++( int ) volatile noexcept requires ( is_integral_v<value_type> ) {
            return this->fetch_add( 1 );
        }

        /* Atomically performs pre-decrement the current value. The operation is read-modify-write operation */
        inline value_type operator--( void ) noexcept requires ( is_integral_v<value_type> ) {
            return __atomic_sub_fetch( addressof( m_value ), 1, int( memory_order_seq_cst ) );
        }

        /* Atomically performs pre-decrement the current value. The operation is read-modify-write operation */
        /* TODO: Deprecated if 'atomic<T>::is_always_lock_free == false' */
        inline value_type operator--( void ) volatile noexcept requires ( is_integral_v<value_type> ) {
            return __atomic_sub_fetch( addressof( m_value ), 1, int( memory_order_seq_cst ) );
        }

        /* Atomically performs post-decrement of the current value */
        inline value_type operator--( int ) noexcept requires ( is_integral_v<value_type> ) {
            return this->fetch_sub( 1 );
        }

        /* Atomically performs post-decrement of the current value */
        /* TODO: Deprecated if 'atomic<T>::is_always_lock_free == false' */
        inline value_type operator--( int ) volatile noexcept requires ( is_integral_v<value_type> ) {
            return this->fetch_sub( 1 );
        }

        /* Atomically replaces the current value with the result fo arithmetic addition of internal value and the value given.
         * That is, it performs atomic post increment */
        inline value_type fetch_add( value_type value, memory_order order = memory_order_seq_cst ) noexcept requires ( is_integral_v<value_type> ) {
            return __atomic_fetch_add( addressof( m_value ), value, int( order ) );
        }

        /* Atomically replaces the current value with the result fo arithmetic addition of internal value and the value given.
         * That is, it performs atomic post increment */
        /* TODO: Deprecated if 'atomic<T>::is_always_lock_free == false' */
        inline value_type fetch_add( value_type value, memory_order order = memory_order_seq_cst ) volatile noexcept requires ( is_integral_v<value_type> ) {
            return __atomic_fetch_add( addressof( m_value ), value, int( order ) );
        }

        /* Atomically replaces the current value with the result fo arithmetic subtraction of internal value and the value given.
         * That is, it performs atomic post decrement */
        inline value_type fetch_sub( value_type value, memory_order order = memory_order_seq_cst ) noexcept requires ( is_integral_v<value_type> ) {
            return __atomic_fetch_sub( addressof( m_value ), value, int( order ) );
        }

        /* Atomically replaces the current value with the result fo arithmetic subtraction of internal value and the value given.
         * That is, it performs atomic post decrement */
        /* TODO: Deprecated if 'atomic<T>::is_always_lock_free == false' */
        inline value_type fetch_sub( value_type value, memory_order order = memory_order_seq_cst ) volatile noexcept requires ( is_integral_v<value_type> ) {
            return __atomic_fetch_sub( addressof( m_value ), value, int( order ) );
        }

        /* TODO: fetch_and(), fetch_or(), fetch_xor() */

        /* TODO: Much more to come here */

    };

    template<>
    struct atomic<bool> {
        /* TODO */
    };

}