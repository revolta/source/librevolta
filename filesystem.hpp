#pragma once

#include "cstddef.hpp"
#include "bits/file_descriptor.hpp"

namespace ReVolta::Filesystem {

    /* FIXME: Find a better way to configure the maximum which at the end defines the
     * size of the memory_pool storing all the instances and thus have the direct impact
     * on memory footprint */
    static constexpr size_t max_mounted_filesystems { 5 };

    class path {
        
    };

}