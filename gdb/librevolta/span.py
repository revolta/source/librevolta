import gdb.printing

class span_printer:
    """Print span<>"""

    class iterator:
        def __init__( self, begin, size ):
            self.count = 0
            self.begin = begin
            self.size = size

        def __iter__( self ):
            return self

        def __next__( self ):
            if self.count == self.size:
                raise StopIteration
            count = self.count
            self.count = self.count + 1
            return ( '[%d]' % count, ( self.begin + count ).dereference() )

    def __init__( self, value ):
        self.value = value
        if( value.type.template_argument( 1 ) == gdb.parse_and_eval( 'static_cast<size_t>(-1)' ) ):
            self.size = value['m_extent']['m_value']
        else:
            self.size = value.type.template_argument( 1 )

    def children( self ):
        return self.iterator( self.value['m_ptr'], self.size )

def lookup_span_printers( collection ):
    collection.add_printer( 'ReVolta::span', 'ReVolta::span<.*>$', span_printer )