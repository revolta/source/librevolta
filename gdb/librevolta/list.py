import gdb.printing

# TODO: Move to separate utilities module
# TODO: Find more self explaining variable names
def find_type( orig, name ):
    typ = orig.strip_typedefs()
    while True:
        search = str( typ ) + '::' + name
        try:
            return gdb.lookup_type( search )
        except RuntimeError:
            pass
        # The type was not found, try the superclass. Only the first superclass is checked
        field = typ.fields()[ 0 ]
        if not field.is_base_class:
            raise ValueError( "Cannot find type {}::{}".format( orig, name ) )
        typ = field.type

class list_printer:
    """Print ReVolta::list<> container"""

    class iterator:
        
        def next_node_pointer( self, node ):
            return node['m_next']['m_ptr']['m_ptr']

        def __init__( self, node_type, tail ):
            self.node_type = node_type
            self.tail_pointer = tail.address
            self.node_pointer = self.next_node_pointer( tail )
            self.count = 0

        # An iterable object shall implement the '__iter__' which is expected to return iterator object.
        # Creates iterator object once the iteration is initialized
        def __iter__( self ):
            return self

        # An iterator object implements '__next__', which is expected to return the next element of the iterable object that returned it
        # and to raise a 'StopIteration' exception when no more elements are available.
        # To move to the next element.
        def __next__( self ):
            if self.node_pointer == self.tail_pointer:
                raise StopIteration
            current_node = ( self.node_pointer.cast( self.node_type.pointer() ) ).dereference()
            # Store current node's next pointer as the next node to be processed in the next iteration
            self.node_pointer = self.next_node_pointer( current_node )
            count = self.count
            self.count = self.count + 1
            return ('[%d]' % count, current_node['m_data'])

    def __init__( self, value ):
        self.value = value

    def children( self ):
        allocator_type = find_type( self.value.type, 'allocator_type' ).strip_typedefs()
        node_type = find_type( gdb.lookup_type( 'ReVolta::Detail::list_traits<{}>'.format( allocator_type ) ), 'node_type' )
        # Return iterable object 'iterator'
        return self.iterator( node_type, self.value['m_tail'] )

class list_iterator_printer:
    """Print list_iterator<> iterator"""

    def __init__( self, value ):
        self.value = value

    def children( self ):
        allocator_type = self.value.type.template_argument( 0 )
        node_type = find_type( gdb.lookup_type( 'ReVolta::Detail::list_traits<{}>'.format( allocator_type ) ), 'node_type' ).strip_typedefs()
        yield 'node', self.value['m_node']['m_ptr'].cast( node_type.pointer() ).dereference()

class list_node_printer:
    """Print Detail::list_node<>"""

    def __init__( self, value):
        self.value = value

    def children( self ):
        allocator_type = self.value.type.template_argument( 0 )
        node_type = find_type( gdb.lookup_type( 'ReVolta::Detail::list_traits<{}>'.format( allocator_type ) ), 'node_type' )
        print( node_type )
        yield 'raw', self.value.address
        yield 'next', self.value['m_next']['m_ptr']['m_ptr'].cast( node_type.pointer() ).dereference()
        yield 'previous', self.value['m_previous']['m_ptr'].cast( node_type.pointer() ).dereference()
        yield 'value', self.value['m_data']
        

def lookup_list_printers( collection ):
    collection.add_printer( 'ReVolta::list', 'ReVolta::list<.*>$', list_printer )
    collection.add_printer( 'ReVolta::list_iterator', 'ReVolta::list_iterator<.*>$', list_iterator_printer )
    collection.add_printer( 'ReVolta::list_node', 'ReVolta::Detail::list_node<.*>$', list_node_printer )