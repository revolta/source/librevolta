import gdb.printing

from .list import lookup_list_printers
from .memory import lookup_memory_printers
from .span import lookup_span_printers

def register_librevolta_printers():
    librevolta_printers = gdb.printing.RegexpCollectionPrettyPrinter( 'ReVolta' )

    lookup_list_printers( librevolta_printers )
    lookup_memory_printers( librevolta_printers )
    lookup_span_printers( librevolta_printers )

    gdb.printing.register_pretty_printer( gdb.current_objfile(), librevolta_printers )