import gdb.printing

class smart_ptr_printer:
    """Common base class for all applicable smart pointer types"""

    def __init__( self, type, value ):
        self.value = value
        self.pointer = self.value['m_ptr'].cast( self.value.type.template_argument( 0 ).pointer() )
        self.value_type = type

    def children( self ):
        if self.pointer != 0:
            yield self.value_type, self.pointer
        else:
            yield self.value_type + '= <nullptr>', None

class unique_ptr_printer( smart_ptr_printer ):
    def __init__( self, value ): 
        smart_ptr_printer.__init__( self, type='ReVolta::unique_ptr<T = {}, DELETER = {}>'.format( str( value.type.template_argument( 0 ) ), str( value.type.template_argument( 1 ) ) ), value = value )

    def children( self ):
        return smart_ptr_printer.children( self )

class base_ptr_printer( smart_ptr_printer ):
    def __init__( self, value ): 
        smart_ptr_printer.__init__( self, type='ReVolta::base_ptr<T = {}>'.format( str( value.type.template_argument( 0 ) ) ), value = value )

    def children( self ):
        return smart_ptr_printer.children( self )

class observer_ptr_printer( smart_ptr_printer ):
    def __init__( self, value ): 
        smart_ptr_printer.__init__( self, type='ReVolta::observer_ptr<T = {}>'.format( str( value.type.template_argument( 0 ) ) ), value = value )

    def children( self ):
        return smart_ptr_printer.children( self )

class shared_ptr_printer( smart_ptr_printer ):
    def __init__(self, value):
        smart_ptr_printer.__init__(self, type='ReVolta::shared_ptr<T = {}>'.format( str( value.type.template_argument( 0 ) ) ), value = value )

    def children( self ):
        return smart_ptr_printer.children( self )

def lookup_memory_printers( collection ):
    collection.add_printer( 'ReVolta::unique_ptr', 'ReVolta::unique_ptr<.*>$', unique_ptr_printer )
    collection.add_printer( 'ReVolta::base_ptr', 'ReVolta::base_ptr<.*>$', base_ptr_printer )
    collection.add_printer( 'ReVolta::observer_ptr', 'ReVolta::observer_ptr<.*>$', observer_ptr_printer )
    collection.add_printer( 'ReVolta::shared_ptr', 'ReVolta::shared_ptr<.*>$', shared_ptr_printer )
