from setuptools import setup

setup( name='librevolta',
    version='0.1',
    description='librevolta pretty printers',
    url='https://gitlab.com/revolta',
    author='Martin Kopecky',
    author_email='martin.monster696@gmail.com',
    license='MIT',
    packages=['librevolta'],
    zip_safe=False )