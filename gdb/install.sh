#! /bin/bash

revolta_root_dir=$1

# Remove everything installed previously
rm -rf ${revolta_root_dir}/sysroot/usr/share/gcc/python/

pip install ${revolta_root_dir}/source/librevolta/gdb/ --target ${revolta_root_dir}/sysroot/usr/share/gcc/python/ --upgrade

# Remove all build files once the package is installed
rm -rf ${revolta_root_dir}/source/librevolta/gdb/build/
rm -rf ${revolta_root_dir}/source/librevolta/gdb/librevolta.egg-info/