
#pragma once

#include "type_traits.hpp"

namespace ReVolta {

    template<typename E> requires is_enum<E>::value
    class flags {
    public:
        flags( E value ) noexcept : m_value( to_underlying( value ) ) {}

        flags operator|( const E value ) noexcept {
            m_value |= to_underlying( value );
        }

        flags & operator|=( const E value ) noexcept {
            m_value |= to_underlying( value );
            return *this;
        }

        flags & operator=( const E value ) noexcept {
            m_value = to_underlying( value );
        }

        template<E FLAG>
        bool is_set( void ) noexcept {
            return ( m_value & to_underlying( FLAG ) );
        }

    private:
        /* Abstracted type to store 'combined' value */
        typename underlying_type<E>::type m_value;
    };

    template<typename E> requires is_enum<E>::value
    flags<E> operator|( const E lhs, const E rhs ) noexcept {
        return ( flags( lhs ) | rhs );
    }

}

