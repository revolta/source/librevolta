#pragma once

#include <type_traits.hpp>
#include <exception.hpp>
#include <chrono.hpp>
#include <functional.hpp>
#include <memory.hpp>

namespace ReVolta {

    class thread {
    public:
#if false    
        using native_handle_type = ???
#endif
        /* The class 'thread::id' is a lightweight trivially copyable class that serves as an unique 
         * identifier of 'thread' */
        class id {
            /* TODO: the ID itself, usually 'native_handle_type' */
        public:
            id( void ) noexcept {}

        private:
            friend bool operator==( id x, id y ) noexcept;

            /* TODO: Enable once three way comparison is available */
            /* TODO: Wrap by __cpp_lib_three_way_comparison */
#if false
            friend strong_ordering operator<=>( id x, id y ) noexcept;
#endif
        };

    private:
        id m_id;

    public:
        /* Creates a new thread object with does not represent a thread */
        thread( void ) noexcept = default;

        /* Creates a new thread object and associates it with a thread of execution. The new thread of execution starts executing. */
        template<typename CALLABLE, typename... ARGUMENTS>
            requires ( !is_same_v<remove_cvref_t<CALLABLE>, thread> )
        explicit thread( CALLABLE && func, ARGUMENTS &&... arguments ) { 
            /* TODO - Start the thread (aka '_M_start_thread()'), dependent on pthread_create */ 
            /* TODO: invoke( auto( forward<CALLABLE>( func ) ), auto( forward<ARGUMENTS>( arguments ) )... ) */
        }

        /* The copy constructor is deleted as threads are not copyable. No two thread objects may represent the same thread of execution */
        thread( const thread & ) = delete;

        /* Move constructor. Constructs the thread object to represent the thread of execution that was represented
         * by 'other'. After this call 'other' no longer represents the thread of execution */
        thread( thread && other ) noexcept {
            this->swap( other );
        }

        /* if '*this' has an associated thread ('joinable() == true'), 'terminate()' is called.
         * A thread object does not have an associated thread (and is safe to destroy) after:
         * - it was default constructed
         * - it was moved from
         * - 'join()' has been called
         * - 'detach()' has been called */
        ~thread( void ) {
            if( this->joinable() ) {
                terminate();
            }
        }

        /* As the thread is non-copyable, is implicitly non-copy assignable as well */
        thread & operator=( const thread & ) = delete;

        /* if '*this' still has an associated running thread ('joinable() == true'), calls 'terminate()'.
         * Otherwise assigns the state of 'other' to '*this' and sets 'other' to a default constructed state.
         * After this call, 'this->get_id()' is equal to the value of 'other.get_id()' prior to the call and no longer
         * represents a thread of execution */
        thread & operator=( thread && other ) noexcept {
            /* If *this still has an associated running thread (i.e. 'joinable() == true'), calls terminate(). */
            if( this->joinable() ) {
                terminate();
            }
            /* Assign the state of 'other' to '*this' and sets 'other' to default constructed state.
             * At this point *this is not representing the thread of execution (is equal to default constructed one)
             * so a simple swap will put the 'other' into default constructed state as requested */
            this->swap( other );
            return *this;
            
        }

        /* Check whether the thread object identifies an active thread of execution. Default constructed thread is not joinable. 
         * A thread that has finished executing code, but has not yet been joined is still considered an active thread of execution. */
        bool joinable( void ) const noexcept {
            return this->m_id != id{};
        }

        /* Returns a value of 'thread::id' identifying the thread associated with '*this'.
         * If there is no thread associated, default constructed 'thread::id' is returned. */
        id get_id( void ) const noexcept {
            return this->m_id;
        }

        /* Returns the number of concurrent threads supported by the implementation. Hint only */
        static unsigned int hardware_concurrency( void ) noexcept;

        /* Blocks the current thread until the thread identified by '*this' finishes its execution.
         * The completion fo the thread identified by '*this' synchronizes with the corresponding successful return from 'join()'.
         * No synchronization is performed on '*this' itself. */
        void join( void );

        /* Separates the thread of execution from the thread object, allowing execution to continue independently. Any allocated
         * resources will be freed once the thread exits. After calling 'detach()', '*this' no longer owns any thread */
        void detach( void );

        void swap( thread & other ) noexcept {
            ReVolta::swap( this->m_id, other.m_id );
        }
    };

    inline void swap( thread & x, thread & y ) noexcept {
        x.swap( y );
    }

    inline bool operator==( thread::id x, thread::id y ) noexcept {
        /* TODO: Compare native handles stored within the thread::id */
    }

    /* TODO: Enable once three way comparison is available */
    /* TODO: Wrap by __cpp_lib_three_way_comparison */
#if false
    strong_ordering operator<=>( thread::id lhs, thread::id rhs ) noexcept {
        /* TODO */
    }
#endif

    namespace Detail {
        
        void sleep_for( chrono::seconds sec, chrono::nanoseconds nanosec );

    }

    namespace this_thread {

        void yield( void ) noexcept;

        thread::id get_id( void ) noexcept;

        /* Blocks the execution of the current thread for at least the specified 'sleep_duration'. This function may block for longer time
         * than 'sleep_duration' due to scheduling or resource contention delays. */
        template<typename REP, typename PERIOD>
        inline void sleep_for( const chrono::duration<REP, PERIOD> & sleep_duration ) {
            /* Cannot sleep for negative time period */
            if( sleep_duration <= sleep_duration.zero() ) return;
            chrono::seconds sec { chrono::duration_cast<chrono::seconds>( sleep_duration ) };
            chrono::nanoseconds nanosec { chrono::duration_cast<chrono::nanoseconds>( sleep_duration - sec ) };
            Detail::sleep_for( sec, nanosec );      
        }

        /* Blocks the execution fo the current thread until specified 'sleep_time' has been reached */
        template<Clock CLOCK, typename DURATION>
        inline void sleep_until( const chrono::time_point<CLOCK, DURATION> & sleep_time ) {
            auto now = CLOCK::now();
            if( CLOCK::is_steady ) {
                if( now < sleep_time ) sleep_for( sleep_time - now );
                return;
            }
            while( now < sleep_time ) {
                sleep_for( sleep_time - now );
                now = CLOCK::now();
            }
        }

    }

}