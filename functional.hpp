#pragma once

#include "bits/move.hpp"

namespace ReVolta {

    /* Function object for performing comparisons. Unless specialized, invokes operator< on type T */
    template<typename T = void>
    struct less {
        /* Checks whether lhs is less than rhs */
        constexpr bool operator()( const T & lhs, const T & rhs ) const {
            return lhs < rhs;
        }
    };

    template<>
    struct less<void> {
        template<typename T, typename U>
        constexpr auto operator()( T && lhs, U && rhs ) const -> decltype( forward<T>( lhs ) < forward<U>( rhs ) ) {
            return forward<T>( lhs ) < forward<U>( rhs );
        }
    };

    /* Function object for performing comparisons. Unless specialized, invokes operator> on type T */
    template<typename T = void>
    struct greater {
        /* Checks whether lhs is greater than rhs */
        constexpr bool operator()( const T & lhs, const T & rhs ) const {
            return lhs > rhs;
        }
    };

    template<>
    struct greater<void> {
        template<typename T, typename U>
        constexpr auto operator()( T && lhs, U && rhs ) const -> decltype( forward<T>( lhs ) > forward<U>( rhs ) ) {
            return forward<T>( lhs ) > forward<U>( rhs );
        }
    };

    /* Function object for performing comparisons. Unless specialised, invokes operator== on type T */
    template<typename T = void>
    struct equal_to {
        /* Checks whether lhs is equal to rhs */
        constexpr bool operator()( const T & lhs, const T & rhs ) const {
            return lhs == rhs;
        }
    };

    template<>
    struct equal_to<void> {
        template<typename T, typename U>
        constexpr auto operator()( T && lhs, U && rhs ) const -> decltype( forward<T>( lhs ) == forward<U>( rhs ) ) {
            return forward<T>( lhs ) == forward<U>( rhs );
        }
    };

    /* 'reference_wrapper' detail implementation */
    namespace Detail {

        template<typename T>
        constexpr T & FUN( T & t ) noexcept {
            return t;
        }

        template<typename T>
        void FUN( T && ) = delete;

    }

    /* 'reference_wrapper' is a class template that wraps a reference in a copyable, assignable object. 
     * It is frequently used as a mechanism to store references inside standard containers (like 'vector')
     * which cannot normally hold references. */
    template<typename T>
    class reference_wrapper {
    private:
        T * m_ptr;

    public:
        using type = T;

        /* Converts 'other' to 'T &' as if by 'T & t = forward<U>( other );', then stores a reference to 'other' */
        template<typename U, class = decltype( Detail::FUN<T>( declval<U>() ) )> requires ( !is_same_v<reference_wrapper, remove_cvref_t<U>> )
        constexpr reference_wrapper( U && other ) noexcept( noexcept( Detail::FUN<T>( forward<U>( other ) ) ) )
            : m_ptr( addressof( Detail::FUN<T>( forward<U>( other ) ) ) )
        {}

        /* Copy constructor. Stores a reference to 'other.get()' */
        reference_wrapper( const reference_wrapper & other ) noexcept = default;

        /* Copy assignment operator. Drops the current reference and stores a reference to 'other.get()' */
        reference_wrapper & operator=( const reference_wrapper & other ) noexcept = default;

        /* Returns the stored reference */
        constexpr operator T &( void ) const noexcept {
            return *this->m_ptr;
        }

        /* Returns the stored reference */
        constexpr T & get( void ) const noexcept {
            return *this->m_ptr;
        }

        /* Calls the Callable object, reference to which is stored. This function is available only if the stored reference points to a Callable object */
        template<typename... ARGUMENTS>
        constexpr invoke_result_t<T &, ARGUMENTS...> operator()( ARGUMENTS &&... arguments ) const {
            return invoke( this->get(), forward<ARGUMENTS>( arguments )... );
        }
    };

    /* Deduction guide */
    template<typename T>
    reference_wrapper( T & ) -> reference_wrapper<T>;

    namespace Detail {

        template<typename>
        inline constexpr bool is_reference_wrapper_v = false;

        template<typename U>
        inline constexpr bool is_reference_wrapper_v<reference_wrapper<U>> = true;

        template<typename C, typename POINTED, typename T1, typename... ARGUMENTS>
        constexpr decltype( auto ) invoke_memptr( POINTED C::* f, T1 && t1, ARGUMENTS &&... arguments ) {
            if constexpr ( is_function_v<POINTED> ) {
                if constexpr ( is_base_of_v<C, decay_t<T1>> ) {
                    return ( forward<T1>( t1 ).*f)( forward<ARGUMENTS>( arguments )... );
                } else if constexpr ( is_reference_wrapper_v<decay_t<T1>> ) {
                    return ( t1.get().*f )( forward<ARGUMENTS>( arguments )... );
                } else {
                    return ( ( *forward<T1>( t1 ) ).*f )( forward<ARGUMENTS>( arguments )... );
                }
            } else {
                static_assert( is_object_v<POINTED> && sizeof...( arguments ) == 0 );
                if constexpr ( is_base_of_v<C, decay_t<T1>>) {
                    return forward<T1>( t1 ).*f;
                }
                else if constexpr ( is_reference_wrapper_v<decay_t<T1>> ) {
                    return t1.get().*f;
                } else {
                    return ( *forward<T1>( t1 ) ).*f;
                }
            }
        }

    }

    /* Invoke the Callable object 'f' with the parameters 'arguments' as by 'INVOKE( forward<F>( f ), forward<ARGUMENTS>( arguments )... )' */
    template<typename CALLABLE, typename... ARGUMENTS>
        requires ( is_invocable_v<CALLABLE, ARGUMENTS...> )
    constexpr invoke_result_t<CALLABLE, ARGUMENTS...> invoke( CALLABLE && f, ARGUMENTS &&... arguments ) noexcept( is_nothrow_invocable_v<CALLABLE, ARGUMENTS...> ) {
        if constexpr ( is_member_pointer_v<decay_t<CALLABLE>> ) {
            return Detail::invoke_memptr( f, forward<ARGUMENTS>( arguments )... );
        } else {
            return forward<CALLABLE>( f )( forward<ARGUMENTS>( arguments )... );
        }
    }

    /* Invoke the Callable object 'f' with parameters 'arguments' as by 'INVOKE<RET>( forward<F>( f ), forward<ARGUMENTS>( arguments )... )'.
     * Returns the value returned by 'f', implicitly converted to 'RET', if 'RET' is not 'void', none otherwise */
    template<typename RET, typename CALLABLE, typename... ARGUMENTS>
        requires ( is_invocable_r_v<RET, CALLABLE, ARGUMENTS...> )
    constexpr RET invoke_r( CALLABLE && f, ARGUMENTS &&... arguments ) noexcept( is_nothrow_invocable_r_v<RET, CALLABLE, ARGUMENTS...> ) {
        if constexpr ( is_void_v<RET> ) {
            invoke( forward<CALLABLE>( f ), forward<ARGUMENTS>( arguments )... );
        } else {
            return invoke( forward<CALLABLE>( f ), forward<ARGUMENTS>( arguments )... );
        }

    }

}