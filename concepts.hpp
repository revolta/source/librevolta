#pragma once

#include "type_traits.hpp"
#include "bits/move.hpp"

namespace ReVolta {

	template<typename T>
	concept integral = is_integral<T>::value;
	
	template<size_t N>
	concept NonZeroSize = ( N > 0 );

	template<typename T, typename U>
	concept same_as = is_same_v<T, U> && is_same_v<U, T>;

	template<typename FROM, typename TO>
	concept SafelyConvertible = 
		is_convertible<typename FROM::pointer, typename TO::pointer>::value &&
		!is_array<typename TO::element_type>::value
	;

	template<typename FROM, typename TO>
	concept SafelyAssignable =
		SafelyConvertible<FROM, TO> &&
		is_assignable<typename TO::deleter_type &, typename FROM::deleter_type &&>::value
	;

	template<typename T>
    concept Retainable = requires ( T instance ) {
        { instance.set_use_count( 1 ) };
        { instance.get_use_count() } -> integral; 
        { instance.max_use_count() } -> integral;
    };

	template<typename FROM, typename TO>
	concept convertible_to =
		is_convertible_v<FROM, TO> &&
		requires {
			static_cast<TO>( declval<FROM>() );
		};

	/* TODO */
	template<typename T>
	concept destructible = true; /* is_nothrow_destructible_v<T>; */

	template<typename T, typename... ARGUMENTS>
	concept constructible_from =
		destructible<T> && is_constructible_v<T, ARGUMENTS...>
	;

	template<typename T>
	concept move_constructible = 
		constructible_from<T, T> && convertible_to<T, T>
	;

	template<typename T>
	concept copy_constructible =
		move_constructible<T> &&
		constructible_from<T, T &> && convertible_to<T &, T> &&
		constructible_from<T, const T &> && convertible_to<const T &, T> &&
		constructible_from<T, const T> && convertible_to<const T, T>
	;

	template<typename T, typename U>
	concept common_reference_with = 
		same_as<common_reference_t<T, U>, common_reference_t<U, T>> &&
		convertible_to<T, common_reference_t<T, U>> &&
		convertible_to<U, common_reference_t<T, U>>
	;

	template<typename DERIVED, typename BASE>
	concept derived_from =
		is_base_of_v<BASE, DERIVED> &&
		is_convertible_v<const volatile DERIVED *, const volatile BASE *>
	;

	template<typename LHS, typename RHS>
	concept assignable_from = 
		is_lvalue_reference_v<LHS> &&
		common_reference_with<const remove_reference_t<LHS> &, const remove_reference_t<RHS> &> &&
		requires( LHS lhs, RHS && rhs ) {
			{ lhs = forward<RHS>( rhs ) } -> same_as<LHS>;
		}
	;

	template<typename T>
	concept swappable =
		requires( T & a, T & b ) {
			ReVolta::swap( a, b );
		}
	;

	template<typename T>
	concept movable =
		is_object_v<T> &&
		move_constructible<T> &&
		assignable_from<T &, T> &&
		swappable<T>
	;

	template<typename T>
	concept copyable = 
		copy_constructible<T> &&
		movable<T> &&
		assignable_from<T &, T&> &&
		assignable_from<T &, const T &> &&
		assignable_from<T &, const T>
	;

	template<typename T>
	concept default_initializable = 
		constructible_from<T> &&
		requires { T{}; } &&
		requires { ::new( static_cast<void *>( nullptr ) ) T; }
	;

	template<typename T>
	concept signed_integral = integral<T> && is_signed_v<T>;

	namespace Detail {

		template<typename B>
		concept boolean_testable_impl = convertible_to<B, bool>;

		template<typename B>
		concept boolean_testable =
			convertible_to<B, bool> &&
			requires( B && b ) {
				{ !forward<B>( b ) } -> boolean_testable_impl;
			}
		;

		template<typename T, typename U>
		concept WeaklyEqualityComparableWith =
			requires( const remove_reference_t<T> & t, const remove_reference_t<U> & u ) {
				{ t == u } -> boolean_testable;
				{ t != u } -> boolean_testable;
				{ u == t } -> boolean_testable;
				{ u != t } -> boolean_testable;
			}
		;

	}

	template<typename T>
	concept equality_comparable = Detail::WeaklyEqualityComparableWith<T, T>;

	template<typename T, typename U>
	concept equality_comparable_with = 
		equality_comparable<T> &&
		equality_comparable<U> &&
		common_reference_with<const remove_reference_t<T> &, const remove_reference_t<U> &> &&
		equality_comparable<common_reference_t<const remove_reference_t<T> &, const remove_reference_t<U> &>> &&
		Detail::WeaklyEqualityComparableWith<T, U>
	;

	namespace Detail {

        template<typename T, typename U>
        concept PartiallyOrderedWith = 
            requires( const remove_reference_t<T> & t, const remove_reference_t<U> & u ) {
                { t <  u } -> boolean_testable;
                { t >  u } -> boolean_testable;
                { t <= u } -> boolean_testable;
                { t >= u } -> boolean_testable;
                { u <  t } -> boolean_testable;
                { u >  t } -> boolean_testable;
                { u <= t } -> boolean_testable;
                { u >= t } -> boolean_testable;
            }
        ;

    }

    template<typename T>
    concept totally_ordered = equality_comparable<T> && Detail::PartiallyOrderedWith<T, T>;

    template<typename T, typename U>
    concept totally_ordered_with = 
        totally_ordered<T> &&
        totally_ordered<U> &&
        equality_comparable_with<T, U> &&
        totally_ordered<common_reference_t<const remove_reference_t<T> &, const remove_reference_t<U> &>> &&
        Detail::PartiallyOrderedWith<T, U>
    ;

	/* NAMED REQUIREMENT: MoveAssignable
	 * See: https://en.cppreference.com/w/cpp/named_req/MoveAssignable */
	template<typename T>
	concept MoveAssignable =
		is_move_assignable_v<T>
	;

	/* NAMED REQUIREMENT: MoveInsertable
	 * See: https://en.cppreference.com/w/cpp/named_req/MoveInsertable */
	template<typename T>
	concept MoveInsertable = true;

	template<typename T>
	concept MoveConstructible = true;

	/* NAMED REQUIREMENT: CopyAssignable
	 * See: https://en.cppreference.com/w/cpp/named_req/CopyAssignable */
	template<typename T>
	concept CopyAssignable =
		/* The type 'T' satisfies 'CopyAssignable' if 'T' satisfies 'MoveAssignable' */
		MoveAssignable<T> &&
		is_copy_assignable_v<T>
	;

	/* NAMED REQUIREMENT: CopyInsertable
	 * See: https://en.cppreference.com/w/cpp/named_req/CopyInsertable */
	template<typename T>
	concept CopyInsertable = true;

	/* NAMED REQUIREMENT: EmplaceConstructible
	 * See: https://en.cppreference.com/w/cpp/named_req/EmplaceConstructible */
	template<typename T>
	concept EmplaceConstructible = true;

	/* NAMED REQUIREMENT: DefaultInsertable
	 * See: https://en.cppreference.com/w/cpp/named_req/DefaultInsertable */
	template<typename T>
	concept DefaultInsertable = true;

	/* NAMED REQUIREREMT: Swappable
	 * See: */
	template<typename T>
	concept Swappable =
		swappable<T>
	;

	/* https://en.cppreference.com/w/cpp/named_req/Erasable
	 * With the default allocator, this requirement is equivalent to the validity of std::destroy_at(p), 
	 * which accepts class types with accessible destructors and all scalar types, as well as arrays thereof. */
	/* TODO */
	template<typename T>
	concept Erasable = true;

	/* NAMED REQUIREMENT: Allocator */
	template<typename ALLOCATOR>
	concept Allocator =
		!is_pointer_v<ALLOCATOR> &&
		is_default_constructible_v<ALLOCATOR> &&
		is_class_v<ALLOCATOR> &&
		is_empty_v<ALLOCATOR> &&
		requires( ALLOCATOR allocator, typename ALLOCATOR::pointer ptr, size_t n ) {
			/* Allocator shall define the pointer type */
			typename ALLOCATOR::pointer;
			/* Allocator shall define allocation functions */
			{ allocator.allocate( n ) };

			{ allocator.deallocate( ptr, n ) };
		};

	/* NAMED REQUIREMENT: Deleter */
	template<typename DELETER>
	concept Deleter =
		!is_pointer_v<DELETER> &&
		is_default_constructible_v<DELETER> &&
		is_empty_v<DELETER> &&
		requires( DELETER deleter, typename DELETER::pointer ptr ) {
			/* Deleter must define pointer type to be allocated */
			typename DELETER::pointer;
			/* Deleter must define operator() to do the deletion itself */
			{ deleter( ptr ) };
	};

	/* NAMED REQUIREMENT: Container
	 * See: https://en.cppreference.com/w/cpp/named_req/Container */
	template<typename C>
	concept Container = 
		/* Default constructible - C() */
		default_initializable<C> &&
		/* Copy constructible */
		copy_constructible<C> &&
		/* Move constructible */
		move_constructible<C> &&
		/* Copy & Move assignable */
		CopyAssignable<C> &&
		/* Destructible */
		destructible<C> &&
		/* Equality comparable: 'a == b', 'a != b' */
		equality_comparable<C> &&
		/* Type alias definitions */
		requires( C & container ) {
			typename C::value_type;
			typename C::reference;
			typename C::const_reference;
			typename C::iterator;
			typename C::const_iterator;
			typename C::difference_type;
			typename C::size_type;

			/* TODO: Shall return Iterator - is there any concept describing that? */
			{ container.begin() };
			{ container.end() };
			{ container.cbegin() };
			{ container.cend() };
			{ container.size() } -> integral;
			{ container.max_size() } -> integral;
			{ container.empty() } -> Detail::boolean_testable;
		}
	;

	/* NAMED REQUIREMENT: SequenceContainer */
	template<typename C>
	concept SequenceContainer =
		Container<C>
		/* TODO: Much more to come here */
	;

	/* NAMED REQUIREMENT: AllocatorAwareContainer
	 * An AllocatorAwareContainer is a Container that holds an instance of an Allocator
	 * and uses that instance in all its member functions to allocate and deallocate memory 
	 * and to construct and destroy objects in that memory
	 * See: https://en.cppreference.com/w/cpp/named_req/AllocatorAwareContainer */
	template<typename C>
	concept AllocatorAwareContainer =
		Container<C> &&	
		requires( C & container ) {			
			typename C::allocator_type;
#if true			
			/* allocator_type::value_type must be the same as T::value_type */
			requires (
				same_as<typename C::allocator_type::value_type, typename C::value_type>// &&
				//constructible_from<C, typename C::allocator_type>
			);		
			{ container.get_allocator() } -> Allocator;
#endif			
		}
	;

	template<typename T>
	concept semiregular = copyable<T> && default_initializable<T>;

	template<typename T>
	concept regular = semiregular<T> && equality_comparable<T>;

	template<typename F, typename... ARGUMENTS>
	concept invocable = is_invocable_v<F, ARGUMENTS...>;

	template<typename F, typename... ARGUMENTS>
	concept regular_invocable = invocable<F, ARGUMENTS...>;

	/* The concept 'predicate<F, ARGUMENTS...>' specifies that 'F' is a predicate that accepts arguments 
	 * whose types and value categories are encoded by 'ARGUMENTS...', i.e., it can be invoked 
	 * with these arguments to produce a boolean-testable result. */
	template<typename F, typename... ARGUMENTS>
	concept predicate = 
		regular_invocable<F, ARGUMENTS...> &&
		Detail::boolean_testable<invoke_result_t<F, ARGUMENTS...>>
	;

}
