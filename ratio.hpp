#pragma once

#include <cstdint.hpp>
#include <type_traits.hpp>

namespace ReVolta {

    namespace Detail {

        /* TODO: Rename */
        /* TODO: Try to replace by standard function/definition */
        template<intmax_t N>
        struct __static_sign : integral_constant<intmax_t, ( N < 0 ) ? -1 : 1> {};

        /* TODO: Rename */
        /* TODO: Try to replace by standard function/definition */
        template<intmax_t N>
        struct __static_abs : integral_constant<intmax_t, N * __static_sign<N>::value> {};

        /* TODO: Rename */
        /* TODO: Try to replace by standard function/definition */
        template<intmax_t N, intmax_t Q>
        struct __static_gcd : __static_gcd<Q, ( N % Q )> {};

        template<intmax_t N>
        struct __static_gcd<N, 0> : integral_constant<intmax_t, __static_abs<N>::value> {};

        template<intmax_t Q>
        struct __static_gcd<0, Q> : integral_constant<intmax_t, __static_abs<Q>::value> {};

    }

    template<intmax_t NUM, intmax_t DENOM = 1>
        /* Denominator cannot be zero. Prevent overflow as well */
        requires ( ( DENOM != 0 ) && ( NUM >= -__INTMAX_MAX__ && DENOM >= -__INTMAX_MAX__ ) )
    struct ratio {
        static constexpr intmax_t num { NUM * Detail::__static_sign<DENOM>::value / Detail::__static_gcd<NUM, DENOM>::value };

        static constexpr intmax_t den { Detail::__static_abs<DENOM>::value / Detail::__static_gcd<NUM, DENOM>::value };

        using type = ratio<num, den>;
    };

    namespace Detail {

        template<typename T>
        struct is_ratio : false_type {};

        template<intmax_t NUM, intmax_t DEN>
        struct is_ratio<ratio<NUM, DEN>> : true_type {};

    }

    namespace Arithmetics {

        template<intmax_t N, intmax_t Q>
            /* TODO: Many requires... checks */
#if false
            static const uintmax_t __c = uintmax_t(1) << (sizeof(intmax_t) * 4);

            static const uintmax_t __a0 = __static_abs<_Pn>::value % __c;
            static const uintmax_t __a1 = __static_abs<_Pn>::value / __c;
            static const uintmax_t __b0 = __static_abs<_Qn>::value % __c;
            static const uintmax_t __b1 = __static_abs<_Qn>::value / __c;

            static_assert(__a1 == 0 || __b1 == 0,
                "overflow in multiplication");
            static_assert(__a0 * __b1 + __b0 * __a1 < (__c >> 1),
                "overflow in multiplication");
            static_assert(__b0 * __a0 <= __INTMAX_MAX__,
                "overflow in multiplication");
            static_assert((__a0 * __b1 + __b0 * __a1) * __c
                <= __INTMAX_MAX__ -  __b0 * __a0,
                "overflow in multiplication");
#endif
        struct __safe_multiply {
            static const intmax_t value = N * Q;
        };

        template<typename R1, typename R2>
        class ratio_multiply {
            static const intmax_t gcd1 { Detail::__static_gcd<R1::num, R2::den>::value };
            static const intmax_t gcd2 { Detail::__static_gcd<R2::num, R2::den>::value };

        public:
            using type = ratio<__safe_multiply<( R1::num / gcd1 ), ( R2::num / gcd2 )>::value, __safe_multiply<( R1::den / gcd2 ), (R2::den / gcd1)>::value>;

            static constexpr intmax_t num { type::num };
            static constexpr intmax_t den { type::den };
        };

        template<typename R1, typename R2>
            /* Avoid division by zero */
            requires ( R2::num != 0 )
        struct ratio_divide {
            using type = typename ratio_multiply<R1, ratio<R2::den, R2::num>>::type;

            static constexpr intmax_t num = type::num;
            static constexpr intmax_t den = type::den;
        };

    }

    template<typename R1, typename R2>
    using ratio_multiply = typename Arithmetics::ratio_multiply<R1, R2>::type;

    template<typename R1, typename R2>
    using ratio_divide = typename Arithmetics::ratio_divide<R1, R2>::type;

    using nano  = ratio<1, 1000000000>;
    using micro = ratio<1, 1000000>;
    using milli = ratio<1, 1000>;

}