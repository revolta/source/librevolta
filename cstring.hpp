#pragma once

#include "cstddef.hpp"
#include "cstdint.hpp"

namespace ReVolta {

    inline void * memcpy( void * destination, const void * source, size_t num ) noexcept {
        uint8_t * dest = static_cast<uint8_t *>( destination );
        const uint8_t * src = static_cast<const uint8_t *>( source );
        for( size_t index { 0 }; index < num; index++ ) {
            dest[ index ] = src[ index ];
        }
        return destination;
    }

    /* TODO: Perform test to verify */
    inline int strcmp( const char * lhs, const char * rhs ) {
        while( 1 ) {
#if false 
            int result = ( ( *lhs == 0 ) || ( *lhs != *rhs ) );
         
            if( __builtin_expect( ( result ), 0 ) ) break;
#else
            /* NOTE: See: 
             * 1) https://stackoverflow.com/a/31540623/5677080
             * 2) https://en.cppreference.com/w/cpp/language/attributes/likely */
            if( !( ( *lhs == 0 ) || ( *lhs != *rhs ) ) ) [[likely]] break;
#endif           
            ++lhs;
            ++rhs;
        }
        return ( *lhs - *rhs );
    }

    inline int memcmp( const void * lhs, const void * rhs, size_t count ) noexcept {
        const unsigned char * s1 { static_cast<const unsigned char *>( lhs ) };
        const unsigned char * s2 { static_cast<const unsigned char *>( rhs ) };
        while( count --> 0 ) {
            if( *s1 != *s2 ) [[likely]] {
                return ( *s1 - *s2 );
            }
            ++s1;
            ++s2;
        }
        return 0;
    }

}