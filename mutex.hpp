#pragma once

#include "bits/mutex.hpp"
#include "bits/unique_lock.hpp"
#include "tuple.hpp"

namespace ReVolta {

    template<typename MUTEX>
    class lock_guard {
    public:
        using mutex_type = MUTEX;

        explicit lock_guard( mutex_type & m ) noexcept : m_device( m ) {
            m_device.lock();
        }

        lock_guard( mutex_type & m, adopt_lock_t ) noexcept : m_device( m ) {}

        lock_guard( const lock_guard & ) noexcept = delete;

        ~lock_guard( void ) noexcept {
            m_device.unlock();
        }

    private:
        mutex_type & m_device;
    };

    namespace Detail {

        /* The main purpose of having the unique_lock construction dedicated to  separate function
         * is to deduce the lock type */
        template<typename L>
        inline unique_lock<L> _try_to_lock( L & lock ) noexcept {
            return unique_lock<L>{ lock, try_to_lock };
        }

        template<int INDEX, bool CONTINUE = true>
        struct try_lock_impl {
            template<typename... Ls>
            static void do_try_lock( tuple<Ls &...> & locks, int & index ) {
                index = INDEX;
                auto lock = _try_to_lock( get<INDEX>( locks ) );
                if( lock.owns_lock() ) {
                    constexpr bool cont = INDEX + 2 < sizeof...( Ls );
                    using try_locker = try_lock_impl<INDEX + 1, cont>;
                    try_locker::do_try_lock( locks, index );
                    if( index == -1 ) lock.release();
                }
            }
        };

        template<int INDEX>
        struct try_lock_impl<INDEX, false> {
            template<typename... Ls>
            static void do_try_lock( tuple<Ls &...> locks, int & index ) {
                index = INDEX;
                auto lock = _try_to_lock( get<INDEX>( locks ) );
                if( lock.owns_lock() ) {
                    index = -1;
                    lock.release();
                }
            }
        };
        
    }

    /* Locks the given Lockable objects lock_1, lock_2, ..., lockn using a deadlock avoidance algorithm to avoid deadlock */
    /* TODO: Shall it be constrained by concept "Lockable"? */
    template<typename L1, typename L2, typename... Ls>
    void lock( L1 & lock_1, L2 & lock_2, Ls &... others ) noexcept {
        while( true ) {
            using try_locker = Detail::try_lock_impl<0, sizeof...( Ls ) != 0>;
            unique_lock<L1> first{ lock_1 };
            int index;
            auto locks = tie( lock_2, others... );
            try_locker::do_try_lock( locks, index );
            if( index == -1 ) {
                first.release();
                return;
            }
        }
    }

}