/*
 * cstdint.hpp
 *
 *  Created on: 18. 6. 2020
 *      Author: martin
 */

#pragma once

#include <stdint.h>

namespace ReVolta {

	using ::int8_t;
	using ::int16_t;
	using ::int32_t;
	using ::int64_t;

	using ::int_fast8_t;
	using ::int_fast16_t;
	using ::int_fast32_t;
	using ::int_fast64_t;

	using ::int_least8_t;
	using ::int_least16_t;
	using ::int_least32_t;
	using ::int_least64_t;

	using ::intmax_t;
	using ::intptr_t;

	using ::uint8_t;
	using ::uint16_t;
	using ::uint32_t;
	using ::uint64_t;

	using ::uint_fast8_t;
	using ::uint_fast16_t;
	using ::uint_fast32_t;
	using ::uint_fast64_t;

	using ::uint_least8_t;
	using ::uint_least16_t;
	using ::uint_least32_t;
	using ::uint_least64_t;

	using intmax_t = __INTMAX_TYPE__;
	using uintmax_t = __UINTMAX_TYPE__;

	constexpr const uint8_t ENABLED { 1 };
	constexpr const uint8_t DISABLED { 0 };

	struct uint48_t {
        uint64_t value : 48;
    } __attribute__((packed));

	static_assert( sizeof( uint48_t ) == 6 );

}
