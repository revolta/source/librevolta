/*
 * iostream
 *
 *  Created on: 8. 5. 2020
 *      Author: martin
 */

#pragma once

#include "input_stream.hpp"
#include "output_stream.hpp"

#include "framebuffer.hpp"

namespace ReVolta {

	/* TODO: Make this read/write composite device or move it to output_stream.hpp */
	template<typename... DEVICES>
	struct make_composite_device {

		template<typename... DRIVERS> class drivers {};

		template<typename DRIVER, typename... OTHERS>
		class drivers<DRIVER, OTHERS...> : public drivers<OTHERS...> {
		public:
			drivers( DRIVER & driver, OTHERS & ... others ) : drivers<OTHERS...>( others... ), m_device( driver ) {}

			void write( const char * str, const ForegroundColor fg, const BackgroundColor bg ) noexcept {
				/* Check at compile time whether the device supports color settings. If not, colors shall be ignored */
				if constexpr ( DRIVER::supports_color_configuration == true ) {
					/* Write current device */
					m_device.write( str, fg, bg );
				}
				else {
					m_device.write( str );
				}

				/* If there are more devices to write to... */
				if constexpr ( sizeof...(OTHERS) > 0) {
					drivers<OTHERS...>::write( str, fg, bg );
				}
			}

		private:
			DRIVER & m_device;
		};

		using type = drivers<DEVICES...>;
	};

	struct cout_traits {
		using output_device = typename make_composite_device<Kernel::Framebuffer>::type;
		static constexpr Color foreground_color { Color::GRAY };
		static constexpr Color background_color { Color::BLACK };
	};

	struct cerr_traits {
		/* NOTE: output_device must be a reference type otherwise a copy happens */
		using output_device = Kernel::Framebuffer &;
		static constexpr Color foreground_color { Color::RED };
		static constexpr Color background_color { Color::BLACK };
	};

	struct clog_traits {
		using output_device = Kernel::Framebuffer &;
		static constexpr Color foreground_color { Color::WHITE };
		static constexpr Color background_color { Color::BLACK };
	};

	struct cin_traits {

	};

	using cout_t = Kernel::output_stream<cout_traits>;
	using cerr_t = Kernel::output_stream<cerr_traits>;
	using clog_t = Kernel::output_stream<clog_traits>;

	/* Standard output stream */
	extern cout_t cout;

	/* Standard error stream */
	extern cerr_t cerr;

	/* Standard log stream */
	extern clog_t clog;

	/* Standard input stream */
	//extern cin_t cin;

}
