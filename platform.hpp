/*
 * platform.hpp
 *
 *  Created on: 2. 3. 2020
 *      Author: martin
 */

#pragma once

#include "feature.hpp"

#include "framebuffer.hpp"

namespace ReVolta {

	/* Platform configuration */
	class Platform {
	public:
		/* TODO: All platform features shall be defined here either pointing to implemented feature (type)
		 * or to 'NotSupported' declaration having no definition and thus being able to be detected */
		using Output = Framebuffer;
		using Input = NotSupported;
		using Log = Framebuffer;
#ifndef __USE_MULTIBOOT2
		//using ConfigProvider = Multiboot;
		using ConfigProvider = NotSupported;
#else
		using ConfigProvider = Multiboot2;
#endif
	};

}
