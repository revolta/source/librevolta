#pragma once

#include "cstddef.hpp"
#include "cstdint.hpp"

namespace ReVolta {

	/* TODO: Move all the helpers and other non-directly usable types to detail namespace */
	template<typename T>
	class reference_wrapper;

	/* Integral_constant */
	template<typename T, T VALUE>
	struct integral_constant {
		static constexpr T value = VALUE;
		using value_type = T;
		using type = integral_constant<T, VALUE>;
		constexpr operator value_type() const noexcept { return value; }
		constexpr value_type operator()() const noexcept { return value; }
	};

	template<bool, typename T = void>
	struct enable_if {};

	template<typename T>
	struct enable_if<true, T> { 
		using type = T;
	};

	template<bool COND, typename T = void>
	using enable_if_t = typename enable_if<COND, T>::type;

	template<typename...>
	using void_t = void;

	/* Provides the member typedef type that names T (i.e., the identity transformation). */
	template<typename T>
	struct type_identity {
		using type = T;
	};

	template<typename T>
	using type_identity_t = typename type_identity<T>::type;

	using true_type = integral_constant<bool, true>;
	
	using false_type = integral_constant<bool, false>;

	template<bool B, class T, class F>
	struct conditional { 
		using type = T; 
	};

	template<class T, class F>
	struct conditional<false, T, F> { 
		using type = F;
	};

	template<bool B, typename T, typename F>
	using conditional_t = typename conditional<B, T, F>::type;

	template<typename...> struct __and_;

	template<> 
	struct __and_<> : public true_type {};

	template<typename B1> 
	struct __and_<B1> : public B1 {};

	template<typename B1, typename B2> 
	struct __and_<B1, B2> : public conditional<B1::value, B2, B1>::type {};

	template<typename B1, typename B2, typename ... Bn> 
	struct __and_<B1,B2,Bn...> : public conditional<B1::value, __and_<B2,Bn...>, B1>::type {};

	template<typename...> struct __or_;

	template<> 
	struct __or_<> : public false_type {};

	template<typename B1> 
	struct __or_<B1> : public B1 {};

	template<typename B1, typename B2> 
	struct __or_<B1, B2> : public conditional<B1::value, B1, B2>::type {};

	template<typename B1, typename B2, typename ... Bn> 
	struct __or_<B1,B2,Bn...> : public conditional<B1::value, __or_<B2,Bn...>, B1>::type {};

	template<typename T> struct __not_ : public integral_constant<bool, !bool( T::value )> {};

	/* TODO: Get rid of that */
	template<typename... COND>
	using _Require = enable_if_t<__and_<COND...>::value>;

	template<typename> 
	struct is_array : public false_type {};

	template<typename T, size_t SIZE> 
	struct is_array<T[SIZE]> : public true_type {};

	template<typename T> 
	struct is_array<T[]> : public true_type {};

	template<typename T>
	inline constexpr bool is_array_v = is_array<T>::value;

	template<typename, unsigned = 0>
	struct extent;

	template<typename, unsigned U>
	struct extent : public integral_constant<size_t, 0> {};

	template<typename T, unsigned U, size_t SIZE>
	struct extent<T[ SIZE ], U> : public integral_constant<size_t, ( U == 0 ) ? SIZE : extent<T, U - 1>::value> {};

	template<typename T, unsigned U>
	struct extent<T[], U> : public integral_constant<size_t, ( U == 0 ) ? 0 : extent<T, U - 1>::value> {};

	template<typename T>
	struct remove_extent {
		using type = T;
	};

	template<typename T, size_t SIZE>
	struct remove_extent<T[ SIZE ]> {
		using type = T;
	};

	template<typename T>
	struct remove_extent<T[]> {
		using type = T;
	};

	template<typename T>
	using remove_extent_t = typename remove_extent<T>::type;

	template<typename T>
	struct is_bounded_array : public integral_constant<bool, ( extent<T>::value > 0 )> {};

	template<typename T>
	inline constexpr bool is_bounded_array_v = is_bounded_array<T>::value;

	template<typename T>
	struct is_unbounded_array : public __and_<is_array<T>, __not_<extent<T>>> {};

	template<typename T>
	inline constexpr bool is_unbounded_array_v = is_unbounded_array<T>::value;

	template<typename>
	struct is_const : public false_type {};

	template<typename T>
	struct is_const<T const> : public true_type {};

	template<typename T>
	inline constexpr bool is_const_v = is_const<T>::value;

	template<typename>
	struct is_volatile : public false_type {};

	template<typename T>
	struct is_volatile<T volatile> : public true_type {};

	template<typename T>
	inline constexpr bool is_volatile_v = is_volatile<T>::value;

	template<typename T>
	struct is_function : public integral_constant<bool, !is_const<const T>::value> {};

	template<typename T>
	struct is_function<T &> : public false_type {};

	template<typename T>
	struct is_function<T &&> : public false_type {};

	template<typename T>
	inline constexpr bool is_function_v = is_function<T>::value;

	template<typename T> 
	struct is_void : public false_type {};

	template<> 
	struct is_void<void> : public true_type {};

	template<typename T>
	inline constexpr bool is_void_v = is_void<T>::value;

	template<typename T>
	struct is_enum : public integral_constant<bool, __is_enum( T )> {};

	template<typename T>
	inline constexpr bool is_enum_v = is_enum<T>::value;

	template<typename T>
	struct is_class : public integral_constant<bool, __is_class( T )> {};

	template<typename T>
	inline constexpr bool is_class_v = is_class<T>::value;

	template<typename T>
	struct is_union : public integral_constant<bool, __is_union( T )> {};

	template<typename T>
	inline constexpr bool is_union_v = is_union<T>::value;

	template<typename T>
	struct is_empty : public integral_constant<bool, __is_empty( T )> {};

	template<typename T>
	inline constexpr bool is_empty_v = is_empty<T>::value;

	template<typename T>
    struct is_final : public integral_constant<bool, __is_final( T )> {};

	template<typename T>
	inline constexpr bool is_final_v = is_final<T>::value;

	/* lvalue reference typedefs */
	template<typename>
	struct is_lvalue_reference : public false_type {};

	template<typename T>
	struct is_lvalue_reference<T &> : public true_type {};

	template<typename T>
	inline constexpr bool is_lvalue_reference_v = is_lvalue_reference<T>::value;

	template<typename>
	struct is_rvalue_reference : public false_type {};

	template<typename T>
	struct is_rvalue_reference<T &&> : public true_type {};

	template<typename T>
	inline constexpr bool is_rvalue_reference_v = is_rvalue_reference<T>::value;

	template<typename T>
	struct is_reference : public __or_<is_lvalue_reference<T>, is_rvalue_reference<T>>::type {};

	template<typename T>
	inline constexpr bool is_reference_v = is_reference<T>::value;

	template<typename T>
    struct is_trivially_copyable : public integral_constant<bool, __is_trivially_copyable( T )> {};

	template<typename T>
	inline constexpr bool is_trivially_copyable_v = is_trivially_copyable<T>::value;

	/* TODO See type_traits.h #227 */
	template<typename T>
	concept complete_or_unbounded = 
		true
	;

	template<complete_or_unbounded T, typename... ARGUMENTS>
	struct is_constructible : public integral_constant<bool, __is_constructible( T, ARGUMENTS... )> {};

	template<complete_or_unbounded T, typename... ARGUMENTS>
	inline constexpr bool is_constructible_v = is_constructible<T, ARGUMENTS...>::value;

	/* Default constructible is a class that is constructible without any additional constructor arguments */
	template<complete_or_unbounded T>
	struct is_default_constructible : public is_constructible<T>::type {};

	template<complete_or_unbounded T>
	inline constexpr bool is_default_constructible_v = is_default_constructible<T>::value;

	namespace Detail {

		template<typename T, typename = void>
		struct is_referenceable : public false_type {};

		template<typename T>
		struct is_referenceable<T, void_t<T &>> : public true_type {};

		template<typename T>
		inline constexpr bool is_referenceable_v = is_referenceable<T>::value;

	}

	template<complete_or_unbounded T>
	struct is_copy_constructible;

	template<complete_or_unbounded T>
		requires ( !Detail::is_referenceable_v<T> )
	struct is_copy_constructible<T> : public false_type {};

	template<complete_or_unbounded T>
		requires ( Detail::is_referenceable_v<T> )
	struct is_copy_constructible<T> : public is_constructible<T, const T &> {};

	template<complete_or_unbounded T>
	inline constexpr bool is_copy_constructible_v = is_copy_constructible<T>::value;

	template<complete_or_unbounded T>
	struct is_move_constructible;

	template<complete_or_unbounded T>
		requires ( !Detail::is_referenceable_v<T> )
	struct is_move_constructible<T> : public false_type {};

	template<complete_or_unbounded T>
		requires ( Detail::is_referenceable_v<T> )
	struct is_move_constructible<T> : public is_constructible<T, T &&> {};

	template<complete_or_unbounded T>
	inline constexpr bool is_move_constructible_v = is_move_constructible<T>::value;

	/* FIXME: __is_nothrow_constructible is nowhere defined */
	template<complete_or_unbounded T, typename... ARGUMENTS>
	struct is_nothrow_constructible : integral_constant<bool, __is_nothrow_constructible( T, ARGUMENTS... )> {};

	template<complete_or_unbounded T>
	inline constexpr bool is_nothrow_constructible_v = is_nothrow_constructible<T>::value;

	/* FIXME: __is_nothrow_constructible is nowhere defined */
	template<complete_or_unbounded T>
	struct is_nothrow_default_constructible : public integral_constant<bool, __is_nothrow_constructible( T )> {};

	template<complete_or_unbounded T>
	inline constexpr bool is_nothrow_default_constructible_v = is_nothrow_default_constructible<T>::value;

	template<complete_or_unbounded T>
	struct is_nothrow_copy_constructible;

	template<complete_or_unbounded T>
		requires ( !Detail::is_referenceable_v<T> )
	struct is_nothrow_copy_constructible<T> : public false_type {};

	template<complete_or_unbounded T>
		requires ( Detail::is_referenceable_v<T> )
	struct is_nothrow_copy_constructible<T> : public is_nothrow_constructible<T, const T &> {};

	template<complete_or_unbounded T>
	inline constexpr bool is_nothrow_copy_constructible_v = is_nothrow_copy_constructible<T>::value;

	template<complete_or_unbounded T>
	struct is_nothrow_move_constructible;

	template<complete_or_unbounded T>
		requires ( !Detail::is_referenceable_v<T> )
	struct is_nothrow_move_constructible<T> : public false_type {};

	template<complete_or_unbounded T>
		requires ( Detail::is_referenceable_v<T> )
	struct is_nothrow_move_constructible<T> : public is_nothrow_constructible<T, T &&> {};

	template<complete_or_unbounded T>
	inline constexpr bool is_nothrow_move_constructible_v = is_nothrow_move_constructible<T>::value;

	template<complete_or_unbounded T, typename U>
	struct is_assignable : integral_constant<bool, __is_assignable( T, U )> {};

	template<complete_or_unbounded T, typename U>
	inline constexpr bool is_assignable_v = is_assignable<T, U>::value;

	template<complete_or_unbounded T>
	struct is_copy_assignable : public integral_constant<bool, ( Detail::is_referenceable_v<T> && is_assignable_v<T &, const T&> )> {};

	template<complete_or_unbounded T>
	inline constexpr bool is_copy_assignable_v = is_copy_assignable<T>::value;

	template<complete_or_unbounded T>
	struct is_move_assignable : public integral_constant<bool, ( Detail::is_referenceable_v<T> && is_assignable_v<T &, T &&> )> {};

	template<complete_or_unbounded T>
	inline constexpr bool is_move_assignable_v = is_move_assignable<T>::value;

	template<typename, typename> 
	struct is_same	: public false_type {};

	template<typename T> 
	struct is_same<T, T> : public true_type {};

	template<typename T, typename U>
	inline constexpr bool is_same_v = is_same<T, U>::value;

	template<typename T>
	struct is_object : public __not_<__or_<is_function<T>, is_reference<T>, is_void<T>>>::type {};

	template<typename T>
	inline constexpr bool is_object_v = is_object<T>::value;

	template<typename T>
	struct remove_reference { 
		using type = T;
	};

	template<typename T>
	struct remove_reference<T &> {
		using type = T;
	};

	template<typename T>
	struct remove_reference<T &&> { 
		using type = T;
	};

	template<typename T>
	using remove_reference_t = typename remove_reference<T>::type;

	namespace Detail {

		template<typename T, typename U = T&&> 
		U _declval( int );

		template<typename T>
		T _declval( long );

	}
	
	template<typename T> 
	auto declval( void ) noexcept -> decltype( Detail::_declval<T>( 0 ) );

	template<typename T> 
	struct remove_cv { 
		using type = T;
	};

	template<typename T>
	struct remove_cv<const T> { 
		using type = T;
	};

	template<typename T>
	struct remove_cv<volatile T> { 
		using type = T;
	};

	template<typename T>
	struct remove_cv<const volatile T> { 
		using type = T;
	};

	template<typename T>
	using remove_cv_t = typename remove_cv<T>::type;

	template<typename T>
	struct remove_cvref : remove_cv<T> {};

	template<typename T>
	struct remove_cvref<T &> : remove_cv<T> {};

	template<typename T>
	struct remove_cvref<T &&> : remove_cv<T> {};

	template<typename T>
	using remove_cvref_t = typename remove_cvref<T>::type;

	namespace Detail {

		template<typename T, bool = __or_<is_referenceable<T>, is_void<T>>::value>
		struct add_pointer_helper {
			using type = T;
		};

		template<typename T>
		struct add_pointer_helper<T, true> {
			using type = typename remove_reference<T>::type *;
		};
	}

	template<typename T>
	struct add_pointer : public Detail::add_pointer_helper<T> {};

	template<typename T>
	using add_pointer_t = typename add_pointer<T>::type;

	namespace Detail {

		template<typename T, bool IS_ARRAY = is_array<T>::value, bool IS_FUNCTION = is_function<T>::value>
		struct decay_selector;

		template<typename T>
		struct decay_selector<T, false, false> {
			using type = remove_cv_t<T>;
		};

		template<typename T>
		struct decay_selector<T, true, false> {
			using type = typename remove_extent<T>::type *;
		};

		template<typename T>
		struct decay_selector<T, false, true> {
			using type = typename add_pointer<T>::type;
		};
	}

	template<typename T>
	struct decay {
		using type = typename Detail::decay_selector<typename remove_reference<T>::type>::type;
	};

	template<typename T>
	using decay_t = typename decay<T>::type;

	template<typename... Ts>
	struct common_type;

	/* Specialization for sizeof...( T ) == 0 */
	template<>
	struct common_type<> {};

	/* Specialiation for sizeof...( T ) == 1. The same type, if any, as common_type<T, T> */
	template<typename T>
	struct common_type<T> : public common_type<T, T> {};

	namespace Detail {

		template<typename T>
		struct success_type {
			using type = T;
		};

		struct failure_type {};

		/* If sizeof...( T ) == 2 */
		/* TODO: Might be merged with 'template<typename T1, typename T2> struct common_type<T1, T2>' */
		template<typename T1, typename T2, typename D1 = decay_t<T1>, typename D2 = decay_t<T2>>
		struct common_type_impl {
			using type = common_type<D1, D2>;
		};

		struct do_common_type_impl {
			template<typename T, typename U>
			using cond_t = decltype( true ? declval<T>() : declval<U>() );

			template<typename T, typename U>
			static success_type<decay_t<cond_t<T, U>>> s_test( int );
		};

		template<typename T1, typename T2>
		struct common_type_impl<T1, T2, T1, T2> : private do_common_type_impl {
			using type = decltype( s_test<T1, T2>( 0 ) );
		};

		template<typename...>
		struct common_type_pack {};

		template<typename, typename, typename = void>
		struct common_type_fold;

		template<typename C, typename... Ts>
		struct common_type_fold<C, common_type_pack<Ts...>, void_t<typename C::type>> : public common_type<typename C::type, Ts...> {};

		template<typename C, typename T>
		struct common_type_fold<C, T, void> {};

	}

	/* If sizeof...( T ) == 2 */
	template<typename T1, typename T2>
	struct common_type<T1, T2> : public Detail::common_type_impl<T1, T2>::type {};

	/* If sizeof...( T ) > 2 */
	template<typename T, typename U, typename... OTHERS>
	struct common_type<T, U, OTHERS...> : public Detail::common_type_fold<common_type<T, U>, Detail::common_type_pack<OTHERS...>> {};

	template<typename T1, typename T2>
	using common_type_t = typename common_type<T1, T2>::type;

	template<typename T, typename U, template<typename> class T_QUAL, template<typename> class U_QUAL>
	struct basic_common_reference {};

	namespace Detail {

		template<class T>
		auto test_returnable( int ) -> decltype(
			void ( static_cast<T(*)()>( nullptr ) ), true_type {}
		);

		template<class>
		auto test_returnable(...) -> false_type;

		template<class FROM, class TO>
		auto test_implicitly_convertible( int ) -> decltype(
			void( declval<void(&)( TO )>()( declval<FROM>() ) ), true_type {}
		);

		template<class, class>
		auto test_implicitly_convertible(...) -> false_type;

	}

	template<typename FROM, typename TO>
	struct is_convertible : public integral_constant<bool,
		( decltype( Detail::test_returnable<TO>( 0 ) )::value && decltype( Detail::test_implicitly_convertible<FROM, TO>( 0 ) )::value ) ||
		( is_void<FROM>::value && is_void<TO>::value )
	> {};

	template<typename FROM, typename TO>
	inline constexpr bool is_convertible_v = is_convertible<FROM, TO>::value;

	template<typename TO_ELEMENT_TYPE, typename FROM_ELEMENT_TYPE>
	using is_array_convertible = is_convertible<FROM_ELEMENT_TYPE(*)[], TO_ELEMENT_TYPE(*)[]>;

	namespace Detail {
			
		template<typename UNQUALIFIED, bool IS_CONST, bool IS_VOLATILE>
		struct cv_selector;

		template<typename UNQUALIFIED>
		struct cv_selector<UNQUALIFIED, false, false> {
			using type = UNQUALIFIED;
		};

		template<typename UNQUALIFIED>
		struct cv_selector<UNQUALIFIED, false, true> {
			using type = volatile UNQUALIFIED;
		};

		template<typename UNQUALIFIED>
		struct cv_selector<UNQUALIFIED, true, false> {
			using type = const UNQUALIFIED;
		};

		template<typename UNQUALIFIED>
		struct cv_selector<UNQUALIFIED, true, true> {
			using type = const volatile UNQUALIFIED;
		};

		template<typename QUALIFIED, typename UNQUALIFIED, bool IS_CONST = is_const<QUALIFIED>::value, bool IS_VOLATILE = is_volatile<QUALIFIED>::value>
		struct match_cv_qualifiers {
			using type = typename cv_selector<UNQUALIFIED, IS_CONST, IS_VOLATILE>::type;
		};

		template<typename FROM, typename TO>
		using copy_cv = typename match_cv_qualifiers<FROM, TO>::type;

		template<typename X, typename Y>
		using cond_res = decltype( false ? declval<X(&)()>()() : declval<Y(&)()>()() );

		template<typename X, typename Y>
		using condres_cvref = cond_res<copy_cv<X, Y> &, copy_cv<Y, X> &>;

		/* TODO: Convert SFINAE into concept checks aka 'requires" to get rid of 'enable_if' */
		template<typename X, typename Y, typename = void>
		struct common_ref_impl {};

		/* TODO: Shall be suffixed by '_t" to indicate it is about the type */
		template<typename X, typename Y>
		using common_ref = typename common_ref_impl<X, Y>::type;

		template<typename X, typename Y>
		struct common_ref_impl<X &, Y &, void_t<condres_cvref<X, Y>>> : enable_if<is_reference_v<condres_cvref<X, Y>>, condres_cvref<X, Y>> {};

		template<typename X, typename Y>
		using common_ref_C = remove_reference_t<common_ref<X &, Y &>> &&;

		template<typename X, typename Y>
		struct common_ref_impl<X &&, Y &&, _Require<is_convertible<X &&, common_ref_C<X, Y>>, is_convertible<Y &&, common_ref_C<X, Y>>>> {
			using type = common_ref_C<X, Y>;
		};

		template<typename X, typename Y>
		using common_ref_D = common_ref<const X &, Y &>;

		template<typename X, typename Y>
		struct common_ref_impl<X &&, Y &, _Require<is_convertible<X &&, common_ref_D<X, Y>>>> {
			using type = common_ref_D<X, Y>;
		};

		template<typename X, typename Y>
		struct common_ref_impl<X &, Y &&> : public common_ref_impl<Y &&, X &> {};

		template<typename T>
		struct xref {
			template<typename U>
			using type = copy_cv<T, U>;
		};

		template<typename T>
		struct xref<T &> {
			template<typename U>
			using type = copy_cv<T, U> &;
		};

		template<typename T>
		struct xref<T &&> {
			template<typename U>
			using type = copy_cv<T, U> &&;
		};

		template<typename T1, typename T2>
		using basic_common_ref = typename basic_common_reference<remove_cvref_t<T1>, remove_cvref_t<T2>, xref<T1>::template type, xref<T2>::template type>::type;
		
		template<typename T1, typename T2, int BULLET = 1, typename = void>
		struct common_reference_impl : common_reference_impl<T1, T2, BULLET + 1> {};

		template<typename T1, typename T2>
		struct common_reference_impl<T1 &, T2 &, 1, void_t<common_ref<T1 &, T2 &>>> {
			using type = common_ref<T1 &, T2 &>;
		};

		template<typename T1, typename T2>
		struct common_reference_impl<T1 &&, T2 &&, 1, void_t<common_ref<T1 &&, T2 &&>>> {
			using type = common_ref<T1 &&, T2 &&>;
		};

		template<typename T1, typename T2>
		struct common_reference_impl<T1 &, T2 &&, 1, void_t<common_ref<T1 &, T2 &&>>> {
			using type = common_ref<T1 &, T2 &&>;
		};

		template<typename T1, typename T2>
		struct common_reference_impl<T1 &&, T2 &, 1, void_t<common_ref<T1 &&, T2 &>>> {
			using type = common_ref<T1 &&, T2 &>;
		};

		template<typename T1, typename T2>
		struct common_reference_impl<T1, T2, 2, void_t<basic_common_ref<T1, T2>>> {
			using type = basic_common_ref<T1, T2>;
		};

		template<typename T1, typename T2>
		struct common_reference_impl<T1, T2, 3, void_t<cond_res<T1, T2>>> {
			using type = cond_res<T1, T2>;
		};

		template<typename T1, typename T2>
		struct common_reference_impl<T1, T2, 4, void_t<common_type_t<T1, T2>>> {
			using type = common_type_t<T1, T2>;
		};

		template<typename T1, typename T2>
		struct common_reference_impl<T1, T2, 5, void> {};
	}

	template<typename T>
	struct add_cv {
		using type = const volatile T;
	};

	template<typename T>
	struct add_const {
		using type = const T;
	};

	template<typename T>
	struct add_volatile {
		using type = volatile T;
	};

	namespace Detail {

		template<typename> 
		struct is_pointer_helper : public false_type {};

		template<typename T> 
		struct is_pointer_helper<T*> : public true_type {};

	}

	template<typename T>
	struct is_pointer : public Detail::is_pointer_helper<remove_cv<T>>::type {};

	template <typename T>
  	inline constexpr bool is_pointer_v = is_pointer<T>::value;

	namespace Detail {

		template<typename T, bool = is_referenceable<T>::value>
		struct add_lvalue_reference_helper {
			using type = T;
		};

		template<typename T>
		struct add_lvalue_reference_helper<T, true> {
			using type = T &;
		};

	}

	template<typename T>
	struct add_lvalue_reference : public Detail::add_lvalue_reference_helper<T> {};

	template<typename... Ts>
	struct common_reference;

	template<typename... Ts>
	using common_reference_t = typename common_reference<Ts...>::type;

	/* sizeof...( T ) == 0 */
	template<>
	struct common_reference<> {};

	/* sizeof...( T ) == 1 */ 
	template<typename T>
	struct common_reference<T> {
		using type = T;
	};

	template<typename T1, typename T2>
	struct common_reference<T1, T2> : Detail::common_reference_impl<T1, T2> {};

	namespace Detail {

		template<typename T1, typename T2, typename... Ts>
		struct common_type_fold<common_reference<T1, T2>, common_type_pack<Ts...>, void_t<common_reference_t<T1, T2>>>
			: public common_reference<common_reference_t<T1, T2>, Ts...>
		{};

	}

	template<typename T1, typename T2, typename... Ts>
	struct common_reference<T1, T2, Ts...> : Detail::common_type_fold<common_reference<T1, T2>, Detail::common_type_pack<Ts...>> {};

	template<typename BASE, typename DERIVED> 
	struct is_base_of : public integral_constant<bool, __is_base_of( BASE, DERIVED )> {};

	template<typename BASE, typename DERIVED>
	inline constexpr bool is_base_of_v = is_base_of<BASE, DERIVED>::value;

	/* For any other type than those for which there is a specialization defined, define false */
	template<typename T> 
	struct is_integral : public false_type {};

	template<>
	struct is_integral<bool> : public true_type {};

	template<>
	struct is_integral<char> : public true_type {};

	template<>
	struct is_integral<unsigned char> : public true_type {};

	template<>
	struct is_integral<short> : public true_type {};

	template<>
	struct is_integral<unsigned short> : public true_type {};

	template<>
	struct is_integral<int> : public true_type {};

	template<>
	struct is_integral<unsigned int> : public true_type {};

	template<>
	struct is_integral<long> : public true_type {};

	template<>
	struct is_integral<unsigned long> : public true_type {};

	template<>
	struct is_integral<long long> : public true_type {};

	template<>
	struct is_integral<unsigned long long> : public true_type {};

	template<typename T>
	inline constexpr bool is_integral_v = is_integral<T>::value;

	/* TODO: Should be true only for arithmetic types */
	template<typename T> 
	struct is_signed : public integral_constant<bool, ( T( -1 ) < T( 0 ) )> {};

	template<typename T>
	inline constexpr bool is_signed_v = is_signed<T>::value;

	template<typename T>
	struct is_floating_point : integral_constant<bool,
		is_same_v<float, typename remove_cv<T>::type> ||
		is_same_v<double, typename remove_cv<T>::type> ||
		is_same_v<long double, typename remove_cv<T>::type>
		/* TODO: More C++23 floating point types to add here, if supported */
	> {};

	template<typename T>
	inline constexpr bool is_floating_point_v = is_floating_point<T>::value;

	template<typename T>
	constexpr typename remove_reference<T>::type && move( T && t ) noexcept {
		return static_cast<typename remove_reference<T>::type &&>( t );
	}

	template<typename T>
		requires ( is_nothrow_move_constructible_v<T> && is_copy_constructible_v<T> )
	constexpr const T & move_if_noexcept( T & t ) noexcept {
		return move( t );
	}

	template<typename T>
		requires ( !( is_nothrow_move_constructible_v<T> && is_copy_constructible_v<T> ) )
	constexpr T && move_if_noexcept( T & t ) noexcept {
		return move( t );
	}

	template<typename T>
	constexpr T && forward(typename remove_reference<T>::type & t) noexcept {
		return static_cast<T &&>(t);
	}

	template<typename T>
	constexpr T && forward(typename remove_reference<T>::type && t) noexcept {
	  static_assert( !is_lvalue_reference<T>::value, "template argument substituting T is an lvalue reference type" );
	  return static_cast<T &&>( t );
	}

	template<typename T>
	constexpr void swap( T & a, T & b ) noexcept {
		T c( move( a ) );
		a = move( b );
		b = move( c );
	}

	namespace Detail {

		template<typename T>
		struct is_nothrow_swappable_helper {
		private:
			static integral_constant<bool, noexcept( swap( declval<T &>(), declval<T &>() ) )> test( int );

			static false_type test( ... );

		public:
			using type = decltype( test( 0 ) );
		};

	}

	template<typename T>
	struct is_nothrow_swappable : public Detail::is_nothrow_swappable_helper<T>::type {};

	template<typename T>
	inline constexpr bool is_nothrow_swappable_v = is_nothrow_swappable<T>::value;

	namespace Detail {

		template<typename T>
		struct is_member_pointer_helper : public false_type {};

		template<typename T, typename C>
		struct is_member_pointer_helper<T C::*> : public true_type {};

	}

	template<typename T>
	struct is_member_pointer : public Detail::is_member_pointer_helper<remove_cv_t<T>>::type {};

	template<typename T>
	inline constexpr bool is_member_pointer_v = is_member_pointer<T>::value;

	namespace Detail {

		template<typename>
		struct is_member_function_pointer_helper : public false_type {};

		template<typename T, typename C>
		struct is_member_function_pointer_helper<T C:: *> : public is_function<T>::type {};

	}

	template<typename T>
	struct is_member_function_pointer : public Detail::is_member_function_pointer_helper<remove_cv_t<T>>::type {};

	template<typename T>
	inline constexpr bool is_member_function_pointer_v = is_member_function_pointer<T>::value;

	/* Checks whether 'T' is a non-static member object pointer. Provides the member constant value which is equal to 'true', 
	 * if 'T' is a non-static member object pointer type. Otherwise, value is equal to 'false' */
	template<class T>
	struct is_member_object_pointer : public integral_constant<bool, is_member_pointer<T>::value && !is_member_function_pointer<T>::value> {};

	template<typename T>
	inline constexpr bool is_member_object_pointer_v = is_member_object_pointer<T>::value;

	namespace Detail {

		struct invoke_memfun_ref {};
		struct invoke_memfun_deref {};
		struct invoke_memobj_ref {};
		struct invoke_memobj_deref {};
		struct invoke_other {};

		template<typename T, typename TAG>
		struct result_of_success : success_type<T> {
			using invoke_type = TAG;
		};

		struct result_of_memfun_ref_implementation {
			template<typename F, typename T1, typename... ARGUMENTS>
			static result_of_success<decltype( ( declval<T1>().*declval<F>() )( declval<ARGUMENTS>()... ) ), invoke_memfun_ref> test( int );

			template<typename...>
			static failure_type test( ... );
		};

		template<typename P, typename ARGUMENT, typename... ARGUMENTS>
		struct result_of_memfun_ref : private result_of_memfun_ref_implementation {
			using type = decltype( test<P, ARGUMENT, ARGUMENTS...>( 0 ) );
		};

		struct result_of_memfun_deref_implementation {
			template<typename F, typename T1, typename... ARGUMENTS>
			static result_of_success<decltype( ( (* declval<T1>() ).*declval<F>() )( declval<ARGUMENTS>()... ) ), invoke_memfun_deref> test( int );

			template<typename...>
			static failure_type test( ... );
		};

		template<typename P, typename ARGUMENT, typename... ARGUMENTS>
		struct result_of_memfun_deref : private result_of_memfun_deref_implementation {
			using type = decltype( test<P, ARGUMENT, ARGUMENTS...>( 0 ) );
		};

		struct result_of_memobj_ref_implementation {
			template<typename F, typename T1>
			static result_of_success<decltype( declval<T1>().*declval<F>() ), invoke_memobj_ref> test( int );

			template<typename, typename>
			static failure_type test( ... );
		};

		template<typename P, typename ARGUMENT>
		struct result_of_memobj_ref : private result_of_memobj_ref_implementation {
			using type = decltype( test<P, ARGUMENT>( 0 ) );
		};

		struct result_of_memobj_deref_implementation {
			template<typename F, typename T1>
			static result_of_success<decltype( (* declval<T1>() ).*declval<F>() ), invoke_memobj_deref> test( int );

			template<typename, typename>
			static failure_type test( ... );
		};

		template<typename P, typename ARGUMENT>
		struct result_of_memobj_deref : private result_of_memobj_deref_implementation {
			using type = decltype( test<P, ARGUMENT>( 0 ) );
		};

		template<typename P, typename ARGUMENT>
		struct result_of_memobj;

		template<typename RES, typename CLASS, typename ARGUMENT>
		struct result_of_memobj<RES CLASS::*, ARGUMENT> {
			using argument_value = remove_cvref_t<ARGUMENT>;
			using memptr = RES CLASS::*;
			using type = conditional<__or_<is_same<argument_value, CLASS>, is_base_of<CLASS, argument_value>>::value,
				result_of_memobj_ref<memptr, ARGUMENT>,
				result_of_memobj_deref<memptr, ARGUMENT>
			>::type::type;
		};

		template<typename P, typename ARGUMENT, typename... ARGUMENTS>
		struct result_of_memfun;

		template<typename RES, typename CLASS, typename ARGUMENT, typename... ARGUMENTS>
		struct result_of_memfun<RES CLASS::*, ARGUMENT, ARGUMENTS...> {
			using argument_value = typename remove_reference<ARGUMENT>::type;
			using memptr = RES CLASS::*;
			using type = typename conditional<is_base_of<CLASS, argument_value>::value, 
				result_of_memfun_ref<memptr, ARGUMENT, ARGUMENTS...>,
				result_of_memfun_deref<memptr, ARGUMENT, ARGUMENTS...>
			>::type::type;
		};

		template<typename T, typename U = remove_cv_t<T>>
		struct inv_unwrap {
			using type = T;
		};

		template<typename T, typename U>
		struct inv_unwrap<T, reference_wrapper<U>> {
			using type = U &;
		};

		struct result_of_other_implementation {
			template<typename F, typename... ARGUMENTS>
			static result_of_success<decltype( declval<F>()( declval<ARGUMENTS>()... ) ), invoke_other> test( int );

			template<typename...>
			static failure_type test( ... );
		};

		template<bool, bool, typename F, typename... ARGUMENTS>
		struct result_of_implementation {
			using type = failure_type;
		};

		template<typename PTR, typename ARGUMENT>
		struct result_of_implementation<true, false, PTR, ARGUMENT> : public result_of_memobj<decay_t<PTR>, typename inv_unwrap<ARGUMENT>::type> {};

		template<typename PTR, typename ARGUMENT, typename... ARGUMENTS>
		struct result_of_implementation<false, true, PTR, ARGUMENT, ARGUMENTS...> : public result_of_memfun<decay_t<PTR>, typename inv_unwrap<ARGUMENT>::type, ARGUMENTS...> {};

		template<typename F, typename... ARGUMENTS>
		struct result_of_implementation<false, false, F, ARGUMENTS...> : private result_of_other_implementation {
			using type = decltype( test<F, ARGUMENTS...>( 0 ) );
		};

	}

	/* NOTE: Alternative implementation can be found here: https://en.cppreference.com/w/cpp/types/result_of */

	/* Deduces the return type of an INVOKE expression at compile time. 'F' and all types in 'ARGUMENTS' can be any complete type, array of unknown bound, or (possibly cv-qualified) 'void'. */
	template<complete_or_unbounded F, complete_or_unbounded... ARGUMENTS>
	struct invoke_result : public Detail::result_of_implementation<
		is_member_object_pointer_v<typename remove_reference<F>::type>, 
		is_member_function_pointer_v<typename remove_reference<F>::type>,
		F, ARGUMENTS...>::type {};

	template<typename F, typename... ARGUMENTS>
	using invoke_result_t = typename invoke_result<F, ARGUMENTS...>::type;

	namespace Detail {

		/* Primary template is used for invalid 'invoke' expressions */
		template<typename RESULT, typename RET, bool = is_void<RET>::value, typename = void>
		struct is_invocable_implementation : false_type {};

		/* Used for valid 'invoke' and 'invoke<void>' expressions */
		template<typename RESULT, typename RET>
		struct is_invocable_implementation<RESULT, RET, true, void_t<typename RESULT::type>> : true_type {};

		/* Used for 'invoke<R>' to check the implicit conversion to 'R' */
		template<typename RESULT, typename RET>
		struct is_invocable_implementation<RESULT, RET, false, void_t<typename RESULT::type>> {
		private:
			/* The type of the 'invoke' expression */
			static typename RESULT::type get();

			template<typename T>
			static void conv( T );

			/* This overload is visible if 'invoke( f, arguments... )' can convert to 'T '*/
			template<typename T, typename = decltype( conv<T>( get() ) )>
			static true_type test( int );

			template<typename T>
			static false_type test( ... );
		
		public:
			using type = decltype( test<RET>( 1 ) );
		};

	}

	/* Determines whether 'F' can be invoked with the arguments 'ARGUMENTS...'. Formally, determines whether 'INVOKE( declval<F>(), declval<ARGUMENTS>()... )' 
	 * is well formed when treated as an unevaluated operand, where INVOKE is the operation defined in Callable. */
	template<complete_or_unbounded F, complete_or_unbounded... ARGUMENTS>
	struct is_invocable : Detail::is_invocable_implementation<invoke_result<F, ARGUMENTS...>, void>::type {};

	template<complete_or_unbounded F, complete_or_unbounded... ARGUMENTS>
	inline constexpr bool is_invocable_v = is_invocable<F, ARGUMENTS...>::value;

	/* Determines whether 'F' can be invoked with the arguments 'ARGUMENTS...' to yield a result that is convertible to 'R' and the implicit conversion does not bind 
	 * a reference to a temporary object (since C++23). If 'R' is cv 'void', the result can be any type. Formally, determines whether 'INVOKE<R>( declval<F>(), declval<ARGUMENTS>()... )' 
	 * is well formed when treated as an unevaluated operand, where INVOKE is the operation defined in Callable. */
	template<complete_or_unbounded RET, complete_or_unbounded F, complete_or_unbounded... ARGUMENTS>
	struct is_invocable_r : Detail::is_invocable_implementation<invoke_result<F, ARGUMENTS...>, RET>::type {};

	template<complete_or_unbounded RET, complete_or_unbounded F, complete_or_unbounded... ARGUMENTS>
	inline constexpr bool is_invocable_r_v = is_invocable_r<RET, F, ARGUMENTS...>::value;

	namespace Detail {

		template<typename F, typename T, typename... ARGUMENTS>
		constexpr bool does_not_throw( invoke_memfun_ref ) {
			return noexcept( ( declval<typename inv_unwrap<T>::type>().*declval<F>() )( declval<ARGUMENTS>()... ) );
		}

		template<typename F, typename T, typename... ARGUMENTS>
		constexpr bool does_not_throw( invoke_memfun_deref ) {
			return noexcept( ( ( *declval<T>() ).*declval<F>() )( declval<ARGUMENTS>()... ) );
		}

		template<typename F, typename T>
		constexpr bool does_not_throw( invoke_memobj_ref ) {
			return noexcept( declval<typename inv_unwrap<T>::type>().*declval<F>() );
		}

		template<typename F, typename T>
		constexpr bool does_not_throw( invoke_memobj_deref ) {
			return noexcept( ( *declval<T>() ).*declval<F>() );
		}

		template<typename F, typename... ARGUMENTS>
		constexpr bool does_not_throw( invoke_other ) {
			return noexcept( declval<F>()( declval<ARGUMENTS>()... ) );
		}

		template<typename F, typename... ARGUMENTS>
		struct is_nothrow_callable : public integral_constant<bool, does_not_throw<F, ARGUMENTS...>( invoke_result_t<F, ARGUMENTS...>{} )> {};

		template<typename F, typename... ARGUMENTS>
		inline constexpr bool is_nothrow_callable_v = is_nothrow_callable<F, ARGUMENTS...>::value;
	}

	template<complete_or_unbounded F, complete_or_unbounded... ARGUMENTS>
	struct is_nothrow_invocable : public integral_constant<bool, ( is_invocable_v<F, ARGUMENTS...> && Detail::is_nothrow_callable_v<F, ARGUMENTS...> )> {};

	template<complete_or_unbounded F, complete_or_unbounded... ARGUMENTS>
	inline constexpr bool is_nothrow_invocable_v = is_nothrow_invocable<F, ARGUMENTS...>::value;

	template<complete_or_unbounded RET, complete_or_unbounded F, complete_or_unbounded... ARGUMENTS>
	struct is_nothrow_invocable_r : public integral_constant<bool, ( is_invocable_r_v<RET, F, ARGUMENTS...> && Detail::is_nothrow_callable_v<F, ARGUMENTS...> )> {};

	template<complete_or_unbounded F, complete_or_unbounded... ARGUMENTS>
	inline constexpr bool is_nothrow_invocable_r_v = is_nothrow_invocable_r<F, ARGUMENTS...>::value;

	/* TESTING*/
	namespace Testing {

		static_assert( is_invocable_v<int()> );
		static_assert( !is_invocable_v<int(), int> );

		static_assert( is_invocable_r_v<int, int()> );
		static_assert( !is_invocable_r_v<int *, int()> );

		static_assert( is_invocable_r_v<void, void( int ), int> );
		static_assert( !is_invocable_r_v<void, void( int ), void> );

		inline auto invocable_test_func( char ) -> int( * )() {
			return nullptr;
		}

		static_assert( is_invocable_r_v<int( * )(), decltype( invocable_test_func ), char> );
		static_assert( !is_invocable_r_v<int( * )(), decltype( invocable_test_func ), void> );

	}

	template<typename T, typename... Ts> 
	struct contains : public integral_constant<bool, ( is_same<T, Ts>::value || ... )> {};

	/* Ensure U is a reference type */
	template<typename T>
	using reference = typename conditional<is_reference<T>::value, T, const T &>::type;

	/* Utility to find unsigned versions of signed intergral types */
	template<typename T>
	struct make_unsigned { using type = T; };	/* Overload to be used by all unsigned types */

	template<>
	struct make_unsigned<char> {
		using type = unsigned char;
	};

	template<>
	struct make_unsigned<signed char> {
		using type = unsigned char;
	};

	template<>
	struct make_unsigned<short> {
		using type = unsigned short;
	};

	template<>
	struct make_unsigned<int> {
		using type = unsigned int;
	};

	template<>
	struct make_unsigned<long> {
		using type = unsigned long;
	};

	template<>
	struct make_unsigned<long long> {
		using type = unsigned long long;
	};

	template<typename T>
	struct underlying_type { 
		using type = __underlying_type( T );
	};

	template<size_t N>
	struct is_alignment : public integral_constant<bool, ( ( N & ( N - 1 ) ) == 0 )> {};

	template<size_t ALIGNMENT>
	inline constexpr bool is_alignment_v = is_alignment<ALIGNMENT>::value;

	template<size_t ALIGNMENT> 
		requires is_alignment_v<ALIGNMENT>
	constexpr bool is_aligned( const void * address ) noexcept {
		return ( ( reinterpret_cast<uintptr_t>( address ) % ALIGNMENT ) == 0 );
	}

	template<size_t ALIGNMENT>
		requires is_alignment_v<ALIGNMENT>
	constexpr size_t n_bits( void ) noexcept {
#if false
		return ( ALIGNMENT ) ? ( 1 + n_bits<( ALIGNMENT >> 1 )>() ) : -1;
#else
		return __builtin_log2( ALIGNMENT );
#endif
	}

}
