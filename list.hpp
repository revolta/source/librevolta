#pragma once

#include "concepts.hpp"
#include "memory.hpp"
#include "iterator.hpp"
#include "initializer_list.hpp"
#include "algorithm.hpp"

namespace ReVolta {

    namespace Detail {

        template<Allocator ALLOCATOR> class list_node;
        template<Allocator ALLOCATOR> class list_tail_node;

        template<Allocator ALLOCATOR>
        struct list_traits {
            using allocator_type = ALLOCATOR;
            using value_type = typename allocator_type::value_type;
            using node_type = list_node<allocator_type>;
            using node_allocator_type = typename allocator_traits<allocator_type>::rebind_alloc<node_type>::other;
        };

        template<Allocator ALLOCATOR>
        class list_node_base {
            friend class list_tail_node<ALLOCATOR>;

        protected:
            base_ptr<list_node_base> m_next;

            observer_ptr<list_node_base> m_previous;

            constexpr list_node_base( void ) = delete;

            constexpr list_node_base( base_ptr<list_node_base> next, observer_ptr<list_node_base> previous ) : m_next( move( next ) ), m_previous( previous ) {}

            /* Returns true if the tail node is referencing itself internally, thus effectively forming empty list */
            constexpr inline bool is_self_referencing( void ) const {
                /* Return true if 'this->next == this' and 'this->previous == this' as well */
                return ( ( this->next() == make_observer<list_node_base>( this ) ) && ( this->previous() == make_observer<list_node_base>( this ) ) );
            }

        public:
            /* Shall perform recoupling before being deallocated */ 
            constexpr ~list_node_base( void ) {}

            /* List node is non copyable */
            constexpr list_node_base & operator=( const list_node_base & ) = delete;

            /* List node shall be movable */
            constexpr list_node_base & operator=( list_node_base && ) = default;

            /* Get the observer to the next node linked. Internally the node holds the owning pointer of that node but this accessor function returns just the observer (non owning) of that */
            constexpr observer_ptr<list_node_base> next( void ) const {  
                return make_observer( this->m_next );
            }

            /* Get the observer to the previous node linked */
            constexpr observer_ptr<list_node_base> previous( void ) const {
                return make_observer( this->m_previous );
            }

            /* Returns observer pointer to the new node being emplaced */
            template<typename... ARGUMENTS>
            constexpr observer_ptr<list_node_base> emplace_before( const typename list_traits<ALLOCATOR>::node_allocator_type & allocator, ARGUMENTS &&... arguments ) {
                observer_ptr<list_node_base> prev { this->previous() };
                /* Allocate new node which becomes the owner of this and links the original previous node as it's own previous node, and store the newly allocated node to the former previous node of 'this' */
                prev->m_next = base_ptr<list_node_base>{ allocate_unique<typename list_traits<ALLOCATOR>::node_type>( allocator, move( this->previous()->m_next ), move( this->m_previous ), forward<ARGUMENTS>( arguments )... )  };
                /* Set current previous pointer as the observer to owning pointer to the newly allocated node */
                this->m_previous = make_observer<list_node_base>( ( prev->m_next ).get() );
                /* Return observer pointer to the new node allocated */
                return this->previous();
            }

            /* Interconnects current node's previous and next directly effectively to link current node to itself. Returns owning pointer to itself for further destruction */
            constexpr base_ptr<list_node_base> remove( void ) {
                observer_ptr<list_node_base> prev { this->previous() };

                ( this->next()->m_previous ).swap( this->m_previous );
                ( prev->m_next ).swap( this->m_next );

                return move( this->m_next );
            }

            constexpr void swap( list_node_base & other ) noexcept( true/* TODO */ ) {
#if false
                /* TODO: Try to order the sequence of swaps the way a minimum of those temporary observers are required */
                /* TODO: Find better variable names */
                observer_ptr<list_node_base> A { this->previous() };
                observer_ptr<list_node_base> C { this->next() };
                observer_ptr<list_node_base> X { other.previous() };
                observer_ptr<list_node_base> Z { other.next() };

                ( this->m_next ).swap( other.m_next );
                ( A->m_next ).swap( X->m_next );
                ( this->m_previous ).swap( other.m_previous );
                ( C->m_previous ).swap( Z->m_previous );

                ( this->m_next ).swap( other.m_next );
                ( this->previous()->m_next ).swap( other.previous()->m_next );
                ( this->m_previous ).swap( other.m_previous );
                ( this->next()->m_previous ).swap( other.next()->m_previous );
#else
                ( this->previous()->m_next ).swap( other.previous()->m_next );
                ( this->m_previous ).swap( other.m_previous );
                ( this->next()->m_previous ).swap( other.next()->m_previous );
                ( this->m_next ).swap( other.m_next );
#endif
            }
        };

        template<Allocator ALLOCATOR>
        class list_tail_node : public list_node_base<ALLOCATOR> {
        public:
            /* Default constructor constructs the self-referencing 'list_tail_node' */
            constexpr list_tail_node( void ) : list_node_base<ALLOCATOR>( base_ptr<list_node_base<ALLOCATOR>>{ this, blank_deleter<list_tail_node>{} }, make_observer<list_node_base<ALLOCATOR>>( this ) ) {}

            constexpr list_tail_node( base_ptr<list_node_base<ALLOCATOR>> next, observer_ptr<list_node_base<ALLOCATOR>> previous ) noexcept : list_node_base<ALLOCATOR>{ move( next ), previous } {}

            /* Construct tail node by moving the linkage from 'other'. If 'other' does not reference any nodes (is self-referencing), the default 'list_tail_node' is constructed through the default constructor */
            constexpr list_tail_node( list_tail_node && other ) : list_tail_node( base_ptr<list_node_base<ALLOCATOR>>{ nullptr }, make_observer<list_node_base<ALLOCATOR>>( nullptr ) ) {
                if( !other.is_self_referencing() ) {
                    /* 'temp' is a self-referencing 'list_tail_node' */
                    base_ptr<list_node_base<ALLOCATOR>> temp { this, blank_deleter<list_tail_node>{} };
                    /* Move the owning pointer to the 'other' tail node from the last list node to 'temp'. 
                    * After the swap, the 'temp' owns 'other' while the last list node links 'this' tail node */
                    temp.swap( other.previous()->m_next );
                    /* 'this->m_next' equals 'nullptr' thanks to the constructor delegation. 
                    * Once swapped, 'temp->m_next' equals 'nullptr', 'this->m_next' points to the first list_node of the list being moved. */
                    ( temp->m_next ).swap( this->m_next );
                    this->m_next->m_previous = make_observer<list_node_base<ALLOCATOR>>( this );
                    /* Move the link to tail's previous node trom 'other' to 'this' */
                    this->m_previous = move( temp->m_previous );
                    /* Prevent double destruction */
                    temp.release();
                }
            }

            constexpr ~list_tail_node( void ) {
                this->m_next.release();
                this->m_previous.release();
            }

            /* Non-copyable - to avoid two lists referencing the same nodes */
            constexpr list_tail_node( const list_tail_node & ) = delete;

            /* Non-copyable */
            constexpr list_tail_node & operator=( const list_tail_node & ) = delete;

            constexpr list_tail_node & operator=( list_tail_node && other ) {
                list_tail_node{ move( other ) }.swap( *this );
                return *this;
            }

            /* Converts 'this' to self referencing tail node. Returns owning pointer to the originally referenced next node. */
            constexpr base_ptr<list_node_base<ALLOCATOR>> reset( void ) {
                base_ptr<list_node_base<ALLOCATOR>> self { this, blank_deleter<list_tail_node>{} };
                self.swap( this->m_next );
                return self;
            }
        };

        template<Allocator ALLOCATOR>
        class list_node : public list_node_base<ALLOCATOR> {
            typename ALLOCATOR::value_type   m_data;

        public:
            /* By default construct self referencing list node containing the default value of underlying value type */
            constexpr list_node( void ) : list_node_base<ALLOCATOR>( base_ptr<list_node_base<ALLOCATOR>>{ this, default_deleter<list_node>{} }, make_observer<list_node_base<ALLOCATOR>>( this ) ) {
                construct_at( addressof( this->m_data ) );
            }

            template<typename... ARGUMENTS>
            constexpr list_node( base_ptr<list_node_base<ALLOCATOR>> next, observer_ptr<list_node_base<ALLOCATOR>> previous, ARGUMENTS &&... arguments ) : list_node_base<ALLOCATOR>{ move( next ), previous } {
                construct_at( addressof( this->m_data ), forward<ARGUMENTS>( arguments )... );
            }

            constexpr observer_ptr<typename ALLOCATOR::value_type> value( void ) const {
                return make_observer<typename ALLOCATOR::value_type>( addressof( m_data ) );
            }
        };

    }

    template<Allocator ALLOCATOR, typename REFERENCE>
    class list_iterator {
    public:
        using allocator_type = ALLOCATOR;
        using value_type = typename Detail::list_traits<allocator_type>::value_type;
        using node_type = typename Detail::list_traits<allocator_type>::node_type;
        using pointer = observer_ptr<value_type>;
        using reference = REFERENCE;
        using difference_type = ptrdiff_t;
        using iterator_category = bidirectional_iterator_tag;

    private:
        /* Non owning pointer referring to the list node the iterator points to */
        observer_ptr<Detail::list_node_base<allocator_type>> m_node;

    public:
        constexpr list_iterator( void ) : list_iterator( nullptr ) {}

        constexpr list_iterator( nullptr_t ) : m_node( nullptr ) {}

        constexpr explicit list_iterator( const observer_ptr<Detail::list_node_base<allocator_type>> & node ) : m_node( node ) {}

        /* Copy constructor */
        constexpr list_iterator( const list_iterator & other ) : m_node( other.m_node ) {}

        /* Move constructor */
        constexpr list_iterator( list_iterator && other ) : m_node( move( other.m_node ) ) {}

        /* Destructor */
        constexpr ~list_iterator( void ) = default;

        /* Copy assignment operator */
        constexpr list_iterator & operator=( const list_iterator & other ) {
            list_iterator{ other }.swap( *this );
            return *this;
        }

        /* Move assignment operator */
        constexpr list_iterator & operator=( list_iterator && other ) {
            list_iterator{ move( other ) }.swap( *this );
            return *this;
        }

        /* Swap */
        constexpr void swap( list_iterator & other ) noexcept {
            ReVolta::swap( this->m_node, other.m_node );
        }

        /* Prefix increment operator */
        constexpr list_iterator & operator++( void ) noexcept {
            this->m_node = this->m_node->next();
            return *this;
        }

        /* Postfix increment operator */
        constexpr list_iterator operator++( int ) noexcept {
            list_iterator temp { *this };
            this->m_node = this->m_node->next();
            return temp;
        }

        constexpr list_iterator & operator+=( difference_type n ) {
            while( n --> 0 ) {
                this->operator++();
            }
            return *this;
        }

        constexpr list_iterator & operator+( difference_type n ) {
            this->operator+=( n );
            return *this;
        }

        /* Prefix decrement operator */
        constexpr list_iterator & operator--( void ) noexcept {
            this->m_node = this->m_node->previous();
            return *this;
        }

        /* Postfix decrement operator */
        constexpr list_iterator operator--( int ) noexcept {
            list_iterator temp { *this };
            this->m_node = this->m_node->previous();
            return temp;
        }

        constexpr list_iterator & operator-=( difference_type n ) {
            while( n --> 0 ) {
                this->operator--();
            }
            return *this;
        }

        constexpr list_iterator & operator-( difference_type n ) {
            this->operator-=( n );
            return *this;
        }

        constexpr reference operator*( void ) const {
            return *static_cast<node_type *>( this->m_node.get() )->value();
        }

        constexpr pointer operator->( void ) const noexcept {
            return ( static_cast<node_type *>( this->m_node.get() ) )->value();
        }

        constexpr bool operator==( const list_iterator & other ) const noexcept {
            return m_node == other.m_node;
        }

        constexpr bool operator!=( const list_iterator & other ) const noexcept {
            return m_node != other.m_node;
        }

        constexpr Detail::list_node_base<allocator_type> & node( void ) {
            return *m_node;
        }
    };

    template<typename T, Allocator ALLOCATOR = allocator<T>>
    class list {
    public:
        using value_type = T;
        using allocator_type = ALLOCATOR;
        using size_type = size_t;
        using difference_type = ptrdiff_t;
        using reference = value_type &;
        using const_reference = const value_type &;
        using pointer = typename allocator_traits<allocator_type>::pointer;
        using const_pointer = typename allocator_traits<allocator_type>::const_pointer;
        using iterator = list_iterator<allocator_type, reference>;
        using const_iterator = list_iterator<allocator_type, const_reference>;
        using reverse_iterator = ReVolta::reverse_iterator<iterator>;
        using const_reverse_iterator = ReVolta::reverse_iterator<const_iterator>;

        using node_type = typename Detail::list_traits<allocator_type>::node_type;
        using node_allocator_type = typename Detail::list_traits<allocator_type>::node_allocator_type;

        /* Check the relation between the value_type and its allocator */
        static_assert( is_same_v<typename allocator_type::value_type, value_type>, "list must have the same value_type as its allocator" );

    private:
        Detail::list_tail_node<allocator_type> m_tail;
        
        [[no_unique_address]] node_allocator_type m_node_allocator;

        inline node_allocator_type & get_node_allocator( void ) noexcept {
            return m_node_allocator;
        }

        inline const node_allocator_type & get_node_allocator( void ) const noexcept {
            return m_node_allocator;
        }

    public:
        /* Default constructor. Constructs an empty container with a default-constructed allocator */
        list( void ) : list( allocator_type{} ) {}

        /* Constructs an empty container with the given 'allocator' */
        explicit list( const allocator_type & allocator ) : m_tail(), m_node_allocator( allocator ) {}

        /* Constructs the container with count copies of elements with 'value' */
        explicit list( size_type count, const value_type & value, const allocator_type & allocator = allocator_type{} ) : list( allocator ) {
            this->insert( this->cbegin(), count, value );
        } 

        /* Constructs the container with count default-inserted instances of 'value_type'. No copies are made. */
        explicit list( size_type count, const allocator_type & allocator = allocator_type{} ) : list( allocator ) {
            this->insert( this->cbegin(), count, value_type{} );
        }

        /* Constructs the container with the contents of the range ['first', 'last') */
        template<input_iterator I>
        list( I first, I last, const allocator_type & allocator = allocator_type{} ) : list( allocator ) {
            this->insert( this->cbegin(), first, last );
        }

        /* Copy constructor. Constructs the container with the copy of the contents of 'other' */
        list( const list & other ) : list( other, allocator_traits<allocator_type>::select_on_container_copy_construction( other.get_allocator() ) ){}

        list( const list & other, const allocator_type & allocator ) : list( other.begin(), other.end(), allocator ) {}

        /* Move constructor. Constructs the container with the exact contents of other using move semantics. 
         * Allocator is obtained by move-construction from the allocator belonging to other */
        list( list && other ) = default;

private:
        /* Called from list's move assignment operator 'list & operator=( list && other )' */
        void move_assign_allocators_equal( list && other, [[maybe_unused]] true_type allocators_always_equal ) noexcept {
            this->clear();
            /* Move all the nodes from 'other' to 'this' */
            this->m_tail = move( other.m_tail );
            /* Once the allocator propagates, move it as well */
            if constexpr ( allocator_traits<node_allocator_type>::propagate_on_container_move_assingment::value ) {
                this->m_node_allocator = move( other.m_node_allocator );
            }
        }

        /* Called from list's move assignment operator 'list & operator=( list && other )' */
        void move_assign_allocators_equal( list && other, [[maybe_unused]] false_type allocators_always_equal ) {
            /* Even though the allocators are not always equal, they might be equal anyway (but not always) */
            if( this->get_node_allocator() == other.get_node_allocator() ) {
                /* Move considering equal allocators */
                this->move_assign_allocators_equal( move( other ), true_type{} );
            } else {
                /* The rvalue's allocator cannot be moved, or is not equal, so we need to individually move each element. */
                this->assign( make_move_iterator( other.begin() ), make_move_iterator( other.end() ) );
            }
        }

        /* Called from allocator extended move constructor 'list( list && other, const allocator_type & allocator )' */
        list( list && other, const allocator_type & allocator, [[maybe_unused]] true_type allocator_always_equal ) noexcept 
            : m_tail( move( other.m_tail ) ), m_node_allocator( move( other.m_node_allocator ) ) {} 

        /* Called from allocator extended move constructor 'list( list && other, const allocator_type & allocator )' */
        list( list && other, const allocator_type & allocator, [[maybe_unused]] false_type allocator_always_equal ) {
            if( this->get_node_allocator() == other.get_node_allocator() ) {
                /* Move all the nodes from 'other' to 'this' */
                this->m_tail = move( other.m_tail );
            } else {
                insert( this->begin(), make_move_if_noexcept_iterator( other.begin() ), make_move_if_noexcept_iterator( other.end() ) );
            }
        }

public:
        /* Allocator-extended move constructor. Using alloc as the allocator for the new container, moving the contents from other; 
         * if alloc != other.get_allocator(), this results in an element-wise move */
        list( list && other, const allocator_type & allocator ) noexcept( allocator_traits<node_allocator_type>::is_always_equal::value ) 
              /* Redirect to one of the private constructors depending on whether the allocator is always equal or not */
            : list( move( other ), allocator, typename allocator_traits<node_allocator_type>::is_always_equal{} ) {}

        /* Constructs the container with the contents of the initializer list 'init' */
        list( initializer_list<value_type> init, const allocator_type & allocator = allocator_type{} ) : list( init.begin(), init.end(), allocator ) {}

        ~list( void ) = default;

        /* Copy assignment operator. Replaces the contents with a copy of the contents of 'other' */
        list & operator=( const list & other ) {
            /* Prevent self copy over itself */
            if( this != addressof( other ) ) {
                /* If std::allocator_traits<allocator_type>::propagate_on_container_copy_assignment::value is true, 
                 * the allocator of *this is replaced by a copy of that of other. If the allocator of *this after assignment 
                 * would compare unequal to its old value, the old allocator is used to deallocate the memory, 
                 * then the new allocator is used to allocate it before copying the elements. */
                if constexpr ( allocator_traits<node_allocator_type>::propagate_on_container_copy_assignment::value ) {
                    if( !( allocator_traits<node_allocator_type>::is_always_equal::value ) && ( this->get_node_allocator() != other.get_node_allocator() ) ) {
                        /* Replacement allocator cannot free existing storage */
                        this->clear();
                    }
                    this->m_node_allocator = other.m_node_allocator;
                }
                this->assign( other.begin(), other.end() );
            }
            return *this;
        }

        /* Move assignment operator. Replaces the contents with those of other using move semantics 
         * (i.e. the data in other is moved from other into this container). other is in a valid but unspecified state afterwards */
        list & operator=( list && other ) noexcept( allocator_traits<allocator_type>::is_always_equal::value ) {
            /* Move storage if the node allocator propagates on move assignment or is always equel */
            constexpr bool move_storage = ( allocator_traits<node_allocator_type>::propagate_on_container_move_assignment::value || allocator_traits<node_allocator_type>::is_always_equel::value );
            /* Call apropriate move assignment function overload depending on whether the node allocator shall be moved or is equal */
            this->move_assign_allocators_equal( move( other ), integral_constant<bool, move_storage>{} );
            return *this;
        }

        /* Replaces the contents with those identified by initializer list 'init' */
        list & operator=( initializer_list<value_type> init ) {
            this->assign( init.begin(), init.end() );
            return *this;
        }

        /* Replaces the contents with 'count' copies of 'value' */
        void assign( size_type count, const value_type & value ) {
            iterator it { this->begin() };
            /* Until there are enough nodes already existing within current list and there are 'count' copies of 'value' to be inserted */
            while( ( it++ != this->end() ) && ( count-- > 0 ) ) {
                /* Replace the 'value' of 'value_type' within the existing node */
                *it = value;
            }
            /* If there are still some more 'values' to come in, append them. If there are more nodes allocated than the 'count' amount, remove unneeded ones */
            if( count > 0 ) {
                this->insert( this->end(), count, value );
            } else {
                this->erase( it, this->end() );
            }
        }

        /* Replaces the contents with copies of those in the range ['first', 'last'). The behavior is undefined if either argument is an iterator into '*this' */
        template<input_iterator I>
        void assign( I first, I last ) {
            const_iterator first1 { this->begin() };
            const_iterator last1 { this->end() };

            while( ( first1++ != last1 ) && ( first++ != last ) ) {
                *first1 = *first;
            }

            if( first == last ) {
                this->erase( first1, last1 );
            } else {
                this->insert( last1, first, last );
            }
        }

        void assign( initializer_list<value_type> init ) {
            this->assign( init.begin(), init.end() );
        }

        /* Returns the allocator associated with the container */
        allocator_type get_allocator() const noexcept {
            return allocator_type{ m_node_allocator };
        }

        /* Returns a reference to the first element in the container. Calling front on an empty container is undefined */
        reference front( void ) {
            return *( this->begin() );
        }

        /* Returns a reference to the first element in the container. Calling front on an empty container is undefined. */
        const_reference front( void ) const {
            return *( this->begin() );
        }

        /* Returns a reference to the last element in the container. Calling back on an empty container causes undefined behavior */
        reference back( void ) {
            return *prev( this->end() );
        }

        /* Returns a reference to the last element in the container. Calling back on an empty container causes undefined behavior */
        const_reference back( void ) const {
            return *prev( this->end() );
        }

        /* Returns an iterator to the first element of the list */
        iterator begin( void ) noexcept {
            return iterator{ make_observer<node_type>( ( this->m_tail ).next() ) };
        }

        /* Returns an iterator to the first element of the list */
        const_iterator begin( void ) const noexcept {
            return const_iterator{ make_observer<node_type>( ( this->m_tail ).next() ) };
        }

        /* Returns an iterator to the first element of the list */
        const_iterator cbegin( void ) const noexcept {
            return this->begin();
        }

        /* Returns a reverse iterator to the first element of the reversed list. It corresponds to the last element of the non-reversed list.
         * If the list is empty, the returned iterator is equal to rend() */
        reverse_iterator rbegin( void ) noexcept {
            return reverse_iterator{ this->end() };
        }

        /* Returns a reverse iterator to the first element of the reversed list. It corresponds to the last element of the non-reversed list.
         * If the list is empty, the returned iterator is equal to rend() */
        const_reverse_iterator rbegin( void ) const noexcept {
            return const_reverse_iterator{ this->end() };
        }

        /* Returns a reverse iterator to the first element of the reversed list. It corresponds to the last element of the non-reversed list.
         * If the list is empty, the returned iterator is equal to rend() */
        const_reverse_iterator crbegin( void ) const noexcept {
            return const_reverse_iterator{ this->end() };
        }

        /* Returns an iterator to the element following the last element of the list */
        iterator end( void ) noexcept {
            return iterator{ make_observer<Detail::list_node_base<allocator_type>>( addressof( this->m_tail ) ) };
        }

        /* Returns an iterator to the element following the last element of the list */
        const_iterator end( void ) const noexcept {
            return const_iterator{ make_observer<Detail::list_node_base<allocator_type>>( const_cast<Detail::list_tail_node<allocator_type> *>( addressof( this->m_tail ) ) ) };
        }

        /* Returns an iterator to the element following the last element of the list */
        const_iterator cend( void ) const noexcept {
            return this->end();
        }

        /* Returns a reverse iterator to the element following the last element of the reversed list. 
         * It corresponds to the element preceding the first element of the non-reversed list. This element acts as a placeholder,
         * attempting to access it results in undefined behavior */
        reverse_iterator rend( void ) noexcept {
            return reverse_iterator{ begin() };
        }

        /* Returns a reverse iterator to the element following the last element of the reversed list. 
         * It corresponds to the element preceding the first element of the non-reversed list. This element acts as a placeholder,
         * attempting to access it results in undefined behavior */
        const_reverse_iterator rend( void ) const noexcept {
            return const_reverse_iterator{ begin() };
        }

        /* Returns a reverse iterator to the element following the last element of the reversed list. 
         * It corresponds to the element preceding the first element of the non-reversed list. This element acts as a placeholder,
         * attempting to access it results in undefined behavior */
        const_reverse_iterator crend( void ) const noexcept {
            return const_reverse_iterator{ begin() };
        }

        /* Checks if the container has no elements, i.e. whether 'begin() == end()' */
        [[nodiscard]] bool empty() const noexcept {
            return begin() == end();
        }

        /* Returns the number of elements in the container */
        size_type size( void ) const noexcept {
            return distance( begin(), end() );
        }

        /* Returns the maximum number of elements the container is able to hold due to system or library implementation limitations */
        size_type max_size( void ) const noexcept {
            /* The maximum amount of value_type elements is given by the amount of available memory managed by the node allocator.
             * For instance, if the used allocator is pool_allocator<node_type>, then the maximum available amount of nodes is defined
             * by the size of the pool itself and how many node instances can be allocated within */
            return allocator_traits<node_allocator_type>::max_size( this->get_node_allocator() );
        }

        /* Erases all elements from the container. After this call, size() returns zero */
        void clear( void ) noexcept {
            /* TODO - Isn't that enough to release the head node unique_ptr owning the first node, which is then destroyed, including the 
             * next one etc. producing a kind of avalanche effect to destroy all the nodes... */
            this->erase( this->cbegin(), this->cend() );
        }

        /* Inserts element of value 'value' before 'position' by copy. Returns iterator pointing to the inserted 'value' */
        iterator insert( const_iterator position, const value_type & value ) requires ( CopyInsertable<value_type> ) {
            return this->emplace( position, value );
        }

        /* Inserts element of value 'value' before 'position' using move semantics. Returns iterator pointing to the inserted 'value' */
        iterator insert( const_iterator position, value_type && value ) requires ( MoveInsertable<value_type> ) {
            return this->emplace( position, move( value ) );
        }

        /* Insert 'count' copies fo the 'value' before 'position'. Returns iterator pointing to the first element inserted, or 'position' if 'count' == 0 */
        iterator insert( const_iterator position, size_type count, const value_type & value ) requires ( CopyAssignable<value_type> && CopyInsertable<value_type> ) {
            iterator first_element_inserted { this->begin() };
            /* Check whether the count is non-zero */
            if( count-- > 0 ) {
                /* Insert first element */
                first_element_inserted = emplace( position--, value );
                /* If there are more to be inserted, do so... */
                while( count --> 0 ) {
                    this->emplace( position, value );
                }
            } else {
                /* If 'count == 0', return 'position' converted to 'iterator' type */
                first_element_inserted = const_iterator_cast( *this, position );
            }
            return first_element_inserted;
        }

        /* Insert elements from range ['first', 'last') before 'position'. The behavior is undefined if 'first' and 'last' are iterators into '*this'.
         * Returns iterator pointing to the first element inserted, or 'position' if 'first' == 'last' */
        template<input_iterator I>
        iterator insert( const_iterator position, I first, I last ) requires ( EmplaceConstructible<value_type> ) {
            iterator first_element_inserted { this->begin() };
            /* Check the emptyness of the [first, last) range. If not empty, emplace the elements if non-empty */
            if( first != last ) {
                /* Emplace the first element and store the iterator pointing to it into 'first_element_inserted' */
                first_element_inserted = this->emplace( position, *( first++ ) );
                /* Emplace remaining elements */
                while( first < last ) {
                    this->emplace( position, *( first++ ) );
                }
            } else {
                /* 'first == last' so return the 'position' cast to non-const iterator */
                first_element_inserted = const_iterator_cast( *this, position );
            }
            /* Return the iterator to the first element inserted */
            return first_element_inserted;   
        }

        /* Insert elements from initializer list before 'position'. Returns iterator pointing to the first element inserted, or 'position' if 'init' is empty */
        iterator insert( const_iterator position, initializer_list<value_type> init ) requires ( EmplaceConstructible<value_type> ) {
            return this->insert( position, init.begin(), init.end() );
        }

        /* Insert a new element into the container directly before 'position'. Returns iterator pointing to the emplaced element. */
        template<typename... ARGUMENTS>
        iterator emplace( const_iterator position, ARGUMENTS &&... arguments ) {
            /* Allocate new node using the arguments provided and link it before the given 'position'. This means to update the linking from 'position''s previous and the 'position' itself */
            return iterator{ position.node().template emplace_before( this->get_node_allocator(), forward<ARGUMENTS>( arguments )...  ) };
        }

        /* Removes element at 'position'. Returns iterator following the last removed element */
        iterator erase( const_iterator position ) {
            /* Updates the linkage of the node identified by the 'position' which means to connect position's previous and position's next together (and the backwards direction respectively) */
            /* NOTE: Returns an owning pointer to the removed node. Once it is discarded (return value of remove() call unused), the destructor of the node is called. */
            position.node().remove();
            /* FIXME: Causes somehow infinite loop or something... */
#if false
            /* Return the iterator following the last element removed */
            return const_iterator_cast( *this, ( position + 1 ) );
#else
            return iterator{};
#endif
        }

        /* Removes the elements in the range [first, last) */
        iterator erase( const_iterator first, const_iterator last ) {
            while( first != last ) {
                first = erase( first );
            }
            /* Return the iterator following the last removed element, which is 'last' because it is range's excluded endpoint and thus effectively points to the element following the last one removed */
            return const_iterator_cast( *this, last );
        }

        /* Inserts new element to the beginning of the container. Returns a reference to the inserted element */
        template<typename... ARGUMENTS>
        reference emplace_front( ARGUMENTS &&... arguments ) requires ( EmplaceConstructible<value_type> ) {
            return *( this->emplace( this->cbegin(), forward<ARGUMENTS>( arguments )... ) );
        }

        /* Prepends the given element value to the beginning of the container. The new element is initialized as a copy of 'value'. */
        void push_front( const value_type & value ) requires ( CopyInsertable<value_type> ) {
            this->emplace_front( value );
        }

        /* Prepends the given element value to the beginning of the container. 'value' is moved into the new element. */
        void push_front( value_type && value ) requires ( MoveInsertable<value_type> ) {
            this->emplace_front( move( value ) );
        }

        /* Removes the first element of the container. Calling pop_back on an empty container results in undefined behavior. */
        void pop_front( void ) {
            this->erase( this->cbegin() );
        }

        /* Appends a new element to the end of the container. Returns a reference to the inserted element */
        template<typename... ARGUMENTS>
        reference emplace_back( ARGUMENTS &&... arguments ) requires ( EmplaceConstructible<value_type> ) {
            return *( this->emplace( this->cend(), forward<ARGUMENTS>( arguments )... ) );
        }

        /* Appends the given element value to the end of the container. The new element is initialized as a copy of 'value'. */
        void push_back( const value_type & value ) requires ( CopyInsertable<value_type> ) {
            this->emplace_back( value );
        }

        /* Appends the given element value to the end of the container. 'value' is moved into the new element. */
        void push_back( value_type && value ) requires ( MoveInsertable<value_type> ) {
            this->emplace_back( move( value ) );
        }

        /* Removes the last element of the container. Calling pop_back on an empty container results in undefined behavior. */
        void pop_back( void ) {
            this->erase( --this->cend() );
        }

        /* Resizes the container to contain 'count' elements. If the current size is greater than 'count', the container 
         * is reduced to its first 'count' elements. If the current size is less than 'count', additional default-inserted elements are appended */
        void resize( size_type count ) requires ( DefaultInsertable<value_type> ) {
            this->resize( count, value_type{} );
        } 

        /* Resizes the container to contain 'count' elements. If the current size is greater than 'count', the container 
         * is reduced to its first 'count' elements. If the current size is less than 'count', additional copies of value are appended */
        void resize( size_type count, const value_type & value ) requires ( CopyInsertable<value_type> ) {
            /* To avoid repeated size calculations, save the value as local variable */
            const size_type size { this->size() };
            if( size < count ) {
                /* Append remaining elements using a copy of 'value' */
                this->insert( this->end(), ( count - size ), value );
            } else {
                /* Reduce to first 'count' elements. If current size is the same as requested 'count' nothing is performed */
                this->erase( ( this->cend() - ( size - count ) ), this->cend() );
            }
        }

        /* Exchanges the contents of the container with those of other. Does not invoke any move, copy, or swap operations on individual elements */
        void swap( list & other ) noexcept( noexcept( allocator_traits<allocator_type>::is_always_equal::value ) ) {
            /* Swap the tail nodes to effectively swap the contents of both containers without any node moves or copies */
            this->m_tail.swap( other.m_tail );
            /* If 'allocator_traits<node_allocator_type>::propagate_on_container_swap::value' is 'true', then the allocators are exchanged 
             * using an unqualified call to non-member swap. Otherwise, they are not swapped (and if 'get_allocator() != other.get_allocator()', 
             * the behavior is undefined). */
            if constexpr ( allocator_traits<node_allocator_type>::propagate_on_container_swap::value ) {
                ReVolta::swap( this->m_node_allocator, other.m_node_allocator );
            }
        }

        /* Merges two sorted lists into one. The lists should be sorted into ascending order. 
         * No elements are copied. The container 'other' becomes empty after the operation. The function does nothing if 'other' refers 
         * to the same object as '*this'. If 'get_allocator() != other.get_allocator()', the behavior is undefined. No iterators or references 
         * become invalidated, except that the iterators of moved elements now refer into '*this', not into other. The first version uses 
         * 'operator<' to compare the elements. */
        void merge( list & other ) {
            this->merge( move( other ) );
        }

        /* Merges two sorted lists into one. The lists should be sorted into ascending order. 
         * No elements are copied. The container 'other' becomes empty after the operation. The function does nothing if 'other' refers 
         * to the same object as '*this'. If 'get_allocator() != other.get_allocator()', the behavior is undefined. No iterators or references 
         * become invalidated, except that the iterators of moved elements now refer into '*htis', not into 'other'. Uses 
         * operator< to compare the elements. */
        void merge( list && other ) {
            /* TODO */
        }

        /* Merges two sorted lists into one. The lists should be sorted into ascending order. 
         * No elements are copied. The container 'other' becomes empty after the operation. The function does nothing if 'other' refers 
         * to the same object as '*this'. If 'get_allocator() != other.get_allocator()', the behavior is undefined. No iterators or references 
         * become invalidated, except that the iterators of moved elements now refer into '*this', not into other. Uses the given comparison function 'compare'. */
        template<typename COMPARE>
        void merge( list & other, COMPARE compare ) {
            this->merge( move( other ), compare );
        }

        /* Merges two sorted lists into one. The lists should be sorted into ascending order. 
         * No elements are copied. The container 'other' becomes empty after the operation. The function does nothing if 'other' refers 
         * to the same object as '*this'. If 'get_allocator() != other.get_allocator()', the behavior is undefined. No iterators or references 
         * become invalidated, except that the iterators of moved elements now refer into '*this', not into other. Uses the given comparison function 'compare'. */
        template<typename COMPARE>
        void merge( list && other, COMPARE compare ) {
            /* TODO */
        }

        /* Transfers elements from one list to another. Transfers all elements from other into '*this'. The elements are inserted before 
         * the element pointed to by pos. The container other becomes empty after the operation. The behavior is undefined if other 
         * refers to the same object as '*this'.*/
        void splice( const_iterator position, list & other ) {
            this->splice( position, move( other ) );
        }

        /* Transfers elements from one list to another. Transfers all elements from other into '*this'. The elements are inserted before 
         * the element pointed to by pos. The container other becomes empty after the operation. The behavior is undefined if other 
         * refers to the same object as '*this'.*/
        void splice( const_iterator position, list && other ) {
            /* TODO */
        }

        /* Transfers elements from one list to another. Transfers the element pointed to by it from other into '*this'. The element 
         * is inserted before the element pointed to by 'position'. */
        void splice( const_iterator position, list & other, const_iterator iterator ) {
            this->splice( position, move( other ), iterator );
        }

        /* Transfers elements from one list to another. Transfers the element pointed to by it from other into '*this'. The element 
         * is inserted before the element pointed to by 'position'. */
        void splice( const_iterator position, list && other, const_iterator iterator ) {
            /* TODO */
        }

        /* Transfers elements from one list to another. Transfers the elements in the range [first, last) from other into '*this'. 
         * The elements are inserted before the element pointed to by 'position'. The behavior is undefined if 'position' is an iterator 
         * in the range [first,last) */
        void splice( const_iterator position, list & other, const_iterator first, const_iterator last ) {
            this->splice( position, move( other ), first, last );
        }

        /* Transfers elements from one list to another. Transfers the elements in the range [first, last) from other into '*this'. 
         * The elements are inserted before the element pointed to by 'position'. The behavior is undefined if 'position' is an iterator 
         * in the range [first,last) */
        void splice( const_iterator position, list && other, const_iterator first, const_iterator last ) {
            /* TODO */
        }

        /* Removes all elements that are equal to 'value'. Returns the number of elements removed. */
        size_type remove( const value_type & value ) {
            /* Make up the unary predicate returning 'true' once the element being evaluated equals the value given, 'false' otherwise. */
            return remove_if( [&]( const value_type element ) { return element == value; } );
        }

        /* Removes all elements for which 'predicate' returns 'true'. Returns the number of elements removed.
         * The expression F( v ) must be convertible to bool for every argument 'v' of type (possibly 'const') 'T', regardless of value category, 
         * and must not modify 'v'. Thus, a parameter type of 'T&' is not allowed, nor is 'T' unless for 'T' a move is equivalent to a copy. */
        template<typename UNARY_PREDICATE> requires ( predicate<UNARY_PREDICATE, const value_type> )
        size_type remove_if( UNARY_PREDICATE p ) {
            const_iterator first { this->cbegin() };
            const_iterator last { this->cend() };
            /* Amount of elements removed */
            size_t num_of_elements_removed { 0 };
            /* Iterate through the whole list */
            while( first != last ) {
                /* In case the value match */
                if( p( *first ) ) {
                    /* Erase the element matches the 'value' */
                    this->erase( first );
                    num_of_elements_removed++;
                }
            }
            return num_of_elements_removed;
        }

        /* Reverses the order of the elements in the container. No references or iterators become invalidated. */
        void reverse( void ) noexcept {
            /* NOTE: This implementation using the reverse algorithm internally uses 'iter_swap(...)' which internally swaps the values
             * stored within the nodes, rather than the nodes itself. The swap function is quite cheap for movable types but how about
             * non movable, copyable only types? In such case, it would be much quicker to swap the nodes using the linkage modifications 
             * than to copy potentially copy-expensive objects...
             * Once this is an issue, implement 'list_node<...>::swap( list_node<> & other )' to perfom the linkage update, perform the reverse
             * operation using that 'swap()' */
            /* FIXME: This most probably invalidates the iterators and references as the value store within is changed, so this simply shall not work... */
            ReVolta::reverse<iterator, node_swap>( this->begin(), this->end() );
        }

        /* Removes all consecutive duplicate elements from the container. Only the first element in each group of equal elements is left. 
         * The behavior is undefined if the selected comparator does not establish an equivalence relation. Uses 'operator==' to compare the elements */
        size_type unique( void ) {
            /* TODO */
            return 0;
        }

        /* Removes all consecutive duplicate elements from the container. Only the first element in each group of equal elements is left. 
         * The behavior is undefined if the selected comparator does not establish an equivalence relation. Uses the given binary 'predicate' to compare the elements. 
         * The signature of the predicate function should be equivalent to the following: 'bool pred(const Type1 &a, const Type2 &b);' */
        template<typename BINARY_PREDICATE> requires ( predicate<BINARY_PREDICATE, const value_type &, const value_type &> )
        size_type unique( BINARY_PREDICATE p ) {
            /* TODO */
            return 0;
        }

        /* Sorts the elements in ascending order. The order of equal elements is preserved. Uses 'operator<' to compare the elements */
        void sort( void ) {
            /* TODO */
        }

        /* TODO: Implement concept describing compare and apply there */
        /* Sorts the elements in ascending order. The order of equal elements is preserved. Uses the given comparison function 'compare' */
        template<typename COMPARE>
        void sort( COMPARE compare ) {
            /* TODO */
        }
    };

    /* Checks if the contents of lhs and rhs are equal, that is, they have the same number of elements and each element in lhs compares equal with the element in rhs at the same position. */
    template<typename T, Allocator ALLOCATOR>
    bool operator==( const list<T, ALLOCATOR> & lhs, const list<T, ALLOCATOR> & rhs ) {
        typename list<T, ALLOCATOR>::const_iterator lhs_iterator { lhs.cbegin() };
        typename list<T, ALLOCATOR>::const_iterator rhs_iterator { rhs.cbegin() };

        typename list<T, ALLOCATOR>::const_iterator lhs_end { lhs.cend() };
        typename list<T, ALLOCATOR>::const_iterator rhs_end { rhs.cend() };

        /* Iterate through the whole lists comparing elements at the same positions */
        while( ( lhs_iterator != lhs_end ) && ( rhs_iterator != rhs_end ) && ( *lhs_iterator == *rhs_iterator ) ) {
            ++lhs_iterator;
            ++rhs_iterator;
        }
        /* Once the one-by-one comparisons are true for all the element pairs, return 'true', 'false' otherwise */
        return ( ( lhs_iterator == lhs_end ) && ( rhs_iterator == rhs_end ) );
    }

    /* TODO Enable once three way comparision is available */
#if false
    template<typename T, Allocator ALLOCATOR>
    /* TODO */ operator<=>( const list<T, ALLOCATOR> & lhs, const list<T, ALLOCATOR> & rhs ) {

    }
#endif

    /* Specializes the 'swap' algorithm for 'list'. Swaps the contents of 'lhs' and 'rhs' */
    template<typename T, Allocator ALLOCATOR>
    void swap( list<T, ALLOCATOR> & lhs, list<T, ALLOCATOR> & rhs ) noexcept( noexcept( lhs.swap( rhs ) ) ) {
        lhs.swap( rhs );
    }

    /* Erases all elements that compare equal to value from the container. */
    template<typename T, Allocator ALLOCATOR, typename U>
    typename list<T, ALLOCATOR>::size_type erase( list<T, ALLOCATOR> & container, const U & value ) {
        return container.remove_if( [&]( auto & element ) { return element == value; } );
    }
     
    /* Erases all elements that satisfy the predicate pred from the container. */
    template<typename T, Allocator ALLOCATOR, typename UNARY_PREDICATE> requires ( predicate<UNARY_PREDICATE, const T> )
    typename list<T, ALLOCATOR>::size_type erase_if( list<T, ALLOCATOR> & container, UNARY_PREDICATE p ) {
        return container.remove_if( p );
    }

    /* Deduction guide */
    /* This deduction guide is provided for list to allow deduction from an iterator range. */
    template<input_iterator I, Allocator ALLOCATOR = allocator<typename iterator_traits<I>::value_type>>
    list( I, I, ALLOCATOR = ALLOCATOR() ) -> list<typename iterator_traits<I>::value_type, ALLOCATOR>;

}