#pragma once

#include "utility.hpp"
#include "memory.hpp"
#include "tagged_union.hpp"
#include "type_traits.hpp"

namespace ReVolta {

	/* Helper to define initial state machine state */
	template<typename INITIAL_STATE>
	inline constexpr tag_type<INITIAL_STATE> initial_state {};

	template<typename... STATES>
	class state_machine {
	public:
		using state = tagged_union<STATES...>;

		/* State machine shall be explicitly constructed using defined initial STATE */
		template<typename STATE, typename... ARGUMENTS> requires IsAlternative<STATE, STATES...>
		state_machine( tag_type<STATE>, ARGUMENTS &&... arguments ) noexcept
			:	m_state( { initial_state<STATE>, forward<ARGUMENTS>( arguments )... } )
		{}

		class transition {
		protected:
			transition( observer_ptr<state> current_state ) noexcept : m_state( move( current_state ) ) {}

			template<typename NEXT_STATE, typename... ARGUMENTS>
			void perform( ARGUMENTS &&... arguments ) noexcept {
				m_state->template emplace<NEXT_STATE>( forward<ARGUMENTS>( arguments )... );
			}

		private:
			/* NOTE: Non-owning pointer to state */
			observer_ptr<state> m_state { nullptr };
		};

		/* TODO: Apply concept to TRANSITION */
		/* NOTE: The return type of transition() is defined by the return value in operators operator() */
		template<typename TRANSITION, typename... ARGUMENTS>
		inline decltype(auto) perform_transition( ARGUMENTS &&... arguments ) noexcept {
			/* Apply visitor class containing the operator() for each possible type stored within
			 * the tagged_union to perform action specific to stored type instance within.
			 * The visitor shall return via the visit() function call - the value returned by selected
			 * operator() overload */
			return m_state.visit( TRANSITION( m_state ), forward<ARGUMENTS>( arguments )...  );
		}

		template<typename STATE>
		bool in_state( void ) const noexcept {
			return m_state.template holds_alternative<STATE>();
		}

	private:
		/* TODO: shall be owned by unique_ptr ? */
		state m_state;
	};

}
