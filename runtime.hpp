/*
 * runtime.hpp
 *
 *  Created on: 27. 8. 2020
 *      Author: martin
 */

#pragma once

namespace ReVolta {

	using GlobalConstructor = void (*)();

	inline void call_global_constructors( GlobalConstructor * start, GlobalConstructor * end ) {
		for( GlobalConstructor * ctor = start; ctor != end; ++ctor ) {
			(* ctor)();
		}
	}

}
