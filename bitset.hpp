#pragma once

#include <cstdint.hpp>

namespace ReVolta {

    template<size_t N>
    class bitset {
        using word_type = unsigned long;

        static constexpr size_t bits_per_byte { 8 };
        /* Calculate how many bits are stored in the word type */
        static constexpr size_t bits_per_word { bits_per_byte * sizeof( word_type ) };
        /* Calculate how many words are needed to store at minimum the defined amount of bits (N) */
        static constexpr size_t n_words { ( N / bits_per_word ) + ( ( N % bits_per_word == 0 ) ? 0 : 1 ) };

        word_type m_data [ n_words ];

        constexpr static size_t which_bit( size_t position ) noexcept {
            return position % bits_per_word;
        }

        constexpr static size_t which_byte( size_t position ) noexcept {
            return which_bit( position ) / bits_per_byte;
        }

        constexpr static size_t which_word( size_t position ) noexcept {
            return position / bits_per_word;
        }

        constexpr static word_type mask_bit( size_t position ) noexcept {
            return static_cast<word_type>( 1 ) << which_bit( position );
        }

        constexpr word_type & get_word( size_t position ) noexcept {
            return m_data[ which_word( position ) ];
        }

        constexpr const word_type & get_word( size_t position ) const noexcept {
            return m_data[ which_word( position ) ];
        }

        constexpr void check_bounds( size_t position ) const {
            if( position > this->size() ) {
                /* TODO: Throw 'out_of_range' '*/
            }
        }

    public:
        /* Proxy object to allow users to interact with individual bits of a bitset since stadard C++ types 
         * like references and pointers are not built with enough precision to specify individual bits. 
         * The primary use of bitset::reference is to provide an l-value that can be returned from operator[] */
        class reference {
            constexpr reference( void ) = delete;

            friend class bitset;

            word_type * m_ptr;
            size_t m_position;

        public:
            constexpr reference( bitset & b, size_t position ) noexcept : m_ptr( addressof( b.get_word( position ) ) ), m_position( which_bit( m_position ) ) {}

            constexpr ~reference( void ) noexcept {}

            constexpr reference & operator=( bool value ) noexcept {
                if( value ) {
                    *( this->m_ptr ) |= bitset::mask_bit( this->m_position );
                } else {
                    *( this->m_ptr ) &= bitset::mask_bit( this->m_position );
                }
                return *this;
            }

            constexpr reference & operator=( const reference & other ) noexcept {
                if( *( other.m_ptr ) & bitset::mask_bit( other.m_position ) ) {
                    *( this->m_ptr ) |= bitset::mask_bit( this->m_position );
                } else {
                    *( this->m_ptr ) &= bitset::mask_bit( this->m_position );
                }
                return *this;
            }

            constexpr bool operator~( void ) const noexcept {
                return ( *( this->m_ptr ) & bitset::mask_bit( this->m_position ) ) == 0;
            }

            constexpr operator bool( void ) const noexcept {
                return ( *( this->m_ptr ) & bitset::mask_bit( this->m_position ) ) != 0;
            }
           
            constexpr reference & flip( void ) noexcept {
                *( this->m_ptr ) ^= bitset::mask_bit( this->m_position );
                return *this;
            }
        };

        /* Default constructor. Constructs a bitset with all bits set to zero */
        constexpr bitset( void ) noexcept {}

        constexpr bitset( const unsigned long long value ) noexcept {}

        /* TODO: Add basic_string constructors once the basic_string itself is available */

        /* Returns 'true' if the value of each bit in '*this' equals the value of the corresponding bit in 'other', otherwise 'false' */
        constexpr bool operator==( const bitset & other ) const noexcept {
            for( size_t idx { 0 }; idx < this->size(); ++idx ) {
                if( m_data[ idx ] != other.m_data[ idx ] ) return false;
            }
            return true;
        }

        constexpr bool operator[]( size_t position ) const {
            return ( this->get_word( position ) & mask_bit( position ) ) != static_cast<word_type>( 0 );
        }

        constexpr reference operator[]( size_t position ) {
            return reference{ *this, position };
        }

        /* Returns the value of the bit at the 'position' (counting from 0).
         * Unlike 'operator[]' performs a bounds check and throws 'out_of_range' if position does not correspond to a valid position in the bitset */
        constexpr bool test( size_t position ) const {
            /* Check the value of position. Throws 'out_of_range' exception if the test fails. */
            this->check_bounds( position );
            /* Just perform the test itself */
            return ( ( this->get_word( position ) & bitset::mask_bit( position ) ) != static_cast<word_type>( 0 ) );
        }

        /* Checks if all the bits are set to 'true', otherwise 'false' */
        constexpr bool all( void ) const noexcept {
            /* Iterate through all the WORDs in the bitset (not over particular bits) except the very last one which might be "incomplete" (not all the bits are used by bitset) */
            for( size_t idx { 0 }; idx < ( n_words - 1 ); ++idx ) {
                if( this->m_data[ idx ] != ~static_cast<word_type>( 0 ) ) {
                    return false;
                }
            }
            /* Evaluate the very last few bits */
            return this->m_data[ n_words - 1 ] == ( ~static_cast<word_type>( 0 ) >> ( ( n_words * bits_per_word ) - this->size() ) );
        }

        /* Checks if any of the bits are set to 'true', otherwise 'false' */
        constexpr bool any( void ) const noexcept {
            /* Iterate through all the WORDs in the bitset (not over particular bits) */
            for( size_t idx { 0 }; idx < n_words; ++idx ) {
                if( this->m_data[ idx ] != static_cast<word_type>( 0 ) ) {
                    return true;
                }
            }
            return false;
        }

        /* Checks if none of the bits are set to 'true', otherwise 'false' */
        constexpr bool none( void ) const noexcept {
            return !this->any();
        }

        /* Returns the number of the bits that are set to 'true' */
        constexpr size_t count( void ) const noexcept {
            size_t result { 0 };
            /* Iterate through all the WORDs in the bitset (not over particular bits) */
            for( size_t idx { 0 }; idx < n_words; ++idx ) {
                /* Add the amount of the bits set to one to the overall sum (the 'result') */
                result += __builtin_popcountl( m_data[ idx ] );
            }
            return result;
        }

        /* Returns the number of bit that the bitset holds */
        constexpr size_t size( void ) const noexcept {
            return N;
        }

        /* Sets the bits to the result of binary AND on corresponding pairs of bits of '*this' and 'other'. */
        constexpr bitset & operator&=( const bitset & other ) noexcept {
            /* Iterate through all the WORDs in the bitset (not over particular bits) */
            for( size_t idx { 0 }; idx < n_words; ++idx ) {
                /* Perform logical AND on WORDs from this and 'others' 's */
                this->m_data[ idx ] &= other.m_data[ idx ];
            }
        }

        /* Sets the bits to the result of binary OR on corresponding pairs of bits of '*this' and 'other'. */
        constexpr bitset & operator|=( const bitset & other ) noexcept {
            /* Iterate through all the WORDs in the bitset (not over particular bits) */
            for( size_t idx { 0 }; idx < n_words; ++idx ) {
                this->m_data[ idx ] |= other.m_data[ idx ];
            }
        }

        /* Sets the bits to the result of binary XOR on corresponding pairs of bits of '*this' and 'other'. */
        constexpr bitset & operator^=( const bitset & other ) noexcept {
            /* Iterate through all the WORDs in the bitset (not over particular bits) */
            for( size_t idx { 0 }; idx < n_words; ++idx ) {
                this->m_data[ idx ] ^= other.m_data[ idx ];
            }
        }

        /* Returns a temporary copy of *this with all bits flipped (binary NOT). */
        constexpr bitset operator~( void ) const noexcept {
            return bitset( *this ).flip();
        }

        constexpr bitset operator<<( size_t position ) const noexcept {
            /* TODO */
        }

        constexpr bitset & operator<<=( size_t position ) const noexcept {
            /* TODO */
            return *this;
        }

        constexpr bitset operator>>( size_t position ) const noexcept {
            /* TODO */
        }

        constexpr bitset & operator>>=( size_t position ) const noexcept {
            /* TODO */
            return *this;
        }

        /* Sets all bits to 'true' */
        constexpr bitset & set( void ) noexcept {
            for( size_t idx { 0 }; idx < n_words; ++idx ) {
                this->m_data[ idx ] = ~static_cast<word_type>( 0 );
            }
            return *this;
        }

        /* Sets the bit at 'position' to the 'value'.
         * Throws 'out_of_range if 'position' does not correspond to a valid position within the 'bitset' */
        constexpr bitset & set( size_t position, bool value = true ) {
            /* Check the 'position' */
            this->check_bounds( position );
            if( value ) {
                this->get_word( position ) |= bitset::mask_bit( position );
            } else {
                this->get_word( position ) &= ~bitset::mask_bit( position );
            }
            return *this;
        }

        /* Sets all bits to 'false' */
        constexpr bitset & reset( void ) noexcept {
            for( size_t idx { 0 }; idx < n_words; ++idx ) {
                this->m_data[ idx ] = static_cast<word_type>( 0 );
            }
            return *this;
        }

        /* Sets the bit at 'position' to 'false'. 
         * Throws 'out_of_range if 'position' does not correspond to a valid position within the 'bitset' */
        constexpr bitset & reset( size_t position ) {
            /* Check the 'position' */
            this->check_bounds( position );
            this->get_word( position ) &= ~bitset::mask_bit( position );
            return *this;
        }

        /* Flips all bits, i.e. changes 'true' values to 'false' and 'false' values to 'true'. (like 'operator~', but in-place) */
        constexpr bitset & flip( void ) noexcept {
            for( size_t idx {0}; idx < n_words; ++idx ) {
                this->m_data[ idx ] = ~this->m_data[ idx ];
            }
            return *this;
        }

        /* Flips the bit at 'position', i.e. changes 'true' value to 'false' and 'false' value to 'true'. 
         * Throws 'out_of_range if 'position' does not correspond to a valid position within the 'bitset' */
        constexpr bitset & flip( size_t position ) {
            /* Check the 'position' */
            this->check_bounds( position );
            this->get_word( position ) ^= bitset::mask_bit( position );
            return *this;
        }

        /* TODO: add 'to_string()' conversions once available */

        /* Converts the contents of the bitset to an unsigned long integer.
         * Throws 'overflow_error' if the value cannot be represented in 'unsigned long' */
        constexpr unsigned long to_ulong( void ) const {
            for( size_t idx { 1 }; idx < n_words; ++idx ) {
                if( this->m_data[ idx ] ) {
                    /* TODO: Throw 'overflow_error' */
                }
            }
            return this->m_data[ 0 ];
        }

        /* Converts the contents of the bitset to an unsigned long long integer.
         * Throws 'overflow_error' if the value cannot be represented in 'unsigned long long' */
        constexpr unsigned long to_ullong( void ) const {
            if constexpr ( sizeof( unsigned long long ) > sizeof( unsigned long ) ) {
                for( size_t idx { 2 }; idx < n_words; ++idx ) {
                    if( this->m_data[ idx ] ) {
                        /* TODO: Throw 'overflow_error' */
                    }
                }
                return this->m_data[ 0 ] + ( static_cast<unsigned long long>( this->m_data[ 1 ] ) << bits_per_word );
            } else {
                return this->to_ulong();
            }
        }
    };

    /* Returns a 'bitset<N>' containing the result of binary AND on corresponding pairs of bits of 'lhs' and 'rhs'. */
    template<size_t N>
    constexpr bitset<N> operator&( const bitset<N> & lhs, const bitset<N> & rhs ) noexcept {
        return bitset<N>( lhs ) &= rhs;
    }

    /* Returns a 'bitset<N>' containing the result of binary OR on corresponding pairs of bits of 'lhs' and 'rhs'. */
    template<size_t N>
    constexpr bitset<N> operator|( const bitset<N> & lhs, const bitset<N> & rhs ) noexcept {
        return bitset<N>( lhs ) |= rhs;
    }

    /* Returns a 'bitset<N>' containing the result of binary XOR on corresponding pairs of bits of 'lhs' and 'rhs'. */
    template<size_t N>
    constexpr bitset<N> operator^( const bitset<N> & lhs, const bitset<N> & rhs ) noexcept {
        return bitset<N>( lhs ) ^= rhs;
    }

    /* TODO: iostram insertion and extraction operators */
}