#pragma once

#include "array.hpp"
#include "cstddef.hpp"
#include "type_traits.hpp"
#include "utility.hpp"
#include "iterator.hpp"
#include "bits/extent.hpp"
#include "bits/range_access.hpp"

namespace ReVolta {

    /* TODO: Move it around */
    template<typename T>
    class normal_iterator {
    public:
        using value_type = T;
        using pointer = T *;
        using reference = T &;
        using size_type = size_t;
        using difference_type = ptrdiff_t;
        using iterator_category = bidirectional_iterator_tag;

        constexpr normal_iterator( void ) noexcept : normal_iterator( nullptr ) {}

        constexpr normal_iterator( nullptr_t ) noexcept : m_ptr( nullptr ) {}

        /* FIXME: Why 'explicit' does not work here? */
        constexpr normal_iterator( pointer ptr ) noexcept : m_ptr( ptr ) {}

        normal_iterator( const normal_iterator & other ) noexcept : m_ptr( other.m_ptr ) {}

        normal_iterator & operator=( const normal_iterator & other ) noexcept {
            normal_iterator{ other }.swap( *this );
            return *this;
        }

        /* FIXME: What if the iterator is incremented / decremented out of the defined area? */

        /* Prefix increment operator */
        normal_iterator & operator++( void ) noexcept {
            m_ptr++;
            return *this;
        }

        /* Postfix increment operator */
        normal_iterator operator++( int ) noexcept {
            normal_iterator temp { *this };
            m_ptr++;
            return temp;
        }

        /* Prefix decrement operator */
        normal_iterator & operator--( void ) noexcept {
            m_ptr--;
            return *this;
        }

        /* Postfix decrement operator */
        normal_iterator operator--( int ) noexcept {
            normal_iterator temp { *this };
            m_ptr--;
            return temp;
        }

        const reference operator*( void ) const noexcept {
            return *m_ptr;
        }

        reference operator*( void ) noexcept {
            return *m_ptr;
        }

        pointer operator->( void ) const noexcept {
            return m_ptr;
        }

        pointer get( void ) const noexcept {
            return m_ptr;
        }

        bool operator==( const normal_iterator & other ) const noexcept {
            return m_ptr == other.m_ptr;
        }

        bool operator!=( const normal_iterator & other ) const noexcept {
            return m_ptr != other.m_ptr;
        }

        void swap( normal_iterator & other ) noexcept {
            using ReVolta::swap;
            swap( m_ptr, other.m_ptr );
        }

    private:
        pointer m_ptr { nullptr };
    };

    template<typename T, size_t EXTENT = dynamic_extent>
    class span {
    public:
        using element_type = T;
        using value_type = remove_cv<T>::type;
        using size_type = size_t;
        using difference_type = ptrdiff_t;
        using pointer = T *;
        using const_pointer = const T *;
        using reference = T &;
        using const_reference = const T &;
        using iterator = normal_iterator<T>;
        using reverse_iterator = ReVolta::reverse_iterator<iterator>;

        static constexpr size_t extent = EXTENT;

    private:
        pointer m_ptr;

        [[no_unique_address]] Detail::extent_storage<extent> m_extent;

        template<typename R>
        using is_compatible_ref = is_array_convertible<element_type, remove_reference_t<R>>;

    public:
        constexpr span( void ) noexcept requires ( ( EXTENT + 1u ) <= 1u )
            : m_ptr( nullptr ), m_extent( 0 ) {}

        template<contiguous_iterator ITERATOR>
            requires ( is_compatible_ref<iter_reference_t<ITERATOR>>::value )
        explicit( extent != dynamic_extent ) constexpr span( ITERATOR first, size_type count ) noexcept
            : m_ptr( to_address( first ) ), m_extent( count )
        {}

        template<contiguous_iterator ITERATOR, sized_sentinel_for<ITERATOR> END>
            requires ( is_compatible_ref<iter_reference_t<ITERATOR>>::value && ( !is_convertible_v<END, size_type> ) )
        explicit( extent != dynamic_extent ) constexpr span( ITERATOR first, END last ) noexcept( noexcept( last - first ) ) 
            : m_ptr( to_address( first ) ), m_extent( static_cast<size_type>( last - first ) )
        {}

        template<size_t N>
        constexpr span( element_type (& a)[ N ] ) noexcept : span( static_cast<pointer>( a ), N ) {}

        template<typename U, size_t N>
        constexpr span( array<U,N> & a ) noexcept : span( static_cast<pointer>( a.data() ), N ) {}

#if false
        template<typename U, size_t N>
        constexpr span( const array<U,N> & a ) noexcept : span( static_cast<pointer>( a.data() ), N ) {}
#endif

        /* TODO: requires clause #227 */
        template<typename U, size_t N>
        explicit( extent != dynamic_extent && N == dynamic_extent ) constexpr span( const span<U,N> & source ) noexcept
            : m_ptr( source.data() ), m_extent( source.size() )
        {}

        constexpr span( const span & ) noexcept = default;

        constexpr span & operator=( const span & ) noexcept = default;

        constexpr pointer data( void ) const noexcept {
            return m_ptr;
        }

        constexpr size_type size( void ) const noexcept {
            return m_extent.value();
        }

        constexpr size_type size_bytes( void ) const noexcept {
            return this->size() * sizeof( element_type );
        }

        [[nodiscard]] constexpr bool empty( void ) const noexcept {
            return this->size() == 0;
        }

        constexpr reference front( void ) const noexcept {
            return *( this->m_ptr );
        }

        constexpr reference back( void ) const noexcept {
            return *( this->m_ptr + ( size() - 1 ) );
        }

        constexpr reference operator[]( size_type index ) const noexcept {
            return *( this->m_ptr + index );
        }

        constexpr iterator begin( void ) const noexcept {
            return iterator{ this->m_ptr };
        }

        constexpr iterator end( void ) const noexcept {
            return iterator{ this->m_ptr + this->size() };
        }

        constexpr reverse_iterator rbegin( void ) const noexcept {
            return reverse_iterator{ this->end() };
        }

        constexpr reverse_iterator rend( void ) const noexcept {
            return reverse_iterator{ this->begin() };
        }

        /* TODO: Subviews */
    };

    /* Deduction guides */
    /* Allow span element type to be deduced from raw array */
    template<typename T, size_t N>
    span( T ( & )[ N ] ) -> span<T, N>;

    /* Allow span element type to be deduced from ReVolta::array */
    template<typename T, size_t N>
    span( array<T, N> & ) -> span<T, N>;

    template<typename T, size_t N>
    span( const array<T, N> & ) -> span<const T, N>;

    /* TODO: Non-member functions */
    
}